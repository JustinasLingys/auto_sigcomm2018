#!/bin/sh

set -e # fail if there is an error

echo 'Loading the DCTCP kernel module.'
/sbin/modprobe tcp_dctcp

echo '\nSetting DCTCP as the TCP congetion control.'
/sbin/sysctl net.ipv4.tcp_congestion_control=dctcp

echo '\nEnabling ECN on the TCP connections.'
/sbin/sysctl net.ipv4.tcp_ecn=1
exit 0
