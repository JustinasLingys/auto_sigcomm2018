#! /bin/sh

# Script to build QLAS/QSJF priority queues.

echo -n 'Please enter interface name: '
read IF_NAME

 
if [ -z "${IF_NAME}" ]; then
  echo 'No interface has been entered.'
  exit 1
fi


set -v
set -e

# delete the current queing discipline first
/sbin/tc qdisc del dev "${IF_NAME}" root

# add a strict priority ('tc-prio') queing discipline
/sbin/tc qdisc add dev "${IF_NAME}" root handle 1:0 prio bands 4

# check the status
if [ $? -ne 0 ]; then
  echo '\nCannot add a queuing discipline to interface' \'"${IF_NAME}"\'
  exit 1
fi 

echo '\nAdding IPv4 rules for forwarding  SYN,RST SYN,FIN packets.'
/sbin/iptables -t mangle -A OUTPUT -p tcp --tcp-flags SYN,FIN,RST SYN -j DSCP --set-dscp 32 --out-interface "${IF_NAME}"

if [ $? -ne 0 ]; then
  echo 'Failed to set an IPv4 rule for TCP pakcets with SYN,RST SYN and FIN flags'
  exit 1
fi

echo '\nAdding filters to filter packets to different priority queues.'
 
/sbin/tc filter add dev "${IF_NAME}" parent 1:0 protocol ip prio 2 u32 match ip dsfield 0x80 0xf0 flowid 1:1

/sbin/tc filter add dev "${IF_NAME}" parent 1:0 protocol ip prio 2 u32 match ip dsfield 0x40 0xf0 flowid 1:2

/sbin/tc filter add dev "${IF_NAME}" parent 1:0 protocol ip prio 2 u32 match ip dsfield 0x20 0xf0 flowid 1:3

/sbin/tc filter add dev "${IF_NAME}" parent 1:0 protocol ip prio 2 u32 match ip dsfield 0x00 0xf0 flowid 1:4

# Make the ordinary pfifo as default queuing discipline of the first 
# priority queues and the last one must have htb
/sbin/tc qdisc replace dev "${IF_NAME}" parent 1:1 handle 10:0 pfifo
/sbin/tc qdisc replace dev "${IF_NAME}" parent 1:2 handle 20:0 pfifo
/sbin/tc qdisc replace dev "${IF_NAME}" parent 1:3 handle 30:0 pfifo
/sbin/tc qdisc replace dev "${IF_NAME}" parent 1:4 handle 40:0 pfifo

echo '\nAdded priority queues to' "'${IF_NAME}'"

echo "\n========================= Finished configuring ====================="
exit 0
