#! /bin/sh

# Script to build AuTO priority queues


# A simple function for retrieving a link's 
# network speed in Mbits/s
get_if_speed_mbps()
{

if_speed=$(cat /sys/class/net/"${1}"/speed)

return "${if_speed}"

}


echo -n 'Please enter interface name: '
read IF_NAME

 
if [ -z "${IF_NAME}" ]; then
  echo '\nNo interface has been entered.'
  exit 1
fi


# delete the current queing discipline first
/sbin/tc qdisc del dev "${IF_NAME}" root


# add a strict priority ('tc-prio') queing discipline
/sbin/tc qdisc add dev "${IF_NAME}" root handle 1:0 prio bands 4

if [ $? -ne 0 ]; then
  echo '\nFailed to add the `tc-prio` queuing discipline to ' \`"${IF_NAME}"\`
  exit 1
fi


echo '\nAdding IPv4 rules for forwarding  SYN,RST SYN,FIN packets.'
/sbin/iptables -t mangle -A OUTPUT -p tcp --tcp-flags SYN,FIN,RST SYN -j DSCP --set-dscp 32 --out-interface "${IF_NAME}"


if [ $? -ne 0 ]; then
  echo 'Failed to add IPv4 forwarding rules.'
  exit 1
fi

echo '\nAdding filters to filter packets to different priority queues.'
 
/sbin/tc filter add dev "${IF_NAME}" parent 1:0 protocol ip prio 2 u32 match ip dsfield 0x80 0xf0 flowid 1:1

if [ $? -ne 0 ]; then
  echo 'Failed to add a filter.'
  exit 1
fi

/sbin/tc filter add dev "${IF_NAME}" parent 1:0 protocol ip prio 2 u32 match ip dsfield 0x40 0xf0 flowid 1:2

if [ $? -ne 0 ]; then
  echo 'Failed to add a filter.'
  exit 1
fi

/sbin/tc filter add dev "${IF_NAME}" parent 1:0 protocol ip prio 2 u32 match ip dsfield 0x20 0xf0 flowid 1:3

if [ $? -ne 0 ]; then
  echo 'Failed to add a filter.'
  exit 1
fi

/sbin/tc filter add dev "${IF_NAME}" parent 1:0 protocol ip prio 2 u32 match ip dsfield 0x00 0xf0 flowid 1:4

if [ $? -ne 0 ]; then
  echo 'Failed to add a filter.'
  exit 1
fi

# Make the ordinary pfifo as default queuing discipline of the first 
# priority queues and the last one must have htb
/sbin/tc qdisc replace dev "${IF_NAME}" parent 1:1 handle 10:0 pfifo

if [ $? -ne 0 ]; then
  echo 'Failed to replace `qdisc` of a priority queue.'
  exit 1
fi

/sbin/tc qdisc replace dev "${IF_NAME}" parent 1:2 handle 20:0 pfifo

if [ $? -ne 0 ]; then
  echo 'Failed to replace `qdisc` of a priority queue.'
  exit 1
fi

/sbin/tc qdisc replace dev "${IF_NAME}" parent 1:3 handle 30:0 pfifo

if [ $? -ne 0 ]; then
  echo 'Failed to replace `qdisc` of a priority queue.'
  exit 1
fi

/sbin/tc qdisc replace dev "${IF_NAME}" parent 1:4 handle 40:0 htb default 0

if [ $? -ne 0 ]; then
  echo 'Failed to replace `qdisc` of a priority queue.'
  exit 1
fi

echo '\nAdded priority queues to' \`"${IF_NAME}"\`


# now create the default class for all large flows that are
# controlled by the Central System. One class is created
# in order to enable sharing among flows (rate sharing)
get_if_speed_mbps "${IF_NAME}"
FOUND_IF_SPEED="$?"


if [ ! -n "${FOUND_IF_SPEED}" ] || [ "${FOUND_IF_SPEED}" -ne "${FOUND_IF_SPEED}" ]; then
  echo '\nError: cannot rertrieve the network card speed.'
  exit 1
fi

if [ "${FOUND_IF_SPEED}" -le 0 ]; then
  echo '\nThe interface is not active and its speed is non-positive.'
  exit 1
fi

echo '\nRetrieved link speed' "${FOUND_IF_SPEED}" 'Mbps'

/sbin/tc class add dev "${IF_NAME}" parent 40:0 classid 40:1 htb rate "${FOUND_IF_SPEED}"mbit ceil "${FOUND_IF_SPEED}"mbit prio 5

if [ $? -ne 0 ]; then
  echo 'Failed to add the shared `htb` class.'
  exit 1
fi


# try to create a new filter hash table for refering filters to
# this particular hash table
/sbin/tc filter add dev "${IF_NAME}" parent 40:0 handle 0x00100000 protocol  ip prio 10 u32 divisor 1

if [ $? -ne 0 ]; then
  echo 'Failed to create a new filter hash table.'
  exit 1
fi

# add a filter to link to the new hash table
# (only TCP packets are scheduled)
/sbin/tc filter add dev "${IF_NAME}" parent 40:0 protocol ip prio 10 u32 link 0x00100000 match ip protocol 0x06 0xff

if [ $? -ne 0 ]; then
  echo 'Failed to link to a newly created hash table.'
  exit 1
fi

# do the same with '40:1' htb class
/sbin/tc filter add dev "${IF_NAME}" parent 40:1 handle 0x00100000 protocol  ip prio 10 u32 divisor 1


if [ $? -ne 0 ]; then
  echo 'Failed to create a new filter hash table.'
  exit 1
fi


# add a filter to link to the new hash table (all received packets)
/sbin/tc filter add dev "${IF_NAME}" parent 40:1 protocol ip prio 10 u32 link 0x00100000 match ip protocol 0x00 0x00


if [ $? -ne 0 ]; then
  echo 'Failed to link to a newly created hash table.'
  exit 1
fi


echo "\n========================= Finished configuring ====================="
exit 0
