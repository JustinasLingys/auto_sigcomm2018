#! /bin/sh


# A script for cleaning up the results of 
# the 'mlfq_tc.sh/auto_mlfq.sh' scripts.

echo -n 'Please enter interface name: '
read IF_NAME

if [ -z "${IF_NAME}" ]; then
  echo 'No interface has been entered.'
  exit 1
fi


set -e # fail if there is an error

# Delete the root queueing discipline
echo '\nDeleting the root queuing discipline of' "${IF_NAME}"
/sbin/tc qdisc del dev "${IF_NAME}" root

echo '\nDeleteting the IPv4 rule for the TCP SYN,FIN and RST SYN packets'
/sbin/iptables -t mangle -D OUTPUT -p tcp --tcp-flags SYN,FIN,RST SYN -j DSCP --set-dscp 32 --out-interface "${IF_NAME}"

echo '\nDone cleaning up multi-level feedback queuing.'
exit 0
