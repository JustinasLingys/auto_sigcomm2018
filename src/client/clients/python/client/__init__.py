# -*- coding: utf-8 -*-

__all__ = ['client', 'simple_client', 'incast_client']

#import client.pias_static_client  as cli_mod
#import client.test_client         as cli_mod
import  client.async_client as cli_mod

def run_client(cli_type, params):
  
  if cli_type is None:
    print ('No Client Type')
    return False



  # no need to pass the client type

  # only simple is supported now
  if cli_type == 'client':
    return cli_mod.run_client(**params) 

  print ('No recognisible client type found.') 

  return False
