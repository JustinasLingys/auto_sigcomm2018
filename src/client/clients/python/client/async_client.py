# -*- coding: utf-8 -*-

# A client program for generating flow requests. This program is 
# similar to the one at github.com/HKUST-SING/TrafficGenerator 
# (src/client/client.c).
# However, due to the the nature of machine learning, the client
# is a bit modified to better fit evaluation. The module uses the
# Python asyncio module to handle the concurrent connections. 


# Requirements: minimum Python 3.4


# Python 3.4/3.5 std lib
import asyncio
import os
import threading
import concurrent.futures
import socket
import signal
import time
import random
import struct
import collections 
import sys



# local modules
from   net.protocols import DefaultProtocol
from   net.protocols import ConnState
import net
import errors
import utils.cdf
import utils.GS_timing
from   utils import async_common as common






TERM_FLAG = False # global variable for
									# terminating the program

MAXIMUM_LOSS    = 3 # how many times all operations can fail
										# before stop the system



###################### Functions and coroutines ######################

# To close the emulation properly, cancel all
# pending tasks.
def cancel_all_tasks(event_loop):
	for task in asyncio.Task.all_tasks(loop=event_loop):
		task.cancel()



# A coroutine to stop an event loop
@asyncio.coroutine
def stop_event_loop(event_loop):
	cancel_all_tasks(event_loop)



# Signal handler to terminate the
# program/emulation. 
def shutdown_callback(signum, frame):

	global TERM_FLAG
	TERM_FLAG = True # terminate the program



# A separate thread is allocated for purely handling
# IO operations
def run_io_forever(event_loop):
	asyncio.set_event_loop(event_loop)

	try:
		event_loop.run_forever()
	except Exception as exp:
		print("Error: run_io_forever: {0}".format(exp))
	



# A class that represents a batch of requests
class BatchRequest(object):
	def __init__( self, req_size=0, req_server_id=0,
								req_rate=0, req_sleep_us=0,
								req_load=0, req_cdf=0):


		self.meta = common.FlowMetadata(0, req_size, req_rate,
																		req_load, req_cdf)
		self.req_server   =  req_server_id
		self.req_sleep_us =  req_sleep_us


# Per-server state
class PerServerInfo(object):
	"""
		Class maintains a state (number of active/inactive flows) per
		a particular server. It also keeps state if the server still
		can accept more connections.
	"""

	def __init__(self, server_id, server_addr, max_conns):
		self.serv_id     = server_id
		self.rem_addr    = server_addr
		self.ac_flows    = {}    # active flows
		self.ic_flows    = {}    # inactive flows 
		self.upper_limit = max_conns
		self.can_more    = True  # if the server can still accept
											    	 # more new connections

													# since scheduling happens on
													# one thread and IO operations on
													# the other one, need a lock

		

	"""
		Method for checking if the server can accept more
		new connections.
	"""
	def can_accept_more(self):
		return (self.can_more and\
						self.upper_limit > (len(self.ac_flows) + len(self.ic_flows)))



	"""
		Insert a new connected connection to the server state.
		
		@param conn : connection that has connected to the server
									of this state

		@return : True on success, False on failure
	"""
	def insert_connection(self, conn):
	
		if not self.can_accept_more():
			# reject the new connection
			return False		


		# check the state of the connection:
		if conn.get_state() == ConnState.CONNECTED:
			self.ic_flows[conn.get_id()] = conn
			conn.set_IO_callbacks(self.flow_success, 
														self.flow_failure)

		
		return True # successfully created a new connection



	"""
		Method returns the number of currently available 
		connections for starting new flows to the server.

		@return  : number of available connections
	"""
	def available_conns(self):
		return len(self.ic_flows)


	
	"""
		Issue a new flow to the server whose state is stored by
		this instance of PerFlowInfo.

		@param req        : flow metadata request.
		@param event_loop : event loop to use
		
		@return           : future
	"""
	def issue_request(self, req, event_loop):
		
	
		# create a future to promise to fulfill
		# the result
		fut_res = event_loop.create_future()

		if self.ic_flows and req.size > 0:

			# get an available connection
			sock_key, sock_val = self.ic_flows.popitem()
		
			# now schedule the request at the server
			self.ac_flows[sock_key] = (sock_val, fut_res)

			# write the request 
			sock_val.write_flow_metadata(req)

		elif req.size <= 0:
			# nothing to send
			fut_res.set_result(True)

		else:
			# no inactive connections
			fut_res.set_result(False)

		# return future
		return fut_res




	"""
		Method to release all the resources and close
		the PerServerInfo instance.
	"""
	def close(self):

		# just close all the operations
		for flow in self.ac_flows.items():
			try:
				flow[1][1].set_result(False)
			except:
				pass # ignore
			finally:
				flow[1][0].close_protocol()

		for flow in self.ic_flows.items():
			flow[1].close_protocol()

		# release all the resources
		self.ic_flows   = {}
		self.ac_flows   = {}
		self.can_more   = False
		self.rem_addr   = None
		self.serv_id    = None
			

	"""
		Method used as a callback by a protocol to notify the 
		server state that a flow has completed.
		
		@param prot : protocol instance
	"""
	def flow_success(self, prot):

			# must be an active flow
			find_res = self.ac_flows.get(prot.get_id(), None)


			assert find_res, "PerServerInfo::flow_success: cannot find an active flow."

			# set the future
			find_res[1].set_result(True)
			# remove/mark as an inactive flow
			self.ic_flows[prot.get_id()] = prot
			self.ac_flows[prot.get_id()] = None
			del self.ac_flows[prot.get_id()]


	"""
		Method is used as a callback by a protocol to notify the
		server state that a flow has failed. Failure error code
		is provided.

		@param prot    : instance of protocol
		@param errcode : error code experienced by the flow

	"""
	def flow_failure(self, prot, errcode):


		find_res = self.ac_flows.get(prot.get_id(), None)
		

		if find_res:
			self.can_more = True # one connection has been closed

			find_res[1].set_result(False)
			find_res[0].close_protocol()

			del self.ac_flows[prot.get_id()]

		else:
			# may happen that an inactive connection has been
			# closed by the remote server
			find_res = self.ic_flows.get(prot.get_id(), None)
			if find_res:
				# remove this connection
				del self.ic_flows[prot.get_id()]
				prot.close_protocol()





	"""
		This method is called every time a connection has received
		a signal from the remote server. A new connection needs to
		wait for a signal from the remote server to ensure it can
		work.
	"""
	def flow_ready(self, prot, stat_code):


		if	stat_code == errors.STAT_SUCCESS:
			# can use this connection for sending data
			self.ic_flows[prot.get_id()] = prot
			prot.set_IO_callbacks(self.flow_success, self.flow_failure)

		elif stat_code == errors.ERR_REJECT:
			# cannot accept more connections
			self.can_more = False
			
		else:
			pass



# A common interface for running a particular client
#
# parameters:
#			loads          :  list of network load values
#			cdfs           :  list of files that represent flow sizez (CDFs)
#			config_file    :  client configuration file
#			batch_size     :  size of requests that a client can generate 
#			               :  per run
#			run_time       :  client running time in seconds (if None, forever)
#			seed           :  random seed used for flow geneation
#			switch_period  :  only valid if len(loads) > 1 or len(cdfs) > 1
#			max_per_server :  maximum nunber of conns a client can have
#												per server

def run_client( loads, cdfs, config_file, batch_size,
								max_per_server,
								run_time=None, seed=None, 
								switch_period=None):


	if run_time and run_time <= 0:
		print("Running time cannot be negative or 0.")
		return False

	# check if multiple loads/cdfs have been passed
	change_set = False
	if len(loads) > 1 or len(cdfs) > 1:
		change_set = True


	# check if switch_period has been passed
	if change_set and (not switch_period or\
						switch_period <= 0):
		print("You must pass a positive value for swithcing (seconds).")
		return False


	# 1. set seed
	if seed:
		# passed some value
		random.seed(seed)
	else:
		# current time
		random.seed(time.time())


	# read the configuration file
	res = _read_config(config_file)


	if not res:
		print("Cannot read configuration.")
		print("Exiting the program...")
		return False

	# returned values
	servers = res[0] # a list of tuples
	rates   = res[1] # tuple


	# need to start running
	# pre=establish a few connections
	conns = [PerServerInfo(idx, addr, max_per_server) for (idx, addr) in 
						zip(range(0, len(servers), 1), servers)]

	# compute sleep overhead
	event_loop = asyncio.get_event_loop()
	usleep_overhead_us = common.get_usleep_overhead(20)

	# try to pre-estabish a few connections with 
	# every server
	res = _preconnect_flows(event_loop, conns)

	if res is False:
		print("Could not pre-establish connections with every server.")
		_close_connections(conns)
		event_loop.run_until_complete(stop_event_loop(event_loop))
		event_loop.stop()
		event_loop.close() # safe to close
		return False

	
	# event handlers for signal
	signal.signal(signal.SIGTERM, shutdown_callback)
	signal.signal(signal.SIGSEGV, shutdown_callback)
	signal.signal(signal.SIGABRT, shutdown_callback)
	signal.signal(signal.SIGHUP,  shutdown_callback)



	# create a thread for running the event loop
	io_thread = threading.Thread( target=run_io_forever, 
																args=(event_loop,))

	io_thread.start() # start lopping

	
	# run one of the settings for generating flows
	pend_futures = None # futures for termination
	try:
		if change_set:
			pend_futures = _run_dynamic_flows( switch_period, loads, cdfs,
														batch_size, servers, rates,
														conns, usleep_overhead_us,
														event_loop, run_time)

		else: # run a static setting
			pend_futures = _run_static_flows(loads[0], cdfs[0], batch_size,
													servers, rates, conns,
													usleep_overhead_us,
													event_loop, run_time)

	except Exception as exp:
		print("Error: _run_x_flows: {0}".format(exp))
		return False

	finally:

		# ensure that event loop terminates when needed	
		fut_res = asyncio.run_coroutine_threadsafe(
														stop_event_loop(event_loop),
														event_loop)

		try:
			fut_res.result(timeout=None)
		except:
			pass

		event_loop.call_soon_threadsafe(event_loop.stop)
		
		
		# wait until the other thread terminates
		io_thread.join()
		
		_close_connections(conns)
		event_loop.close()  # it is safe now to close since the 
												# thread has terminated


		# close all pending futures
		if pend_futures:
			for res_val in pend_futures:
				try:
					res_val.set_result(False)
				except:
					pass

			# destroy all the futures
			for res_val in pend_futures:
				try:
					res_val.result(timeout=0.2)
				except:
					pass

			pend_futures = None # done with the futures



	print("Async Simple Client is terminating.")
	return True



# Read a configuration file
# return:
#			tuple:
#				tuple[0]      : list of servers (ip_addr, port)
#				tuple[1][0]   : list of rates
#				tuple[1][1]   : total rate probability
def _read_config(config_file):
	
	if not config_file:
		return None

	servers = [] # tuple: (ip_addr, port) ip addresses
	rates   = [] # tuple: (value, prob) sending rate value and probability
	rate_prob_total = 0

	with open(config_file, mode="r", encoding="utf-8") as fd:
		for line in fd: # read line by line
			# possible lines:
			#		server ip_addr port
			#		rate   [val]Mbps prob
			vals = line.split()
			if vals[0] == "server": # server line
				try:
					servers.append((vals[1], int(vals[2], 10)))
				except Exception as exp:
					print("_read_config: {0}".format(exp))
					return None

			elif vals[0] == "rate": # rate line
				idx  = 0
				while idx < len(vals[1]) and vals[1][idx].isdigit():
					idx += 1

				try:
					prob = int(vals[2], 10)
					rate_prob_total += prob
					rates.append((int(vals[1][0:idx:1]), prob))
				except Exception as exp:
					print("_read_config: {0}".format(exp))
					return None


	# done reading the file
	if not servers:
		print("_read_config: no server found in configurtion file '{0}'".format(config_file))
		return None

	# by default, no rate limiting
	if not rates:
		rates.append((0, 100))
		rate_prob_total = 100


	return (servers, (rates, rate_prob_total))





# Make sure that there are enough connections to
# start with.
def _preconnect_flows(event_loop, conns):


	iss_cors  = []  # coroutines that will run
								  # to initialize the connections

	def noop_callback(tran_id, op_code, sockfd):
		pass
	
	
	for serv_info in conns:	
		server_idx = serv_info.serv_id	

		for _ in range(0, net.TG_PAIR_INIT_CONN, 1):

			
			# create a new connection in background
			iss_cors.append(
								event_loop.create_connection(
								lambda: DefaultProtocol(0, 
								noop_callback,
								conns[server_idx].flow_ready),
								host=serv_info.rem_addr[0], port=serv_info.rem_addr[1],
								family=socket.AF_INET, proto=socket.IPPROTO_TCP))


	# schedule all the coroutines and try to connect
	serv_idx  = 0
	cor_idx   = 0
	for init_conn in iss_cors:
		if cor_idx == net.TG_PAIR_INIT_CONN:
			serv_idx += 1 # next server
			cor_idx   = 0


		try:
			trans, prot = event_loop.run_until_complete(init_conn)
		except:
			return False # exception occured
		else:
			# check if the connection succeeded
			if prot.get_state() != ConnState.WAITING and\
					prot.get_state() != ConnState.CONNECTED:

				# if the connection failed to start or connect,
				# stop further processing
				return False # something went wrong


	# all connections have successfully connected
	return True 
		 
		


# Terminate all existing connections
def _close_connections(all_conns):

	# terminate all connections of a server
	for one_server in all_conns:
		one_server.close()




# Set batch/request variables
#
def _set_req_variables(	load, cdf, batch_size, 
												servers, rates):


	req_size_total      =  0
	req_interval_total  =  0
	rate_total          =  0


  # load a new cdf
	cdf_table = utils.cdf.CDFTable()
	# initialize the cdf file
	cdf_table.load_cdf(cdf)


	# calculate average request arrival interval
	period_us = (cdf_table.avg_cdf()*8.0) / load / net.TG_GOODPUT_RATIO
	if period_us <= 0.0:
		print("_set_req_vatriables: error: period_us is non-positive.")
		return None


	batch_requests = [BatchRequest()]*batch_size

	# calculate the maximum number a server can still sustain
	if not batch_requests or len(batch_requests) != batch_size:
		print("_set_req_variables: error: cannot allocate memory for", end=" ")
		print("batch requests.")
		return None

	max_rand_val = len(servers) - 1
	# initialize the batch of requests
	for req in batch_requests:
		req.meta.size  = int(cdf_table.gen_random_cdf()) # flow size (bytes)
		req.meta.load  = load
		req.meta.cdf   = common.get_cdf_idx(cdf)
		req.req_server = random.randint(0, max_rand_val)
		
		# rates read in _read_config
		req.meta.rate = common.gen_value_weight(rates[0], rates[1])
		# sleep internval based on poisson process
		req.req_sleep_us = common.poisson_gen_interval(1.0 / period_us)
		
		# update total values
		req_size_total     += req.meta.size
		req_interval_total += req.req_sleep_us
		rate_total         += req.meta.rate


	# return requests
	return batch_requests






# Coroutine for running IO operations
@asyncio.coroutine
def _run_request(request, server_info):

	# check if the server can still accept more
	# connections


	try:

		event_loop = asyncio.get_event_loop()

		if server_info.available_conns() > 0:
			# issue a new request
			fut_obj = server_info.issue_request(request, event_loop)

			result = yield from asyncio.wait_for(fut_obj, None, loop=event_loop)

			# wait for a result

			return result

		elif server_info.can_accept_more():
			# need to check if it is possible add more connections
			# try to connect in the background and then 
			# run issue_request

			def noop_callback(tran_id, op_code, sockfd):
				pass

			sock_cor = event_loop.create_connection(
										lambda: DefaultProtocol(0, 
										noop_callback,
										server_info.flow_ready),
										host=server_info.rem_addr[0],
										port=server_info.rem_addr[1],
										family=socket.AF_INET, 
										proto=socket.IPPROTO_TCP)


			# schedule the coroutine to run
			conn_fut = asyncio.ensure_future(sock_cor, loop=event_loop)
		
			# wait to finish
			try:
				transport, prot = yield from asyncio.wait_for(conn_fut, None, loop=event_loop)
			except Exception as exp: # some exception
				return True # don't try the same request

			else:

				if server_info.available_conns() > 0:

					fut_obj =  server_info.issue_request(request, event_loop)
					result = yield from asyncio.wait_for( fut_obj, 
																							None, 
																							loop=event_loop)

					return result


		else:
			pass

		return False

	except asyncio.CancelledError:
		return False



def _generate_random_pair(limit_one, limit_two):

	return (random.randint(0, limit_one), random.randint(0, limit_two))


# Generate flows from this client to remote
# servers
def _run_static_flows(load, cdf, batch_size, servers,
											rates, conns, 
											usleep_overhead_us, 
											event_loop, duration=None):


	global TERM_FLAG, MAXIMUM_LOSS 
									# need to keep references to the globals


	start_time_is = time.time() # get current time
	end_time_is   = None if (duration is None) else (float(start_time_is + duration))



	curr_loss    = 0 
	pend_futures = collections.deque(maxlen=batch_size)
	pend_reqs    = collections.deque(maxlen=batch_size)


	while not TERM_FLAG:
		# generate a request/batch of flows

		# if pend_reqs is not empty
		# first reissue old requests
		if pend_reqs:
			iter_items = min(batch_size, len(pend_reqs))
			
			requests = [pend_reqs.popleft() for _ in range(0, 1, iter_items)]

		else:
			requests = []	


		if len(requests) < batch_size:
			gen_extra  = batch_size - len(requests)
			extra_reqs = _set_req_variables(load, cdf, gen_extra,
		 																  servers, rates)

			if not extra_reqs:
				print("Error: _run_staitc_flows: cannot create requests.")
				break

			else:
				requests += extra_reqs
	

		# collect the futures
		# check if all previous failed
		num_of_failures = 0
		
		for tmp_fut in pend_futures:
			try:
			
				op_res = tmp_fut[0].result(None)			

			except:
				# exception considered as an error
				pend_reqs.append(tmp_fut[1])
				num_of_failures += 1
			else:
				if not op_res:
					pend_reqs.append(tmp_fut[1])
					num_of_failures += 1


		# check if the time is over
		if end_time_is and time.time() >= end_time_is:
			break # no more processing


		if pend_futures and len(pend_futures) == num_of_failures:
			# ignore all pending requests
			pend_reqs.clear()
			curr_loss += 1
			
			# failed that many times in a row
			if curr_loss == MAXIMUM_LOSS:
				break

		else:
			curr_loss = 0 # some progress has been made


		# clear all futures
		pend_futures.clear()
		
		# run one request at a time
		sleep_us = 0

		for req in requests:

			sleep_us += req.req_sleep_us
			
			if sleep_us > usleep_overhead_us:
				utils.GS_timing.delayMicroseconds(sleep_us - usleep_overhead_us)	
				sleep_us = 0					

			# schedule the operations on 
			# the IO thread
			tmp_fut = asyncio.run_coroutine_threadsafe(
													_run_request( req.meta, 
																				conns[req.req_server]),
													event_loop)

			# enqueue the future
			pend_futures.append((tmp_fut, req))

	# if futures have not been finished
	# finish them 
	return pend_futures



# Generate flows from this client to remote
# servers
def _run_dynamic_flows( switch_period, loads, cdfs,
												batch_size, servers, rates,
												conns, usleep_overhead_us,
												event_loop, duration=None):

	global TERM_FLAG, MAXIMUM_LOSS 
									# need to keep references to the globals


	# generate a random pair of integers for choosing a
	# combination of (cdf, load)
	# keep limits for generating integers
	SIZE_LOADS = len(loads) - 1
	SIZE_CDFS  = len(cdfs)  - 1
	cdf_idx    = 0
	load_idx   = 0

	start_time_is = time.time() # get current time
	end_time_is   = None if (duration is None) else (float(start_time_is + duration))

	# when to switch
	switch_time = time.time() + switch_period



	# for ensuring that all requests are issued
	pend_futures  = collections.deque(maxlen=batch_size)
	pend_reqs     = collections.deque(maxlen=batch_size)
	curr_loss     = 0


	while not TERM_FLAG:
		
		# generate a request/batch of flows

		if pend_reqs:
			iter_items = min(batch_size, len(pend_reqs))

			requests = [pend_reqs.popleft() for _ in range(0, 1, iter_items)]
			
		else:
			requests = [] # no pending requests


		if len(requests) < batch_size:
			gen_extra  = batch_size - len(requests)
			extra_reqs = _set_req_variables(loads[load_idx], 
																	cdfs[cdf_idx], 
																	gen_extra,
		 															servers, rates)

			if not extra_reqs:
				print("Error: _set_req_variables cannot create new requests.")
				break

			else:
				requests += extra_reqs



		# collect the futures from the previous
		# batch
		num_of_failures = 0

		for tmp_fut in pend_futures:
		
			try:
				op_res = tmp_fut[0].result(None)

			except:
				# exception is an error
				pend_reqs.append(tmp_fut[1])
				num_of_failures += 1

			else:
				if not op_res:
					pend_reqs.append(tmp_fut[1])
					num_of_failures += 1


	
		# check if the time is over
		if end_time_is and time.time() >= end_time_is:
			break # no more processing


		# make sure that some progress has been made
		if pend_futures and len(pend_futures) == num_of_failures:
			
			# generate new requests
			pend_reqs.clear()			
			curr_loss += 1

			if curr_loss == MAXIMUM_LOSS:
				# failed that many batches straight
				break

		else:
			curr_loss = 0 # made some progress


		# clear all futures 
		pend_futures.clear()


		sleep_us = 0
		for req in requests:
			
			sleep_us += req.req_sleep_us
			
			if sleep_us > usleep_overhead_us:
				utils.GS_timing.delayMicroseconds(sleep_us - usleep_overhead_us)
				sleep_us = 0
			

			# schedule the operations on 
			# the IO thread
			tmp_fut = asyncio.run_coroutine_threadsafe(
																_run_request(req.meta, 
																						conns[req.req_server]),
																event_loop)


			# enqueue the future
			pend_futures.append((tmp_fut, req))


		# check if need to change the setting
		if time.time() >= switch_time:
			rand_load = load_idx
			rand_cdf  = cdf_idx

			# make sure a different setting
			while load_idx == rand_load and cdf_idx == rand_cdf:
				load_idx, cdf_idx = _generate_random_pair(SIZE_LOADS, 
																									SIZE_CDFS)


			# update the switch time
			switch_time = time.time() + switch_period

	# if futures have not been finished
	# finish them
	return pend_futures
