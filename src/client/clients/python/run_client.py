# -*- coding: utf-8 -*-

import client
import argparse
import os
import sys


# maximum number of concurrent connections
# a client can establish with a remote server
__UPPER_LIMIT__ = 1000000


def _run_emulation(client_type, params):
 
	# before running the emulation, store pid
	with open('proc_id_client.txt', mode='w', encoding="utf-8") as fd:
		fd.write('Emulation process id: {0}\n'.format(os.getpid()))
  
	return client.run_client(client_type, params) 



# check  if a file exists
# retur True on success, False on failure
def _check_file(filename):

	return os.path.isfile(filename)



if __name__ == '__main__':
  
	# parse all parameters for running an emulation(client)
	parser = argparse.ArgumentParser(description='Parse user inputs\
																			for the emulation.')
  

	parser.add_argument('--loads', nargs='+', type=int,
											required=True, 
											help='A list of load values.')

	parser.add_argument('--cdfs', nargs='+', type=str,
											required=True,
											help='A list of CDF files.')


	parser.add_argument('--config_file', type=str,
											required=True,
											help='Configuration file which stores a list of\
											servers, rates.')


	parser.add_argument('--batch', type=int,
											required=True,
											help='Number of requests per batch.')


	parser.add_argument('--run_time', type=int,
											required=False, default=None,
											help='Emulation run time (seconds).')


	parser.add_argument('--seed', type=int,
											required=False, default=None,
											help='Random seed.')


	parser.add_argument('--switch_period', type=int,
												required=False, default=None,
												help='If more than one load or cdf is used,\
															the program will swtich between the passed\
															values (seconds).')


	parser.add_argument('--server_max', type=int, 
											required=True,
											help='Maximum number of flows a remote\
														server can accept from this client.')
          

	parser.add_argument('--client_type', type=str,
											required=False, choices=['client'],
											default='client', help='Type of client to run.')


	# parse the passed arguments
	args = parser.parse_args()

	# make sure that the number is positive of
	# flow per server
	if args.server_max < 1 or args.server_max > __UPPER_LIMIT__:
		print('A client can establish', end=' ')
		print('[{0}, {1}]'.format(1, __UPPER_LIMIT__), end=' ')
		print('concurrent connections with a remote server.')
		sys.exit(0)
  

	# check if the batch size is greater than 0
	if args.batch < 1:
		print('Batch size must be a positibe integer.')
		sys.exit(0)



	# check if a file exists
	for flow_file in args.cdfs:
		if _check_file(flow_file) is False:
			print("CDF file '{0}' does not exist.".format(flow_file))
			sys.exit(0)


	# check if the configuration file exists
	if _check_file(args.config_file) is False:
		print("Configurtion file '{0}' does not exist.".format(args.config_file))
		sys.exit(0)


	params = {'loads'           :  args.loads,
						'cdfs'            :  args.cdfs,
						'config_file'     :  args.config_file,
						'batch_size'      :  args.batch,
						'run_time'        :  args.run_time,
						'seed'            :  args.seed,
						'switch_period'   :  args.switch_period,
						'max_per_server'  :  args.server_max}
    

	res = _run_emulation(args.client_type, params)

	print ('\n\n===========================================================')


	if not res:
		print ('Something went wrong. Please check your', end=' ')
		print ('hardware settings or check your parameters.')
	else:
		print ('Emulation has succesfully finished!')
