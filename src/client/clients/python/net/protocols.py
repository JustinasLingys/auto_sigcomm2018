# -*- coding: utf-8 -*-

# The file contains implementations of the asyncio 
# server protocol for creating new connections. The default
# protocol shall be used as it implements the most recent
# version of the asyncio.Protocol interface.


# Python 3.4/3.5 std lib
import asyncio
import socket
import enum

# local modules
import net
import errors
import utils.async_common as common

@enum.unique
class ConnState(enum.Enum):
	INITIAL   = 1 # initialized a new connection
	WAITING   = 2 # just opened connection and waiting for a signal
	CONNECTED = 3 # # remote server accepts the connection
	REJECTED  = 4 # remote server rejects the connection
	CLOSED    = 5 # failed connection (equivalent to closed)



class FirstProtocol(asyncio.Protocol):
	"""
		FirstProtocol is the first implementation of the 
		asyncio.Protocol interface. It helps manage active sockets
		and uses callbacks for reading data.
	"""

	def __init__( self, instance_id,
								connection_callback,
								ready_callback):


		self.id         = instance_id          # unique id for connecting

		# callbacks
		self.conn_call  = connection_callback  # on connected callback
		self.op_succ    = None                 # io success callback
		self.op_fail    = None     						 # io failure callback
		self.op_ready   = ready_callback       # when ready

		self.flow_info  =  None
		self.transport  =  None
		self.state      =  ConnState.INITIAL   # start with initial
		self.tmp_buff   =  []                  # temporary buffer


	def connection_made(self, transport):
		# make sure it's tcp and 
		# can set dctcp fields
		sock = transport.get_extra_info("socket", None)
		conn_id = self.id # connecting id (only for a new connection)
		
		assert sock, "Cannot retrieve a socket from the transport."
		

		self.transport = transport

		# initialize the connected socket to some values
		try:
			sock.setsockopt(socket.SOL_SOCKET,  socket.SO_REUSEADDR, 1)
			sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY,  1)
			sock.setsockopt(socket.IPPROTO_IP,  
														socket.IP_TOS, net.PIAS_PRIO_DSCP[0])

		except Exception as exp:
			print("FirstProtocol::connection_made:", exp)
			self.conn_call(conn_id, errors.ERR_INTERNAL, -1)
			self.close_protocol()
	
		else:
			# socket initialized and prepared for using it
			self.id = sock.fileno()
			self.conn_call(conn_id, errors.STAT_SUCCESS, sock.fileno())
			# move to the waiting state
			self.state = ConnState.WAITING

		finally:
			# reset the 'connecting' callback
			self.conn_call = None
			


	def  data_received(self, raw_data):

	
		# connection must be connected/approved for data
		if self.state == ConnState.CONNECTED: # connected

			if not self.flow_info:
				# discard the data (no flow is expected)
				return

			# means received data from the server			

			self.flow_info.size -= len(raw_data) # update the remaining size

			if self.flow_info.size <= 0: # done (need to send a signal)
				
				try:
					self.transport.write(b'\x0f')
				except:
					self.op_fail(self, errors.ERR_INTERNAL)
					self.close_protocol()
				else:
					self.op_succ(self) # success on a write operation
					self.flow_info = None

		elif self.state == ConnState.WAITING:
			# the server has sent a signal if it accepts
			# or rejects the new connection
			self.tmp_buff.append(raw_data)
			signal_val = b"".join(self.tmp_buff)


			if len(signal_val) == net.ESTABLISH_LEN:
				# compare the bytes
				if signal_val == net.OK_CONNECTION:
					self.state = ConnState.CONNECTED
					self.op_ready(self, errors.STAT_SUCCESS)
				else:
					# notify the caller about this error
					self.op_ready(self, errors.ERR_REJECT)
					self.close_protocol() # close protocol


				# reset the buffer to point to nothing
				self.tmp_buff = None

		elif self.state == ConnState.CLOSED:
			pass # ignore
		else:
			if self.op_fail:
				self.op_fail(self, errors.ERR_INTERNAL)
			self.close_protocol()


	def connection_lost(self, exp):
		# need to check which case
		if self.conn_call:
			# internal error to connect
			self.conn_call(self.id, errors.ERR_INTERNAL, -1)
		elif self.op_fail:
				# means the connection has successfully connected
				# before, but failed somewhere in the middle
				self.op_fail(self, errors.ERR_INTERNAL)
		else:
			pass

		# release the system resources
		self.close_protocol()



	def set_IO_callbacks(self,  success_callback,
															failure_callback):

		self.op_succ  = success_callback
		self.op_fail  = failure_callback


	def write_flow_metadata(self, flow_meta):


		# need check the state of the protocol
		# before processing further
		if self.state == ConnState.CONNECTED:	
		
			# try to write the metadata
			raw_meta = common.encode_metadata(flow_meta)
	
			# sends the metadata asynchronously
			try:
				self.transport.write(raw_meta)
			except Exception as exp:
				print("FirstProtocol::write_flow_metadata: error:", exp)
				self.op_fail(self, errors.ERR_INTERNAL)
				self.close_protocol()

			else:
				self.flow_info = flow_meta

		elif self.state == ConnState.WAITING:
			# sigal that need to try later
			self.op_fail(self, errors.ERR_AGAIN)

		elif self.state == ConnState.CLOSED:
			pass # do nothing
		else:
			# some internal error
			self.op_fail(self, errors.ERR_INTERNAL)
			self.close_protocol()


	
	def close_protocol(self):
		if self.transport:
			try:
				self.transport.close()
			except:
				pass # ignore
			finally:
				self.transport  = None
				self.tmp_buff   = None


		# reset all the callbacks
		self.op_succ    =  None
		self.op_fail    =  None
		self.op_ready   =  None
		self.flow_info  =  None
		self.state = ConnState.CLOSED # closed the connection


	def get_id(self):
		return self.id


	def get_state(self):
		return self.state
		 


DefaultProtocol = FirstProtocol
