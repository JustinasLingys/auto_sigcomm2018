# -*- coding: utf-8 -*-

# PIAS global variables

# Idle time in us
PIAS_IDLE_TILE = 500

# RTOmin in us
PIAS_RTO_MIN = 9*1000

# Threshold of consecutive TCP timeouts to reset priority
PIAS_TIMEOUT_THRESH = 3

# Sequence gap in bytes to identify consecutive TCP timeouts
PIAS_TIMEOUT_SEQ_GAP = 3*1448

# Shall we enable aging to prevent long-term starvation
PIAS_ENABLE_AGING = True

# Shall we enable debug mode
PIAS_DEBUG_MODE = 0


# DSCP priorities
#PIAS_PRIO_DSCP    = [0x1c, 0x18, 0x14, 0x10, 0x0c, 0x08, 0x04, 0x00]
PIAS_PRIO_DSCP     = (0x80, 0x40, 0x20, 0x00)
# PIAS default thresholds
#PIAS_PRIO_THRESH  = [759*1460, 1132*1460, 1456*1460, 1737*1460, 2010*1460, 2199*1460, 2325*1460]

# server/client parameters

# flow meta data size
TG_METADATA_SIZE = 20 # look at utils.common

# default server port
TG_SERVER_PORT = 5001

# default number of backlogged connections for listen()
import socket
TG_SERVER_BACKLOG_CONN  = socket.SOMAXCONN
# maximum number of concurrent flows per server
TG_MAX_PER_SERVER_FLOWS = 2

# maximum number of writes to write in a 'send' system call
TG_MAX_WRITE  = (1 << 20)
MAX_WRITE_BUF = ['\0']*TG_MAX_WRITE

# minimum number of bytes to write in a 'send' sysstem call 
# (used with rate limiting)
TG_MIN_WRITE  = (1 << 16)
MIN_WRITE_BUF = ['\0']*TG_MIN_WRITE 

# maximum amount of data to read in a 'recv' system call
TG_MAX_READ = (1 << 20)

# default initial number of TCP connections per pair
TG_PAIR_INIT_CONN = 1

# default goodput / link capacity ratio
TG_GOODPUT_RATIO = (1448.0 / (1500 + 14 + 4 + 8 + 12))


# simple client-server protocol for notifying the client if
# a connection can be established 

import struct
OK_CONNECTION = struct.pack('<I', 1) # successful connection
NO_CONNECTION = struct.pack('<I', 0) # cannot accept more connections
ESTABLISH_LEN = 4
