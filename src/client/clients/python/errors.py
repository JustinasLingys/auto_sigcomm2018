# -*- coding: utf-8 -*-


# Operation status codes
STAT_SUCCESS =   0  # operation successfully completed
ERR_AGAIN    =   1  # the connection is not ready yet for data
ERR_REJECT   =   2  # remote server rejects a new connection
ERR_INTERNAL = 255  # internal error (unknown)
