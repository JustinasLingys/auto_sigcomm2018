# -*- coding: utf-8 -*-

# A file that contains scripts for reading a CDF file and using
# it later for generating flows.

import sys
import random


# MAXIMUM RANDOM integer
RAND_MAX = sys.maxsize


class CDFEntry(object):

  def __init__(self, value=0.0, cdf=0.0):
    self.m_value = value
    self.m_cdf   = cdf


# CDF distribution
class CDFTable(object):
  
  def __init__(self):
    self.m_table     =  []
    self.m_min_cdf   = 0.0
    self.m_max_cdf   = 1.0


# get CDF distribution from a given file
  def load_cdf(self, filename):

    # open a file and read line by line
    with open(filename, mode='r', encoding='utf-8') as fd:
      for line in fd:
        vals = line.split()
        # if length is not 2, skip
        if len(vals) >= 2:
          value = float(vals[0])
          cdf   = float(vals[1]) 
          self.m_table.append(CDFEntry(value, cdf))
          
          # update min/max values of the table
          if self.m_min_cdf > cdf:
            self.m_min_cdf = cdf

          if self.m_max_cdf < cdf:
            self.m_max_cdf = cdf 
         
           
# print CDF distribution information
  def print_cdf(self):

    for entry in self.m_table:
      print ("%.2f %.2f" % (entry.m_value, entry.m_cdf))


# get average value of CDF distribution
  def avg_cdf(self):
    
    if not self.m_table:
      return 0.0

    value = self.m_table[0].m_value / 2
    prob  = self.m_table[0].m_cdf
    avg   = (value * prob)
  
    # skip the first entry
    for idx in range(1, len(self.m_table), 1):
      value = (self.m_table[idx].m_value + self.m_table[idx-1].m_value) / 2
      prob  = self.m_table[idx].m_cdf - self.m_table[idx-1].m_cdf
 
      # update average value
      avg += (value * prob)


    return avg


  def _interpolate(self, x, x1, y1, x2, y2):
    if x1 == x2:
      return (y1 + y2) / 2

    else:
      return (y1 + (x - x1) * (y2 - y1) / (x2 - x1))


# generate a random floating point number from min to max
  def _rand_range(self, min_cdf, max_cdf):
    return (min_cdf + random.randint(0, RAND_MAX) 
            * (max_cdf - min_cdf) / RAND_MAX)
  
     
# generate a random value based on CDF distribution
  def gen_random_cdf(self):
  
    # if the table is empty,
    # return a zero
    if not self.m_table:
      return 0.0 


    x = self._rand_range(self.m_min_cdf, self.m_max_cdf)
   
    # check for the first entry
    if x <= self.m_table[0].m_cdf:
      return self._interpolate(float(x), 0.0, 0.0, 
                               self.m_table[0].m_cdf, 
                               self.m_table[0].m_value)


    for idx in range(1, len(self.m_table), 1):
      
      if x <= self.m_table[idx].m_cdf:
        return self._interpolate(float(x), 
                     self.m_table[idx-1].m_cdf,
                     self.m_table[idx-1].m_value,
                     self.m_table[idx].m_cdf,
                     self.m_table[idx].m_value)


    # return the last value by default
    return self.m_table[-1].m_value

