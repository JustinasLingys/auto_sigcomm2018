# -*- coding: utf-8 -*-

# A Commno utility module that provides similar utilities to the one
# in HKUSt-SING flow generator.


import os.path
import struct
import random
import math
import sys
RAND_MAX = sys.maxsize

# local module for getting precise time
import utils.GS_timing as GS_timing
import net


class FlowMetadata(object):
  def __init__(self, fid, fsize, ftos, frate, fload, fcdf):
    self.id   = fid
    self.size = fsize
    self.tos  = ftos
    self.rate = frate
    self.load = fload # network load (Mbps)
    self.cdf  = fcdf  # cdf index in the 'NET_CDFS' dictionary


__NET_CDFS__={'DCTCP_CDF' : 0, 'FB_CDF' : 1, 'VL2_CDF' : 2}

# get an index for a cdf string
def get_cdf_idx(cdf_path):
  # subdivide the path into values and return the index
  file_name = os.path.basename(cdf_path)
  # look for the file
  if not file_name:
    return -1 # no such file
  
  # discard file extension
  title = file_name.split('.')

  idx = __NET_CDFS__.get(title[0], -1)

  return idx


# return the string of a CDF for an index 
def get_cdf_string(cdf_idx):

  for (val_str, val_idx) in __NET_CDFS__.iteritems():
    if val_idx == cdf_idx: # found string
      return val_str

# no such index
  return None


# generate a poisson process arrival interval
def poisson_gen_interval(avg_rate):
  if avg_rate > 0:
    cal = float(random.randint(0, RAND_MAX-1)) / RAND_MAX
    return -(math.log(1.0 - cal) / avg_rate)

  else:
    return 0.0


# calculate usleep overhead
def get_usleep_overhead(iter_num):
  
  if iter_num <= 0:
    return 0


  start = GS_timing.micros() # get a timestamp in microseconds
  
  for idx in range(0, iter_num, 1):
    GS_timing.delayMicroseconds(0)

  end = GS_timing.micros()   # get a timestmap in microseconds
  
  # get time in microseconds
  tot_sleep_us = (end - start)

  return (tot_sleep_us / iter_num)


# randomly generate a value based on weights
def gen_value_weight(vals_weights, weight_total):

  val = random.randint(0, RAND_MAX) % weight_total
  
  for var, wght in vals_weights:
    if val < wght:
      return var

    else:
      val -= wght

  return vals_weights[-1][0] # default value



# This function attempts to read bytes and finishes if:
# 1. count bytes have been read;
# 2. end of a file is reached, or for a network socket, 
#    the connection is closed
# 3. sockfd.recv produces an error
# More details can be found in github in HKUST-SING/TrafficGenerator

# dummy_buf if the read data is meaningful or not
def read_exact(sockfd, count, max_per_read, dummy_buf):
 
  total_read       = 0 # total read bytes 
  bytes_to_read    = 0 # maximum number of bytes to
                       # read in next recv call

  if count <= 0:
    return (None, 0)

 
  recv_data = [] # an array of received data

  # keep looping and reading data
  while count > 0:
    bytes_to_read = max_per_read if (count > max_per_read) else count

    # wrap in try-catch
    try: 
      read_data = sockfd.recv(bytes_to_read, 0)
    except Exception as exp:
      # soemthing wrong with the socket
      print (exp)
      return (None, -1)
    
    # if an error need to check
    if not read_data or len(read_data) == 0:
      break


    # data has been read
    if not dummy_buf:
      recv_data.append(read_data)
   
    count       -= len(read_data)
    total_read  += len(read_data)


  
  return (''.join(recv_data), total_read)



# This function attempts to write an exact number of bytes
# and only finishes int theee cases:
# 1. count bytes have been sent
# 2. send produces an error
# For more information, please read HKUST-SING/TrafficGnerator
# on github.
# Returns the number of sent bytes or a negative number on an error

def write_exact(sockfd, s_data, count, max_per_write, rate_mbps,
                sleep_overhead_us, dummy_buf):

  bytes_total_write = 0 # total number of bytes that have been written
  bytes_to_write    = 0 # maximum number of bytes to write 
                        # in next send() call

  sleep_us = 0
  write_us = 0
  cur_ptr  = 0

  while count > 0:
    bytes_to_write = max_per_write if (count > max_per_write) else count
    cur_ptr = 0 if (dummy_buf) else bytes_total_write
    
    tv_start = GS_timing.micros()
    
    # wrap in try-catch
    try:
      sent_bytes = sockfd.send(s_data[cur_ptr:(cur_ptr+bytes_to_write):1], 0)
    except:
      # exception in writing (something wrong with the socket)
      # signal the user
      return -1    

    tv_end = GS_timing.micros()
    
    if sent_bytes <= 0:
      break

    write_us =  (tv_end - tv_start)
    sleep_us += ((sent_bytes * 8.0)  / rate_mbps - write_us) if (rate_mbps > 0) else 0

    # update counters and sleep
    bytes_total_write  += sent_bytes
    count              -= sent_bytes

    if sleep_overhead_us < int(sleep_us):
      GS_timing.delayMicroseconds(int(sleep_us - sleep_overhead_us))
      sleep_us = 0
   

  return bytes_total_write 

# read the metadata of a flow and return tuples of metadata
# If not success, return 'None'
def read_flow_metadata(sockfd):


  meta, read_bytes = read_exact(sockfd, net.TG_METADATA_SIZE, 
                                net.TG_METADATA_SIZE, False)
  


  if meta is None or read_bytes != net.TG_METADATA_SIZE:
    print ('Error: read metadata error. returning None')
    return None


  # return the metadata values
  # structure:
  #   id
  #   size
  #   tos
  #   rate
  fid, fsize, ftos, frate, fload, fcdf = struct.unpack('<IIIIII', meta)
  
  return FlowMetadata(fid, fsize, ftos, frate, fload, fcdf)
 

# write a flow request into a socket and return True if a success;
# otherwise return 'False' 
def write_flow_req(sockfd, meta):
  # encode metada into a string 
  meta_send = struct.pack('<IIIIII', meta.id,   meta.size, 
                                     meta.tos,  meta.rate,
                                     meta.load, meta.cdf)

  if meta_send is None or len(meta_send) != net.TG_METADATA_SIZE:
    # something didn't work
    return False

  return (write_exact(sockfd, meta_send, net.TG_METADATA_SIZE,  
                      net.TG_METADATA_SIZE, 
                      0, 20, False) == net.TG_METADATA_SIZE)

