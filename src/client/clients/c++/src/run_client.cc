/*
 * Main file contains logic for initializing the clients.
 * All the steps are similar for any client.
 */


#include <cstdlib>
#include <cstdio>
#include <cstring>

#include "net/values.h"
#include "utils/common_struct.h"
#include "client/clients.h"



using sing_client::param_list_t;

/*Read command line parameters and check if they follow the pattern:
 * {--param_title param_value}
 */
static bool read_args(const int argc, const char* argv[],
               struct param_list_t* const params);


/* Print the usage of the command line */
static void print_usage(const char* program);





int main(const int argc, const char* argv[])
{

  struct param_list_t* parameters = sing_client::create_param_list();
  bool read_success = true;

  if(argc > 1)
  { 
    read_success = read_args(argc-1, (argv+1),
                             parameters);

  }

  if(read_success)
  {
    /*run emulation*/
    sing_client::run_emulation(parameters);
  }
  else
  {
    print_usage(argv[0]);
  }

  /*release parameter memory*/
  sing_client::release_param_list(parameters);

  return EXIT_SUCCESS;
}



bool read_args(const int argc, const char* argv[],
               struct param_list_t* const params)
{

 if(argc % 2 != 0)
 {
   printf("Either multiple values per parameter or a parameter is missing a value.\n");
   return false;
 }
  
 /*Read two adjacent values and check if they follow the pattern*/
 int i;
 bool insert_res; 
 const char* ptr = NULL;

 for(i = 0; i < argc; i += 2)
 {

  if(strlen(argv[i]) < 3)
  {
    printf("'%s' does not match the parameter pattern.\n", argv[i]);
    return false;
  }

  if(argv[i][0] == '-' && argv[i][1] == '-')
  {
    ptr  = argv[i];
    ptr += 2;
    insert_res = insert_param(params, ptr, argv[i+1]);
    if(!insert_res)
    {
      printf("Error: cannot insert '%s' with the value '%s'.\n", 
             argv[i], argv[i+1]);

      return false;
    } 
  }
  else
  {
    printf("'%s' missing a '-'?\n", argv[i]);
    return false;
  }

 }//for

 return true;

}


void print_usage(const char* program)
{

  printf("%s  --parameter_name  parameter_value  ...\n", program);

}
