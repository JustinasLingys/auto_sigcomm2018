#ifndef CLIENT_SEND_PROTOCOL_SING_H
#define CLIENT_SEND_PROTOCOL_SING_H


#include <memory>


#include "../utils/common_struct.h"
#include "values.h"

namespace sing_client
{


using MetaPtr = std::unique_ptr<struct flow_info_t, decltype(&deallocate_metadata)>;


/**
 * Keep current state of a connection.
 */
enum class ConnState: int
{

  ERROR     = -1, // something is bad with the connection
  INITIAL   =  1, // connection is not connected (newly created)
  WAITING   =  2, // waiting for the server to reply
  REJECTED  =  3, // remote server has rejected the connection
  NEW_META  =  4, // waiting for a new flow metadata
  OLD_META  =  5, // metadata has not been completely sent yet
  RECV_FLOW =  6, // receiving flow data from the client
	SEND_FCT  =  7, // sending the FCT for the remote server
  WAIT_FCT  =  8, // waiting for a signal that a flow has 
                  // competed (only for server)
  CLOSED    =  9  // closed the connection

};


/**
 * Connection errors.
 */
enum class ConnStatus: int
{
  SUCCESS      = 0,   // connection completed an operation with
                      // a success
  MOD_READ     = 1,   // modify for reading
  MOD_WRITE    = 2,   // modify for writing
  ERR_AGAIN    = 3,   // try again later 
  ERR_REJECT   = 4,   // remote server cannot accept more new connections
  ERR_IO       = 5,   // failed an IO operation
  ERR_INTERNAL = 255  // some other error

};


class Protocol
{
  /**
   * Maintains state per connection and handles send/recv data
   */

  public:
    Protocol();
    ~Protocol();

    
    void        set_metadata(MetaPtr& meta);
    MetaPtr&    get_metadata();
    ConnStatus  run_protocol(const int sockfd);
    void        set_state(const ConnState state) noexcept;        
    ConnState   get_state() const noexcept;
    void        close_protocol();
    
     

  private:
    /*Private utility methods*/
    ConnStatus process_connect(const int sockfd);
    ConnStatus process_wait(const int sockfd);
    ConnStatus process_new_flow(const int sockfd);
    ConnStatus process_old_flow(const int sockfd);
    ConnStatus process_recv_flow(const int sockfd);
		ConnStatus process_send_fct(const int sockfd);
   


  private:
    MetaPtr         flow_;   /*current flow info*/
    size_t          size_;   /*how much more to recv*/ 
    ConnState       state_;  /* current state of the connection*/

                    /*buffer for encoded metadata*/
    char            meta_buff_[TG_METADATA_SIZE]; 
                                      /*buffer for storing metadata*/  

    char            accept_sign_[TG_SERVER_FLAG_SIZE]; 
                   

    


}; // class Protocol




} // namespace sing_client


#endif /*CLIENT_SEND_PROTOCOL_SING_H*/
