#include <cerrno>
#include <algorithm>
#include <utility>

#include <unistd.h>
#include <sys/socket.h>


#include "protocol.h"


static constexpr size_t MAX_TCP_BUFFER_SIZE = (10*1024*1024);
static char SHARED_BUFFER[MAX_TCP_BUFFER_SIZE];
//static char SEND_SIGNAL[1] = {'a'}; 
constexpr static  std::uint32_t SEND_SIGNAL = 0U;

namespace sing_client{


Protocol::Protocol()
: flow_(nullptr, deallocate_metadata),
  size_(0),
  state_(ConnState::INITIAL)
{}

Protocol::~Protocol()
{
  close_protocol();
}

void
Protocol::close_protocol()
{
  if(flow_)
  {
    flow_.reset(nullptr);
  }

  size_    = 0;
  state_   = ConnState::CLOSED;
}


ConnState
Protocol::get_state() const noexcept
{
  return state_;
}


void
Protocol::set_state(const ConnState state) noexcept
{
  state_ = state;
}

void
Protocol::set_metadata(MetaPtr& meta)
{
  flow_.swap(meta);
}

MetaPtr&
Protocol::get_metadata()
{ 
  state_ = ConnState::NEW_META; // expect new metadata
  return flow_;
}


ConnStatus
Protocol::run_protocol(const int sockfd)
{

  /*Based on the internal state, decide the action*/
  switch(state_)
  {
    case ConnState::INITIAL:
    {
      return process_connect(sockfd);
    }
    
    case ConnState::WAITING:
    {
      return process_wait(sockfd);
    }

    case ConnState::REJECTED:
    {
      return ConnStatus::ERR_REJECT;
    }

    case ConnState::NEW_META:
    {
      return process_new_flow(sockfd);
    }

    case ConnState::OLD_META:
    { 
     return process_old_flow(sockfd);
    }

    case ConnState::RECV_FLOW:
    {
      return process_recv_flow(sockfd);
    }

    case ConnState::SEND_FCT:
    {
      return process_send_fct(sockfd);
    }

    default:
    {
      return ConnStatus::ERR_IO;
    }
  } // switch

}


ConnStatus
Protocol::process_connect(const int sockfd)
{
  
  /*this method gets executed once a new connection has been made*/
  int result;
  socklen_t result_len = sizeof(result);  

  if(getsockopt(sockfd, SOL_SOCKET, SO_ERROR, &result, &result_len) < 0)
  {
    return ConnStatus::ERR_INTERNAL;
  }

  if(result != 0)
  { /*failed to connect*/
    return ConnStatus::ERR_INTERNAL;
  }

  /*connected to a server*/
  state_ = ConnState::WAITING;

  /*wait for a signal from the remote server*/
  size_  = TG_SERVER_FLAG_SIZE; 

  
  ConnStatus tmp_res = process_wait(sockfd);
  if(tmp_res == ConnStatus::ERR_AGAIN)
  {
    return ConnStatus::MOD_READ; /*register for reading*/
  }
  else
  {
   return tmp_res;
  }

}


ConnStatus
Protocol::process_wait(const int sockfd)
{
 
  /*try reading data from the socket*/
  char* ptr = (accept_sign_ + (TG_SERVER_FLAG_SIZE - size_));
  ssize_t read_bytes = read(sockfd, static_cast<void*>(ptr), size_);
  
  if(read_bytes <= 0)
  {
   if(read_bytes < 0 && (errno == EAGAIN || errno == EWOULDBLOCK))
   {
     return ConnStatus::ERR_AGAIN;
   }
   else
   {
     // means the connection has been closed or failed
     return ConnStatus::ERR_IO;
   } 
  }

  size_ -= static_cast<size_t>(read_bytes); /*update the value of remaining read*/
  ptr   += read_bytes;

  while(size_ > 0)
  {
    
    /*try again*/
    read_bytes = read(sockfd, static_cast<void*>(ptr), size_);
    
    if(read_bytes <= 0)
    {
      if(read_bytes < 0 && (errno == EAGAIN || errno == EWOULDBLOCK))
      {
        break; /* no more data*/
      }

     return ConnStatus::ERR_IO;
    }

   /*successfully read more data*/
   size_ -= static_cast<size_t>(read_bytes);
   ptr   += read_bytes;
    
     
  }//while


  if(size_ > 0)
  {
    /*need to read more*/
    return ConnStatus::ERR_AGAIN;
  }
  else
  { /*finished reading the flag from the server*/

    if(sing_client::server_accept(accept_sign_))
    { /*the server accepts the new connection*/
      state_ = ConnState::NEW_META;
      if(flow_)
      {
        /*remaining flow size is the new flow*/
        size_ = static_cast<size_t>(flow_->fsize);
       
      }
      else
      {
        size_ = 0; /*no flow value*/
      }
      
      return ConnStatus::MOD_WRITE; 
    }
    else
    {
     /*rejected by the remote server*/
     state_ = ConnState::REJECTED;
     return ConnStatus::ERR_REJECT;
    }

  }
  
}


ConnStatus
Protocol::process_new_flow(const int sockfd)
{
 
  if(!flow_)
  {
    // this protocol has nothing to process
    return ConnStatus::SUCCESS;
  }
  else
  {
   /*new flow metadata has to be issued*/
   const bool enc_res = encode_metadata(flow_.get(), 
                               static_cast<char*>(meta_buff_));
   if(!enc_res)
   {
     /*reset the flow*/
     printf("Protocol::process_new_flow: failed to encode the flow.\n");
     flow_.reset(nullptr);
     return ConnStatus::SUCCESS;
   }

 
   /*try to write as much metadata as possible*/
   ssize_t written_bytes = write(sockfd, 
                                 static_cast<void*>(meta_buff_), 
                                 TG_METADATA_SIZE);


   if (written_bytes <= 0)
   {
     if(written_bytes < 0 && (errno == EAGAIN || errno == EWOULDBLOCK))
     {
       return ConnStatus::ERR_AGAIN;
     }
     else
     {
       return ConnStatus::ERR_IO;
     }    
  
   }//if
   else
   {
     const size_t sent_data = static_cast<const size_t>(written_bytes);
     if(sent_data == TG_METADATA_SIZE)
     {
       /*wait for the response (flow) from the server*/
       state_ = ConnState::RECV_FLOW;
       size_  = static_cast<size_t>(flow_->fsize);

			 if(size_ == 0)
       {
       	/*write if possible the response*/
        state_ = ConnState::SEND_FCT;
        return process_send_fct(sockfd);
       }
       else
       {
         return ConnStatus::MOD_READ; /*need to read flow*/
       }
     }
    else
    {
      size_  = (TG_METADATA_SIZE - static_cast<size_t>(written_bytes));
      state_ = ConnState::OLD_META; // need to write again


      return ConnStatus::ERR_AGAIN;
    }
   }
   

  }//else


}


ConnStatus
Protocol::process_old_flow(const int sockfd)
{

  if(!flow_)
  {
    state_ = ConnState::NEW_META;
    return ConnStatus::SUCCESS;
  }

  /*keep writing the remaining metadata*/

  char* ptr = (meta_buff_ + (TG_METADATA_SIZE - size_));

  /*try to write as much metadata as possible*/
  ssize_t written_bytes = write(sockfd, static_cast<void*>(ptr), size_);

  if (written_bytes <= 0)
  {
    if(written_bytes < 0 && (errno == EAGAIN || errno == EWOULDBLOCK))
    {
      return ConnStatus::ERR_AGAIN;
    }
    else
    {
      return ConnStatus::ERR_IO;
    }    
  
 }//if
 else
 {
   size_ -= static_cast<size_t>(written_bytes); // written some bytes
   if(size_ == 0)
   {
     /*wait for the response (flow) from the server*/
     state_ = ConnState::RECV_FLOW;
     size_  = static_cast<size_t>(flow_->fsize);
     

		 if(size_ == 0)
     { /*the generated flow size is zero*/
      state_ = ConnState::SEND_FCT;
 			return process_send_fct(sockfd);

     }
     else
     {
       return ConnStatus::MOD_READ; /*need to read*/
     }
   }
   else
   {
     return ConnStatus::ERR_AGAIN;
   }
  } //else

}


ConnStatus
Protocol::process_recv_flow(const int sockfd)
{

  if(size_ > 0)
  {  
    // still have to read some data
    size_t one_shot = std::min<size_t>(size_, MAX_TCP_BUFFER_SIZE);
    ssize_t read_data = read(sockfd, static_cast<void*>(SHARED_BUFFER), 
                             one_shot);
     
    if(read_data <= 0)
    {
      if(read_data < 0 && (errno == EAGAIN || errno == EWOULDBLOCK))
      {
        return ConnStatus::ERR_AGAIN;
      }
      else
      {
        return ConnStatus::ERR_IO;
      }
      
    }
    else
    { /*successfully read some data*/
      size_ -= static_cast<size_t>(read_data);
      
      while(size_ > 0)
      {
        one_shot = std::min<size_t>(size_, MAX_TCP_BUFFER_SIZE);
        read_data = read(sockfd, static_cast<void*>(SHARED_BUFFER), 
                         one_shot);

        if(read_data <= 0)
        {
          if(read_data < 0 && (errno == EAGAIN || errno == EWOULDBLOCK))
          {
            break;
          }

          else
          {
            return ConnStatus::ERR_IO;
          }
        }

        size_ -= static_cast<size_t>(read_data); /*update the remaining size*/

      }//while


      if(size_ > 0)
      {
        return ConnStatus::ERR_AGAIN;
      }
      else
      {
        /*done reading the flow, send fct signal*/
        state_ = ConnState::SEND_FCT;
        auto fct_res = process_send_fct(sockfd);

        if(fct_res == ConnStatus::ERR_AGAIN)
        {
          return ConnStatus::MOD_WRITE; /*wait for writing*/
        }
        else
        {
          return fct_res;
        }
        
      }

    }//else (read_data > 0)

  }//if
  else // (size == 0)
  {
    /*done reading the flow, send fct signal*/
    state_ = ConnState::SEND_FCT;

    auto fct_res = process_send_fct(sockfd);
    
    if(fct_res == ConnStatus::ERR_AGAIN)
    {
      return ConnStatus::MOD_WRITE; /*wait for writing*/
    }
    else
    {
      return fct_res;
    }
        
  }//else
}


ConnStatus
Protocol::process_send_fct(const int sockfd)
{
  /*send the fct end signal*/
  /*(notify the server that the flow has been received)*/
  const ssize_t written_bytes = write(sockfd, (void*) &SEND_SIGNAL, sizeof(std::uint32_t));


  /*check if completed*/
  if(written_bytes <= 0)
  {
    /*check if the socket rejected*/
    if(written_bytes < 0 && (errno == EAGAIN || errno == EWOULDBLOCK))
    {
      return ConnStatus::ERR_AGAIN; // try one more time
    }
    
    else
    {
      return ConnStatus::ERR_IO; 
    }
  }
  else
  {
    /*sent the FCT signal*/
    flow_.reset(nullptr);
    size_  = 0;
    state_ = ConnState::NEW_META;
    
    return ConnStatus::SUCCESS; /*done with this flow*/
  }
  
}


} // namespace sing_client
