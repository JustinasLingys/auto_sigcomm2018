#ifndef NET_VALUES_SING_H
#define NET_VALUES_SING_H

#include <cstdint>

#include "../utils/common_struct.h"

namespace sing_client{



/*Metadata size in bytes*/
constexpr size_t TG_METADATA_SIZE    = 5*sizeof(uint32_t);
constexpr size_t TG_SERVER_FLAG_SIZE = sizeof(uint32_t);

/*if server accepts a new connection*/
bool server_accept(const char* raw_bytes);

/*Initial number of connections*/
#define TG_PAIR_INIT_CONN 5

/* backlog connections for listen */
#define TG_SERVER_BACKLOG_CONN 128


/*default goodput / link capacity ratio*/
#define TG_GOODPUT_RATIO (1448.0 / (1500 + 14 + 4 + 8 + 12))

/*define PIAS TOS values*/
constexpr uint8_t PIAS_TOS[] = {0x80, 0x40, 0x20, 0x00};


} // namesapce client_sing

#endif
