
#include <cinttypes>
#include <cctype>
#include <limits>
#include <cstdlib>
#include <fstream>
#include <string>
#include <cstring>
#include <future>
#include <vector>
#include <deque>
#include <chrono>
#include <unordered_map>
#include <memory>
#include <thread>
#include <cassert>
#include <exception>



#include <unistd.h>
#include <signal.h>


#include <sys/epoll.h>
#include <sys/time.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <sys/types.h>
#include <sys/socket.h>



#include "clients.h"
#include "../utils/file_ops.h"
#include "../utils/common_struct.h"
#include "../utils/cdf.h"
#include "../net/protocol.h"
#include "../net/values.h"

namespace sing_client{

#define MAX_EVENTS 10000


/*Global variables for accessing them easily*/
uint32_t      load         = 0;
const char*   cdf_file     = NULL;
const char*   config_path  = NULL;
unsigned int  batch_size   = 0;
unsigned      run_time     = ::std::numeric_limits<unsigned>::max(); /*running time in seconds*/
unsigned int  seed         = 0;
unsigned int  max_io_desc  = 0;
unsigned int  client_id    = 0; /*server id for discarding ip of the client*/

int           RUN_AGAIN    = 1; /*for terminating an emulation.*/

std::atomic<unsigned int>  alloc_desc(0); // number of allocated
                                          // file descriptors

std::atomic<bool>           epoll_loop(true);



enum class ReqRes: int
{
  COMPLETED = 0, /*request completed*/
  TRY_AGAIN = 1, /*try one more time*/
  FAILED    = 2, /*request failed*/
  TERMINATE = 3  /*signal to terminate*/

}; // request result value for thread communication


/*A request of a flow.*/
struct batch_request_t
{
  MetaPtr                                   metadata;
  unsigned int                              server_id;
  unsigned int                              req_sleep_us;
  std::unique_ptr<Protocol>                 protocol;
  int                                       sockfd;
  std::unique_ptr<std::promise<ReqRes>>     comm_sign;


  batch_request_t()
  : metadata(nullptr, deallocate_metadata),
    server_id(0),
    req_sleep_us(0),
    protocol(nullptr),
    sockfd(-1),
    comm_sign(nullptr)
  {}


  batch_request_t(struct batch_request_t&& other)
  : metadata(nullptr, deallocate_metadata),
    server_id(other.server_id),
    req_sleep_us(other.req_sleep_us),
    protocol(nullptr),
    sockfd(other.sockfd),
    comm_sign(nullptr)

  {
    metadata.swap(other.metadata);
    protocol.swap(other.protocol);
    other.sockfd       = -1;
    other.server_id    =  0;
    other.req_sleep_us =  0;
    comm_sign.swap(other.comm_sign);
  }


  
};

/*Per Server information for connecting.*/
struct per_server_info_t
{
  unsigned int               server_id;
  std::atomic<unsigned int>  total_conns; // total connections
                                          // with this server
  struct sockaddr_in         server_addr;


  std::mutex                 list_lock;
  // map of available connections for a new flow
  std::unordered_map<int, std::unique_ptr<Protocol> > avail_conns;
  std::atomic<bool>                                   can_more; 


};



static bool convert_to_ulong(const char* num_str, unsigned long* value)
{

  const char* ptr;

  for(ptr = num_str; *ptr; ++ptr)
  {
    if(!std::isdigit(*ptr))
    {
      return false;
    }
  }

  /*it is safe to convert the string into an unsigned int*/
  *value = std::strtoul(num_str, NULL, 10);

  return true;
}

static bool convert_to_uint(const char* num_str, unsigned int* value)
{

  unsigned long tmp_val;
  const bool res_conv = convert_to_ulong(num_str, &tmp_val);

  if(res_conv)
  {
    *value = (unsigned int) tmp_val;
  }

  return res_conv;
}


/* get number of servers (mutex does not support move constructor)*/
template<typename ServerStruct>
static typename::std::vector<ServerStruct>::size_type 
get_server_number(const char * config_file)
{

  std::fstream conf_fd;
  conf_fd.open(config_file, std::fstream::in);
  
  if(!conf_fd.is_open())
  {
    printf("'async_client' cannot open '%s' file for reading.\n", config_file);
    return 0;
  }
  

  /*file is open*/
  /*get number of servers*/

  typename std::vector<ServerStruct>::size_type num_of_servers = 0;
  std::string server_line;
  unsigned int tmp_id;
  decltype(server_line.size()) char_idx, end_idx;
  const auto str_len = std::strlen("server");
  bool conv_flag = true;

  while(std::getline(conf_fd, server_line))
  {
    char_idx = server_line.find("server");
    if(char_idx != std::string::npos)
    {
			/*need to check the id of the server*/
      /*next field has to be id*/
      char_idx += str_len;
      while(char_idx < server_line.size() && std::isspace(server_line[char_idx]))
      {
        ++char_idx;
      }

      /*make sure more data is on the line*/
      if(char_idx >= server_line.size())
      {
        printf("'async_client' server line does not contain id.\n");
        num_of_servers = 0;
        break;
      }
      
      /*read next value*/
      end_idx = char_idx;
      while(end_idx < server_line.size() && std::isdigit(server_line[end_idx]))
      {
        ++end_idx;
      }

      if(char_idx == end_idx)
      {
        printf("'async_client' server line does not caontain id.\n");
        num_of_servers = 0;
        break;
      }

      /*try to convert into an unsigned int*/
      try
        {
          tmp_id = static_cast<unsigned int>(std::stoul(server_line.substr(char_idx, end_idx-char_idx), NULL, 10));
        
        }catch(const std::exception& exp)
        {
          printf("'async_client' reading file: 'client id' value '%s' is invalid.\n", server_line.substr(char_idx, end_idx-char_idx).c_str());

          conv_flag = false;
        }

       if(!conv_flag)
       {
         num_of_servers = 0;
         break;
       }    
 

       /*increment the number of servers only if it does not match id*/
       if(client_id != tmp_id)
       {
         ++num_of_servers;
       }

    }

  }

  conf_fd.close(); /*close the file*/

  return num_of_servers;

}


/* read the configuration file*/
static bool read_config(const char* config_file,
                        std::vector<struct per_server_info_t>& servers,
                        std::vector<unsigned int>& rate_vals,
                        std::vector<unsigned int>& rate_probs,
                        unsigned int* rate_prob_total)
{
  
  std::fstream file_fd;
  
  /*open file for reading*/
  file_fd.open(config_file, std::fstream::in);
  if(!file_fd.is_open())
  {
    printf("'async_client' cannot open '%s' file for reading.\n", config_file);
    return false;
  }

  
  *rate_prob_total        = 0; 
  unsigned int unique_id  = 0;
  unsigned int tmp_client;
  const auto server_len   = std::string("server").size();
  const auto rate_len     = std::string("rate").size();

  std::string line;
  
  decltype(line.size()) pos_val;
  /*keep reading until no more data*/
  while(std::getline(file_fd, line))
  {
    pos_val = line.find("server");
    if(pos_val != std::string::npos)
    {
      /*found server*/
      auto idx = pos_val + server_len;
      while(idx < line.size() && std::isspace(line.at(idx)))
      {
        ++idx;
      }

      if(idx >= line.size()) 
      {
        printf("'async_client' reading file: 'server' missing client id, address and port.\n");
        file_fd.close();
        return false;
      }   

      /*read the client id*/
      auto end_idx = idx;
      bool conv_flag = true;
      while(end_idx < line.size() && std::isdigit(line[end_idx]))
      {
        ++end_idx;
      }

      if(end_idx >= line.size() || idx == end_idx)
      {
        printf("'async_client' server line does not contain id.\n");
        file_fd.close();
        return false;
      }

      /*try to convert into an unsigned int*/
      try
        {
          tmp_client = static_cast<unsigned int>(std::stoul(line.substr(idx, end_idx-idx), NULL, 10));
        
        }catch(const std::exception& exp)
        {
          printf("'async_client' reading file: 'client id' value '%s' is invalid.\n", line.substr(idx, end_idx-idx).c_str());

          conv_flag = false;
        }

       if(!conv_flag)
       {
         file_fd.close();
         return false;
       }      
      
       /*ignore the line if the line points to the same machine*/
       if(tmp_client == client_id)
       {
         continue; // skip this line
       }  
        
       idx = end_idx; // skip white spaces

       while(idx < line.size() && std::isspace(line[idx]))
       {
         ++idx;
       }


      /*read the address string*/
      end_idx = idx;
      while(end_idx < line.size() && !std::isspace(line.at(end_idx)))
      {
        ++end_idx;
      }

      if(end_idx >= line.size() || idx == end_idx)
      {
        printf("'async_client' reading file: 'server' missing address and port.\n");
        file_fd.close();
        return false;
      }

      /*get the string*/
      std::string ip_addr(line.substr(idx, end_idx-idx));
      
      /*get port number*/
      idx = end_idx;
      while(idx < line.size() && std::isspace(line.at(idx)))
      {
        ++idx;
      }
     
      if(idx >= line.size())
      {
        printf("'async_client' reading file: 'server' missing the port.\n");
        file_fd.close();
        return false;

      }
      
      end_idx = idx;
      
      /*read port number*/
      while(end_idx < line.size() && std::isdigit(line.at(end_idx)))
      {
        ++end_idx;
      }
      int port_num = -1;

      try
      {
        port_num = std::stoi(line.substr(idx, end_idx-idx), nullptr, 10);
        if(port_num < 0)
        {
          printf("'asybc_client' reading file: port number is negative.\n");
        }

      }catch(const std::exception& exp)
      {
        printf("'async_client' reading file: cannot convert %s into an int.\n", line.substr(idx, end_idx-idx).c_str());
        
      }
       
      if(port_num < 0)
      {
        file_fd.close();
        return false;
      }
      /*check the port for short limit*/
      const unsigned int check_limit = static_cast<const unsigned int>(std::numeric_limits<uint16_t>::max());
      if((unsigned int)port_num > check_limit)
      {
        printf("'async_client' reading file: port number '%i' is greater than  2^16-1. \n", port_num);
        file_fd.close();
        return false;
      }

      const uint16_t port_short = static_cast<const uint16_t>(port_num);

      /*convert the address string and the port into binary*/
      auto& new_serv = servers[unique_id];
      /*set memory size to zero*/
      std::memset(&(new_serv.server_addr), 0, sizeof(struct sockaddr_in));
      /*initialize the address structure*/
      if(inet_aton(ip_addr.c_str(), &(new_serv.server_addr.sin_addr)) == 0)
      {
        printf("'async_client' reading config: '%s' is not a valid IP address.\n", ip_addr.c_str());
        file_fd.close();
        return false;
      }
      
      new_serv.server_addr.sin_family = AF_INET;
      new_serv.server_addr.sin_port   = htons(port_short);

     
      new_serv.server_id   = unique_id;
      new_serv.total_conns.store(0, std::memory_order_relaxed);
      ++unique_id; /*change the id*/
      new_serv.can_more.store(true, std::memory_order_relaxed);
    
 
      /*done processing one server*/
    }//if

    else
    {
      pos_val = line.find("rate");
      if(pos_val != std::string::npos)
      {
        /*'rate xxMbps xx'*/
        auto idx = pos_val + rate_len;
        
        while(idx < line.size() && std::isspace(line.at(idx)))
        {
          ++idx;
        }

        if(idx >= line.size())
        {
          printf("'async_client' reading config file: 'rate' has no values.\n");
          file_fd.close();
          return false;
        }

        /*read the rate value*/
        auto end_idx = idx;
        while(end_idx < line.size() && isdigit(line.at(end_idx)))
        {
          ++end_idx;
        }

        unsigned int rate_value;
        /*if reading rate related values succeeded*/
        bool read_rate_val = true;

        try
        {
          rate_value = static_cast<unsigned int>(std::stoul(line.substr(idx, end_idx-idx), NULL, 10));
        
        }catch(const std::exception& exp)
        {
          printf("'async_client' reading file: 'rate' value '%s' is invalid.\n", line.substr(idx, end_idx-idx).c_str());

          read_rate_val = false;
        }
        
        if(!read_rate_val)
        {
          file_fd.close();
          return false;
        }

        /*read probability*/
        idx = end_idx;
        
        while(idx < line.size() && !std::isspace(line.at(idx)))
        {
          ++idx;
        }

        /*read all spaces*/
        while(idx < line.size() && std::isspace(line.at(idx)))
        {
          ++idx;
        }

        if(idx >= line.size())
        {
          printf("'async_client' reading file: 'rate' does not have a probability value.\n");
          file_fd.close();
          return false;
        }       

        /*must have an integer for probability*/
        end_idx = idx;
        while(end_idx < line.size() && std::isdigit(line.at(end_idx)))
        {
          ++end_idx;
        }

        unsigned int prob_val;        

        try
        {
          prob_val = static_cast<unsigned int>(std::stoul(line.substr(idx, end_idx-idx)));
        }catch(const std::exception& exp)
        {
          printf("'async_client' cannot convert '%s' into an unsigned int.\n", line.substr(idx, end_idx-idx).c_str());
          read_rate_val = false;
        }
        
        if(!read_rate_val)
        {
          file_fd.close();
          return false;
        }

        /*done with the reading the 'rate' line*/
        rate_vals.push_back(rate_value);
        rate_probs.push_back(prob_val);
        *rate_prob_total += prob_val;     

      }//if
 
      /*if any other line has been found, just ignore it*/
       

    }//else




  }//while

  /*if rates are empty, add zero*/
  if(rate_vals.empty())
  {
   rate_vals.push_back(0);
   rate_probs.push_back(100);
   *rate_prob_total = 100; /*send at full speed of the link*/
  }

   

  return true;
}


static void print_usage()
{

  printf("'async_client' requires the following parameters:\n");
  printf("'--load'  '--cdf'  '--config_file'\n");  
  printf("'--batch'  '--max_fds'  '--client_id'\n");
  printf("'async_client' also accepts the following parameters:\n");
  printf("'--run_time'  '--seed'\n");

}

/*Convert argument strings to the required values.*/
static bool process_args(const struct param_list_t* const params)
{

  const char* tmp_param;
  bool func_res;


  tmp_param = get_param_value(params, "cdf");
  if(!tmp_param)
  {
   printf("'async_client' requires 'cdf'.\n\n");
   return false;
  }


  /*check if the file exists and can be read*/
  func_res = access_granted(tmp_param, READ_OP);
  if(!func_res)
  {
    printf("'async_client' cannot read '%s' file.\n", tmp_param);
    return false;
  }

  cdf_file = tmp_param;

  tmp_param = get_param_value(params, "config_file");
  if(!tmp_param)
  {
    printf("'async_client' requires 'config_file'.\n\n");
    return false;
  }


  /* check if the file exists and can be read */
  func_res = access_granted(tmp_param, READ_OP);
  if(!func_res)
  {
    printf("'async_client' cannot read '%s' file.\n", tmp_param);
    return false;
  }
  
  config_path = tmp_param;
  
  
  tmp_param = get_param_value(params, "load");
  if(!tmp_param)
  {
    printf("'async_client' requires 'load'.\n");
    return false;
  }


  unsigned long tmp_value;
  bool conv_res = convert_to_ulong(tmp_param, &tmp_value);
  if(conv_res)
  {
    load = static_cast<uint32_t>(tmp_value);
  }
  else
  {
    printf("'async_client' cannot convert '%s' into an unsigned int.\n", tmp_param);
    return false;
  }

  /*client id for running client/server on the same physical machine*/
  tmp_param = get_param_value(params, "client_id");

  if(!tmp_param)
  {
    printf("'async_client' requires 'client_id'.\n");
    return false;
  }

  /*try to convert into an integer*/
  func_res = convert_to_uint(tmp_param, &client_id);

  if(!func_res)
  {
    printf("'async_client' cannot convert '%s' into an unsigned int.\n", tmp_param);

    return false;
  }

  
  tmp_param = get_param_value(params, "batch");
  if(!tmp_param)
  {
    printf("'async_client' requires 'batch'.\n");
    return false;
  }


  func_res = convert_to_uint(tmp_param, &batch_size);
  if(!func_res)
  {
    printf("'async_client' cannot convert '%s' into an unsigned int.\n", tmp_param);
    return false;
  }

  if(batch_size == 0)
  {
    printf("'async_client' 'batch_size' must be a positive integer.\n");
    return false;
  }



  tmp_param = get_param_value(params, "max_fds");
  if(!tmp_param)
  {
    printf("'async_client' requires 'max_fds'.\n");
    return false;
  }


  func_res = convert_to_uint(tmp_param, &max_io_desc);
  if(!func_res)
  {
    printf("'async_client' cannot convert '%s' into an unsigned int.\n", tmp_param);
    return false;
  }

  if(max_io_desc == 0)
  {
    printf("'async_client' 'max_fds' must be a positive integer.\n");
    return false;
  }


  /*check the optional paramters*/
  tmp_param = get_param_value(params, "seed");
  if(tmp_param)
  {
    /*use the seed value*/
    func_res = convert_to_uint(tmp_param, &seed);
    if(!func_res)
    {
      printf("'async_client' cannot convert '%s' into an unsigned int.\n", 
          tmp_param);
      return false;
    }
    srand(seed);
  }
  else
  {
    struct timeval tv_start;
    gettimeofday(&tv_start, NULL);
    srand((tv_start.tv_sec*1000000) + tv_start.tv_usec);
  }

  tmp_param = get_param_value(params, "run_time");
  if(tmp_param)
  {
    /*set run time*/
    func_res = convert_to_uint(tmp_param, &run_time);
    if(!func_res)
    {
      printf("'async_client' cannot convert '%s' into an unsigned int.\n",  tmp_param);
      return false;
    }
  }

  /*parameters have been set*/
  return true;

}


static void run_epoll_thread(const int epollfd,
               std::vector<struct per_server_info_t>& servers);


static void signal_handler(const int signum);




static void run_request(const int epollfd,
                        struct batch_request_t*    req,
                        struct per_server_info_t*  server,
                        std::future<ReqRes>&       res_fut);



static bool generate_flow_requests(
                  const int epollfd,
                  std::thread* thread_handle,
                  const CDFTable* const cdf_gen,
                  std::vector<struct per_server_info_t>& servers, 
                  const std::vector<unsigned int>& rate_vals,
                  const std::vector<unsigned int>& rate_probs,
                  const unsigned int total_prob);


static bool create_batch_requests( 
                 const CDFTable* const cdf_gen,
                 const uint32_t net_load,
                 const typename std::vector<struct batch_request_t*>::size_type req_num,
                 const std::vector<unsigned int>& server_ids,
                 const std::vector<unsigned int>& rate_vals,
                 const std::vector<unsigned int>& rate_probs,
                 const unsigned int total_prob,
                 std::vector<struct batch_request_t*>& req_vec);


bool run_async_client(const struct param_list_t* const params)
{
  bool init_res = true;
  
  init_res = process_args(params);
 
  if(!init_res)
  {
    print_usage();
    return false;
  }
 

 
  auto server_num = get_server_number<struct per_server_info_t>(config_path);
  if(server_num == 0)
  {
    printf("'async_client' no servers in '%s' file.\n", config_path);
    return false;
  }

 
  std::vector<struct per_server_info_t> servers(server_num);
  std::vector<unsigned int>             rate_vals;
  std::vector<unsigned int>             rate_probs;
  unsigned int total_prob; 

  init_res = read_config(config_path, servers,
                         rate_vals,   rate_probs,
                         &total_prob);

  if(!init_res)
  {
    print_usage();
    return false;
  }
 
  if(total_prob != 100)
  {
    printf("'async_client' read the configuration file and rate probabilities don't add up to 100.\n");
    return false;
  }

  assert(rate_vals.size() == rate_probs.size());


  /*create a CDF table for generating sizes of flows.*/
  CDFTable* cdf_gen = new CDFTable();
  
  if(!cdf_gen->load_cdf(cdf_file))
  {
    printf("Cannot load CDf file '%s' for reading.\n", cdf_file);
    delete cdf_gen;
    return false;
  }


  /*create an epoll structure*/
  const int epollfd =  epoll_create1(EPOLL_CLOEXEC);
  if(epollfd == -1)
  {
    printf("'async_client' cannot create an epoll kernel structure.\n");
    printf("Error: '%s'\n", std::strerror(errno));
    delete cdf_gen;
    return false;
  }

  /*register signal handler*/
  if(signal(SIGINT, signal_handler) == SIG_ERR)
  {
    printf("'async_client' cannot handle SIGINT.\n");
    close(epollfd);
    delete cdf_gen;
    return false;
  }
 
  if(signal(SIGILL, signal_handler) == SIG_ERR)
  {
    printf("'async_client' cannot handle SIGILL.\n");
    close(epollfd);
    delete cdf_gen;
    return false;
  }

  if(signal(SIGTERM, signal_handler) == SIG_ERR)
  {
    printf("'async_client' cannot handle SIGTERM.\n");
    close(epollfd);
    delete cdf_gen;
    return false;
  }

  
  /*if(signal(SIGSEGV, signal_handler) == SIG_ERR)
  {
    printf("'async_client' cannot handle SIGSEGV.\n");
    close(epollfd);
    delete cdf_gen;
    return false;
  }*/
  
  /*check if the running time has been passed*/
  if(run_time != std::numeric_limits<unsigned>::max())
  {
   /*set alarm signal for terminating the program*/
   if(signal(SIGALRM, signal_handler) == SIG_ERR)
   {
     printf("'async_client' cannot handle SIGALRM.\n");
     close(epollfd);
     delete cdf_gen;
     return false;
   }
   else 
   {
     /*run an alarm*/
     alarm(run_time);
   }
 
  }// run_time is specified

 
  /*run the IO operations on another thread*/
  epoll_loop.store(true, std::memory_order_release);
  std::thread epoll_thread(run_epoll_thread, epollfd,
                           std::ref(servers)); 
  
 

  /*start running the emulation*/
  auto emu_res = generate_flow_requests(epollfd,
                                        &epoll_thread, cdf_gen,    
                                        servers, rate_vals,  
                                        rate_probs, total_prob);
 
  /*no need to close the epollfd since the epoll thread does so*/

  /*delete the flow size generator*/
  delete cdf_gen;

  /*return the result of the emulation*/
  return emu_res; 
}



static void signal_handler(const int signum)
{
  printf("\n'async_client' is being terminated. Signal number: %i.\n", signum);
  RUN_AGAIN = 0; // finish the loop  

}


static bool generate_flow_requests(
                  const int epollfd,
                  std::thread* thread_handle,
                  const CDFTable* const cdf_gen,
                  std::vector<struct per_server_info_t>& servers, 
                  const std::vector<unsigned int>& rate_vals,
                  const std::vector<unsigned int>& rate_probs,
                  const unsigned int total_prob)
{

  if(run_time == 0)
  {
    /*notify the thread*/
    epoll_loop.store(false, std::memory_order_release);
    thread_handle->join(); // wait for thread to finish

    /*completed running*/
    return true;
  }

  /*compute sleep overhead*/
  const unsigned int usleep_overhead_us = get_usleep_overhead(20);
  const decltype(servers.size()) num_reqs = static_cast<const decltype(servers.size())>(batch_size); 

  /*get server ids*/
  std::vector<unsigned int> server_ids(servers.size());

  /*get server IDs*/
  for(decltype(servers.size()) i = 0; i < servers.size(); ++i)
  {
    server_ids[i] = servers[i].server_id;
  }

  /*signal if the emulation has succeeded*/
  bool emul_success = true;

  std::vector<struct batch_request_t*> requests;

  /*failed requests (need to reissue these requests)*/
  std::deque<struct batch_request_t*> pend_reqs;
 
  /*futures for storing signals*/
  std::vector<std::future<ReqRes> >   fut_signs;

  /*when to pause issueing requests*/
  unsigned int sleep_us;


  /*run until a signal is issued to terminate the emulation*/
  while(RUN_AGAIN)
  { 
    if(pend_reqs.size() < num_reqs)
    {
      /*create new requests*/
      if(!create_batch_requests(cdf_gen, load,
                               (num_reqs - pend_reqs.size()),
                               server_ids, rate_vals, rate_probs,
                               total_prob, requests))
      {
        /*failed to create new requests*/
        printf("generate_flow_requests:: failed to create new requests.\n");
        emul_success = false;
        break; 
      }//if
 
    }//if

    /*add extra from previous requests*/
    for(auto extra_idx = requests.size(); extra_idx < num_reqs; ++extra_idx)
    {
      /*it's guaranteed to fill the batch*/
      requests.push_back(pend_reqs.front());
      pend_reqs.pop_front();
    }


    /*reset sleep time*/
    sleep_us = 0;
    /*allocate memory for the futures*/
    fut_signs.resize(num_reqs);

    /*run the batch*/
    for(decltype(servers.size()) i = 0; i < num_reqs; ++i)
    {
      sleep_us += requests[i]->req_sleep_us;
      
      if(sleep_us > usleep_overhead_us)
      {
        usleep(sleep_us - usleep_overhead_us);
        sleep_us = 0;
      }
      //usleep(200000);

      /*enqueue the request*/
      run_request(epollfd, requests[i], 
                  &(servers[requests[i]->server_id]),
                  fut_signs[i]);

    }//for

   
    /*wait for the batch to complete*/
    for(decltype(fut_signs.size()) idx = 0; idx < fut_signs.size(); ++idx)
    {
      try
      {
        auto flow_res = fut_signs[idx].get();
        if(flow_res != ReqRes::COMPLETED)
        { /*request failed to complete*/
          if(flow_res == ReqRes::TERMINATE)
          {
            printf("generate_flow_requests:: flow_res == ReqRes::TERMINATE\n");
            RUN_AGAIN = 0; /*need to terminate the emulation*/
            break;
          }
          pend_reqs.push_back(requests[idx]);
          requests[idx] = nullptr; // don't delete this request
        }
       
      }catch(const std::exception& exp)
      {
        /*failed the request (retry)*/
        pend_reqs.push_back(requests[idx]);
        requests[idx] = nullptr;
      }
    }

    /*delete all old request*/
    for(auto req_ptr : requests)
    {
      if(req_ptr)
      { /*valid request*/
        delete req_ptr;
      }
    }

    /*done with a batch of requests (clear the fields and start again)*/
    requests.clear();  // clear requests
    fut_signs.clear(); // clear futures

  }//while

  printf("generate_flow_requests: the 'while' loop has been broken.\n");

  /*terminate the epool thread*/
  epoll_loop.store(false, std::memory_order_release);
  
  /*wait for the epoll thread to finish*/
  thread_handle->join();

 /*loop has been terminated, clean up the results*/
 for(auto pend_ptr : pend_reqs)
  {
    if(pend_ptr)
    {
     delete pend_ptr;
    }
  }
  

  /*ensure that all futures have been notified*/
  for(auto cur_req : requests)
  {
    if(cur_req->comm_sign)
    {
      try
      {
        cur_req->comm_sign->set_value(ReqRes::COMPLETED);
      } catch(const std::exception& exp)
       {
        /*ignore the exception since the result is already provided*/
       }
    }
    
    /*close the protocol*/
    if(cur_req->protocol)
    {
      cur_req->protocol->close_protocol();
    }

    /*close socket*/
    if(cur_req->sockfd >= 0)
    {
      close(cur_req->sockfd);
    }
    
    /*delete the request*/
    delete cur_req;
   
  }



  /*now it's safe to collect the futures*/
  for(auto& fut_res : fut_signs)
  {
    try
    {
      assert(fut_res.wait_for(std::chrono::seconds(0)) == std::future_status::ready);

      fut_res.get();
      
    }catch (const std:: exception& exp)
    {
     /*ignore exceptions*/
    }
  }

  /*clear all containers*/
  pend_reqs.clear(); 
  fut_signs.clear();
  requests.clear();

  /*close all sockets*/
  for(auto& one_serv : servers)
  {
    /*no need for locking*/
    for(auto& conn_ref : one_serv.avail_conns)
    {
      conn_ref.second->close_protocol();
      close(conn_ref.first); // close the socket
    }
    one_serv.avail_conns.clear();
  }


  return emul_success;
  


}



static void run_epoll_thread(const int epollfd,
                std::vector<per_server_info_t>& servers)
{
  // thread for processing IO operations

  if(epollfd < 0)
  {
    return;
  }

  struct epoll_event events[MAX_EVENTS];
  int nfds, idx;
  struct batch_request_t*    req_ptr      = nullptr;
  struct per_server_info_t*  flow_server  = nullptr;


  while(epoll_loop.load(std::memory_order_relaxed))
  {
    nfds = epoll_wait(epollfd, events, MAX_EVENTS, 100);
    
    if(nfds == -1)
    {
      printf("run_epoll_thread: nfds == -1\n");
      break;
    }

    if(nfds == 0)
    {
     continue;
    }

    // received some events to process
    for(idx = 0; idx < nfds; ++idx)
    {
      /*any request must bring a pointer to a structure*/
      req_ptr = static_cast<struct batch_request_t*>(events[idx].data.ptr);
   
      if(req_ptr == nullptr)
      {
       /*a bug*/
       printf("run_epoll_thread: req_ptr == nullptr.\n");
       epoll_loop.store(false, std::memory_order_release);
       break;
      }
      
      else
      {
       
       /*process the request*/
       auto pro_res = req_ptr->protocol->run_protocol(req_ptr->sockfd);
       //printf("result of run_protocol: %i.\n", static_cast<int>(pro_res));
       switch(pro_res)
       {
         case ConnStatus::SUCCESS:
           {
             /*flow has successfully completed*/
             /*deregister the file descriptor from the epoll*/
             if(epoll_ctl(epollfd, EPOLL_CTL_DEL, req_ptr->sockfd, nullptr) == -1)
             {
               printf("run_epoll_thread::epoll_ctl(EPOLL_CTL_DEL) failed: '%s'\n", std::strerror(errno));
               /*stop processing*/               
               idx = nfds;
               epoll_loop.store(false, std::memory_order_release);
               /*processing flags false*/

               req_ptr->protocol->close_protocol();
               close(req_ptr->sockfd);
               req_ptr->sockfd = -1;

               /*steal the pointer from the other thread*/
               //auto tmp_res = std::move(req_ptr->comm_sign);

               try{
                 req_ptr->comm_sign->set_value(ReqRes::TERMINATE);
               }catch(const std::exception& exp)
                {
                  //printf("run_epoll_thread:set_value exception: '%s'\n", exp.what());
                }
              
               break;
             }

             /*return the socket to the available connections*/
             flow_server = &(servers[req_ptr->server_id]);
             {
              std::lock_guard<std::mutex> tmp_lock(flow_server->list_lock); 
              /*return the protocol back to the available list*/
              auto res_pair = flow_server->avail_conns.emplace(req_ptr->sockfd, std::move(req_ptr->protocol));
              if(res_pair.second == false)
              {
                //printf("run_epoll_thread:: cannot emplace");
                idx = nfds; // terminate the for loop
                epoll_loop.store(false, std::memory_order_release);
                break;
              }

             } // lock scope

             /*steal the pointer from the other thread*/
             //auto tmp_res = std::move(req_ptr->comm_sign);
              
             try{
               req_ptr->comm_sign->set_value(ReqRes::COMPLETED);
             }catch(const std::exception& exp)
              {
                //printf("run_epoll_thread:set_value exception: '%s'\n", exp.what());
              }
             
             break;
         
           }

         case ConnStatus::MOD_READ:
           {
             /*request is still in progress*/
             events[idx].events = (EPOLLIN | EPOLLET);
             if(epoll_ctl(epollfd, EPOLL_CTL_MOD, req_ptr->sockfd,
                 &events[idx]) == -1)
             {
               /*need to break (error)*/
               printf("run_epoll_thread: epoll_ctrl: '%s'.\n", std::strerror(errno));
               idx = nfds; // break for loop
               epoll_loop.store(false, std::memory_order_release);
             }

             break;

           }
         
         case ConnStatus::MOD_WRITE:
           {
             /*request is still in progress*/
             events[idx].events = (EPOLLOUT | EPOLLET);
             if(epoll_ctl(epollfd, EPOLL_CTL_MOD, req_ptr->sockfd,
                 &events[idx]) == -1)
             {
               /*need to break (error)*/
               printf("run_epoll_thread: epoll_ctrl: '%s'.\n", std::strerror(errno));
               idx = nfds; // break for loop
               epoll_loop.store(false, std::memory_order_release);
             }

             break; 

           }

         case ConnStatus::ERR_AGAIN:
           {
             break; /*do nothing*/
           }

         case ConnStatus::ERR_REJECT:
           {
             /*connection has been rejected by the remote*/
             /*server*/
             flow_server = &(servers[req_ptr->server_id]);
             flow_server->can_more.store(false, std::memory_order_relaxed);
             auto rem_conns = flow_server->total_conns.fetch_sub(1, std::memory_order_relaxed);
             close(req_ptr->sockfd); // close the socket
             req_ptr->sockfd = -1;
             req_ptr->metadata.swap(req_ptr->protocol->get_metadata());
             req_ptr->protocol->close_protocol();

             /*acquire the pointer of the shared future*/
             //auto tmp_res = std::move(req_ptr->comm_sign);
           
          
             /*try again if there are more conns*/
             if(rem_conns > 1)
             {
               try{
                 req_ptr->comm_sign->set_value(ReqRes::TRY_AGAIN);
               }catch(const std::exception& exp)
               {
                 //printf("run_epoll_thread:set_value exception: '%s'\n", exp.what());
               }
             }
             else
             {
               try{
                 req_ptr->comm_sign->set_value(ReqRes::COMPLETED);
               }catch(const std::exception& exp)
               {
                 //printf("run_epll_thread:set_value exception: '%s'\n", exp.what());
               }
             }
      
          
             /*got an available file descriptor*/
             alloc_desc.fetch_sub(1, std::memory_order_relaxed);             
             break;
             

           }

         case ConnStatus::ERR_IO:
           {
             /*might occur during read/write*/
             flow_server = &(servers[req_ptr->server_id]);
             flow_server->total_conns.fetch_sub(1, std::memory_order_relaxed);
             close(req_ptr->sockfd); // close the socket
             req_ptr->sockfd = -1;
             req_ptr->metadata.swap(req_ptr->protocol->get_metadata());
             req_ptr->protocol->close_protocol();
        
             /*take ownership of the future*/
             //auto tmp_res = std::move(req_ptr->comm_sign);

             /*try again*/
             try{
               req_ptr->comm_sign->set_value(ReqRes::TRY_AGAIN); 
             }catch(const std::exception& exp)
             {
               //printf("run_epll_thread:set_value exception: '%s'\n", exp.what());
             }            


             // update the number of available file descriptors
             alloc_desc.fetch_sub(1, std::memory_order_relaxed);

             break;

           }

         default:
           {
             //printf("epoll_run_thread: received the default switch.\n");
             flow_server = &(servers[req_ptr->server_id]);      
             /*signal the other thread that this server is done*/
             flow_server->can_more.store(false, std::memory_order_relaxed);
             auto rem_conns = flow_server->total_conns.fetch_sub(1, std::memory_order_relaxed);
             close(req_ptr->sockfd); // close the socket
             req_ptr->sockfd = -1;
             req_ptr->metadata.swap(req_ptr->protocol->get_metadata());
             req_ptr->protocol->close_protocol();     
             /*take ownership of the shared promise*/
             //auto tmp_res = std::move(req_ptr->comm_sign);
             
             /*try again if there are connections*/
             if(rem_conns > 1)
             {
               try{
                 req_ptr->comm_sign->set_value(ReqRes::TRY_AGAIN);
               }catch(const std::exception& exp)
               {
                 //printf("run_epoll_thread:set_value exception: '%s'\n", exp.what());
               }
             } 
             else
             {
               /*don't retry if there are no connections*/
               try{
                 req_ptr->comm_sign->set_value(ReqRes::COMPLETED);
               }catch(const std::exception& exp)
               {
                 //printf("run_epoll_thread::set_value exception: '%s'\n", exp.what());
               }
             }
       

             // update the number of available file descriptors
             alloc_desc.fetch_sub(1, std::memory_order_relaxed);

             break;
           }
           
       } // switch
      }  // else    
    }//for
    
  }//while


  // close the epoll object so that the other thread 
  // would get an error
  close(epollfd);

}


static bool create_batch_requests( 
  const CDFTable* const cdf_gen,
  const uint32_t net_load,
  const typename std::vector<struct batch_request_t*>::size_type req_num,
  const std::vector<unsigned int>& server_ids,
  const std::vector<unsigned int>& rate_vals,
  const std::vector<unsigned int>& rate_probs,
  const unsigned int total_prob,
  std::vector<struct batch_request_t*>& req_vec)
{

 /*average request arrival interval (us)*/
 const double float_load = static_cast<double>(net_load);
  
 if(float_load <= 0.0)
 {
   printf("create_batch_requests: load is non-positive.\n"); 
   return false;
 }
 
 const double period_us = cdf_gen->avg_cdf() * 8.0 / float_load / TG_GOODPUT_RATIO;

 if(period_us <= 0.0)
 {
   printf("create_batch_requests: arrival interval is negative.\n");
   return false;
 }

 bool op_res = true;

 try
 { // preallicate memory for the requests
   req_vec.resize(req_num);
   for(auto& req_ptr : req_vec)
   {
     req_ptr = nullptr; /*initialize to NULL*/
   }
 }catch(std::exception& exp)
  {
    printf("create_bacth_requests: no memory for requests.\n");
    op_res = false;
  }

 if(!op_res) 
 {
   return false;
 }

 const int num_servers = static_cast<int>(server_ids.size());
 if(num_servers <= 0)
 {
   req_vec.clear();
   printf("create_batch_requests:: num_servers <= 0.\n");
   return false;
 }

 try
 {
  for(decltype(server_ids.size()) i = 0; i < req_num; ++i)
  {
    req_vec[i] = new struct batch_request_t();
    MetaPtr meta_ptr(create_metadata(), deallocate_metadata);
    meta_ptr->fid   = 0;
    meta_ptr->fsize = static_cast<uint32_t>(std::max<double>(4.0, cdf_gen->gen_random_cdf()));
    meta_ptr->frate = static_cast<uint32_t>(
                                  gen_value_weight(rate_vals.data(), 
                                       rate_probs.data(),
                                       rate_vals.size(),
                                       total_prob));
    meta_ptr->fload = net_load;
    meta_ptr->fcdf  = cdf_gen->get_cdf_index();


    /*metada has been initialized*/
    req_vec[i]->req_sleep_us = static_cast<unsigned int>(poisson_gen_interval(1.0 / period_us));

    /*pick a random server*/
		req_vec[i]->server_id = server_ids[std::rand() % num_servers];
    
    /*swap the metadata pointers*/
    req_vec[i]->metadata.swap(meta_ptr);
    
    
    
  }

 }catch(std::exception& exp)
 {
   printf("create_batch_requests: failed to create a reuquest (out of memory).\n");
   /*delete the preallocated memory*/
   for(auto req_ptr : req_vec)
   {
     if(req_ptr)
     { /*pointer points to some memory*/
       delete req_ptr;
     }
   }

   /*clear the memory of the requests*/
   req_vec.clear();
   op_res = false;
 }

 return op_res; /*give a signal to the caller*/
}


static void run_request(const int epollfd,
                        struct batch_request_t*    req,
                        struct per_server_info_t*  server,
                        std::future<ReqRes>&       res_fut)
{
  /*check if the request can be accepted by the remote server*/
  bool can_accept = true;
   
  {
    std::lock_guard<std::mutex> tmp_lock(server->list_lock);
    if(server->avail_conns.empty())
    {
      can_accept = false;
    }
    else
    {
      /*get the first available connection*/
      auto conn_itr = server->avail_conns.begin();
      assert(conn_itr != server->avail_conns.end());
       
      /*copy the values*/  
      req->sockfd = conn_itr->first;
      req->protocol.swap(conn_itr->second);

      /*erase the item*/
      server->avail_conns.erase(conn_itr);
    }
  }//lock scope
  


  /*check if there is an active connection*/
  if(!can_accept)
  {
    if(server->can_more.load(std::memory_order_acquire) &&
             alloc_desc.load(std::memory_order_acquire) < max_io_desc)
    {
   
     const int tmp_sock = socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, IPPROTO_TCP);
     if(tmp_sock >= 0)
     {
       /*initialize socket*/
       int param = 1;
       socklen_t param_size = sizeof(int);
       if(setsockopt(tmp_sock, SOL_SOCKET, SO_REUSEADDR, &param, param_size) < 0)
       {
         /*log this*/
         printf("run_request: setsockopt(SOL_SOCKET, SO_REUSEADDR): '%s'.\n", std::strerror(errno));
         close(tmp_sock);

         /*notify to terminate*/
         std::promise<ReqRes> tmp_promise;
         res_fut = tmp_promise.get_future();

         /*pass signal*/
         try{
           tmp_promise.set_value(ReqRes::TERMINATE);
         }catch(const std::exception& exp)
         {
           //printf("run_request:set_value exception: '%s'\n", exp.what());
         }
         return;
       }

       if(setsockopt(tmp_sock, IPPROTO_TCP, TCP_NODELAY, &param, param_size) < 0)
       {
         /*log this*/
         printf("run_request: setsockopt(IPPROTO_TCP, TCP_NODELAY): '%s'.\n", std::strerror(errno));
         close(tmp_sock);

         /*notify to terminate*/
         std::promise<ReqRes> tmp_promise;
         res_fut = tmp_promise.get_future();

         /*pass signal*/
         try{
           tmp_promise.set_value(ReqRes::TERMINATE);
          }catch(const std::exception& exp)
           {
             //printf("run_request:set_value exception: '%s'\n", exp.what());
           }

         return;
       }
      
      /*set TOS fields*/
      param_size = sizeof(PIAS_TOS[0]);       

      if(setsockopt(tmp_sock, IPPROTO_IP, IP_TOS, &PIAS_TOS[0], param_size) < 0)
       {
         /*log this*/
         printf("run_request: setsockopt(IPPROTO_TOS, IP_TOS): '%s'.\n", std::strerror(errno));
         close(tmp_sock);

         /*notify to terminate*/
         std::promise<ReqRes> tmp_promise;
         res_fut = tmp_promise.get_future();

         /*pass signal*/
         try{
           tmp_promise.set_value(ReqRes::TERMINATE);
         }catch(const std::exception& exp)
          {
            //printf("run_request:set_value exception: '%s'\n", exp.what());
          }
         return;
       }

       
       /*try to connect asynchronously*/
       if((connect(tmp_sock, (struct sockaddr*) &(server->server_addr),
             sizeof(server->server_addr)) == -1) && errno != EINPROGRESS)
         {
          /*assumption is that the remote server has rejected*/
          server->can_more.store(false, std::memory_order_relaxed);
        
          /*close the socket*/
         close(tmp_sock);

         /*notify as completed*/
         std::promise<ReqRes> tmp_promise;
         res_fut = tmp_promise.get_future();

         /*pass signal*/
         if(server->total_conns.load(std::memory_order_acquire) > 0)
         {
           try{
             tmp_promise.set_value(ReqRes::TRY_AGAIN);
            }catch(const std::exception& exp)
            {
              //printf("run_request:set_value exception: '%s'\n", exp.what());
            }
         }
         else
         {
           /*don't retry this operation.*/
           try{
             tmp_promise.set_value(ReqRes::COMPLETED);
           }catch(const std::exception& exp)
            {
              //printf("run_request:set_value exception: '%s'\n", exp.what());
            }
         }
         return;

         }

      
       /*all cases have been passed*/
       /*need to create a new protocol and tie it to the socket*/
       req->protocol.reset(new Protocol());
       req->protocol->set_metadata(req->metadata);
       req->sockfd = tmp_sock;
       req->comm_sign.reset(new std::promise<ReqRes>());
       res_fut = req->comm_sign->get_future();
   
 
       /*try to enqueue the request for processing*/
       struct epoll_event event;
       event.events = (EPOLLOUT | EPOLLET);
       event.data.ptr = static_cast<void*>(req);

       /*the epoll thread may fail and close the epoll object*/
       if(epoll_ctl(epollfd, EPOLL_CTL_ADD, tmp_sock, &event) == -1)
       {
         /*failed to enqueue*/
         close(tmp_sock);
         req->protocol->close_protocol();
         req->protocol.reset(nullptr);
         req->sockfd = -1;

         try{
           req->comm_sign->set_value(ReqRes::TERMINATE);
         }catch(const std::exception& exp)
         {
           //printf("run_request:set_value exception: '%s'\n", exp.what());
         }
 
       }
       else
       {
         /*one connection has been added*/
         server->total_conns.fetch_add(1, std::memory_order_relaxed);
         alloc_desc.fetch_add(1, std::memory_order_relaxed);   
       }     

        return; /*stop processing further*/
 

     }// if tmp_sock >=0
     else
     {
       /*need to check the limits*/
       if(errno == EMFILE  || errno == ENFILE || 
          errno == ENOBUFS || errno == ENOMEM)
       {
         /*the limit of file descriptors has been reached*/
         alloc_desc.store(max_io_desc, std::memory_order_relaxed);
       }
     }
       
    }//if can create a new socket

    /*cannot accept this (*try later*)*/
    std::promise<ReqRes> tmp_promise;
    res_fut = tmp_promise.get_future();

    if(server->total_conns.load(std::memory_order_relaxed))
    {
      /*set the future*/
      try{
        tmp_promise.set_value(ReqRes::TRY_AGAIN);
      }catch(const std::exception& exp)
       {
         //printf("run_request:set_value exception: '%s'\n", exp.what());
       }
    }
    else
    {
     /*no connections with the server (don't try connect again)*/
     try{
       tmp_promise.set_value(ReqRes::COMPLETED);
     }catch(const std::exception& exp)
      {
        //printf("run_request:set_value exception: '%s'\n", exp.what());
      }
    }
  
   }//if
   else
   {
    /*run an epoll request*/
    req->comm_sign.reset(new std::promise<ReqRes>());
    res_fut = req->comm_sign->get_future();
    req->protocol->set_metadata(req->metadata);

    struct epoll_event event;
    event.events    = (EPOLLOUT | EPOLLET);
    event.data.ptr  = static_cast<void*>(req);
 
    /*enqueue the request for writing*/
    if(epoll_ctl(epollfd, EPOLL_CTL_ADD, req->sockfd, &event) == -1)
    {
      /*error happened*/
      req->protocol->close_protocol();
      close(req->sockfd);
      req->sockfd = -1;
      
      try{
        req->comm_sign->set_value(ReqRes::TERMINATE);
      }catch(const std::exception& exp)
       {
         //printf("run_request:set_value exception: '%s'\n", exp.what());
       }
      printf("run_request: '%s'.\n", std::strerror(errno));
    }
    /*successfully enqueued a request*/
 
   
   }//else
}

} // namespace sing_client
