#ifndef CLIENT_EMULATION_SING_H
#define CLIENT_EMULATION_SING_H


#include <cstdio>
#include <cstring>


#include "../utils/common_struct.h"

namespace sing_client{

bool run_async_client(const struct param_list_t* params);
/*bool run_async_incast_client(const struct param_list_t* params);*/
void run_emulation(const struct param_list_t* params);

} // namesapce sing_client

#endif
