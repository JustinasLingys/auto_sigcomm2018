#include <stdio.h>
#include <string.h>


#include "clients.h"
#include "../utils/common_struct.h"


namespace sing_client{

void run_emulation(const struct param_list_t* params)
{

  /*look for the client type parameter*/
  const char* client_type = get_param_value(params,
                                            "client_type");

  bool emu_res;

  if(client_type)
  {
    if(std::strcmp(client_type, "client") == 0)
    {
      emu_res = run_async_client(params);
    }
    else
    { 
      if(std::strcmp(client_type, "incast_client") == 0)
      {
        /*emu_res = run_async_incast_client(params);*/
        emu_res = true;
      }
      
      else
      {
        printf("'%s' client type is not available.\n", client_type);
        printf("Available types:\n{'client', 'incast_client'}\n");
        emu_res = false;
      }
    }
  }
  else
  { // default client is 'client'
    emu_res = run_async_client(params);
  }


  if(emu_res)
  {
    printf("Emulation has successfully completed.\n");
  }
  else
  {
    printf("Emulation has failed. Check your hardware settings and parameters.\n");
  }

}


} // namespace sing_client 
