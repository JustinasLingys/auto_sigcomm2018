#ifndef COMMON_SING_FLOW_H
#define COMMON_SING_FLOW_H

/**
 * This file is based on the file at: 
 * github.com/HKUST-SING/TrafficGenerator/src/common/common.h
 */


#include <cstdint>

namespace sing_client{

#define CDF_FILE_NUMBER 2


/**
* This file contains basic data structures for initializing 
* and running a client.
*/


/**
 * Structure for storing a flow info.
 */
struct flow_info_t
{
  uint32_t fid;   /* flow id */
  uint32_t fsize; /* flow size (bytes) */
  uint32_t frate; /* flow sending rate */
  uint32_t fload; /* network load  (Mbps) */
  uint32_t fcdf;  /* cdf index in the global array */

};


/**
 * Stores a parameter pair (name and value)
 */
struct param_node_t
{
  char* name;
  char* value;

};


/**
 * Parameter list
 */

struct param_list_t
{

  struct param_node_t*  parameters; /* parameter key value */
  unsigned int          number;     /* number of valid parameters */
  unsigned int          capacity;   /* list size */

};



/**
 * For keeping information about a flow.
 */
struct cdf_info_t
{
  char      cdf_name[256];
  int32_t   cdf_idx;
};


extern const struct cdf_info_t CDFS[CDF_FILE_NUMBER];


/* remove a new line char ('\n' or '\r') */
void remove_newline(char* str);


/* generate pisson process arrival interval */
double poisson_gen_interval(const double avg_rate);

/* calculate usleep overhead */
unsigned int get_usleep_overhead(const unsigned int iter_num);


/* randomly generate a value based on weights */
unsigned int gen_value_weight(const unsigned int* vals,
                              const unsigned int* weights,
                              const unsigned int  len,
                              const unsigned int  weight_total);




int32_t get_cdf_index(const char* cdf_path);
const char* get_cdf_string(const int32_t idx);
 


/*Create a parameter list*/
struct param_list_t* create_param_list();
/*Delete a parameter list*/
void release_param_list(struct param_list_t* params);

/*Insert a parameter into the parameter list*/
bool insert_param(struct param_list_t* params, 
									const char* par_key, const char* key_val);


/*Get the value of the 'param_key'*/
const char* get_param_value(const struct param_list_t* params,
                            const char* param_key);


/*Print parameter list as {'param_key' : 'param_value'}*/
void print_param_list(const struct param_list_t* params);

/*Print Error Message*/
void print_error(const char* msg);


/*Encode a metadta structure into raw bytes*/
bool encode_metadata(const struct flow_info_t* meta,
                     char* raw_data);

/* decode a metadata strucuter from raw bytes */
bool decode_metadata(const char* raw_bytes,
                     struct flow_info_t* meta);


/*Create a metadata structure*/
struct flow_info_t* create_metadata();

/*Deallocate metadta structure*/
void deallocate_metadata(struct flow_info_t* meta);

/* copy one metada info to another one */
bool copy_metadata(const struct flow_info_t*  souce,
                         struct flow_info_t*  dest);


} // namespace sing_client

#endif /* COMMON_SING_FLOW_H */
