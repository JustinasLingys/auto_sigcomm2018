#include <sys/types.h>
#include <sys/stat.h>

#include "file_ops.h"


namespace sing_client {

bool file_exists(const char* path_val)
{
  struct stat path_stat;
  stat(path_val, &path_stat);
  

  return ((access(path_val, F_OK) != -1) && 
           S_ISREG(path_stat.st_mode));

}


bool access_granted(const char* path_val, const int io_mask)
{


  return (file_exists(path_val) && 
          (access(path_val, F_OK | io_mask) != -1));
  
}

} // namespace sing_client
