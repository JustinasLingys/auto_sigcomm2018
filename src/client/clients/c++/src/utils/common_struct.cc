#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <libgen.h>
#include <unistd.h>
#include <endian.h>


#include <sys/time.h>


#include "common_struct.h"
#include "../net/values.h"




/*For determining a path separator (shall only be Unix)*/
#ifdef _WIN32
#define SYS_PATH_SEP '\\'
#else
#define SYS_PATH_SEP '/'
#endif

#define PARAM_DEFAULT_SIZE 8


namespace sing_client{

const struct cdf_info_t CDFS[CDF_FILE_NUMBER] = {{"DCTCP_CDF", 0}, {"VL2_CDF", 1}};
const static size_t U32_SIZE  = sizeof(uint32_t);



/*Utility function for initializing a parameter*/
bool init_param(struct param_node_t* param,
                const char* param_key, const char* param_value)
{


  param->name  = NULL;
  param->value = NULL; 

  char* tmp_key  = (char*) std::malloc((std::strlen(param_key) + 1)*sizeof(char));

  if(!tmp_key)
  {
   return false;
  }


  char* tmp_value = (char*) std::malloc((std::strlen(param_value) + 1)*sizeof(char));
  
  if(!tmp_value)
  {
    std::free(tmp_key);
    return false;
  }


  /*copy the key and value*/
  std::strcpy(tmp_key,   param_key);
  std::strcpy(tmp_value, param_value);

  param->name  = tmp_key;
  param->value = tmp_value;


  /*initialized a parameter*/
 return true;

}


void release_param(struct param_node_t* param)
{
  if(param)
  {
    if(param->name)
    {
      std::free(param->name);
      param->name = NULL;
    }
    
    if(param->value)
    {
      std::free(param->value);
      param->value = NULL;
    }
  }

}



struct param_list_t* create_param_list()
{
  struct param_list_t* tmp_list = (struct param_list_t*) std::malloc(sizeof(struct param_list_t));

  if(tmp_list)
  {

    /*preallocate some parameters*/
    tmp_list->parameters = (struct param_node_t*) std::malloc(sizeof(struct param_node_t) * PARAM_DEFAULT_SIZE);

    if(tmp_list->parameters == NULL)
    {
      std::free(tmp_list);
      return NULL;
    }
    else
    {
      tmp_list->number     = 0; 
      tmp_list->capacity   = (unsigned int) PARAM_DEFAULT_SIZE;

      /*initialize the parameter values to NULL*/
      unsigned int i;
      for(i  = 0; i < tmp_list->capacity; ++i)
      {
        tmp_list->parameters[i].name  = NULL;
        tmp_list->parameters[i].value = NULL;
        
      }


      return tmp_list;
    }
  }
  else
  {
    return NULL;
  }

}


void release_param_list(struct param_list_t* params)
{
  if(params)
  {
    unsigned int i;

    for(i = 0; i < params->capacity; ++i)
    {
      release_param(&(params->parameters[i]));   
    }

    std::free(params->parameters);
    params->parameters = NULL;
    params->number     = 0;
		params->capacity   = 0;
    std::free(params);

  }

}


bool insert_param(struct param_list_t* params,
                  const char* par_key, const char* key_val)
{
  if(params && par_key && key_val)
  {

   /* look for duplicates*/
   unsigned int i;
   const char* check_key = NULL;
   for(i = 0; i < params->number; ++i)
   {
     check_key = params->parameters[i].name;
     if(std::strcmp(par_key, check_key) == 0)
     {
       printf("'%s' ", par_key);
       print_error("parameter passed twice.\n");
     }
   }

    const unsigned int new_idx = params->number;


    /* check if need to allocate more memory*/
    if(params->number == params->capacity)
    {
      /*double the size*/
      const unsigned int new_capacity = (params->capacity << 1);
      struct param_node_t* tmp_params = (struct param_node_t*) std::malloc(sizeof(struct param_node_t) * new_capacity);

      if(!tmp_params)
      {
        print_error("Error: cannot deallocate parameters.");
      }

      unsigned int i;
      for(i = 0; i < params->number; ++i)
      {
        tmp_params[i].name  = params->parameters[i].name;
        tmp_params[i].value = params->parameters[i].value;
        
        params->parameters[i].name  = NULL;
        params->parameters[i].value = NULL;
      }

      std::free(params->parameters);
      params->parameters = tmp_params;
      params->capacity   = new_capacity;
    }

    /* guaranteed to have en empty slot */
    bool res =  init_param(&(params->parameters[new_idx]), 
                          par_key, key_val);

    if(res)
    {
      params->number += 1;
    }

    return res;

   
  }
  else
  {
   return false;
  }
  
}


const char* get_param_value(const struct param_list_t* params,
                            const char* param_key)
{


  if(params && param_key)
  {
    /* iterate over the parameter list and return the value string */
    unsigned int i;
    struct param_node_t* ptr = params->parameters;

    for(i = 0; i < params->number; ++i, ++ptr)
    {
      if(std::strcmp(param_key, ptr->name) == 0)
      {
        /* found the parameter */
        return ptr->value;
      }

    }  

  }


  return NULL; /* not found */
}


void print_param_list(const struct param_list_t* params)

{
  printf("Parameter list:\n");

  if(params && params->number > 0)
  {
    unsigned int i;
    for(i = 0; i < params->number; ++i)
    {
      printf("{ %s : %s }\n", params->parameters[i].name, params->parameters[i].value);
    }
  }
  else
  {
    printf("{ : }\n");
  }

  printf("\n");
}


void print_error(const char* msg)
{
  perror(msg);
  exit(EXIT_FAILURE);

}



void remove_newline(char* str)
{
  size_t i;
  const size_t str_len = std::strlen(str);

  for(i = 0; i < str_len; ++i)
  {
    if(str[i] == '\r' || str[i] == '\n')
    {
      str[i] = '\0';
    }
  }

}



double poisson_gen_interval(const double avg_rate)
{

  if(avg_rate > 0.0)
  {
    return -logf(1.0 - (double)(rand() % RAND_MAX) / RAND_MAX) / avg_rate;
  }
  else
  {
   return 0.0;
  }

}


unsigned int get_usleep_overhead(const unsigned int iter_num)
{

  if(iter_num <= 0)
  {
    return 0;
  }

  /*calculate system sleep overhead*/
  unsigned int i;
  unsigned int tot_sleep_us = 0;
  struct timeval tv_start, tv_end;

  gettimeofday(&tv_start, NULL);
  
  for(i = 0;  i < iter_num; ++i)
  {
    usleep(0);
  }

  gettimeofday(&tv_end, NULL);
  tot_sleep_us = (tv_end.tv_sec - tv_start.tv_sec)*1000000\
								+ (tv_end.tv_usec  - tv_start.tv_usec);

  return (tot_sleep_us / iter_num);

}


unsigned int gen_value_weight(const unsigned int* vals, 
                              const unsigned int* weights, 
                              const unsigned int  len, 
                              const unsigned int   weight_total)
{

  unsigned int i;
  unsigned int val = (weight_total > 0) ? ((unsigned int) rand()) % weight_total : 0;

   for(i = 0; i < len; ++i)
   {
     if(val < weights[i])
     {
       return vals[i];
     }
     
     else
     {
       val -= weights[i];
     }
   }


   return vals[len-1];
}


int32_t get_cdf_index(const char* cdf_path)
{
  /* subdivide the path  into directory and file*/
  const size_t path_len = std::strlen(cdf_path);
  
  if(path_len == 0 ||  (path_len > 0 && cdf_path[path_len-1] == SYS_PATH_SEP))
  {
    return -1; /* invalid path */
  }
  else
  {
    char* conv_path = (char*) std::malloc(sizeof(char)*(path_len + 1));
    if(conv_path == nullptr)
    {
      printf("get_cdf_index is out of memory.\n");
      return -1;
    }

    std::strcpy(conv_path, cdf_path);
    char* file_val = basename(conv_path);
  
    /*index to return*/
    int32_t cdf_index = -1;

    if(file_val && std::strlen(file_val) > 0) 
    {
      /*found a file*/
      /*remove file extension if the file has one*/
      char * cdf_ext = file_val;
    
      while(*cdf_ext && *cdf_ext != '.')
      {
        ++cdf_ext; // find either a dot or end
      }
      *cdf_ext = '\0'; // terminate the string
      

      int i;

      for(i = 0; i < CDF_FILE_NUMBER; ++i)
      {
        if(std::strcmp(file_val, CDFS[i].cdf_name) == 0)
        {
          cdf_index = CDFS[i].cdf_idx;
          break;
        }
      }
    }

    /*dellocate allocate path memory*/
    std::free(conv_path);

    return cdf_index;
  }

  return -1;
}



const char* get_cdf_string(const int32_t idx)
{

  int i;
  for(i = 0; i < CDF_FILE_NUMBER; ++i)
  {
    if (CDFS[i].cdf_idx == idx)
    {
      return CDFS[i].cdf_name;
    }
  }

  return NULL;
}


bool encode_metadata(const struct flow_info_t* meta,
                     char* raw_bytes)
{
  if(meta && raw_bytes)
  {

    /*write the integer values into the buffer*/
    const uint32_t fid   = htole32(meta->fid);
    const uint32_t fsize = htole32(meta->fsize);
    const uint32_t frate = htole32(meta->frate);
    const uint32_t fload = htole32(meta->fload);
    const uint32_t fcdf  = htole32(meta->fcdf);

  
    char* ptr   =  raw_bytes;

    /*encode the values into the char array*/
    std::memcpy(ptr, &fid,   U32_SIZE);
    ptr += U32_SIZE;
    std::memcpy(ptr, &fsize, U32_SIZE);
    ptr += U32_SIZE;
    std::memcpy(ptr, &frate, U32_SIZE);
    ptr += U32_SIZE;
    std::memcpy(ptr, &fload, U32_SIZE);
    ptr += U32_SIZE;
    std::memcpy(ptr, &fcdf,  U32_SIZE);

    /*return success */
    return true;
     
  }

  return false;
}



bool decode_metadata(const char* raw_bytes,
                     struct flow_info_t* meta)
{

  if(raw_bytes && meta)
  {
  
    /*read the raw bytes and decode the data*/
    uint32_t tmp_id, tmp_size, tmp_rate, tmp_load, tmp_cdf;
    
    const char* ptr = raw_bytes;

    /*decode the raw bytes into integer values*/
    std::memcpy(&tmp_id,   ptr, U32_SIZE);
    ptr += U32_SIZE; 
    std::memcpy(&tmp_size, ptr, U32_SIZE);
    ptr += U32_SIZE;
    std::memcpy(&tmp_rate, ptr, U32_SIZE);
    ptr += U32_SIZE;
    std::memcpy(&tmp_load, ptr, U32_SIZE);
    ptr += U32_SIZE;
    std::memcpy(&tmp_cdf,  ptr, U32_SIZE);

    /*convert the read values into host order*/
    meta->fid   = le32toh(tmp_id);
    meta->fsize = le32toh(tmp_size);
    meta->fload = le32toh(tmp_load);
    meta->frate = le32toh(tmp_rate);
    meta->fcdf  = le32toh(tmp_cdf);


    return true;
    
  }
  else
  {
    return false;
  }
}



struct flow_info_t* create_metadata()
{

  return ((struct flow_info_t*) std::malloc(sizeof(struct flow_info_t)));

}


void deallocate_metadata(struct flow_info_t* meta)
{
  if(meta)
  {
    std::free(meta);
  }
}


bool copy_metadata(const struct flow_info_t* source,
                         struct flow_info_t* dest)
{

  /*it never fails since no memory allocation*/
  dest->fid    =  source->fid;
  dest->fsize  =  source->fsize;
  dest->frate  =  source->frate;
  dest->fload  =  source->fload;
  dest->fcdf   =  source->fcdf;


  return true;

}


/*This function takes a string of raw bytes and tries to decode the info*/
bool server_accept(const char* raw_data)
{
  if(raw_data)
  {
    /*try to convert into an uint32_t*/
    uint32_t rec_res;

    std::memcpy(&rec_res, raw_data, TG_SERVER_FLAG_SIZE);
    // convert into host memory order
    const uint32_t final_res = le32toh(rec_res);

    return (final_res == 0x00000001);

  }
  else
  {
   return false;
  }

}
} // namespace sing_client
