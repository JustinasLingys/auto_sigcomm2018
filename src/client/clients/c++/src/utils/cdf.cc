
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <fstream>
#include <string>
#include <cstdint>
#include <limits>

#include "common_struct.h"
#include "file_ops.h"
#include "cdf.h"


/**
 * Utility function for computing random cdf.
 */
static double interpolate(const double x,  const double x1,
                          const double y1, const double x2,
                          const double y2)
{

  if(x1 == x2)
  {
    return ((y1 + y2) / 2.0);
  }

  else
  {
    return (y1 + (x - x1)*(y2 - y1) / (x2 -x1));
  }
}


/*generate a random floating point number from min to max*/
static double rand_range(const double min_val, const double max_val)
{

  return (min_val + std::rand()*(max_val - min_val) / RAND_MAX);

}


namespace sing_client{


CDFTable::CDFTable() 
: cdf_idx_(std::numeric_limits<uint32_t>::max())
{
}




CDFTable::~CDFTable()
{
  reset_table();
}


void 
CDFTable::reset_table()
{
  min_cdf_ = 0.0;
  max_cdf_ = 1.0;

  entries_.clear();
}


bool
CDFTable::load_cdf(const char* filename)
{

  if(!filename)
  {
    return false;
  }

  int32_t index_val = -1;


  /*check if the file exists and it's a regular file*/
  if((!sing_client::access_granted(filename, R_OK)) || 
     ((index_val = sing_client::get_cdf_index(filename)) < 0))
  {
    return false;
  }

  
  cdf_idx_ = static_cast<uint32_t>(index_val);


  std::fstream file_fd;

  /*open file for reading*/
  file_fd.open(filename, std::fstream::in);

  if(!file_fd.is_open())
  {
    printf("CDFTable::load_cdf: cannot open '%s' file for reading.\n", filename);
    return false;
  }

  // clear previous values
  reset_table();

  /*read line by line and decode them*/
  std::string cdf_line;
  decltype(cdf_line.size()) pos_val;
  struct CDFTable::cdf_entry tmp_var; 

  while(std::getline(file_fd, cdf_line))
  {
    if(cdf_line.size() == 0)
    {
      continue; // ignore it
    }
    pos_val = 0;
    /*ignore all front spaces*/
    while(pos_val < cdf_line.size() && std::isspace(cdf_line.at(pos_val)))
    {
      ++pos_val;
    }

    if(pos_val >= cdf_line.size())
    {
      continue; /*no data in this line*/
    }

    /*read value and then probability*/
    
    std::sscanf(cdf_line.substr(pos_val).c_str(), "%lf %lf", &(tmp_var.value), &(tmp_var.cdf_prob));

    if(tmp_var.cdf_prob < min_cdf_)
    {
      min_cdf_ = tmp_var.cdf_prob;
    }
    else
    {
      if(tmp_var.cdf_prob > max_cdf_)
      {
        max_cdf_ = tmp_var.cdf_prob;
      }
    }

    /*push the value back*/
    entries_.push_back(tmp_var);
    
  }//while

  file_fd.close(); // close the file

  return (!entries_.empty());
}


void
CDFTable::print_table() const noexcept
{

  if(entries_.empty())
  {
    return;
  }

  for(const auto& val : entries_)
  {
    printf("%.2f %.2f\n", val.value, val.cdf_prob);
  }

}

double
CDFTable::avg_cdf() const noexcept
{
  if(entries_.empty())
  {
    return 0.0;
  }

  double avg = 0.0;
  double tmp_val, tmp_prob;
 
  decltype(entries_.size()) i;

  for(i = 0; i < entries_.size(); ++i)
  {
    if(i ==  0)
    {
      tmp_val   = entries_[i].value / 2.0;
      tmp_prob  = entries_[i].cdf_prob;
    }
    else
    {
      tmp_val  = (entries_[i].value + entries_[i-1].value) / 2.0;
      tmp_prob = (entries_[i].cdf_prob - entries_[i-1].cdf_prob);
    }

    avg += (tmp_val * tmp_prob);
   

  }//for

  return avg;

}


double
CDFTable::gen_random_cdf() const noexcept
{
  if(entries_.empty())
  {
    return 0.0;
  }

  const double x = rand_range(min_cdf_, max_cdf_);
  decltype(entries_.size()) i; 

  for(i = 0; i < entries_.size(); ++i)
  {
    if(x <= entries_[i].cdf_prob)
    {
      if(i == 0)
      {
        return interpolate(x, 0.0, 0.0, entries_[i].cdf_prob, entries_[i].value);
      }
      else
      {
        return interpolate(x, entries_[i-1].cdf_prob, entries_[i-1].value,
                                entries_[i].cdf_prob,   entries_[i].value);
      }
    }//if
  }//for

  return entries_.back().value;
}


} // namesapce sing_client
