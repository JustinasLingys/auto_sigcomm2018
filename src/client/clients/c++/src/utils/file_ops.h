#ifndef FILE_OPS_SING_H
#define FILE_OPS_SING_H


namespace sing_client{

#include <unistd.h>

#define WRITE_OP W_OK 
#define READ_OP  R_OK
#define EXEC_OP  X_OK

bool file_exists(const char* path_val);
bool access_granted(const char* path_val, const int io_mask);

} // namespace sing_client

#endif
