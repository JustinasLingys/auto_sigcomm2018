#ifndef CDF_H
#define CDF_H

/**
 * This file is based on the file at:
 *  github.com/HKUST-SING/TrafficGenerator/src/common/cdf.h
 *
 */

#include <vector>


namespace sing_client{

class CDFTable
{
  public:
    CDFTable();
    ~CDFTable();


   /**
    * Read a file and initialize the internal structures 
    * of the table. On success true is returned; otherwise false.
    */
    bool load_cdf(const char* filename);
 
    /**
     * Print the table.
     */
    void print_table() const noexcept;


    /*Get average value of CDF distribution*/
    double avg_cdf() const noexcept;


    /*Genearate a random value based on CDF distribution*/
    double gen_random_cdf() const noexcept;

    
    /*Get cdf file index*/
    uint32_t get_cdf_index() const noexcept
    {
      return cdf_idx_;
    }



  private:
    /*Utility method for initialing a table*/
    void reset_table();

    /*Private structure for storing values*/
    struct cdf_entry
    {
      double value;
      double cdf_prob;

      cdf_entry(const double init_val  = 0.0, 
                const double init_prob = 0.0)
      : value(init_val),
        cdf_prob(init_prob)
      {}

      cdf_entry(const struct cdf_entry& other)
      : value(other.value),
        cdf_prob(other.cdf_prob)
      {}

      cdf_entry(struct cdf_entry&& other)
      : value(other.value),
        cdf_prob(other.cdf_prob)
      {}

    }; //struct cdf_entry


    /*class attributes*/
    std::vector<struct cdf_entry> entries_;
    double                        min_cdf_; /*maximum value of cdf*/
    double                        max_cdf_; /*minimum value of cdf*/
    uint32_t                      cdf_idx_; /*file index*/


}; // class CDFTable



} // namesapce sing_client



#endif /*CDF_H*/
