# AuTO Client Code



`C++` client was used during evaluation of AuTO. If anyone wants
to implement their own client, please read `src/client/clients/c++/src/net/protocol.{h,cc}` as the files describe the protocol used to generate 
flows. 

To compile the code, go to the root directory of the `C++` client and
run `make -j$(nproc)`. You are supposed to find 'bin' directory with 'client_exec' executable.
