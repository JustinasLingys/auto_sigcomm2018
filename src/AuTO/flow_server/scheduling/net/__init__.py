# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import


from builtins import int



# Python std lib
import mmap
import sys


# Import all classes from the local modules

from scheduling.net.common import (get_cdf_string,  get_cdf_index,
                                   FlowInfo,        TG_METADATA_SIZE,
                                   encode_metadata, decode_metadata,
                                   prio_send,       
                                   convert_to_network_endianness,
                                   convert_to_native_endianness,
                                   read_data,       write_data,
                                   ipv4_str_to_native,
                                   ipv4_str_to_network)


from scheduling.net.exceptions import (CommonSocketError,
                                       IO_OVERUSE_ERROR)

from scheduling.net.constants  import (set_write_buffer,
                                       TG_MAX_WRITE_BUF_SIZE,
                                       TG_WRITE_BUFFER,
                                       TG_SERVER_BACKLOG_CONN,
                                       TG_OK_CONNECTION,
                                       TG_NO_CONNECTION,
                                       TG_ESTABLISH_LEN,
                                       TG_TERM_SIZE,
                                       SYSTEM_PAGE_SIZE)
