# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import


from builtins import int



# Python std lib
import mmap

# Size of write buffer
TG_MAX_WRITE_BUF_SIZE = 1024*1024*10
TG_WRITE_BUFFER       = memoryview(TG_MAX_WRITE_BUF_SIZE*b"0x0f")

# Server max number of pending connections
TG_SERVER_BACKLOG_CONN = 128


# Signal for notifying a client about the status of 
# a connection request
import struct
TG_OK_CONNECTION = struct.pack("<I", 1)   # successful connection
TG_NO_CONNECTION = struct.pack("<I", 0)   # cannot accept more connections
TG_ESTABLISH_LEN = len(TG_NO_CONNECTION)  # that many bytes

# Flow completion signal size
TG_TERM_SIZE = len(struct.pack("<I", 0)) # uint32_t in little endian


# System virtual memory page size
SYSTEM_PAGE_SIZE = int(mmap.PAGESIZE)


def set_write_buffer(buf_size):
  new_buf_size = max(MAX_WRITE_BUF_SIZE, buf_size)

  MAX_WRITE_BUF_SIZE = int(new_buf_size)
  WRITE_BUFFER       = memoryview(new_buf_size*b"0x0f")
