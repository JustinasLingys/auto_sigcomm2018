# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import


from builtins import int



# Python std lib
import sys


# Socket error number [too many file descriptors]
IO_OVERUSE_ERROR = 24

if sys.version_info[0] == 2 and sys.version_info[1] == 7:
  import socket
  CommonSocketError = socket.error

elif sys.version_info[0] == 3:
  CommonSocketError = ConnectionError
else:
  raise OSError("Python {0}.{1} is not supported.".format(sys.version_info[0], sys.version_info[1]))
