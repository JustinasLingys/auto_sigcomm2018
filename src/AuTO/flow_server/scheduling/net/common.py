# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import


from builtins import int


# Python std lib
import struct
import socket
import sys
import os
import os.path
import errno
sys.path.insert(1, os.path.abspath(os.path.join(__file__, os.pardir)))


# import timing module for retrieving precise time
from  scheduling.utils import timing

# Import data buffers for sending 
# data to clients
from scheduling.net.constants   import TG_MAX_WRITE_BUF_SIZE as MAX_WRITE_BUF_SIZE
from scheduling.net.constants  import TG_WRITE_BUFFER       as WRITE_BUFFER
from scheduling.net.exceptions import CommonSocketError
from scheduling.net.constants  import SYSTEM_PAGE_SIZE      as _PAGE_SIZE



class ClosedSocketError(CommonSocketError):
  """
    This exception is raised when the other end of the
    socket is detected to be closed.
  """
  def __init__(self, message):
    super(ClosedSocketError, self).__init__(errno.ECOMM,
                                            message) 
    

__CDF_STRINGS__ = {
  0 : "DCTCP",
  1 : "VL2"
                  }


__CDF_INDICES__ = {
  "DCTCP" : 0,
  "VL2"   : 1
                  } 


# Function converts an integer index of 
# CDF value into a string
def get_cdf_string(cdf_idx):

  cdf_str = __CDF_STRINGS__.get(cdf_idx, None)

  if cdf_str:
    return str(cdf_str)
  else:
    return "Error"


# Convert a CDF string value into an integer
# index which can later be used to retrieve 
# the string value.
def get_cdf_index(cdf_str):
  cdf_idx = __CDF_INDICES__.get(cdf_str, None)

  if cdf_idx is None:
    return -1
  else:
    cdf_idx 



TG_METADATA_SIZE = 20 # below structure size

class FlowMetadata(object):
  """
    Flow metadata class. Instances of this class store
    information about a flow received from a client.
  """
  def __init__(self, fid, fsize, frate, fload, fcdf):
    self.flow_id   = int(fid)
    self.flow_size = int(fsize)
    self.flow_rate = int(frate)
    self.net_load  = int(fload)
    self.flow_cdf  = int(fcdf)


  """
    Return flow size in bytes.
  """
  def get_flow_size(self):
    return self.flow_size


  """
    Return network load value
  """
  def get_net_load(self):
    return self.net_load


  """
    Return CDF which was used to generate flow size.
  """
  def get_flow_cdf_idx(self):
    return self.flow_cdf




# Function for converting an array of bytes into a 
# a metadata object.
def decode_metadata(raw_bytes):
  
  if len(raw_bytes) != TG_METADATA_SIZE:
    return None # wring size

  else:
    meta_vars = struct.unpack("<IIIII", raw_bytes)

    return FlowMetadata(meta_vars[0], meta_vars[1],
                        meta_vars[2], meta_vars[3],
                        meta_vars[4])


# Function for converting an object the FlowMetadata
# class into a byte array.
def encode_metadata(meta):
  if meta and isinstance(meta, FlowMetadata):
    meta_bytes = struct.pack("<IIIII",       meta.flow_id,
                             meta.flow_size, meta.flow_rate,
                             meta.net_load,  meta.flow_cdf)

    if meta_bytes is None or len(meta_bytes) != TG_METADATA_SIZE:
      return None

    else:
      return meta_send

  else:
    return None





class FlowInfo(object):
  """
    Class represents a flow. The context of a flow conatins
    information about how it should be sent/scheduled at this
    server.
  """

  def __init__(self, prio_handle, flow_meta):
    self.frate     =  int(flow_meta.flow_rate) # for sending flows
    self.sleep_us  =  0.0
    self.prev_time =  -1.0        # current time in micros
    self.cur_tos   =  -1          # new flow info

    # extensive info about a flow
    self.flow_info = flow_meta

    
    self.send_sch  = prio_handle  # instance of a scheduler 
                                  # scheduler provides
                                  # values for scheduling flows

  """
    Returns current flow priority. Implementation provides
    the semantics of flow priority.
  """
  def get_flow_priority(self):
    return self.send_sch.get_priority()


  """
    This method returns a tuple: (data_bytes, tos_value)
    The tuple is an instance of core.scheduler.PrioTuple class.
    If the flow has been completed, it should return a None.
  """
  def get_data_tuple(self):
    return self.send_sch.get_tuple()

  """
    This method signals the implementation that a number
    of bytes has been successfully sent at the given priority.
  """
  def update_state(self, sent_bytes, tos_value):
    self.send_sch.update_scheduler(sent_bytes, tos_value)


  """
    Set the current IP TOS value
  """
  def set_ipv4_tos_field(self, new_tos):
    self.cur_tos = int(new_tos)


  """
    Get the current IP tos value
  """
  def get_ipv4_tos_field(self):
    return self.cur_tos


  """
    Get network load value carried by this flow.
  """
  def get_flow_network_load(self):
    return self.flow_info.net_load


  """
    Get information about a workload.
  """
  def get_flow_cdf_index(self):
    return self.flow_info.flow_cdf


  """
    Get flow size in bytes.
  """
  def get_flow_size(self):
    return self.flow_info.flow_size


  """
    Get reference to flow scheduler
  """
  def get_scheduler(self):
    return self.send_sch



# Function sends data with a given IPv4 TOS value
# This function enforces scheduling of a flow placing it 
# in one of K priority queues. The function does not create
# the priority queues, it only exploits them; the priority 
# queues must have been set in advance.

# params:
#     sockfd : socket object
#     flow   : instance of the FlowInfo interface.
#
# return     : True if the flow has completed, False if not.
# raise      : excpetion on an error
def prio_send(sockfd, flow):

  # if previous time is negative, means it is first
  # time the flow is scheduled for sending
  
  curr_time = timing.time_micros()

  diff_us = 0.0 if (flow.prev_time < 0.0) else max(0.0, curr_time - flow.prev_time)

  
  # update the value that the flow has to be suspended for 
  flow.sleep_us -= diff_us

  if flow.sleep_us > 0.0:
    flow.prev_time = float(curr_time)
    return False # need pace the flow


  # send as much data as possible
  while 1:
    flow_tuple = flow.get_data_tuple()
    if not flow_tuple:
      return True # done with this flow

    if flow.cur_tos != flow_tuple.ip_tos:
      # set appropriate IPv4 tos value
      sockfd.setsockopt(socket.IPPROTO_IP, socket.IP_TOS,
                        int(flow_tuple.ip_tos))

      flow.cur_tos = int(flow_tuple.ip_tos)


    sent_bytes = sockfd.send(WRITE_BUFFER[0:min(MAX_WRITE_BUF_SIZE, flow_tuple.data_bytes):1], 0)

    # check if anything has been sent
    if sent_bytes == 0:
      # connection has been closed
      raise ClosedSocketError("Connection closed.")

    else:
      flow.update_state(sent_bytes, int(flow.cur_tos))
      
      # update the previous time
      tmp_prev = curr_time
      curr_time = timing.time_micros()

      # recompute the sleep time
      if flow.frate > 0.0:
        diff_us = (8.0*sent_bytes) / flow.frate - (curr_time - tmp_prev)
      else:
        diff_us = 0.0

      flow.sleep_us += diff_us

      if flow.sleep_us > 0.0 or sent_bytes != flow_tuple.data_bytes:
        break # wait for another chance to write

      
    
  # update the time of previous write
  flow.prev_time = float(curr_time)

  # always check if the flow has been completed
  return (flow.get_data_tuple() == None)



# Network utility function that converts
# an integer from native endiannes 
# to network endiannes
#
# params:
#   int_val : integer to convert
#   int_str : type of integer (e.g., "I", "H", etc.)
def convert_to_network_endianness(int_val, int_str):
  return struct.unpack("".join(["=", int_str]), struct.pack("".join(["!", int_str]), int_val))[0]



# Network utility function that
# converts an integer from network endiannes
# to native endiannes
#
# params:
#   int_val : integer to convert
#   int_str : type of integer (e.g., "I", "H", etc.)
def convert_to_native_endianness(int_val, int_str):
  return struct.unpack("".join(["!", int_str]), struct.pack("".join(["=", int_str]), int_val))[0]


# Network utility function for converting 
# an Ipv4 address string into an int value
# (native endianness)
def ipv4_str_to_native(ipv4_str):
  return struct.unpack("!I", socket.inet_aton(ipv4_str))[0]


# Network utility function for converting 
# an IPv4 address string into an int value
# (network endianness)
def ipv4_str_to_network(ipv4_str):
  return struct.unpack("=I", socket.inet_aton(ipv4_str))[0]


# Network utility function for
# an IPv4 address string to an int value
# (little endianness)
def ipv4_str_to_little(ipv4_str):
  return struct.unpack("<I", socket.inet_aton(ipv4_str))[0]


# Read data from a connection
# If there is an error, throw a CommonSocketError exception.
#
# params:
#   conn      : socket for reading
#   byte_size : number of bytes to read
#
# return:
#   binary array of 'byte_size' size
# 
# except:
#   CommonSocketError on failure
def read_data(conn, byte_size):
  left_reading = int(byte_size)
  raw_data = []

  while left_reading > 0:
    # read in terms of page sizes
    rd_buff   = min(left_reading, _PAGE_SIZE) 
    read_data = conn.recv(rd_buff, 0)
    
    if read_data: # read some data
      raw_data.append(read_data)
      left_reading -= len(read_data)
    else:      
      # socket has been closed by the other end
      raise CommonSocketError("Other end has closed the socket.")

  # means successfully read all the data 
  return b"".join(raw_data)


# Function for sending a binary string 
# to the other end of the socket
# 
# params:
#   conn     : socket descriptor
#   raw_data : raw binary data
#
#  raise:
#    CommonSocketError on failure
def write_data(conn, raw_data):
  if not raw_data:
    return
  else:
    tot_sent = 0
    total_bytes = len(raw_data)

    while tot_sent < total_bytes:
      end_idx    = tot_sent + min(total_bytes - tot_sent, _PAGE_SIZE)
      sent_bytes = conn.send(raw_data[tot_sent:end_idx:1], 0)
     
      if sent_byte:
        tot_sent += sent_bytes
      else:
        # other end has closed its end
        raise CommonSocketError("Other end has closed the socket.")
