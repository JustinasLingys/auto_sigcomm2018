# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import


import os.path
import os

import sys

if sys.version_info[0] == 3 and sys.version_info[1] >= 3:
  import subprocess as py_process
else:
  try:
    # subprocess32 is a dependency of the project
    import subprocess32 as py_process
  except ImportError:
    import subprocess   as py_process


PY_COMMAND = "python3" if sys.version_info[0] == 3 else "python2"


# shared constants amongst all handlers for performing
# request processing.
MAX_IO_QUEUE  = 100    # when to flush the FCT statistics

POLL_INTERVAL = 0.02   # how often the selector polls file
                       # descriptors (seconds)

MAX_IO_QUEUE_LEN = 100 # how large the shared IO queue is



# This map is the same as one in configs/__init__.py only for running the core
__SCHEDULERS__ = {
  "QLAS" : os.path.join("scheduling", "core", "qlas", "qlas_core.py"),
  "QSJF" : os.path.join("scheduling", "core", "qsjf", "qsjf_core.py"),
  "AuTO" : os.path.join("scheduling", "core", "auto", "auto_core.py")
                 }


# log the child process pid for future termination
_CHILD_PROCESS_PATH = os.path.abspath(os.path.join(__file__, 
                                      os.pardir, os.pardir,
                                      os.pardir,
                                      "_scheduling_process.txt"))




# Start a daemon process and run of the scheduling mechanisms.
# The function might raise an exception.
def run_scheduling(log_file, params):
  log_handle  = open(log_file, "w")

  proc_file   = __SCHEDULERS__[params["type"]]

  # start a new daemon process and pass the parameters
  daemon_proc = py_process.Popen(
                  args=[PY_COMMAND, str(proc_file), str(params)],
                  bufsize=-1, stdin=None, stdout=log_handle,
                  stderr=log_handle, shell=False,
                  close_fds=True, universal_newlines=True)

  # a child daemon process has been started
  # save the id for future.
  with open(_CHILD_PROCESS_PATH, "w") as child_fd:
    child_fd.write("Scheduling process pid: {0}\n".format(daemon_proc.pid))
