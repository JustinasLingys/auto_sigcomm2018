# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import

from builtins import int

# Python std lib
import struct
import socket
import collections
from ctypes import Structure
from ctypes import c_uint32
from ctypes import c_uint16
from ctypes import c_uint8 

# make code compatible between Python2/3
try:
  xrange
except NameError:
  xrange = range

# custom modules
from scheduling       import net
from scheduling.utils import pyroute

ACTIVE     = 1     # flow is active
NON_ACTIVE = 2**16 # flow is not flowing 
TERM_ARRAY = 3     # end of a flow array 

class ActiveFlow(Structure):
  """
    Class for representing a currently running flow.
    In other words, an instance of this class is a
    flow that is currently sending some data to a
    remote server. Used for representing the state
    of the local host/server.
  """

  _fields_ = [("src_ip",      c_uint32), 
              ("src_port",    c_uint16),
              ("dst_ip",      c_uint32), 
              ("dst_port",    c_uint16),
              ("protocol",    c_uint8),
              ("priority",    c_uint16),
              ("valid_field", c_uint32)]


  """
    Method for initializing the structure
  """
  def initialize(self):
    self.src_ip      = int(0)
    self.src_port    = int(0)
    self.dst_ip      = int(0)
    self.dst_port    = int(0)
    self.priority    = int(0)
    self.protocol    = int(socket.IPPROTO_TCP)
    self.valid_field = int(TERM_ARRAY)


  """
    Method returns a tuple of the waiting flow
    features that might be used for learning --
    observed state.
  """
  def get_attributes(self):
    return (int(self.src_ip),  
            int(self.src_port),   
            int(self.dst_ip),  
            int(self.dst_port),
            int(self.protocol),
            int(self.priority))

  """
    Method is used for setting attributes/features
    of such a flow.
  """
  def set_attributes(self, attrs):

    if not attrs:
      return
     

    if  "src_ip" in attrs:
      self.src_ip    =  int(attrs["src_ip"])


    if "src_port" in attrs:
      self.src_port  =  int(attrs["src_port"])


    if "dst_ip" in attrs:
      self.dst_ip    =  int(attrs["dst_ip"])


    if "dst_port" in attrs:
      self.dst_port  =  int(attrs["dst_port"])


    if "priority" in attrs:
      self.priority  =  int(attrs["priority"])

    if "protocol" in attrs:
      self.protocol = int(attrs["protocol"])


  """
    Method used for determining if this intance of
    a flow is a running/active flow.
  """
  def is_valid(self):
    return (self.valid_field == ACTIVE)


  def set_valid(self):
    self.valid_field = ACTIVE


  def set_invalid(self):
    self.valid_field = NON_ACTIVE


  """
    Method for checking the status of a shared array.
  """
  def get_field_info(self):
    return self.valid_field



  """
    Invalidate the field
  """
  def invalidate(self):
    self.valid_field = NON_ACTIVE


  """
    Utitilty function for converting data from 
    set_attributes data type to get_attributes type
  """
  @classmethod
  def convert_attr_type(cls, ac_flow):
    assert isinstance(ac_flow, dict), "ActiveFlow::convert_attr_type: received non-dict type: {0}".format(type(ac_flow))
    return (int(ac_flow["src_ip"]),   int(ac_flow["src_port"]),
            int(ac_flow["dst_ip"]),   int(ac_flow["dst_port"]),
            int(ac_flow["protocol"]), int(ac_flow["priority"]))


class CompletedFlow(object):
  """
    Class has a similar function as
    the above class only that this
    class represents completed flows.
  """

  def __init__(self, src_ip, src_port, dst_ip, dst_port,
               priority, flow_size, fct, prot_val=socket.IPPROTO_TCP):

    self.src_ip     =   int(src_ip)    # source ip
    self.src_port   =   int(src_port)  # source port

    self.dst_ip     =   int(dst_ip)    # ip address of the destination
    self.dst_port   =   int(dst_port)  # destination port number
                                         

    self.protocol   =   int(prot_val)  # protocol information


    self.priority   =   int(priority)  # flow priority
    self.flow_size  =   int(flow_size) # flow size in bytes
    self.fct        =   float(fct)     # flow completion time


  """
    Method for retrieving the representation
    of this flow.
  """
  def get_attributes(self):
    return (self.src_ip,    self.src_port,
            self.dst_ip,    self.dst_port,
            self.protocol,  self.flow_size, 
            self.fct)


  """
    Method for setting/updating an instance of
    the RL_Done_Flow.

    Args:
        attrs : attributes for updating a flow
  """
  def set_attributes(self, attrs):
    # update attributes by using
    # the passed values
    if not attrs:
      return

    self.src_ip     =   attrs.get("src_ip",   self.src_ip)
    self.src_port   =   attrs.get("src_port", self.src_port)

    self.dst_ip     =   attrs.get("dst_ip", self.dst_ip)
    self.dst_port   =   attrs.get("dst_port", self.dst_port)

    self.protocol   =   attrs.get("protocol", self.protocol)

    self.priority   =   attrs.get("priority", self.priority)
    self.fct        =   attrs.get("fct", self.fct)
    self.flow_size  =   attrs.get("flow_size", self.flow_size)



# Function for filling a state with a zero
# flow
def get_empty_completed_flow():
  return (0, 0, 0, 0, 0, 0, 0.0)

# Function for returning an empty
# active flow
def get_empty_active_flow():
 return (0,)*6

# Function for checking if the current flow
# is an empty one
def is_empty_active_flow(flow_attrs):
  for one_attr in flow_attrs:
    if one_attr != 0:
     return False

  return True


# Provide a few functions for encoding the
# completed flows into a network endian 
def prepare_flow(aflow):
  snew_ip   = net.convert_to_network_endianness(aflow[0], "I")
  snew_port = net.convert_to_network_endianness(aflow[1], "H")
  dnew_ip   = net.convert_to_network_endianness(aflow[2], "I")
  dnew_port = net.convert_to_netwotk_endianness(aflow[3], "H")
  new_prot  = net.convert_to_network_endianness(aflow[4], "B")
  new_prio  = net.convert_to_network_endianness(aflow[5], "B")
  new_fct   = float(aflow[7])
  new_size  = int(aflow[6])

  # return a new tuple with system values encoded into 
  # the network endian manner.
  return (snew_ip,  snew_port, dnew_ip,  dnew_port,
          new_prot, new_prio,  new_size, new_fct)



# Constant used interally to determine if 
# a binary string is of the correct length
_ACT_BIN_SIZE = int(len(struct.pack("<IHIHBBHH", 0, 0, 0, 0, 0, 0, 0, 0)))

# Utility function for decoding a binary string into
# a flow with required action values.
# Form of the action is:
# IPv4 PORT IPv4 PORT PROTOCOL PRIORITY RATE
#
# # return: PyrouteFlow
# raise: ValueError
def decode_long_action(raw_action):
 
  if raw_action and len(raw_action) == _ACT_BIN_SIZE:
    act_tuple = struct.unpack("<IHIHBBHH", raw_action)
    return (pyroute.PyrouteFlow(src_ip   = int(act_tuple[0]),
                               src_port = int(act_tuple[1]),
                               dst_ip   = int(act_tuple[2]),
                               dst_port = int(act_tuple[3]),
                               prot     = int(act_tuple[4]),
                               dscp     = int(act_tuple[5]),
                               rate     = int(act_tuple[6])),
             int(act_tuple[-1]) # flow index
           )
  else:
    ValueError("decode_long_action: `raw_action` size instead of {0} bytes is {1} bytes.".format(_ACT_BIN_SIZE, len(raw_action)))

# Utility function to wrap processing of a single binary
# string encoding  multiple long actions
def decode_long_actions(raw_actions):

  # need to make sure that the binary string
  # is a multiple of one action string
  flows = []
  flow_idxs = []
  if raw_actions and len(raw_actions) % _ACT_BIN_SIZE == 0:
    for offset in xrange(0, len(raw_actions), _ACT_BIN_SIZE):
      f_info, f_idx = decode_long_action(raw_actions[offset:offset+_ACT_BIN_SIZE:1])
      flows.append(f_info)
      flow_idxs.append(f_idx)
    
    return (flows, flow_idxs) # long actions and their indices in
                              # the shared array
  else:
    raise ValueError("decode_long_actions: `raw_actions` is not a multiple of `_ACT_BIN_SIZE`.")


# Utility function for generating a PyFlow key used to map
# long/large flows to their scheduling structures
# (see scheduling.utils.pyroute module)
def generate_flow_key(long_flow):
  tmp_flow = pyroute.PyrouteFlow(src_ip   = long_flow[0],
                                 src_port = long_flow[1],
                                 dst_ip   = long_flow[2],
                                 dst_port = long_flow[3],
                                 prot     = long_flow[4],
                                 dscp     = 0,
                                 rate     = 0)

  return pyroute.create_py_flow_key(tmp_flow)


# Utility function for calling the `generate_flow_key` 
# function on an iterable object
#
#  Returns:
#    a tuple of keys
def generate_flow_keys(long_flows):
  assert isinstance(long_flows, collections.Sequence), "generate_flow_keys:  `long_flows` is not a Sequence."
  if long_flows:
    return tuple([generate_flow_key(one_flow) for one_flow in long_flows])
  else:
    return ()
