# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import

from builtins import int

# File contains the implementations of reward functions/functors.
# Currently, only the used function is used.

class RelThrou(object):
  """
    A simple class for wrapping the reward function.
    It takes two iterable objects of short/long flows
    and return a scalar reward.
  """
  def __init__(self, scalar=1.0):

    self.prev_throu = 0.0    # previous throughput
    self.res_throu  = 0.0    # throughput one before self.prev_throu
    self.scalar     = scalar # when to consider the reward
                             # as a negative/positve

  """
    Callable that takes two lists: completed short/long flows.
    
    params:
      completed_short_flows : list of completed short flows
      completed_long_flows  : list of completed long flows
      reset                 : reset the reward functor to
                              previous value
                               
                                    

    return:
      scalar reward
  """
  def __call__(self, completed_short_flows=[],
                     completed_long_flows=[],
                     reset = False):

    if reset:
      self.prev_throu = float(self.res_throu)


    # some flows have completed
    if completed_short_flows or completed_long_flows:
    
      tot_bytes = int(0) # bytes
      tot_time  = 0.0 # us

      for one_flow in completed_short_flows:
        tot_bytes += int(one_flow.flow_size)
        tot_time  += float(one_flow.fct)

      for one_flow in completed_long_flows:
        tot_bytes += int(one_flow.flow_size)
        tot_time  += float(one_flow.fct)

     
      # compute relative throughput and reward
      this_throu = tot_bytes / tot_time
      rel_throu  = this_throu / self.prev_throu if (self.prev_throu > 0.0) else 1.0

      # update previous throughputs
      self.res_throu  = float(self.prev_throu)
      self.prev_throu = float(this_throu)
      reward = rel_throu if (rel_throu >= self.scalar) else (-1.0/rel_throu)
     
      return reward # reward has been computed
      
    else:
      return 0.0 # no flow has completed
