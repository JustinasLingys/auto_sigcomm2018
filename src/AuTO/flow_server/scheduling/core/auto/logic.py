# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import


# This module contains the logic of AuTO enforcement and
# monitoring modules.


# Python std lib
import multiprocessing
import collections
import signal
import time
import threading
import random
import socket
import struct
import json
import itertools


from builtins import int
import enum

# try to import the Queue/queue module
try:
  import Queue
  import httplib
except ImportError:
  import queue as Queue
  import http.client as httplib
  xrange = range



# Custom modules
from  scheduling.core.auto import features
from  scheduling.core.auto import thresholds
from  scheduling.utils     import pyroute
from  scheduling           import net


# port number on which the AuTO proces
# listens incoming responses from the CS
# server
AuTO_PORT    = int(60001)

_SIZE_SIZE   = int(len(struct.pack("<I", 0)))
_SIZE_REQ    = int(len(struct.pack("<II", 0, 0)))

# for terminating the program
_GLOB_SIGNAL = threading.Event()
_GLOB_SIGNAL.set()



@enum.unique
class ScheStatus(enum.Enum):
  """
    A Simple state class for informing the main thread
    about the result of the background scheduling.
  """
  OK        = 0   # successful operation
  SOCK_ERR  = 1   # socket related error (communication)
  SCHE_ERR  = 2   # scheduling related error
  PROT_ERR  = 3   # communication protocol error
  TERM_ERR  = 4   # terminal signal
  UNKN_ERR  = 255 # unknown error



# Utility function for converting a list of Completed flows
# intp a 1-D vector.
def _flatten_list_out(list_of_iters):
  return list(itertools.chain.from_iterable(list_of_iters))


def _signal_handler(signum, frame, term_call):
  
  # signal to stop processing further
  global _GLOB_SIGNAL
  _GLOB_SIGNAL.clear() # clear the flag
  # call a callback to interrupt any waiting
  term_call()


# Function for registering for async system
# signal handling
def _register_for_signals(term_call):

  signal.signal(signal.SIGTERM, 
                lambda signum, frame: _signal_handler(signum, frame, term_call))
  signal.signal(signal.SIGHUP,  
                lambda signum, frame: _signal_handler(signum, frame, term_call))
  signal.signal(signal.SIGSEGV, 
                lambda signum, frame: _signal_handler(signum, frame, term_call))



class _CoreSharedFlows(object):
  """
    Class for encapsulating short and long flows.
    Each of the server processes gets an instance of 
    this class to inform the AuTO process about its 
    current state.
  """




  class _FlowHandle(object):
    """
      Handle encapsulates communication between 
      the server processes and the AuTO main process.
    """
    def __init__(self, short_cmpl_flows, 
                       long_cmpl_flows,
                       long_active_flows,
                       index_queue,
                       queue_lock):

      self._short_cmpl_flows = short_cmpl_flows
      self._long_cmpl_flows  = long_cmpl_flows
      self._long_ac_flows    = long_active_flows
      self._len_ac_flow      = len(long_active_flows)
      self._indices          = index_queue
      self._queue_lock       = queue_lock
     

     
    """
      Check if queues are locked
    """
    def _locked_queues(self):
      return (not self._queue_lock.is_set())


    """
      Method for marking a long flow as completed one
    """
    def _completed_long_flow(self, flow_idx):
      
      if flow_idx >= 0: # valid array index
        # mark the flow as completed
        self._long_ac_flows[flow_idx].set_invalid()
      
        # return the index to the shared indices
        try:
          self._indices.put(flow_idx, block=False)
        except Queue.Full:
          # must never happen since the index queue
          # is initialized to a list of values and values
          # are only retrieved and returned from the same queue
          assert False, "_FlowHandle::_completed_long_flows: `index` queue is full."
         


    """
      Enqueue a completed flow (short one)
    """
    def enqueue_compl_short_flow(self, short_flow):
      # check if the queue is unlocked
      if not self._locked_queues():
        try:
          self._short_cmpl_flows.put(short_flow, 
                                     block=False)
        except Queue.Full:
          pass

    """
      Enqueue a completed flow (long flow)
    """
    def enqueue_compl_long_flow(self, long_handle, long_flow):

      # first mark the flow as completed
      self._completed_long_flow(long_handle)

      # append the state if possible
      if not self._locked_queues():
        try:
          self._long_cmpl_flows.put(long_flow, 
                                    block=False)
        except Queue.Full:
          pass 
       
          

    """
      Enqueue an active flow (long flow)
    """
    def enqueue_ac_long_flow(self, ac_long_flow):

      arr_index = -1

      if not self._locked_queues():
        try:
          arr_index = self._indices.get(block=False)
        except Queue.Empty:
          arr_index = -1         
        else:
          # append the information to the shared 
          # array
          self._long_ac_flows[arr_index].set_attributes(ac_long_flow)
          self._long_ac_flows[arr_index].set_valid()
          
         

      return arr_index


      
    """
      Return current priority of an anctive large/long flow.
      The value is read from the shared array.
    """
    def get_flow_priority(self, flow_idx):
      if flow_idx >= 0 and flow_idx < len(self._long_ac_flows):
        return self._long_ac_flows[flow_idx].priority
      else:
        return 0



  """
    Takes parameters of a state representation.
    sample_size : how many flows used for computing the reward
    total_long  : size of long-flow array
    m_s         : completed short flows
    m_l         : completed long flows 
    n_l         : active long flows
    reward_func : callable object to compute the reward
  """
  def __init__(self,    sample_size, total_long,
               m_short, m_long,      n_long,
               reward_func):
    
    # queue contains short completed flows during a period
    self.short_flows = multiprocessing.Queue(maxsize=sample_size)

    # queue contains long completed flows during a period
    self.long_flows  = multiprocessing.Queue(maxsize=sample_size)

    # array contains m_l of active long flows
    self.ac_long_flows = multiprocessing.RawArray(features.ActiveFlow, total_long)


    # for maintaining the list of array indexes
    self.idx_queue    = multiprocessing.Queue(maxsize=total_long)
    for arr_idx in xrange(0, total_long, 1):
      # must never block since initialization is
      # here
      self.idx_queue.put(arr_idx, block=True)
  
    

    # signal for processing when to append the completed flows
    self.state_event  = multiprocessing.Event()

    # state sizes (inputs are in number of flows)
    tmp_len           = len(features.get_empty_completed_flow())
    self.m_short      = m_short*tmp_len # convert to a 1-D vector
    self.m_long       = m_long*tmp_len  # convert to a 1-D vector
    self.n_long       = n_long
    self.comp_reward  = reward_func
  


  """
    Method to initialize the internal structures for 
    processing data.
  """
  def initialize(self):
    # initialize each of the structures
    for one_struct in self.ac_long_flows:
      one_struct.initialize()

    # mark all indexes as available
    for arr_idx in xrange(0, len(self.ac_long_flows), 1):
      self.idx_queue.put(arr_idx, block=False)
  
    # unlock the shared queues
    self.unlock_queues()


  """
    Return only a reference to the shared
    queues, does not provide any futher state
    information.
  """
  def get_flows_handle(self):
  
    
    # return an instance of the handle for server
    # processes
    return _CoreSharedFlows._FlowHandle(self.short_flows,   
                                        self.long_flows,
                                        self.ac_long_flows, 
                                        self.idx_queue,
                                        self.state_event)
            

  """
    If a connection fails, the reources of an ongoing
    large/long flow shall be returned.
  """
  def enqueue_failed_long_flow(self, arr_idx):
    if arr_idx >= 0 and arr_idx < len(self.ac_long_flows):
      # mark the flow as `INACTIVE`
      self.ac_long_flows[arr_idx].set_invalid()
      
      # return index of the shared array
      try:
        self.idx_queue.put(arr_idx, block=False)
      except Queue.Full:
        # shall never happen
        assert False, "_CoreSharedFlow::enqueue_failed+long_flow: index queue is full." 
   


  """
    Return m_s of sRLA
  """
  def _get_ms_value(self):

    res_state = []

    try:
      while True:
        res_state.append(self.short_flows.get(block=False))
    except Queue.Empty:
      pass


    # return a list of flows
    return res_state


  """
    Return m_l of lRLA
  """
  def _get_ml_value(self):
    res_state = []

    try:
      while True:
        res_state.append(self.long_flows.get(block=False))
    except Queue.Empty:
      pass

    # return a list of flows
    return  res_state


  """
    Return n_l of lRLA
  """
  def _get_nl_value(self):
    flows  = []
    f_idxs = []
    
    for one_idx in xrange(0, len(self.ac_long_flows), 1):
      if self.ac_long_flows[one_idx].is_valid():
        flows.append(self.ac_long_flows[one_idx].get_attributes())
        f_idxs.append(one_idx)

    # implementation requires flow's index within the shared array
    return (flows, f_idxs)
        

  """
    For locking the shared queues
  """
  def lock_queues(self):
    self.state_event.clear()

  """
    For unlocking the queues
  """
  def unlock_queues(self):
    self.state_event.set()

  """
    For checking if the queue is locked
  """
  def _queues_locked(self):
    return (not self.state_event.is_set())


  """
    Utility method for making sure that the states
    contains the minimum values.
  """
  def _preprocess_states(self, state_ms, state_ml, state_nl):
    if len(state_ms) < self.m_short:
      # append empty states
      for _ in xrange(0, self.m_short - len(state_ms), 1):
        state_ms.append(features.get_empty_completed_flow())
    
    if len(state_ml) < self.m_long:
      # append empty states
      for _ in xrange(0, self.m_long - len(state_ml), 1):
        state_ml.append(features.get_empty_completed_flow())

    if len(state_nl) < self.n_long:
      for _ in xrange(0, self.n_long - len(state_nl), 1):
        state_nl.append(features.get_empty_active_flow())


  """
    Roll back the reward (something failed, so previous
    reward state should be taken)
  """
  def rollback_state(self):
    # reset the reward function
    self.comp_reward((), (), reset=True)


  """
    Return states
  """
  def get_states(self):
  
    tmp_m_short = self._get_ms_value()
    tmp_m_long  = self._get_ml_value()

    # compute the reward based on completed long-short
    # flows
    res_reward  = self.comp_reward(tmp_m_short, tmp_m_long,
                                   reset=False)

    # convert the lists into lists of attributes
    res_m_short = _flatten_list_out([one_ms_flow.get_attributes() for one_ms_flow in tmp_m_short])

    res_m_long  = _flatten_list_out([one_ml_flow.get_attributes() for one_ml_flow in tmp_m_long])

    res_n_long  = self._get_nl_value()

    # need to check if the state is empty or not
    if res_m_short or res_m_long or res_n_long[0]:  

      return {
              "m_short"  : tuple(res_m_short[0:self.m_short:1]),
              "m_long"   : tuple(res_m_long[0:self.m_long:1]),
              "n_long"   : tuple(res_n_long[0][0:self.n_long:1]),
              "n_idx"    : tuple(res_n_long[1][0:self.n_long:1]),
              "reward"   : res_reward
             }
    else:
      return None # a None


  """
    Modify the priority feature of a flow in the shared
    array.
    
    params:
      flow_idx  : index in the array of large flows
      prio_val  : priority value
  """
  def change_priority_value(self, flow_idx, prio_val):
    if flow_idx >= 0 and flow_idx < len(self.ac_long_flows):
      self.ac_long_flows[flow_idx].priority = int(prio_val)




  """
    Method called by a signle process to release the resources.
  """
  def close(self):
    self.lock_queues() # make sure no other process
                       # tries to access the shared 
                       # structures
    # clear the queues
    self._get_ms_value()
    self._get_ml_value()
  


class SharedFlows(object):
  """
    Wrapper of the Singleton of `_CoreSharedFlows`
    class. This wrapper ensures that all completed large/long
    flows get recorded by the AuTO process to release
    the kernel allocated structures.
  """


  class _HandleWrapper(object):
    """
      Wrapper around an instance of the `_CoreSharefFlows.FlowHandle`
      for making sure that all completed large/flows get recorded.
    """  

    def __init__(self, handle, completion_queue):
      self._handle = handle

      # buffer for all completed large/long flows
      self._buff   = completion_queue

    
    def enqueue_compl_short_flow(self, short_flow):
        # non-blocking call to the underlying handle
        self._handle.enqueue_compl_short_flow(short_flow)


    def enqueue_compl_long_flow(self, long_handle, long_flow):
      # buffer the info about the long_flow
      # and latter enqueue it when required 

      if long_handle >= 0 and self._buff is not None:
        self._buff.append(long_flow)
      self._handle.enqueue_compl_long_flow(long_handle, long_flow)

    """
      Return an opaque handle used by the `enqueue_compl_long_flow`
      to keep info about the flow.
    """
    def enqueue_ac_long_flow(self, ac_long_flow):
      return self._handle.enqueue_ac_long_flow(ac_long_flow)


    """
      Return current long/large flow priority value.
      
      params:
        flow_handle : handle returned by the `enqueue_ac_long_flow`
                      method
    """
    def get_flow_priority(self, flow_handle):
      return self._handle.get_flow_priority(flow_handle)




  def __init__(self, sample_size, total_long,
                     m_short,     m_long,
                     n_long,      reward_func,
                     suspend=True):
                      
                     # singleton (shared state)
    self._sh_state   = _CoreSharedFlows(sample_size,
                                        total_long,
                                        m_short, 
                                        m_long,
                                        n_long,
                                        reward_func) 
                          

    # if maintain a list of completed large/long flows
    self._cmpl_buffer = collections.deque(maxlen=None) if suspend else None
    self._cmpl_large  = multiprocessing.Queue(maxsize=n_long+m_long)


  def initialize(self):
    self._sh_state.initialize()


  def get_flows_handle(self):
    core_hd =  self._sh_state.get_flows_handle()

    # return a wrapper of the returned object
    return SharedFlows._HandleWrapper(core_hd, self._cmpl_buffer)

    

  def lock_queues(self):
    self._sh_state.lock_queues()


  def unlock_queues(self):
    self._sh_state.unlock_queues()

  
  def rollback_state(self):
    self._sh_state.rollback_state()

  def get_states(self):
    return self._sh_state.get_states()

  def change_priority_value(self, flow_idx, prio_val):
    self._sh_state.change_priority_value(flow_idx, prio_val)


  def enqueue_failed_long_flow(self, long_handle, failed_flow):
    if long_handle >= 0 and self._cmpl_buffer is not None:
      # later this flow will be removed by
      # the `AuTO` process
      self._cmpl_buffer.append(failed_flow) 

    self._sh_state.enqueue_failed_long_flow(long_handle)
 
  def close(self):
    self._sh_state.close()


  ###### Wrapper Specific methods ######

  """
    Method for notifying the AuTO process about completed
    large/long flows and that their scheduling must be removed.
  """
  def notify_completion(self):
    if self._cmpl_buffer:
      flows = tuple(self._cmpl_buffer)

      try:
        # enqueue the notifications
        self._cmpl_large.put(flows, block=False)
      except:
        pass # try later one more time
      else:
        # successfully passed info about
        # large flows
        self._cmpl_buffer.clear()


  """
    Method for receiving notifications about completed
    flows.
  """
  def receive_notify(self):
    res = []

    try:
      while True:
        res.append(self._cmpl_large.get(block=False))
    except Queue.Empty:
      pass
    finally:
      return res
        
    



# Start and run a background thread for
# handling CS responses
#
# params:
#   gl_threshs : global thresholds
#   ac_call    : for changing priority of a an active large flow 
def _init_server_thread(gl_threshs, ac_call):

  # server socket for listening incoming
  # respones
  sockfd = None
  
  try:
    sockfd = socket.socket(socket.AF_INET, socket.SOCK_STREAM,
                           socket.IPPROTO_TCP)

    # set socket options
    sockfd.setsockopt(socket.SOL_SOCKET,  socket.SO_REUSEADDR, 1)
    sockfd.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY,  1)
   
    # try to bind to the 'AuTO_PORT' port
    sockfd.bind(("", int(AuTO_PORT)))
    sockfd.listen(3)
    # make sure the socket is blocking
    sockfd.setblocking(True)
  except Exception as exp:
    if sockfd:
      sockfd.close()
    return exp

  # socket has been successfully bound,
  # run a background thread
  class _ServerThread(object):
  
    def __init__(self, socket_desc, 
                 auto_thrshs,
                 prio_call):
      self._sockfd   = socket_desc
      self._thread   = threading.Thread(target=self._handle_io)
      self._thread.daemon = False
      self._sign     = Queue.Queue(maxsize=1)
      self._term     = threading.Event()
      self._term.clear() # make sure is cleared
      self._handle   = auto_thrshs
      self._ac_call  = prio_call
      self._conn     = None




    """
      Background thread method for handling CS 
      responses.
    """
    def _handle_io(self):

      # handle incoming requests and reading data
      while self._term.is_set():
        try:
          conn, addr = self._sockfd.accept()
          self._handle_conn(conn)
        except:
          pass
        

      # the background thread is done
      


    """
      Change values of the shared array into 
      the value received from the CS server.
    """
    def _change_prios(self, long_flows, long_idxs):
     if long_flows:
       assert len(long_flows) == len(long_idxs), "_change_prio: len(flow_flows) != len(long_idxs)"

       # go over active flows and change their priority
       # in the shared array
       for ac_idx, f_info in zip(long_idxs, long_flows):
         self._ac_call(ac_idx, f_info.dscp)
             
          
          



    """
      Handle a newly connected socket
    """
    def _handle_conn(self, conn):
      

      # thresholds occupy that many bytes
      bin_thrsh_size = thresholds.get_gammas_bin_size()
    
      # read that many bytes initially
      init_read_size = _SIZE_REQ + bin_thrsh_size

      # mark the connection as a local one
      self._conn = conn

      # check again if don't need to close
      if self._term.is_set():
        # process the connection
        try:
          while True:
            # first read the size of the message and
            # the gamma values
            raw_bytes = net.read_data(conn, init_read_size)
 
            # convert raw bytes into int values
            recv_gammas  = thresholds.decode_gammas(raw_bytes[_SIZE_REQ::1])

            # alphas - thresholds
            # gammas - gaps between two thresholds
            recv_threshs = thresholds.compute_alphas(recv_gammas)
          

            # get the remaining size of the message
            msg_size = struct.unpack("<I", raw_bytes[0:_SIZE_SIZE:1])[0]

            # read the remaining size of the message
            rem_size  = msg_size - init_read_size
           

            # maybe no need to schedule long/large flows
            if rem_size == 0:
              # no more data to read
              self._handle.set_global_threshs(recv_threshs)
              # successfully taken the received action from 
              # the Central System
              self._sign.put((ScheStatus.OK, ()), block=True)
              continue # reuse the connection
                         # for receiving new actions


            else: # try to receive the actions on
                  # long/large flows
 
              raw_bytes = net.read_data(conn, rem_size)
              assert len(raw_bytes) == rem_size, "_handle_conn: `len(raw_bytes) != rem_size`"
              # need to decode the features into large flows
              # and use this data to schedule large flows
              try:
                long_flows, long_idxs = features.decode_long_actions(raw_bytes)
              except Exception as dec_err:
                # failed to decode long/large flows
                # (signal that need to undo reward)
                self._sign.put((ScheStatus.PROT_ERR, "Failed to decode large/long flows: `{0}`".format(dec_err)), block=True)
                break
              else:
                # successfully decoded long flows
                tc_handles = pyroute.schedule_flows(long_flows)
     
                # make sure all flows have been successfully scheduled
                err_msgs = ["TC Exception: {0}".format(tc_res) for tc_res in tc_handles if isinstance(tc_res, Exception)]
                
                if err_msgs:
                  # some errors ocurred
                  self._sign.put((ScheStatus.SCHE_ERR, str(err_msgs)), block=True) 
                  break
                else:             
                  # done processing (signal a success)
                  self._handle.set_global_threshs(recv_threshs)
                  # set the priorities of the active flows
                  # to their corresponding values
                  self._change_prios(long_flows, long_idxs)
                   
                  # send signal to the main thread to carry on
                  self._sign.put((ScheStatus.OK, tc_handles), block=True)

     
        except Exception as exp:
          self._sign.put((ScheStatus.SOCK_ERR, "Socket  error: {0}".format(exp)), block=True)
        finally:
          # close the connection (release the resources)
          self._conn = None
          self._close_conn(conn)
        
       
      else: # if self._term is `True`  
            # need to terminate
        self._conn = None
        self._close_conn(conn)
     
    

    """
      Try to close a connection
    """
    def _close_conn(self, conn):

      if conn:
        try:
          conn.shutdown(socket.SHUT_RDWR)
        except:
          pass # ignore
        finally:
          # release the resources
          conn.close() 
       
 


    """
      Start running the background thread
    """
    def start_thread(self):
      if not self.is_started():
        self._term.set()
        self._thread.start()

    """
      Stop processing the IO events
    """
    def stop_thread(self):

      if self.is_started():
 
        # signal termination
        self._term.clear()
     
        if self._sockfd:
          # need to close the server listening socket
          self._close_conn(self._sockfd)  
     
        # might need to close
        # the conn used for communication
        if self._conn:
          self._close_conn(self._conn)

        # in case of some error, free the shared
        # queue
        self.act_nonblocking_status()
        # wait for the background thread to terminate
        self._thread.join()

      # reset references to `None`
      self._sockfd  = None
      self._conn    = None 
      self._thread  = None
      

    """
      Check if the thread has been started
    """
    def is_started(self):
      return (self._term.is_set() and self._thread is not None)


    """
      Return the result of receiving actions from the
      Central System.
    """
    def act_status(self):
      return self._sign.get(block=True)

    """
      Return the status of receiving action from
      the Central System in a  non-blocking way
    """
    def act_nonblocking_status(self):
      try:
        tmp_stat = self._sign.get(block=False)
      except Queue.Empty:
        return None
      else:
        return tmp_stat

  
    """
      Get a reference to the queue so that the main thread
      would not need to wait for the server thread to terminate.
    """
    def get_sign_ref(self):
      # create a callback and 
      # return it
      def term_call():
        try:
          self._sign.put((ScheStatus.TERM_ERR, "_signal_handler: terminating."), block=False)
        except Queue.Full:
          pass

      return term_call # callback
       

     

  # return an instance of the `_ServerThread` class
  return _ServerThread(sockfd, gl_threshs, ac_call)



 
# Function for trying connecting to the
# Central System server for communicating with it.
def _connect_to_cs_server(address):
 

  count = 0
  conn_proxy = None # connection handle

  while count < 3:
    try:
      conn_proxy = httplib.HTTPConnection(address[0], address[1], timeout=2)
      conn_proxy.connect()
    except Exception as exp:
      conn_proxy = exp # set exception as the
                       # result
      count += 1       # try again
      
      # wait for a bit before retrying
      time.sleep(float(random.randint(0, 3)))
    else:
      # done
      break
  
  # return proxy
  return conn_proxy
     
    

# Utility function for sending a JSON representation
# of the server state to the CS Server.
#
# Returns:
#   0 : means everything went well
#   1 : means `Continue` has been received (don't expect for
#              a respone from the CS server)
#   2 : means something went wrong ==> break
#
# Raises:
#   Exception if the http proxy fails
def _send_cs_data(s_proxy,     raw_data, 
                  HTTP_HEADER, logger):

  s_proxy.request("POST", "", raw_data,
                   HTTP_HEADER)

  # make sure data has been delivered  
  op_res = s_proxy.getresponse()
  
  if op_res.status != httplib.OK:
    logger("_send_cs_data: `op_res` != `httplib.OK`: {0}".format(op_res.status))
    return 2 # break and stop processing

  
  # need to know if the server has read the whole
  # json data
  res_string = op_res.read()

  if res_string == b"Terminate":
    logger("_send_cs_data: received `Terminate` from the server.")
    return 2 # break and stop processing

  if res_string == b"Continue":
    logger("_send_cs_data: received `Continue` from the server")
    return 1# continue processing, but reset all the updates
         
       
  assert res_string == b"OK", "_send_cs_data: received from the server unknown string `{0}`".format(res_string)


  return 0 # everything went well



# Utility function for creating an object for
# generating transaction ids.
def _create_req_id():

  MAX_VAL = 2**32 # for modulo math

  class UqId(object):
    """
      Class for generating unique ids
      for sending updates to the `CS System`.
    """
    def __init__(self, start_idx=0):
      self._uq_id = start_idx


    """
      For generating unique ids for requests.
    """
    def next_id(self):
      tmp_id = self._uq_id
      self._uq_id = (tmp_id + 1) % MAX_VAL

      return tmp_id

    """
      For rolling back the id to a few steps.
    """
    def roll_back(self, num_back=1):
      assert num_back >= 0, "UqId::roll_back: negative num_back"
      tmp_val = num_back % MAX_VAL
      if tmp_val > self._uq_id:
        self._uq_id = MAX_VAL - tmp_val + self._uq_id
      else:
        self._uq_id = self._uq_id - tmp_val

    @property
    def id_val(self):
      return self._uq_id
    

  return UqId(0)



# Utility function for converting the state into
# a jason string that can be sent over a newtwork
def _prepare_states(state_map, req_id):

  # convert a map received from the AuTO state to
  # a JSON object
   
  return json.dumps({"req_id"           : req_id,
                     "reward"           : state_map["reward"],
                     "short_completed"  : state_map["m_short"],
                     "long_completed"   : state_map["m_long"],
                     "long_active"      : state_map["n_long"],
                     "long_idx"         : state_map["n_idx"]})
  

# Function that runs in a loop while collects
# state about current flow, shares it with a central
# server and updates local structures
def _run_auto_loop(cs_address, update_period,
                   auto_state, gl_thrshs, logger,
                   ifc_idx,    ifc_label,
                   dscp_vals):


  # start listening on incoming requests
  # from the Central System server.
  # 
  # The returned 'cs_thread' provides a handle
  # to receive a state of system updates
  try:
    cs_thread = _init_server_thread(gl_thrshs,  
                                    auto_state.change_priority_value)
  except Exception as th_err:
    with logger:
      logger("_run_auto_loop: `_init_server_thread` failed: {0}".format(th_err))
    return


  if isinstance(cs_thread, Exception):
    # either failed to bind to an interface or
    # create a thread
    with logger:
      logger("_init_server_thread: {0}".format(cs_thread))

    return # done


  # first of all try to connect to the CS server
  comm_proxy = _connect_to_cs_server(cs_address)

  # need to make sure no excpetion occurred 
  if isinstance(comm_proxy, Exception):
    # failed to connect
    with logger:
      logger("_run_auto_loop: {0}".format(comm_proxy)) 

    return # don't progress further
   
  else:

    try:

      # connected to the Central System server 
      # start processing requests
      # (before initialize the pyroute states)
      pyroute.initialize(ifc_idx, ifc_label, dscp_vals)
      cs_thread.start_thread()

      # register for async system signals
      _register_for_signals(cs_thread.get_sign_ref())    

      period_val = float(update_period)
      req_id     = _create_req_id() # a unique id for each of the requests
      global _GLOB_SIGNAL
      HTTP_HEADER = {"Content-Type" : "application/json",
                     "Accept"       : "text/plain, application/json",
                     "Keep-Alive"   : "timeout=120"}

    except Exception as exp:
      with logger as log_err:
        log_err("_run_loop: `{0}`".format(exp))

      return


    sche_map  = {} # long state map for removing scheduling of
                   # large flows

    # run a loop till a signal is received to terminate

    with logger as info_log:

      while _GLOB_SIGNAL.is_set():
    
        # sleep for the given period to 
        # accumulate some flows
        time.sleep(period_val)

        # get the current state for processing
        auto_state.lock_queues()
        cur_state = auto_state.get_states() 

        if not cur_state:
          auto_state.unlock_queues()
          continue # wait for the next round
    
        # process the state and send it to the 
        # Central System server
        json_data = _prepare_states(cur_state, req_id.next_id())

        assert json_data, "`json_data` is `None`: _prepare_states failed."
        
        # try to send data
        send_status = 2 # failure
        while True:
          # try to send a request
          try:
            send_status = _send_cs_data(comm_proxy,  json_data,
                                        HTTP_HEADER, info_log)
                     

          except Exception as comm_exp:
            info_log("_run_auto_loop: comm_proxy.request: {0}".format(comm_exp))
            # retry to connect
            comm_proxy = _connect_to_cs_server(cs_address)

            if isinstance(comm_proxy, Exception):
              info_log("_run_auto_loop: failed reconnect to `CS` server: {0}".format(comm_proxy))
              send_status = 2
              break
          else:
            break # no exception

        # now check for status
        if send_status == 2:
          break # done processing
        elif send_status == 1:
          # undo the changes to the underlying shared
          # state and wait for a next period
          wait_for_sign = False
          
          auto_state.rollback_state()
          req_id.roll_back()
         
        else:
          assert send_status == 0, "_run_auto_loop: send_status == {0}".format(send_status)

          wait_for_sign = True # need to wait for a signal
                               # from the background thread


        # process the complete long flows 
        # (remove kernel data structures)
        finished_large_flows = auto_state.receive_notify()
       
        if finished_large_flows:
          # need to remove the kernel structures
          # for scheduling
          gen_keys = []
          for one_batch in finished_large_flows:
            for one_pr_flow in one_batch:
              gen_keys.append(features.generate_flow_key(one_pr_flow.get_attributes()[0:5:1]))

          if gen_keys:
            # remove all the allocated structures for the
            # received batch of completed flows
            results  = pyroute.remove_flows(gen_keys)

            # need to check if there are any exceptions
            err_msgs = [res_exp for res_exp in results if isinstance(res_exp, Exception)]
            if err_msgs: # some exception occurred
                         # stop processing further
              info_log("_run_auto_loop: `remove_flows` failed: {0}".format(err_msgs))
              break # stop the loop
           
        
        # wait for a signal from the background thread
        if wait_for_sign:

          long_status = cs_thread.act_status()
    
          
          if long_status[0] != ScheStatus.OK:
            # something went wring with the
            # scheduling
            if long_status[0] == ScheStatus.SOCK_ERR:
              
              # undo the changes as the connection is lost
              # new update will be waited for  
              auto_state.rollback_state()
              req_id.roll_back()
          

            else:
              info_log("_run_auto_loop: background thread failed: {0}".format(long_status[1]))
              break

        # unlock the shared queues for collecting 
        # state
        auto_state.unlock_queues() 
      
            
          

      # siganl to terminate has been issued
      # try to close the proxy
      if not isinstance(comm_proxy, Exception):
        try:
          comm_proxy.close()
        except Exception as close_exp:
          info_log("_run_auto_loop: close `proxy`: {0}".format(close_exp))
        finally:
          comm_proxy = None

      # terminate the server thread
      info_log("_run_auto_loop: stopping the background thread.")
      cs_thread.stop_thread()
      info_log("_run_auto_loop: stopped and joined the background thread.") 
    
      # release the allocated pyroute resources
      pyroute.close()
                       


# Function for running the AuTO process and handling
# local traffic. The function runs the core of AuTO.
#
#  params:
#    auto_cs_server  : tuple(IPv4, port) of the CS server
#    auto_cs_period  : period for sending updates (seconds)
#    auto_state      : instance of the SharedFlows class
#    auto_thresholds : instance of thresholds for setting them
#    auto_logger     : object for logging
#    auto_if_idx     : interface index for scheduling
#    auto_if_label   : interface label for scheduling
#    auto_dscp       : dscp values used for scheduling flows
def run_auto(auto_cs_server, auto_cs_period,
             auto_state,     auto_thresholds, 
             auto_logger,    auto_if_idx,
             auto_if_label,  auto_dscp):

  _run_auto_loop(auto_cs_server, auto_cs_period,  
                 auto_state,     auto_thresholds, 
                 auto_logger,    auto_if_idx,
                 auto_if_label,  auto_dscp)
