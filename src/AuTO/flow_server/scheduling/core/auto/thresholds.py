# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import



# File contains classes to represent AuTO sRLA thresholds


# dependencies
from builtins import int

# standard library 
import multiprocessing
import collections
import ctypes
import struct
import time



try:
  xrange
except NameError:
  xrange = range

# custom modules



_GAMMA_LEN = int(-1)
_GAMMA_STR = ""
_MAX_GAP    = int(2**32-1)
_ITEM_SIZE  = int(len(struct.pack("<I", 0)))


# Need to know the number of thresholds
def init_global(thresholds):

  if isinstance(thresholds, collections.Sequence):
    if thresholds:
      global _GAMMA_LEN, _GAMMA_STR
      _GAMMA_LEN = len(thresholds)
      _GAMMA_STR = "".join(["<", "I"*len(thresholds)])
    else:
      raise IOError("Thresholds must be a non-empty iterable.")
  else:
    raise IOError("Threholds must be a non-empty iterable.")


# Return the size of gammas (gaps)
# as a binary string
def get_gammas_bin_size():
  
  if _GAMMA_LEN > 0:
    return _GAMMA_LEN*_ITEM_SIZE
  else:
    raise IOError("Thresholds module has not been initialized.")


# Encode gammas
# Encode the gamma values into 
# a binary string. If any error
# occurs, return 'None'
def enocode_gammas(gamma_vals):

  if _GAMMA_LEN <= 0 or len(gamma_vals) != _GAMMA_LEN:
    raise ValueError("encode_gammas: passed a wrong number of 'gammas'.")
  else:
    enc_vals = [max(_MAX_GAP, th_val) for th_val in thresh_vals]
    # encode the values into binary 
    return struct.pack(str(_GAMMA_STR), *enc_vals)


# Decode a binary string into
# a tuple of integers. If any error occurs,
# return 'None' (gamma values are returned) 
def decode_gammas(raw_bytes):

  if len(raw_bytes) == _GAMMA_LEN*_ITEM_SIZE:
    # decode the string
    return struct.unpack(str(_GAMMA_STR), raw_bytes)
  else:
    raise ValueError("decode_gammas: received binary array does not match the required size.")


# Compute threshold values from gamma
# (gap) values
def compute_alphas(gamma_vals):

  if len(gamma_vals) == _GAMMA_LEN:
    thrsh_vals = [gamma_vals[0]]

    for idx in xrange(1, _GAMMA_LEN, 1):
      tmp_val = gamma_vals[idx] + thrsh_vals[idx-1]
      thrsh_vals.append(tmp_val)    


    # return a tuple of values
    return tuple(thrsh_vals)
  else:
    raise ValueError("compute_alphas: received number an incorrect number of gamma values.")



# A Structure that represents RL AuTO sRLA Thresholds
class AuTOThrshs(object):

  def __init__(self, arr, count):
    
    # shared integer array
    # that stores AuTO thresholds
    self.thrshs    = arr
    # shared counter(active readers)
    self.readers   = count

# A structure that is shared among all flows and
# the AuTO module. The strucutre ecapsulates lower
# level details of accessing AuTO short flow thresholds.
class GlobalThrshs(object):
  def __init__(self, thrsh_1, thrsh_2, state):
    self.auto_thrsh_1 = thrsh_1
    self.auto_thrsh_2 = thrsh_2
    state.clear() # make sure the program starts at a cleared state
    self.state = state

  # return Global AuTO thresholds
  # method hides internal locking so clients/flows become
  # much easier since they don't need to handle locking.
  def get_global_threshs(self):
    if self.state.is_set(): # means need to use thrsh2
      with self.auto_thrsh_2.readers.get_lock():
        self.auto_thrsh_2.readers.value += 1

      # check one more time if the flag has not changed
      # SKIP this case
      # TODO: Later need to introduce double-locking
      # The writer may update the flag and find readers 0
      # while a reader was locking (a small possible bug)
      ##########  KNOWN BUG ############################
      
      # copy the thresholds
      tmp_thrshs = [thr for thr in self.auto_thrsh_2.thrshs] 
      
      # a reader finished reading
      with self.auto_thrsh_2.readers.get_lock():
        self.auto_thrsh_2.readers.value -= 1

      return tuple(tmp_thrshs)

    else: # means need to use thrsh1
      with self.auto_thrsh_1.readers.get_lock():
        self.auto_thrsh_1.readers.value += 1

      # check one more time if the flag has not changed
      # SKIP this case
      # TODO: Later need to introduce double-locking
      # The writer may update the flag and find readers 0
      # while a reader was locking (a small possible bug)
      ##########  KNOWN BUG ############################
      
      # copy the thresholds
      tmp_thrshs = [thr for thr in self.auto_thrsh_1.thrshs] 
      
      # a reader finished reading
      with self.auto_thrsh_1.readers.get_lock():
        self.auto_thrsh_1.readers.value -= 1

      return tuple(tmp_thrshs)


  # writer sets thresholds
  # no need for locking since the writer is the only
  # one which accesses an array. After setting, the state is
  # is updated
  def set_global_threshs(self, thrshs):

    # for iterating over two lists
    num_of_items = len(thrshs)

    if self.state.is_set(): # means need to update
                            # thrsh_1 since thrsh_2 is used now


      # wait until all readers of thrsh_1 completed
      while self.auto_thrsh_1.readers.value > 0:
        time.sleep(0.0) # yield this process
  
      # now can update the thresholds
      for idx in xrange(0, num_of_items, 1):
        self.auto_thrsh_1.thrshs[idx] = thrshs[idx]

      # notify the readers about it
      self.state.clear() 

    else: # means need to update
          # thrsh_2 since thrsh_1 is used now

      # wait until all readers of thrsh_2 completed
      while self.auto_thrsh_2.readers.value > 0:
        time.sleep(0.0) # yield this process
  
      # now can update the thresholds
      for idx in xrange(0, num_of_items, 1):
        self.auto_thrsh_2.thrshs[idx] = thrshs[idx]

      # notify the readers about it
      self.state.set() 
  

# 0 - DCTCP_CDF
# 1 - VL2_CDF
# refer to utils.common.py
QLAS_STATIC_THRESHOLDS = { 0 : { 800 : [909*1460, 1329*1460, 1648*1460, 
                                        1960*1460, 2143*1460, 2337*1460, 
                                        2484*1460],
                                 700 : [999*1460, 1305*1460, 1564*1460, 
                                        1763*1460, 1956*1460, 2149*1460, 
                                        2309*1460],
                                 600 : [956*1460, 1381*1460, 1718*1460, 
                                        2028*1460, 2297*1460, 2551*1460, 
                                        2660*1460],
                                 500 : [1059*1460, 1412*1460, 1643*1460,
                                        1869*1460, 2008*1460, 2115*1460,
                                        2184*1460]
                                
                               },
                          1 : { 800 : [745*1460, 1083*1460, 1391*1460, 
                                       13689*1460, 14936*1460, 21149*1460,
                                       27245*1460],
                                700 : [907*1460, 1301*1460, 1619*1460,
                                       12166*1460, 12915*1460, 21313*1460,
                                       26374*1460],
                                600 : [840*1460, 1232*1460, 1617*1460,
                                       11950*1460, 12238*1460, 21494*1460,
                                       25720*1460],

                                500 : [805*1460, 1106*1460, 1401*1460,
                                       10693*1460, 11970*1460, 21162*1460,
                                       22272*1460]
                              }  
                         }
