# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import


# Dependencies
from   builtins import int


# Python std lib
import sys
import errno
import multiprocessing
import ctypes
import ast
import os
import os.path



# include the very top directory of scheduling
sys.path.insert(1, os.path.abspath(os.path.join(__file__, os.pardir,
                                               os.pardir, os.pardir,
                                               os.pardir)))
# include pyroute2 from the library directory
sys.path.insert(0, os.path.abspath(os.path.join(__file__,  os.pardir,
                                                os.pardir, os.pardir,
                                                os.pardir, os.pardir,
                                                os.pardir, os.pardir,
                                                "lib",     "pyroute2_0.4.21")))

# import pyroute2
import pyroute2

# Custom modules
from   scheduling.core.auto    import handler
from   scheduling.core.auto    import features
from   scheduling.core.auto    import thresholds
from   scheduling.core.auto    import logic
from   scheduling.core.auto    import rewards
import scheduling.utils.logging    as utl_log

try:
  import Queue

except ImportError:
  import queue as Queue
  xrange = range 


# A process cannot have more than 2**16 active connections
# (maximum number of ports)
MAX_PORTS = int(2**16)


# Stop server processes
def _stop_servers(serv_procs):

  for one_proc in serv_procs:
    one_proc.stop_server()


  # wait for all the processes to terminate
  for one_proc in serv_procs:
    if one_proc.is_alive():
      one_proc.join()



# Function determines if the process is owned by a
# user with administrative rights.
def _determine_root():

  ip_test = None

  try:
    # create a netlink socket
    ip_test = pyroute2.IPRoute()
    ip_test.link("add", ifname="vp125p125", 
                 peer="vp125p126", kind="veth")
  except pyroute2.NetlinkError as exp:
    if exp.code == errno.EPERM:
      return False
    else:
      return True
  else:
    ip_test.link("del", ifname="vp125p125", 
                 peer="vp125p126", kind="veth")
    return True
  finally:
    # close the socket
    if ip_test:
      ip_test.close()



# A utility function for creating a structure of thresholds
def _create_threshs(threshs):
  
  # need to pass two instance of thresholds.AuTOThreshs
  first_temp  = thresholds.AuTOThrshs(
    multiprocessing.RawArray(ctypes.c_uint32, tuple(threshs)),
    multiprocessing.Value(ctypes.c_uint, 0, lock=True))

  second_temp = thresholds.AuTOThrshs(
    multiprocessing.RawArray(ctypes.c_uint32, tuple(threshs)),
    multiprocessing.Value(ctypes.c_uint, 0, lock=True))

  return thresholds.GlobalThrshs(first_temp, second_temp,
                                 multiprocessing.Event())
  
  
# Utility function which runs the AuTO logic handling
def _run_auto_proc(cs_address, cs_period,
                   gl_threshs, auto_handle, 
                   servers,    log_file,
                   ifc_info,   prio_tos):

  # blocking call
  logic.run_auto(auto_cs_server  = cs_address,
                 auto_cs_period  = cs_period,
                 auto_state      = auto_handle,
                 auto_thresholds = gl_threshs,
                 auto_logger     = utl_log.create_logger(log_file),
                 auto_if_idx     = ifc_info[0],
                 auto_if_label   = ifc_info[1],
                 auto_dscp       = prio_tos)

  # stop all the server processes
  _stop_servers(servers)
  
  # stopping the service
  auto_handle.close()
  
 

def main(params):


  if not _determine_root():
    print("The process must be owned by a user with administrative rights.\n(Use either the root user or sudo)")
  
  else:
    # an administrator
    # convert a string into a Python dictionary
    param_dict = ast.literal_eval(params)

    print("A child process has been successfully started.")

    log_dir_path  = str(param_dict["log_dir"])  # logging directory
    eval_dir_path = str(param_dict["eval_dir"]) # evaluation directory
    prio_threshs  = tuple(param_dict["demotion_thresholds"])
    prio_tos      = tuple(param_dict["ipv4_tos"]) # IPv4 TOS values

    # initialize the threshold strucures for later
    # processing
    thresholds.init_global(prio_threshs)


    # state sizes
    m_s           = int(param_dict["short_completed_flows"])
    m_l           = int(param_dict["long_completed_flows"])
    n_l           = int(param_dict["long_active_flows"])

   
    rew_ratio     = float(param_dict["reward_ratio"]) 
    total_arr     = int(param_dict["available_long_slots"])
    reward_size   = int(param_dict["sample_size"])
    user_name     = str(param_dict["system_user"])
    ifc_info      = tuple(param_dict["ifc_info"])


    # Central System server address 
    # (only one address is currently supported)
    cs_address    = tuple(param_dict["cs_server"])[0]


    # period of sending updates
    cs_period     = float(param_dict["rl_update_period"])
    
    # create the threshold object
    global_threshs = _create_threshs(prio_threshs)

    # shared stated between the AuTO and server
    # processes
    auto_state = logic.SharedFlows(sample_size = reward_size,
                                   total_long  = total_arr,
                                   m_short     = m_s,
                                   m_long      = m_l,
                                   n_long      = n_l,
                                   reward_func = rewards.RelThrou(rew_ratio))

    # create servers (port numbers must be unique)
    server_processes = [ handler.create_handler(serv_addr, global_threshs,
                           prio_tos, 
                           os.path.join(eval_dir_path, 
                             "fct_eval_{0}.log".format(
                             serv_addr[1])),
                           os.path.join(log_dir_path,
                             "log_file_{0}.log".format(
                             serv_addr[1])),
                             auto_state,
                             user_name
                                      ) for serv_addr in param_dict["addresses"]]


    # now try starting the server processes
    try:
      for server in server_processes:
        server.start_server()

    except Exception as exp:
      print("auto_core::main: {0}".format(exp))
      _stop_servers(server_processes)

    else:
      # successfully started all the servers
      # create a log file for the main process
      main_log = os.path.join(log_dir_path, "log_file_auto_main.log")
      _run_auto_proc(cs_address,       cs_period,
                     global_threshs,   auto_state,
                     server_processes, main_log,
                     ifc_info,         prio_tos)
      

    finally:
      print("auto_core::main has completed")

   

if __name__ == "__main__":

  # make sure that the process receives a dictionary of
  # parameters
  assert len(sys.argv) == 2, "Child process has not received a dictionary of parameters."
  main(sys.argv[1])
