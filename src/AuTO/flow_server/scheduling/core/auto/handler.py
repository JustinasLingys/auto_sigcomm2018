# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import


# Python std lib


try:
  import Queue
  import selectors2 as selectors

except ImportError:
  import queue as Queue
  import selectors
  xrange = range



import pwd
import grp
import os
import collections
import socket
import threading
import errno


from multiprocessing import Process
from multiprocessing import Event
from multiprocessing import Pipe
from multiprocessing import reduction



from builtins import int


# custom modules
from   scheduling                    import net
from   scheduling.core.scheduler     import PrioTuple
from   scheduling.core.scheduler     import PacketScheduler
from   scheduling.core.connection    import ConnState
from   scheduling.core.connection    import FlowMeasure
from   scheduling.core.connection    import RawWriteState
from   scheduling.core.connection    import RawReadState
from   scheduling.core.auto.features import CompletedFlow
from   scheduling                    import core
import scheduling.utils.logging      as utl_log

class SchedulerAuTO(PacketScheduler):
  """
    Class for encapsulating the state of an ongoing
    flow. The class produces instances which are used
    by 'net.common.prio_send' to send packets of a flow.
  """



  """
    Params:

      flow_tuple    : same as qlas
      lowest_prio   : when need to register with the AuTO
                      as a long flow

      flow_info     : 5-tuple info
      flow_handle   : an instance of a FlowHandle that
                      handles enqueueing info of a flow
                      into the corrent queue.

      sockid        : socket id used by a handler
                      to refer to failed large/long flows
      conns         : long/large failed flows


  """
  def __init__(self,  flow_tuples, lowest_prio, 
                      flow_size,   flow_info,   
                      flow_handle, sockid,
                      conns):

    super(SchedulerAuTO, self).__init__("AuTO")

    self.sock_id    = sockid   # socket id
    self.failed_conns = conns  # for knowing if need to
                               # discrd large/long state
   
    # flow_tuples has the same meaning as in the qlas handler.
    self.mlfq_tuples = collections.deque(flow_tuples)

    # current flow priority
    self.curr_prio = int(flow_tuples[0][1])

    # bytes to send at the current prio
    self.rem_prio  = int(flow_tuples[0][0])

    # for checking if need to register
    self.kth_queue = lowest_prio 

    # discard the first tuple
    self.mlfq_tuples.popleft()

    self.long_handle = None
    self.flow_info   = flow_info
    self.tot_size    = flow_size
    self.auto_handle = flow_handle # AuTo specific info
    if self._is_long_flow():
      self.long_handle = flow_handle.enqueue_ac_long_flow({
                       "src_ip"   : flow_info.src_ip,
                       "src_port" : flow_info.src_port,
                       "dst_ip"   : flow_info.dst_ip,
                       "dst_port" : flow_info.dst_port,
                       "protocol" : flow_info.protocol,
                       "priority" : self.curr_prio
                                                          })

      # register as an active large/long flow
      self.failed_conns[self.sock_id] = self.long_handle

  """
    Get active flow handle.
  """
  def _get_long_handle(self):
    return self.long_handle


  """
    Get current flow priority
  """
  def get_priority(self):
    if self._is_long_flow():
      assert self._get_long_handle() != None, "SchedulerAuTO::get_priority: `_get_long_handle` returns `None`"
      return self.auto_handle.get_flow_priority(self._get_long_handle())
    else:
      return self.curr_prio


  """
    Return a named tuple: ("data_bytes", "ip_tos") for scheduling
    pakcets. Scheduler have a mapping from priority to ip_tos.
  """
  def get_tuple(self):
    if self.rem_prio > 0:
      return PrioTuple(data_bytes=int(self.rem_prio),
                       ip_tos=int(self.curr_prio))
    else:
      return None # signal the end of a flow

  """
    Update internal state on a successful send operation.
  """
  def update_scheduler(self, sent_bytes, tos_value):
    # update the remaining bytes to be sent
    # at the current priority
    self.rem_prio -= sent_bytes

    assert self.curr_prio == tos_value,\
             "SchedulerAuTO::update_state: self.curr_prio != tos_value ({0} != {1})".format(self.curr_prio, tos_value)

    if self.rem_prio <= 0 and self.mlfq_tuples:
      # a new priority queue has to be used
      self.rem_prio  = int(self.mlfq_tuples[0][0])
      self.curr_prio = int(self.mlfq_tuples[0][1])

      # pop the head
      self.mlfq_tuples.popleft()
      # check if need to register as a large/long flow
      if self._is_long_flow():

        # large/long flow
        self.long_handle = self.auto_handle.enqueue_ac_long_flow({
                       "src_ip"   : self.flow_info.src_ip,
                       "src_port" : self.flow_info.src_port,
                       "dst_ip"   : self.flow_info.dst_ip,
                       "dst_port" : self.flow_info.dst_port,
                       "protocol" : self.flow_info.protocol,
                       "priority" : self.curr_prio})


        # register as an active large/long flow
        self.failed_conns[self.sock_id] = self.long_handle
        
  

  """
    For checking when to register with the AuTO service.
  """
  def _is_long_flow(self):
    return (self.curr_prio == self.kth_queue)

  """
    Called by a handler to enqueue info of the flow.

    params:
      fct_val : fct value passed to the shared queue
  """
  def enqueue_compl_flow(self, fct_val):
    if self._is_long_flow():

      assert self._get_long_handle() != None, "SchedulerAuTO::enqueue_compl_flow: self.long_handle is `None`"
      self.auto_handle.enqueue_compl_long_flow(self.long_handle,
                        CompletedFlow(self.flow_info.src_ip,
                                      self.flow_info.src_port,
                                      self.flow_info.dst_ip,
                                      self.flow_info.dst_port,
                                      self.get_priority(),
                                      self.tot_size,
                                      fct_val,
                                      self.flow_info.protocol))

      # uregister this flow from handler's list of
      # active flows (possible failures)
      del self.failed_conns[self.sock_id]      

    else:
      self.auto_handle.enqueue_compl_short_flow(
                        CompletedFlow(self.flow_info.src_ip,
                                      self.flow_info.src_port,
                                      self.flow_info.dst_ip,
                                      self.flow_info.dst_port,
                                      self.get_priority(),
                                      self.tot_size,
                                      fct_val,
                                      self.flow_info.protocol))

      
    



  """
    A simple factory method for creating callbacks which are
    used for creating instances of the SchedulerAuTO class.
  """
  @classmethod
  def create_auto_scheduler(cls, flow_threshs, tos_vals, auto_ref):

    FlowTuple = collections.namedtuple("FlowTuple", ["src_ip",
                                                     "src_port",
                                                     "dst_ip",
                                                     "dst_port",
                                                     "protocol"])
  
    # create a function for creating schedulers
    def factory_func_auto(sock_id,
                          conns,
                          flow_size,
                          src_ip,
                          src_port,
                          dst_ip,
                          dst_port,
                          proto):

      # retrieve the latest AuTO MLFQ thresholds
      thr_values = flow_threshs.get_global_threshs()
  

      # make sure something has been received
      assert thr_values and len(thr_values) > 0, "factory_func_auto: no threshold values retrieved"      

      # similar to QLAS, generate gaps between two priorities
      thrsh_gaps_gammas    = [0]*len(thr_values)
      
      thrsh_gaps_gammas[0] = int(thr_values[0]) # first gap == threshold 1 
      
      for tmp_idx in xrange(1, len(thr_values), 1):
        # compute gammas for scheduling
        thrsh_gaps_gammas[tmp_idx] = int(thr_values[tmp_idx] - thr_values[tmp_idx-1])

      # done computing the values for every priority 
      # queue

      # thresholds  : [0, th1], (th1, th2], ...
      # ip tos vlas :  prio_1,     prio_2,  ... 

      rem_size   =  int(flow_size) # reference to total size
      prio_idx   =  0              # priority index
      sch_tuples =  []             # lists for sending
            

      while rem_size > 0 and prio_idx < len(thrsh_gaps_gammas):
        sch_tuples.append((int(min(rem_size, thrsh_gaps_gammas[prio_idx])),
                           int(tos_vals[prio_idx])))

        # update the priority index and remaining size
        rem_size -= thrsh_gaps_gammas[prio_idx]
        prio_idx += 1

      # enqueue the last bytes at the lowest priority
      if rem_size > 0:
        # a large flow
        sch_tuples.append((int(rem_size), int(tos_vals[-1])))
    

      # create an instance of the Scheduler class for sending
      # a flow
      return cls(tuple(sch_tuples), int(tos_vals[-1]),
                 flow_size,
                 FlowTuple(src_ip   = src_ip,
                           src_port = src_port,
                           dst_ip   = dst_ip,
                           dst_port = dst_port,
                           protocol = proto),
                 auto_ref.get_flows_handle(),
                 sock_id, conns)

    
    return factory_func_auto  
   


# Function for writing statistics to
# a disk for persistence.
def disk_io_func(stat_file, stat_queue):

  with open(stat_file, "a", buffering=-1) as stat_fd:
  
    while True:
      # only written when have data
      ret_stats = stat_queue.get(block=True)

      if ret_stats:
        # iterate and write
        for stat_line in ret_stats:
          stat_fd.write(stat_line)

        # make sure data has been flushed
        stat_fd.flush()
        os.fsync(stat_fd)

      else:
        break # received a signal to stop
              # processing statistics.



class AuTOHandler(Process):

  """
    A simple process class with a state shared with the 
    AuTO RL process. The AuTO RL process is the main process
    that creates/starts the handlers and communicates with
    the Central System.
  """

  def __init__(self, addr, prio_threshs, prio_tos,
               stat_file, log_file, state_ref,
               system_user):
    super(AuTOHandler, self).__init__()
    
    # handler attributes
    self.selector  = None
    self.ip_addr   = tuple(addr)
    self.comm_pipe = None  
    self.flag      = Event()

    self.ac_larges = {} # map for unregistering large
                        # failed flows

    self.sch_factory = SchedulerAuTO.create_auto_scheduler(prio_threshs,
                                                           prio_tos,
                                                           state_ref)

    self.auto_ref    = state_ref # for flushing info about
                                 # completed long flows

    self.can_accept = prio_tos[0] # can accept more connections
    self.conn_refs  = {}          # active connections


    # user who owns this process (may be 'root')
    self.proc_user  = str(system_user)


    # for logging information
    self.sys_logger = str(log_file)
    self.fcts       = collections.deque(maxlen=None)

    # an IO thread for writing FCT info into a file
    self.disk_queue  = Queue.Queue(maxsize=int(core.MAX_IO_QUEUE_LEN))
    self.disk_thread = threading.Thread(target=disk_io_func, 
                                        args=(stat_file, self.disk_queue))

    self.disk_thread.daemon = False


  """
    The run method is called when a new process is created and 
    is started. It implements the logic of the handler.
  """

  def run(self):
    # change the process user
    # the main process must be started with the
    # 'root' priveleges, but the server processes don't need
    # the 'root' priveleges.

    logger_obj = None

    try:
      
      # use the system user to own the process
      user_id  = pwd.getpwnam(self.proc_user).pw_uid
      group_id = grp.getgrnam(self.proc_user).gr_gid


      # remove all group priveleges
      os.setgroups([])

      # assign the process to a different user
      os.setgid(group_id)
      os.setuid(user_id)
     

      # get a logger for logging errors
      logger_obj = utl_log.create_logger(self.sys_logger)

    except Exception as exp:
      with open("_some_unexpected_exp.log", "a") as exp_fd:
        exp_fd.write("Error AuTOHandler::run: '{0}'\n".format(exp))

      return # don't process further

    else: # proceed further
      
      # logger supports the context manager protocol
      # use the logger with 'with' to ensure all log
      # messages are flushed to a disk before the program
      # ends

      with logger_obj:
      
        # build a socket from a file descriptor
        accept_sock = None 

        try:
          self.comm_pipe.poll(None)
          
          accept_sock = socket.fromfd(reduction.recv_handle(self.comm_pipe),
                                      socket.AF_INET, socket.SOCK_STREAM,
                                      socket.IPPROTO_TCP)

        except Exception as exp: 
          logger_obj("AuTOHandler::run: {0}".format(exp))
          accept_sock = None

        else: 
          # make sure the socket has its options set
          try:
            accept_sock.setsockopt(socket.SOL_SOCKET,
                                   socket.SO_REUSEADDR, 1)
            accept_sock.setsockopt(socket.IPPROTO_TCP,
                                   socket.TCP_NODELAY,  1)
      
            accept_sock.setsockopt(socket.IPPROTO_IP,  
                                   socket.IP_TOS,  int(self.can_accept))

            accept_sock.setblocking(False) # use a selector
            
          except Exception as exp_two:
            logger_obj("AuTOHandler::run: {0}".format(exp_two))
            try:
              accept_sock.shutdown(socket.SHUT_RDWR)
            except:
              pass
            finally:
              accept_sock.close()
              accept_sock = None

        finally: # the pipe has to be closed
          try:
            self.comm_pipe.close()
          except:
            pass
          finally:
            self.comm_pipe = None 


        # is the socket is not 'None', run the process
        if accept_sock:
          self.can_accept = True
          
          self.selector = selectors.DefaultSelector()
          self.selector.register(accept_sock, selectors.EVENT_READ,
                                 data=(self.accept_conn, None))

          # keep a global reference to the logger
          self.sys_logger = logger_obj

          # run the process
          self._run_server(accept_sock)

  """
    Method which runs the selector and multiplexes the events.
  """
  def _run_server(self, accept_sock):
 
 
    # start the IO thread  
    self.disk_thread.start()

    # set the flag
    self.flag.set()

    poll_delay = float(core.POLL_INTERVAL)
  

    try:
      while self.flag.is_set():
        events = self.selector.select(timeout=poll_delay)

        for key, mask in events:
          # key.data[0] - method which handles the event
          # key.data[1] - data required by the method
          callback = key.data[0]
          callback(key.fileobj, mask, key.data[1])

    except Exception as exp:
      self.sys_logger("AuTOHandler::_run_server: {0}".format(exp))

    finally:
      try:
        self.selector.unregister(accept_sock)
        accept_sock.shutdown(socket.SHUT_RDWR)
      except:
        pass
      finally:
        accept_sock.close()

        # release all the resources
        self._clean_up()

    self.sys_logger("AuTOHandler::_run_server: Completed.")


  """
    Method for starting a server process. The method ensures
    that a process either successfully bind to its address or
    raises an exception.
  """
  def start_server(self):
    
    # check if the address can be binded
    lnt_sock = None
    parent   = None
    child    = None

    try:
      lnt_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM,
                               socket.IPPROTO_TCP)
      
      # set socket options
      lnt_sock.setsockopt(socket.SOL_SOCKET,  socket.SO_REUSEADDR, 1)
      lnt_sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY,  1)
      
      # make the listening socket have the highest periority
      lnt_sock.setsockopt(socket.IPPROTO_IP,  socket.IP_TOS,  int(self.can_accept))
   

      # try to bind to the address
      lnt_sock.bind(self.ip_addr)
      
      # listen on the socket
      lnt_sock.listen(net.TG_SERVER_BACKLOG_CONN)

      # set the socket to non-blocking mode (use with a selector)
      lnt_sock.setblocking(False)

      # create a pair of pipes and pass the socket to 
      # a server process
      parent, child  = Pipe()
      self.comm_pipe = child

      reduction.send_handle(parent, lnt_sock.fileno(), self.pid) 

    except Exception as exp:
      raise exp
    
    else: 
      # stat a server process
      self.start()

    finally:
      # the parent process either way has to close the socket
      if lnt_sock:
        lnt_sock.close()

      if parent:
        parent.close()



  
  """
    Method is called when a process has to be signalled to be
    stopped.
  """
  def stop_server(self):
    self.flag.clear()
  



  """
    Method for cleaning up a failed connection or an active one
    for stopping the connection.
  """

  def _close_conn(self, sockfd):
  
    if sockfd is not None and sockfd.fileno() >= 0:
      try:
        self.selector.unregister(sockfd) 
      except:
        pass # just ignore

      finally:
        # check if a large flow
        map_key     = sockfd.fileno()
        long_handle = self.ac_larges.get(map_key, None)
        
        if long_handle is not None:
          # need to unregister a failed large/long
          # ongoing flow
          conn_ctx = self.conn_refs[map_key]
          self.auto_ref.enqueue_failed_long_flow(long_handle,
                          CompletedFlow(conn_ctx[1][0],
                                        conn_ctx[1][1],                                           conn_ctx[1][2],
                                        conn_ctx[1][3],
                                        0, 0, 0.0,
                                        conn_ctx[1][4]))

          # remove this faled conn from
          # `ac_larges`
          del self.ac_larges[map_key]
          # inform the `AuTO` process about this
          self.auto_ref.notify_completion()

        # remove the socket from the connection map 
        del self.conn_refs[map_key] 

        sockfd.close() # close the socket
        


  """
    Method for releasing all the resources.
  """
  def _clean_up(self):


    # close all the sockets
    all_socket_filenos = tuple(self.conn_refs)
    for sock_fd in all_socket_filenos:
      self._close_conn(self.conn_refs[sock_fd][0])
      
  
    # flush the remaining FCT info
    if self.fcts:
      self.disk_queue.put(tuple(self.fcts), block=True)    

    # send a signal to the IO thread to terminate.
    self.disk_queue.put(None, block=True)

    # wait the IO thread to terminate
    self.disk_thread.join()


  """
    Method for pasing FCT information to the disk thread.
  """
  def _flush_fcts(self):
    # try to pass a tuple of FCTs
    fct_batch = tuple(self.fcts)

    try:
      self.disk_queue.put(fct_batch, block=False)

    except Queue.Full:
      # don't discard the fcts
      pass

    else:
      # successfully enqueued a tuple of FCTs
      self.fcts.clear()






  """
    Method that is called when a new connection is required.
    @param: dummy : nothing (only used for callback compatibility)
  """ 
  def accept_conn(self, sockfd, mask, dummy):
  
    # new connection has been requested
    conn = None

    try:
      conn, peer_addr = sockfd.accept() # should not block

    except IOError as exp:
      if exp.errno == net.IO_OVERUSE_ERROR:
        self.can_accept = False # cannot accept more 


    else:

      # initialize the newly created socket
      try:
        conn.setsockopt(socket.SOL_SOCKET,  socket.SO_REUSEADDR, 1)
        conn.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY,  1)
         
        # make the socket non-blocking
        conn.setblocking(False)

      except:
        # close the socket and don't do anyhting else
        try:
          conn.shutdown(socket.SHUT_RDWR)
        except:
          pass
        finally:
          conn.close()

      else: # send the status of the new connection to
            # the client

        # keep a reference to the newly created socket
        sock_addr = conn.getsockname()
      
  

        self.conn_refs[conn.fileno()] = (conn, 
            (net.ipv4_str_to_native(sock_addr[0]),
             sock_addr[1],
             net.ipv4_str_to_native(peer_addr[0]),
             peer_addr[1],
             socket.IPPROTO_TCP))

        status_value = RawWriteState(None, 0, 
                       ConnState.ACCEPTED if self.can_accept\
                                          else ConnState.REJECTED)

        # send the status of the connection to the client
        self.selector.register(conn, selectors.EVENT_WRITE,
                               data=(self.send_status, status_value))



  """
    The method sends the status of a request to connect.
    If the server cannot accept more flows/connections,
    the server rejects it, else accepts.

    @param sockfd       :  socket file descriptor
    @param mask         :  selectors.EVENT mask 
    @param status_code  :  acceptance status

  """
  def send_status(self, sockfd, mask, status_code):
 
 
    # send status code to a client
    if status_code.get_op_state() == ConnState.ACCEPTED:
      status_data = net.TG_OK_CONNECTION
    else:
      status_data = net.TG_NO_CONNECTION

    raw_stat = status_data[status_code.get_data_offset()::1]
      
    try: 
      sent_bytes = sockfd.send(raw_stat, 0)
    except Exception as exp:
      self.sys_logger("QLASHandler::send_status:{0}".format(exp))
        
      # close the socket
      self._close_conn(sockfd)

    else:
      # need to make sure the entire status code has been sent
      if sent_bytes:
        status_code.set_data_offset(status_code.get_data_offset() + sent_bytes)
          
        # if done sending, change the state
        if status_code.get_data_offset() == net.TG_ESTABLISH_LEN:
          
          if status_code.get_op_state() == ConnState.ACCEPTED:
            self.selector.modify(sockfd, selectors.EVENT_READ,
                                 data=(self.read_conn,
                                RawReadState(ConnState.WAIT_META)))

          else:
            # close the connection
            self._close_conn(sockfd)

      else: # the connection has been closed by the other side
        self._close_conn(sockfd)



  """
    Method for dealing with sockets for reading data.

    @param sockfd      :  socket object
    @param mask        :  selectors.EVENT mask
    @param read_state  :  state for reading
  """
  def read_conn(self, sockfd, mask, read_state):
 
    raw_bytes = b"" # refernece to received data

    # determine which data is received
    if read_state.get_op_state() == ConnState.WAIT_META:
      try:
        # read flow metadata
        raw_bytes = sockfd.recv(net.TG_METADATA_SIZE -\
                                read_state.get_data_offset(), 0)
      except Exception as exp:
        self.sys_logger("QLASHandler::read_conn: {0}".format(exp))
        self._close_conn(sockfd)

      else:
        # check if any data has been read
        if raw_bytes: # 
          read_state.read_data(raw_bytes)

          if read_state.get_data_offset() == net.TG_METADATA_SIZE:
         
            # fully read metadata
            fmeta = net.decode_metadata(read_state.get_raw_data())

            assert fmeta != None, "AuTOHandler::read_conn: decode_metadata produces 'None'"
            conn_ctx = self.conn_refs[sockfd.fileno()]
             

            # create an instance of a scheduler
            flow_handle = net.FlowInfo(
                            self.sch_factory(
                              sockfd.fileno(),
                              self.ac_larges,
                              fmeta.get_flow_size(),
                              conn_ctx[1][0],
                              conn_ctx[1][1],
                              conn_ctx[1][2],
                              conn_ctx[1][3],
                              conn_ctx[1][4]), 
                            fmeta)

            # create a wrapper for the flow
            fct_mes = FlowMeasure(flow_handle, ConnState.FLOW)

            # try to write as soon as possible
            self._immediate_write(sockfd, fct_mes)

            
        else: # connection has been closed
          self._close_conn(sockfd)

    elif read_state.get_op_state() == ConnState.WAIT_FCT:
    
      # waiting for a client to send a signal that it 
      # has fully received a flow
      try:
        raw_bytes = sockfd.recv(net.TG_TERM_SIZE - read_state.get_data_offset(), 0)

      except Exception as exp:
        # cannot read a connection
        self.sys_logger("OSJFHandler::read_conn: {0}".format(exp))
        self._close_conn(sockfd)

      else:
      
        if raw_bytes: # read some data
          read_state.read_data(raw_bytes)
       
          if read_state.get_data_offset() == net.TG_TERM_SIZE:
          
            # done with this flow
            read_state.end_flow() # record the end of the flow
            # get reference to the auto scheduler and mark it 
            # as completed
            auto_flow = read_state.get_flow_metadata().get_scheduler()
            assert auto_flow != None, "AuTOHandler::read_conn: auto_flow is `None`"
            auto_flow.enqueue_compl_flow(read_state.get_fct())
            self.fcts.append(read_state.get_measure_line())

            # check if need to flush some statistics to the
            # IO thread
            if len(self.fcts) >= core.MAX_IO_QUEUE:
              # time to flush some statistics
              self._flush_fcts()

            # change the state of the connection so that
            # the same connection can be reused for a new flow
            read_state.clear()
            read_state.set_op_state(ConnState.WAIT_META)

            # try to flush all completef large/long flows
            self.auto_ref.notify_completion()
          

        else: # socket has been closed
          self._close_conn(sockfd)
            
    else: # something wrong with states
      self.sys_logger("QLAS::read_conn: Wrong connection state: {0}".format(read_state.get_op_state()))
      self._close_conn(sockfd) # close this connection  



  """
    Method for starting to send the data of a flow.

    @param sockfd    :  socket file descriptor
    @param fct_state :  object for writing data
  """
  def _immediate_write(self, sockfd, fct_state):

    fct_state.set_op_state(ConnState.FLOW) # sending flow data
    fct_state.start_flow() # beginning to send a flow

    completed_flow = False # done sending data

    try:
      completed_flow = net.prio_send(sockfd, fct_state.meta)
    except net.CommonSocketError as exp:
      if exp.errno == errno.EAGAIN or exp.errno == errno.EWOULDBLOCK:
        # wait for another chance to write
        self.selector.modify(sockfd, selectors.EVENT_WRITE,
                                     data=(self.write_conn,
                                           fct_state))

        
      else: # some other excpetion
        self._close_conn(sockfd)

    else:
      # need to check of the flow has completed
      if completed_flow:
      
        # modify the state
        fct_state.set_data_offset(0) # new read operation
        fct_state.set_op_state(ConnState.WAIT_FCT)
        self.selector.modify(sockfd, selectors.EVENT_READ,
                             data=(self.read_conn, fct_state))

      else:
        # wait for another chance to write
        self.selector.modify(sockfd, selectors.EVENT_WRITE,
                                     data=(self.write_conn,
                                           fct_state))

 
          

  """
    Method for continuation of sending a flow data. This
    method is called when a socket is capable of sending data
    again. It differs from the _immediate_write in such a 
    way that the method handles a previously blocked flow.

    @param sockfd     : socket descriptor
    @param mask       : selectors.EVENT
    @param flow_state : flow context

  """
  def write_conn(self, sockfd, mask, flow_state):
  
    # just try send more data
    completed_flow = False # done sending data

    try:
      completed_flow = net.prio_send(sockfd, flow_state.meta)
    except net.CommonSocketError as exp:
      if exp.errno != errno.EAGAIN and exp.errno != errno.EWOULDBLOCK:      
        self._close_conn(sockfd) # something bad happened
    else:
      # need to check if the flow has completed
      
      if completed_flow:
        # modify the state
        flow_state.set_data_offset(0) # new READ
        flow_state.set_op_state(ConnState.WAIT_FCT)
        self.selector.modify(sockfd, selectors.EVENT_READ,
                             data=(self.read_conn, flow_state))
 

      




# Function which creates instances of the AuTOHandler class.
#
# params:
#
# ip_addr     : address tuple for a server process to bind to
# prio_thresh : priority thresholds (instead of values object)
# prio_tos    : priority tos values
# fct_file    : file for recording flow completion time
# log_file    : file for logging errors
# state_obj   : object for passing flow information
# proc_user   : system user who owns the server processes
def create_handler(ip_addr,  prio_threshs, 
                   prio_tos, fct_file,
                   log_file, state_obj,
                   proc_user
                   ):
  return AuTOHandler(ip_addr,  prio_threshs,
                     prio_tos, fct_file,
                     log_file, state_obj,
                     proc_user)
