# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import



from builtins import int




# Python std lib
import time
import collections


# Custom Python modules
from scheduling.net import common


"""
Factory for creating named tuples for sending data.
Intances of such class are returned by the 'get_tuple'
method of the PacketScheduler class.
"""
PrioTuple = collections.namedtuple("PrioTuple", ["data_bytes", "ip_tos"])


class PacketScheduler(object):

  """
    Interface for scheduler to send packets to remote hosts.
    The schedulers maintain information about flows and provide
    information for the 'net.common.prio_send' function to
    send data packets.
  """
  def __init__(self, cl_type):
    self.sch_name = str(cl_type)

  """
    Return the current scheduler-specific priority of a flow.
    Usually, it is a IPv4 DSCP/ToS value.
  """
  def get_priority(self):
    raise NotImplementedError("PacketScheduler::get_priority")

  """
    Return a named tuple: ("data_bytes", "ip_tos") for scheduling
    packets. Schedulers have a mapping from priority to ip_tos.
  """
  def get_tuple(self):
    raise NotImplementedError("PacketScheduler::get_tuple")

  """
    'net.common.prio_send' provides information about successful
     sent number of bytes and IPv4 ToS value for an instnace of 
     the class to update its internal state. 

     params:
       send_bytes : how many bytes have been sent
       tos_value  : IPv4 ToS header value
  """
  def update_scheduler(self, send_bytes, tos_value):
    raise NotImplementedError("PacketScheduler::update_scheduler")


  """
    Return the type string of the scheduler (e.g., "QLAS", "QSJF", "AuTO")
  """
  def get_type(self):
    return self.sch_type 
