# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import



import sys
import signal
import ast
import os
import os.path


# include the very top directory of scheduling
sys.path.insert(1, os.path.abspath(os.path.join(__file__, os.pardir,
                                               os.pardir, os.pardir,
                                               os.pardir)))

# Custom modules
from scheduling.core.qlas import handler



# Stop server processes
def _stop_servers(serv_procs):

  for one_proc in serv_procs:
    one_proc.stop_server()


  # wait for all the processes to terminate
  for one_proc in serv_procs:
    if one_proc.is_alive():
      one_proc.join()




def _signal_handler(signum, frame):
  print("_signal_handler has been called")

# Function suspends the current process to wait
# for a signal to terminate 
def _wait_for_signal(serv_procs):

  # register signal handlers
  signal.signal(signal.SIGTERM, _signal_handler)
  signal.signal(signal.SIGHUP,  _signal_handler)
  signal.signal(signal.SIGSEGV, _signal_handler)

  # suspend the current process and wait for
  # any signal
  signal.pause()


  # stop all the server processes
  _stop_servers(serv_procs)
  

def main(params):


  # convert a string into a Python dictionary
  param_dict = ast.literal_eval(params)

  print("A child process has been successfully started.")

  log_dir_path  = str(param_dict["log_dir"])  # logging directory
  eval_dir_path = str(param_dict["eval_dir"]) # evaluation directory
  prio_threshs  = tuple(param_dict["demotion_thresholds"])
  prio_tos      = tuple(param_dict["ipv4_tos"]) # IPv4 TOS values



  # create servers (port numbers must be unique)
  server_processes = [ handler.create_handler(
                         serv_addr, prio_threshs, prio_tos,
                             os.path.join(eval_dir_path, 
                             "fct_eval_{0}".format(serv_addr[1])),
                             os.path.join(log_dir_path,
                             "log_file_{0}".format(serv_addr[1])))\
                       for serv_addr in param_dict["addresses"] ]  




  # now try starting the server processes
  try:
    for server in server_processes:
      server.start_server()

  except Exception as exp:
    print("qlas_core::main: {0}".format(exp))
    _stop_servers(server_processes)

  else:
    # successfully started all the servers
    _wait_for_signal(server_processes)


  finally:
    print("qlas_core::main has completed")

   

if __name__ == "__main__":

  # make sure that the process receives a dictionary of
  # parameters
  assert len(sys.argv) == 2, "Child process has not received a dictionary of parameters."
  main(sys.argv[1])
