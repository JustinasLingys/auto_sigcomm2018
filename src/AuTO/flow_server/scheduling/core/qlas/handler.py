# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import


# Dependencies
from builtins  import int

try:
  xrange
except NameError:
  xrange = range

# Python std library
import os
import os.path
import socket
import errno
import threading
import collections
import sys


# Append some path values
sys.path.insert(1, os.path.abspath(os.path.join(__file__,  os.pardir,
                                                os.pardir, os.pardir,
                                                os.pardir)))


try:
  import Queue
except ImportError:
  import queue as Queue



from multiprocessing import Process
from multiprocessing import Pipe
from multiprocessing import reduction
from multiprocessing import Event


try:
  import selectors
except ImportError:
  import selectors2 as selectors



# Custom Python modules
from   scheduling.core.connection import ConnState
from   scheduling.core.connection import FlowMeasure
from   scheduling.core.connection import RawWriteState
from   scheduling.core.connection import RawReadState
from   scheduling.core.scheduler  import PrioTuple
from   scheduling.core.scheduler  import PacketScheduler
from   scheduling                 import core  
import scheduling.utils.logging       as utl_log


# Import network related structures and constants
from   scheduling import net




class SchedulerQLAS(PacketScheduler):
  """
    Class for encapsulating the state of an ongoing
    flow. The class produces instances which are used
    by 'net.commom.prio_send' to send packets of a flow.
  """

  """
    Since the size of a flow is known beforehand, priority
    is explicitly passed into the constructor
    (the flow is assigned with a lower priority once some bytes
     have been sent).
  """
  def __init__(self, flow_tuples):
  
    super(SchedulerQLAS, self).__init__("QLAS")
 
    # flow_tuples contains a list of tuples: [[thr_value, ip_tos], ...].
    # A flow is sent at some priority until the threshold value is
    # passed.
    self.mlfq_tuples = collections.deque(flow_tuples)
   
    # flow priority value
    self.curr_prio = int(flow_tuples[0][1])
    # bytes to send at the current priority
    self.rem_prio  = int(flow_tuples[0][0])

    # discard the first tuple
    self.mlfq_tuples.popleft()

  """
    Provide the current flow priority.
  """
  def get_priority(self):
    return self.curr_prio



  """
    Return a tuple: [send_bytes at this IP_TOS value]
  """
  def get_tuple(self):
    if self.rem_prio > 0:
      return PrioTuple(data_bytes=int(self.rem_prio), 
                       ip_tos=int(self.curr_prio))
    else:
      return None # signal the end of a flow


  """
    Interface implementation.
  """
  def update_scheduler(self, sent_bytes, tos_value):

    # update the remaining bytes to be sent
    # in the current priority
    self.rem_prio -= sent_bytes

    assert self.curr_prio == tos_value,\
             "SchedulerQLAS::update_state: self.curr_prio != tos_value ({0} != {1})".format(self.curr_prio, tos_value)

    if self.rem_prio <= 0 and self.mlfq_tuples:
     
      # a new priority queue has to be used
      self.rem_prio  = int(self.mlfq_tuples[0][0])
      self.curr_prio = int(self.mlfq_tuples[0][1])

      # pop the first item
      self.mlfq_tuples.popleft()
        


  """
    A simple factory method for creating callbacks which are
    used for creating instances of the SchedulerQLAS class.
  """
  @classmethod
  def create_qlas_scheduler(cls, flow_threshs, tos_vals):
    # create a function for creating schedulers.
    # Instances of SchedulerQLAS are created.

    # generate gaps between two priority classes
    thrsh_gaps_gammas    = [0]*len(flow_threshs)

    thrsh_gaps_gammas[0] = int(flow_threshs[0]) # first gap == threshold 1
    
    for tmp_idx in xrange(1, len(flow_threshs), 1):
      # compute gammas for scheduling
      thrsh_gaps_gammas[tmp_idx] = int(flow_threshs[tmp_idx] - flow_threshs [tmp_idx-1]) 


    # This function generates instanes of SchedulerQLAS
    # which are later used by net.prio_send for sending flows.
    def factory_func_qlas(flow_size):
      # thresholds : [0, th1], (th1, th2], ...
      # ip tos vlas:  prio_1,    prio_2,   ...

      rem_size   = int(flow_size) # reference to total size
      prio_idx   = 0              # priority index for looping
      sch_tuples = []             # list used by scheduling instances
                                  # to schedule flows
      
      


      while rem_size > 0 and prio_idx < len(thrsh_gaps_gammas):
        sch_tuples.append((int(min(rem_size, thrsh_gaps_gammas[prio_idx])),                          int(tos_vals[prio_idx])))

        # update the priority index and remaining size
        rem_size -= thrsh_gaps_gammas[prio_idx]
        prio_idx += 1

    
      # enqueue the last bytes at the lowest priority
      if rem_size > 0:
        sch_tuples.append((int(rem_size), int(tos_vals[-1])))

     
      # create an instance of the SchedulerQLAS class
      return cls(sch_tuples)

    return factory_func_qlas
    


# Function for writing statistics to 
# a disk for persistence.
def disk_io_func(stat_file, stat_queue):

  with open(stat_file, "a", buffering=-1) as stat_fd:
    
    while True:
      # only written when have data
      ret_stats = stat_queue.get(block=True)

      if ret_stats:
        # iterate and write
        for stat_line in ret_stats:
          stat_fd.write(stat_line)

        # make sure data has been flushed
        stat_fd.flush()
        os.fsync(stat_fd)

      else:
        break # received a signal to stop
              # processing statistics.



class QLASHandler(Process):
  """
    Class which encapsulates the processing of 
    requests. The class uses asynchronous handling
    in order to make the system scalable.
  """
  
  def __init__(self, addr, prio_threshs, prio_tos,
                     stat_file, log_file):
    super(QLASHandler, self).__init__()

    # handler attributes
    self.selector  = None
    self.ip_addr   = tuple(addr)
    self.comm_pipe = None 
    self.flag      = Event() # signal for terminating this process

    self.sch_factory = SchedulerQLAS.create_qlas_scheduler(prio_threshs,
                                                           prio_tos)

    self.can_accept = True # can accept more connections 
    self.conn_refs  = {}   # active connections
  

        
    # for logging information
    self.sys_logger  = str(log_file)
    self.fcts        = collections.deque(maxlen=None)
    
    # an IO thread for writing FCT info into a file
    self.disk_queue  = Queue.Queue(maxsize=int(core.MAX_IO_QUEUE_LEN))
    self.disk_thread = threading.Thread(target=disk_io_func,
                         args=(stat_file, self.disk_queue))    


    self.disk_thread.daemon = False


  



  """
    Try to start a server process. If any failure occurs, an 
    excpetion is thrown to the caller.
  """
  def start_server(self):
 
    # check if the address can be binded
    lnt_sock = None
    parent   = None
    child    = None
    
    try:
      lnt_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM,
                               socket.IPPROTO_TCP)

      # set socket options
      lnt_sock.setsockopt(socket.SOL_SOCKET,  socket.SO_REUSEADDR, 1)
      lnt_sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY,  1)

      # try to bind to the address
      lnt_sock.bind(self.ip_addr)

      # listen on the socket
      lnt_sock.listen(net.TG_SERVER_BACKLOG_CONN)

      # set the socket to non-blocking mode (use with a selector)
      lnt_sock.setblocking(False)

      # create a pair of pipes and pass the socket to a server
      # process

      parent, child  = Pipe()
      self.comm_pipe = child
      reduction.send_handle(parent, lnt_sock.fileno(), self.pid)

    except Exception as exp:
      raise exp

    else:
      # start a server process
      self.start()

    finally:
      # the parent process either way has to close the socket
      if lnt_sock:
        lnt_sock.close()

      if parent:
        parent.close()


  """
    Stop the server process.
  """ 
  def stop_server(self):
    self.flag.clear()
 

  
  """
    The run method is called when a new process is created
    and is started. It implements the logic of the handler. 
  """
  def run(self):

    logger_obj = None
    
    try:
      # get a logger for logging errors
      logger_obj = utl_log.create_logger(self.sys_logger)
    except Exception as exp:
      with open("_some_random_exp", "a") as exp_fd:
        exp_fd.write("Error QLASHandler::run: '{0}'".format(exp))
      return # if cannot create a logger, just terminate

    else: # proceed further

      # logger supports the context manager protocol so
      # it should be used with 'with'.

      with logger_obj:

        # build a socket from a file descriptor
        accept_sock = None

        try:
          self.comm_pipe.poll(None)

          accept_sock = socket.fromfd(reduction.recv_handle(self.comm_pipe),
                                      socket.AF_INET, socket.SOCK_STREAM,
                                      socket.IPPROTO_TCP)

        except Exception as exp:
          logger_obj("QLASHandler::run: {0}".format(exp))
          accpept_sock = None # failed to build a socket
        


        else:
          # make sure the socket has its options set
    
          try:
            accept_sock.setsockopt(socket.SOL_SOCKET,
                                   socket.SO_REUSEADDR, 1)
            accept_sock.setsockopt(socket.IPPROTO_TCP,
                                   socket.TCP_NODELAY,  1)

            accept_sock.setblocking(False)

          except Exception as exp_two:
            logger_obj("QLASHandler::run: {0}".format(exp_two))
            try:
              accept_sock.shutdown(socket.SHUT_RDWR)

            except:
              pass

            finally:
              accept_sock.close()
              accept_sock = None

        finally: # close the inter-process pipe
          try:
            self.comm_pipe.close()
          except:
            pass
          finally:
            self.comm_pipe = None


        # run the created server process if the socket has been
        # initialized
        if accept_sock:
          self.selector = selectors.DefaultSelector()
          self.selector.register(accept_sock, selectors.EVENT_READ,
                                 data=(self.accept_conn, None))   


          # keep a global reference to the logger
          self.sys_logger = logger_obj

          # run the process
          self._run_server(accept_sock)
  

  """
    Method which runs the selector and multiplexes the events.
  """
  def _run_server(self, accept_sock):
  
    # start the IO thread
    self.disk_thread.start()

    # set the flag
    self.flag.set()

    poll_delay = float(core.POLL_INTERVAL)

    try:
      while self.flag.is_set():
        events = self.selector.select(timeout=poll_delay)
       
        for key, mask in events:
       
          # key.data[0] - method which handles the event
          # key.data[1] - data required by the method
          callback = key.data[0]
          callback(key.fileobj, mask, key.data[1])

    except Exception as exp:
      self.sys_logger("OSJFHandler::_run_server: {0}".format(exp))

    finally:
      try:
        self.selector.unregister(accept_sock)
        accept_sock.shutdown(socket.SHUT_RDWR)
      except:
        pass
      finally:
        accept_sock.close()

        # release all the resources
        self._clean_up()


    self.sys_logger("QLASHandler::_run_server: Completed.")
        


  """
    Method for cleaning up a failed connection or an active one
    for stopping the connection.
  """

  def _close_conn(self, sockfd):
  
    if sockfd and sockfd.fileno() >= 0:
      try:
        self.selector.unregister(sockfd) 
      except:
        pass # just ignore

      finally:
        # remove the socket from the connection map
        try:
          del self.conn_refs[sockfd.fileno()]
        except:
          pass # there should be no excpetion
        finally:
          sockfd.close() # close the socket
        


  """
    Method for releasing all the resources.
  """
  def _clean_up(self):


    # close all the sockets
    all_socket_filenos = tuple(self.conn_refs)
    for sock_fd in all_socket_filenos:
      self._close_conn(self.conn_refs[sock_fd])
      
  
    # flush the remaining FCT info
    if self.fcts:
      self.disk_queue.put(tuple(self.fcts), block=True)    

    # send a signal to the IO thread to terminate.
    self.disk_queue.put(None, block=True)

    # wait the IO thread to terminate
    self.disk_thread.join()


  """
    Method for pasing FCT information to the disk thread.
  """
  def _flush_fcts(self):
    # try to pass a tuple of FCTs
    fct_batch = tuple(self.fcts)

    try:
      self.disk_queue.put(fct_batch, block=False)

    except Queue.Full:
      # don't discard the fcts
      pass

    else:
      # successfully enqueued a tuple of FCTs
      self.fcts.clear()




  """
    Method that is called when a new connection is required.
    @param: dummy : nothing (only used for callback compatibility)
  """ 
  def accept_conn(self, sockfd, mask, dummy):
  
    # new connection has been requested
    conn = None

    try:
      conn, addr = sockfd.accept() # should not block

    except IOError as exp:
      if exp.errno == net.IO_OVERUSE_ERROR:
        self.can_accept = False # cannot accept more 


    else:
      # initialize the newly created socket
      try:
        conn.setsockopt(socket.SOL_SOCKET,  socket.SO_REUSEADDR, 1)
        conn.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY,  1)
         
        # make the socket non-blocking
        conn.setblocking(False)

      except:
        # close the socket and don't do anyhting else
        try:
          conn.shutdown(socket.SHUT_RDWR)
        except:
          pass
        finally:
          conn.close()

      else: # send the status of the new connection to
            # the client

        # keep a reference to the newly created socket
        self.conn_refs[conn.fileno()] = conn

        status_value = RawWriteState(None, 0, 
                       ConnState.ACCEPTED if self.can_accept\
                                          else ConnState.REJECTED)

        # send the status of the connection to the client
        self.selector.register(conn, selectors.EVENT_WRITE,
                               data=(self.send_status, status_value))



  """
    The method sends the status of a request to connect.
    If the server cannot accept more flows/connections,
    the server rejects it, else accepts.

    @param sockfd       :  socket file descriptor
    @param mask         :  selectors.EVENT mask 
    @param status_code  :  acceptance status

  """
  def send_status(self, sockfd, mask, status_code):
  
    # send status code to a client
    if status_code.get_op_state() == ConnState.ACCEPTED:
      status_data = net.TG_OK_CONNECTION
    else:
      status_data = net.TG_NO_CONNECTION

    raw_stat = status_data[status_code.get_data_offset()::1]
      
    try: 
      sent_bytes = sockfd.send(raw_stat, 0)
    except Exception as exp:
      self.sys_logger("QLASHandler::send_status:{0}".format(exp))
        
      # close the socket
      self._close_conn(sockfd)

    else:
      # need to make sure the entire status code has been sent
      if sent_bytes:
        status_code.set_data_offset(status_code.get_data_offset() + sent_bytes)
          
        # if done sending, change the state
        if status_code.get_data_offset() == net.TG_ESTABLISH_LEN:
          
          if status_code.get_op_state() == ConnState.ACCEPTED:
            self.selector.modify(sockfd, selectors.EVENT_READ,
                                 data=(self.read_conn,
                                RawReadState(ConnState.WAIT_META)))

          else:
            # close the connection
            self._close_conn(sockfd)

      else: # the connection has been closed by the other side
        self._close_conn(sockfd)



  """
    Method for dealing with sockets for reading data.

    @param sockfd      :  socket object
    @param mask        :  selectors.EVENT mask
    @param read_state  :  state for reading
  """
  def read_conn(self, sockfd, mask, read_state):
  
    raw_bytes = b"" # refernece to received data

    # determine which data is received
    if read_state.get_op_state() == ConnState.WAIT_META:
      try:
        # read flow metadata
        raw_bytes = sockfd.recv(net.TG_METADATA_SIZE -\
                                read_state.get_data_offset(), 0)
      except Exception as exp:
        self.sys_logger("QLASHandler::read_conn: {0}".format(exp))
        self._close_conn(sockfd)

      else:
        # check if any data has been read
        if raw_bytes: # 
          read_state.read_data(raw_bytes)

          if read_state.get_data_offset() == net.TG_METADATA_SIZE:
            # fully read metadata
            fmeta = net.decode_metadata(read_state.get_raw_data())

            assert fmeta, "QLAS::read_conn: decode_metadata produces 'None'"
            

            # create an instance of a scheduler
            flow_handle = net.FlowInfo(
                            self.sch_factory(fmeta.get_flow_size()),
                            fmeta)

            # create a wrapper for the flow
            fct_mes = FlowMeasure(flow_handle, ConnState.FLOW)
            # try to write as soon as possible
            self._immediate_write(sockfd, fct_mes)

            
        else: # connection has been closed
          self._close_conn(sockfd)

    elif read_state.get_op_state() == ConnState.WAIT_FCT:
      # waiting for a client to send a signal that it 
      # has fully received a flow
      try:
        raw_bytes = sockfd.recv(net.TG_TERM_SIZE -\
                                read_state.get_data_offset(), 0)

      except Exception as exp:
        # cannot read a connection
        self.sys_logger("OSJFHandler::read_conn: {0}".format(exp))
        self._close_conn(sockfd)

      else:
        if raw_bytes: # read some data
          read_state.read_data(raw_bytes)

          if read_state.get_data_offset() == net.TG_TERM_SIZE:
            # done with this flow
            read_state.end_flow() # record the end of the flow
            
            self.fcts.append(read_state.get_measure_line())

            # check if need to flush some statistics to the
            # IO thread
            if len(self.fcts) >= core.MAX_IO_QUEUE:
              # time to flush some statistics
              self._flush_fcts()

            # change the state of the connection so that
            # the same connection can be reused for a new flow
            read_state.clear()
            read_state.set_op_state(ConnState.WAIT_META)

        else: # socket has been closed
          self._close_conn(sockfd)
            
    else: # something wrong with states
      self.sys_logger("QLAS::read_conn: Wrong connection state: {0}".format(read_state.get_op_state()))
      self._close_conn(sockfd) # close this connection  



  """
    Method for starting to send the data of a flow.

    @param sockfd    :  socket file descriptor
    @param fct_state :  object for writing data
  """
  def _immediate_write(self, sockfd, fct_state):

    fct_state.set_op_state(ConnState.FLOW) # sending flow data
    fct_state.start_flow() # beginning to send a flow

    completed_flow = False # done sending data

    try:
      completed_flow = net.prio_send(sockfd, fct_state.meta)
    except net.CommonSocketError as exp:
      if exp.errno == errno.EAGAIN or exp.errno == errno.EWOULDBLOCK:
        # wait for another chance to write
        self.selector.modify(sockfd, selectors.EVENT_WRITE,
                                     data=(self.write_conn,
                                           fct_state))

        
      else: # some other excpetion
        self._close_conn(sockfd)

    else:
      # need to check of the flow has completed
      
      if completed_flow:
        # modify the state
        fct_state.set_data_offset(0) # prepare for a READ
        fct_state.set_op_state(ConnState.WAIT_FCT)
        self.selector.modify(sockfd, selectors.EVENT_READ,
                             data=(self.read_conn, fct_state))

      else:
        # wait for another chance to write
        self.selector.modify(sockfd, selectors.EVENT_WRITE,
                                     data=(self.write_conn,
                                           fct_state))

 
          

  """
    Method for continuation of sending a flow data. This
    method is called when a socket is capable of sending data
    again. It differs from the _immediate_write in such a 
    way that the method handles a previously blocked flow.

    @param sockfd     : socket descriptor
    @param mask       : selectors.EVENT
    @param flow_state : flow context

  """
  def write_conn(self, sockfd, mask, flow_state):
  
    # just try send more data
    completed_flow = False # done sending data

    try:
      completed_flow = net.prio_send(sockfd, fct_state.meta)
    except net.CommonSocketError as exp:
      if exp.errno != errno.EAGAIN and exp.errno != errno.EWOULDBLOCK:      
        self._close_conn(sockfd) # something bad happened
    else:
      # need to check of the flow has completed
      
      if completed_flow:
        # modify the state
        fct_state.set_data_offset(0) # prepare for a READ
        fct_state.set_op_state(ConnState.WAIT_FCT)
        self.selector.modify(sockfd, selectors.EVENT_READ,
                             data=(self.read_conn, fct_state))
 

      


# Function for creating a process for handling 
# flows.
#
# params:
#
#  ip_addr      : address tuple for a server process to bind to
#  prio_threshs : priority thresholds
#  prio_tos     : IPv4 TOS fields used for prioritizing flows
#  fct_file     : file for recording flow completion times
#  log_file     : file for logging errors
#
def create_handler(ip_addr, prio_threshs, 
                   prio_tos, fct_file, 
                   log_file 
                   ):
  return QLASHandler(ip_addr, prio_threshs, prio_tos,
                    fct_file, log_file)
