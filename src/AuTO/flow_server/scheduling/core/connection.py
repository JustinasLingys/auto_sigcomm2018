# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import



from builtins import int




# Python std lib
import time
import enum


# Custom Python modules
from scheduling.net   import common
from scheduling.utils import timing

@enum.unique
class ConnState(enum.Enum):
  """
    A simple class for maintaining the state of an ongoing flow.
  """
  WAIT_META = 1 # expecting to receive flow metadata
  WAIT_FCT  = 2 # expecting a signal of flow completion
  FLOW      = 3 # sending flow data
  REJECTED  = 4 # rejected a new connection due to system constraints
  ACCEPTED  = 5 # accpeted a new connection
  CLOSED    = 6 # closed connection



class IOOpState(object):
  """
    Class for storing an IO operation state. READ/WRITE operations
    inherit from this class.
  """
  def __init__(self, data=b"", data_offset=0,
                     state=ConnState.CLOSED):

    self.raw_data    = data
    self.data_offset = int(data_offset)
    self.state       = state



  """
    Return the current state of an IO operation.
  """
  def get_op_state(self):
    return self.state


  """
    Set the current state of an IO operation.
  """
  def set_op_state(self, new_state):
    self.state = new_state


  """
    Get data offset.
  """
  def get_data_offset(self):
    return self.data_offset


  """
    Set data offset.
  """
  def set_data_offset(self, new_offset):
    self.data_offset = new_offset


  """
    Get a reference to the data field.
  """
  def get_raw_data(self):
    return self.raw_data


  """
    Set the data field/
  """
  def set_raw_data(self, new_data):
    self.raw_data = new_data



  """
    Clear the maintained state
  """
  def clear(self):
    self.raw_data    = b""
    self.state       = ConnState.CLOSED
    self.data_offset = 0



class RawWriteState(IOOpState):
  """
    Class for raw byte WRITE operations.
  """
  def __init__(self, data=b"", start_offset=0,
                     state=ConnState.CLOSED):
    super(RawWriteState, self).__init__(data, start_offset,
                                        state)


class RawReadState(IOOpState):
  """
    Class for raw byte READ operations.
  """
  def __init__(self, read_state=ConnState.CLOSED):
    super(RawReadState, self).__init__(data=[], data_offset=0,
                                       state=read_state)


  """
    Read binary data into an internal buffer.
  """
  def read_data(self, binary_data):
    self.raw_data.append(binary_data)
    self.data_offset += len(binary_data)


  """
    Convert a buffer into a binary array.
  """
  def get_raw_data(self):
    return b"".join(self.raw_data)



  """
    Clear the state
  """
  def clear(self):
    super(RawReadState, self).clear()
    self.raw_data = []



class FlowMeasure(RawReadState):
  """
    Class for wrapping state of an ongoing flow.
    Instances of this class maintain flow performance
    measurements.
  """
  def __init__(self, fmeta, flow_state=ConnState.CLOSED):
    super(FlowMeasure, self).__init__(flow_state)

    self.meta     = fmeta
    self.beg_time = 0.0 # start time of a flow in microseconds
    self.end_time = 0.0 # end time of a flow in microseconds
    self.cdf_str  = ""  # CDF string value

    self._init_internal(fmeta)


  
  def _init_internal(self, fmeta):
    self.cdf_str  = common.get_cdf_string(fmeta.get_flow_cdf_index())
 

  """
   Method for receiving a referece to a FlowInfo metadata.
  """
  def get_flow_metadata(self):
    return self.meta


  """
    Method returns the size of the flow this measurement has 
    reference to (in bytes).
  """
  def get_flow_size(self):
    return self.meta.get_flow_size()

  """
    Method computes flow completion time in microseconds.
  """
  def get_fct(self):
    return (self.end_time - self.beg_time)

  """
    Method is called when a flow is started to be sent.
  """
  def start_flow(self):
    self.beg_time = timing.time_micros()
    self.end_time = float(self.beg_time)


  """
    Method is called when a flow is completely sent.
  """
  def end_flow(self):
    self.end_time = timing.time_micros()


  """
    Return network load value.
  """
  def get_net_load(self):
    return self.meta.get_flow_network_load()


  def get_cdf_string(self):
    return self.cdf_str


  """
    A nice string for storing statistics about this
    flow.
  """
  def get_measure_line(self):
    return "{0},{1},{2},{3},{4}\n".format(
                                      time.time(), # system time
                                      self.get_flow_size(),
                                      self.get_fct(), 
                                      self.get_net_load(),
                                      self.get_cdf_string())
 
  """
    Clear internal infomarion.
  """
  
  def clear(self):

    self.meta     = None
    self.beg_time = 0.0 # start time of a flow in microseconds
    self.end_time = 0.0 # end time of a flow in microseconds
    self.cdf_str  = ""  # CDF string value

    super(FlowMeasure, self).clear()


