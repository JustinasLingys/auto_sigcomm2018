# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import


try:
  basestring
except NameError:
  basestring = str


import os

class LogWrapper(object):
  """
    A wrapper class that makes sure that 
    a valid file descriptor is closed once the object
    is being destroyed.
  """
  
  def __init__(self):
    self._file_desc = None
    self._filename  = ""
    self._mode      = ""

  def init_log(self, filename, mode_str="a"):
    self._filename = filename
    self._mode     = mode_str


  def __enter__(self):
    if self._file_desc is None:
      self._file_desc = open(self._filename, mode="a",
                             buffering=-1)
    return self


  def __exit__(self, exc_type, exc_value, traceback):
    self._close_resources()


  def log_msg(self, msg, msg_level=None):

    if msg and isinstance(msg, basestring): # make sure it's not empty
      raw_data = msg if msg[-1] == "\n" else "".join([msg, "\n"])
    
      try: # try to write the message
        self._file_desc.write(raw_data)
      except:
        self._close_resources()



  def __call__(self, msg, msg_level=None):
    self.log_msg(msg, msg_level)


  def _close_resources(self):
    if self._file_desc:
      try:
        self._file_desc.flush()
        os.fsync(self._file_desc)
      except:
        pass
      finally:
        self._file_desc.close()
        self._file_desc = None


# Function opens a file for writing and ensures that
# the process can write to a disk.
def create_logger(filename):

  tmp_log = LogWrapper()

  tmp_log.init_log(filename)

  return tmp_log  
