from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import




try:
  from PyTiming.GS_timing import micros as time_micros
  from PyTiming.GS_timing import millis as time_millis

except ImportError:
  from .PyTiming.GS_timing import micros as time_micros
  from .PyTiming.GS_timing import millis as time_millis
