# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import


import platform
import multiprocessing


def _check_if_linux():
 
  sys_name = platform.system().lower()
  if "linux" in sys_name:
    return True
  else:
    return False

# Read the number of cpus from
# a system file
def _read_cpu_count():

  sys_conf = None
  cpu_num  = -1

  try:
    sys_conf = open("/proc/cpuinfo", "r")
    cpu_num  = sys_conf.read().count("processor\t:")
  except:
    cpu_num = -1
  finally:
    if sys_conf is not None:
      sys_conf.close()

    return cpu_num


# Read the speed of 'ifc_name' in Mbps
def _read_link_speed(ifc_name):
  
  sys_conf   = None
  link_speed = -1
  
  try:
    sys_conf = open("/sys/class/net/{0}/speed".format(ifc_name), "r")
    link_speed = int(sys_conf.read())
  except:
    link_speed = -1
  finally:
    if sys_conf is not None:
      sys_conf.close()

    return link_speed
  

# Utility function for returning the number of 
# logical cores on the platform
#
#
# Return a positive integer if it posible to determine
# number of cores. A negative integer, if it is not possible.
# 
def cpu_count():

  if not _check_if_linux():
    raise IOError("System is not Linux!")
  else:
    try:
      cpu_count = multiprocessing.cpu_count()
    except NotImplementedError:
      # try to retrive the number from the system files
      cpu_count = _read_cpu_count()
    finally:
      return cpu_count # may be negative 


# Utility function for reading link speed.
def link_speed(ifc_name):

  if not _check_if_linux():
    raise IOError("System is not Linux!")
  else:
    return _read_link_speed(ifc_name)
