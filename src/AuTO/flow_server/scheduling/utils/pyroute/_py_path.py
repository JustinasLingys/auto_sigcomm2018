# -*- coding: utf-8 -*- 

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import


# pyroute2 module 

import sys
import os
import os.path


# path to a slightly modified pyroute2 module
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), 
                                                os.pardir,
                                                os.pardir,
                                                os.pardir, 
                                                os.pardir,
                                                os.pardir,
                                                os.pardir,
                                                "lib",     
                                                "pyroute2_0.4.21")))

# import the moduel pyroute2
import pyroute2 as pyroute2
