# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import


# compatible int
from builtins import int

# import the moduel pyroute2
import socket

from scheduling.utils.pyroute._py_path import pyroute2


# Utility function to retrieve an interface
# index given an IPv4 address
def ipv4_to_if_index(ip_addr):
 
  # throw an exception on an error
  with pyroute2.IPRoute() as ipr:
    
    # retrieve information about all links
    links_info = ipr.get_addr(family=socket.AF_INET)

    for one_link in links_info:
      if ip_addr == one_link.get_attr("IFA_ADDRESS"):
        return int(one_link["index"])

    raise ValueError("No match found for `{0}`".format(ip_addr))



# Utility function to retrieve an interface
# label given an IPv4 address
def ipv4_to_if_label(ip_addr):
 
  # throw an exception on an error
  with pyroute2.IPRoute() as ipr:
     
    # retrieve information about all links
    links_info = ipr.get_addr(family=socket.AF_INET)

    for one_link in links_info:
      if ip_addr == one_link.get_attr("IFA_ADDRESS"):
        return one_link.get_attr("IFA_LABEL")


    raise ValueError("No match found for `{0}`".format(ip_addr))
