# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import


# Dependencies
from builtins import int
import six


# Python std lib
import collections
import socket
import threading


try:
  xrange
except NameError:
  # Python3 code
  xrange = range



# import pyroute2 from local path
from scheduling.utils.pyroute._py_path import pyroute2
from scheduling.utils.pyroute          import _tc_proc as sys_tc
from scheduling.utils                  import resources


# Acceptable protocol values
############# PROTOCOL VALUES ####################
PYR_IPv4 = pyroute2.protocols.ETH_P_IP
############# PROTOCOL VALUES ####################



# Internal handle returned by the 
# `schedule_flow` function on success
# This internal handle is later used by the
# `remove_flow` function to remove internal
# state maintained by module.

class _PyFilter(object):

  """
    Fields represent filter attributes
  """
  classid      = 0
  dsmarkid     = 0
  qdisc_filter = 0
  class_filter = 0
  rate         = 0
  ceil         = 0
  dscp         = 0

  def __init__(self, classid,       dsmarkid,
                     qdisc_filter,  class_filter,
                     dsmark_filter, rate,         
                     ceil,          dscp):

    self._classid       = classid
    self._dsmarkid      = dsmarkid
    self._qdisc_filter  = qdisc_filter
    self._class_filter  = class_filter
    self._dsmark_filter = dsmark_filter
    self._rate          = rate
    self._ceil          = ceil
    self._dscp          = dscp


  @property
  def classid(self):
    return self._classid


  @classid.setter
  def classid(self, classid):
    self._classid = classid


  @property
  def dsmarkid(self):
    return self._dsmarkid


  @dsmarkid.setter
  def dsmarkid(self, dsmarkid):
    self._dsmarkid = dsmarkid


  @property
  def qdisc_filter(self):
    return self._qdisc_filter


  @qdisc_filter.setter
  def qdisc_filter(self, qdisc_filter):
    self._qdisc_filter = qdisc_filter


  @property
  def class_filter(self):
    return self._class_filter


  @class_filter.setter
  def class_filter(self, class_filter):
    self._class_filter = class_filter


  @property
  def dsmark_filter(self):
    return self._dsmark_filter


  @dsmark_filter.setter
  def dsmark_filter(self, dsmark_filter):
    self._dsmark_filter = dsmark_filter


  @property
  def rate(self):
    return self._rate


  @rate.setter
  def rate(self, rate):
    self._rate = rate


  @property
  def ceil(self):
    return self._ceil


  @ceil.setter
  def ceil(self, ceil):
    self._ceil = ceil


  @property
  def dscp(self):
    return self._dscp


  @dscp.setter
  def dscp(self, dscp):
    self._dscp = dscp
 

  
# The PyrouteFlow class needed for scheduling a flow
PyrouteFlow = collections.namedtuple( "PyrouteFlow", 
                                    ["src_ip", 
                                     "src_port",
                                     "dst_ip", 
                                     "dst_port",
                                     "prot",   
                                     "dscp",
                                     "rate"],
                                    verbose=False)


class _FilterHandle(object):
  """
    A wrapper for generating a unique 
    filter id that can be used later for
    deleting the filter.
  """

  class TableState(object):
    """
      `tc` uses hash tables for storing filters.
       This class maintains a hash table state.
    """
    def __init__(self, handle, start_idx=1):
      self._handle  = handle
      self._nodeids = collections.deque(xrange(start_idx, 4096, 1))
      self._size    = len(self._nodeids)


    def get_filter(self):
      if len(self._nodeids) > 0:
        nodeid = self._nodeids.popleft()
        return (self._handle | nodeid)
      else:
        return None


    def reclaim_filter(self, filter_handle):
      assert filter_handle > 0 and filter_handle & 0xfff00000 == self.get_table_handle(), "TableState::reclaim_filter_handle: wrong handle (table_handle = {0} ,filter_handle = {1})".format(self.get_table_handle(), filter_handle)

      self._nodeids.append(filter_handle & 0x00000fff)

      return (self._size == len(self._nodeids))


    def clear(self):
      self._hanlde  = None
      self._nodeids = None
      self._size    = None

    def available(self):
      return (len(self._nodeids) > 0)

    def get_table_handle(self):
      return self._handle
      


  def __init__(self, if_name, table_handle=None,
                     alloc_new_tables=False):

    self.link_label   = if_name
    self.tables       = {}
    self.empty_tables = []
    self.alloc_tables = alloc_new_tables

    if table_handle is not None and isinstance(table_handle, int):
      self.tables[table_handle] = _FilterHandle.TableState(table_handle, 1)
      self.cur_handle           = table_handle 
      self.next_handle          = table_handle
      self._increment_next()
    else:
      self.cur_handle           = 0x00100000
      self.next_handle          = self.cur_handle
      self._increment_next()
      self.tables[self.cur_handle] = _FilterHandle.TableState(0x00100000, 1)
    


  def _compute_table_key(self, filter_id):
    return (filter_id & 0xfff00000)

  def _increment_next(self):
    self.next_handle += 0x00100000
    
    if self.next_handle > 0xfff00000:
      raise ValueError("_FilterHandle::_increment_next: handle has exceeded the maximum value of `0xfff00000`.")


  def reclaim_filter_handle(self, filter_handle):
    table_id = self._compute_table_key(filter_handle)
    if self.tables[table_id].reclaim_filter(filter_handle) and len(self.tables) > 1:
      # one table has become empty (check if other tables
      # have empty slots)
      sel_id = table_id

      for table_key, table_val in six.iteritems(self.tables):
        if table_val.available() and table_key != table_id:
          # found a hash table with slots
          sel_id = table_key
          break

      if sel_id != table_id: # found another table
        self.empty_tables.append((table_id, self.tables[table_id]))
        del self.tables[table_id]

      # update the current handle
      self.cur_handle = sel_id



  def get_filter_handle(self):
    hash_table = self.tables[self.cur_handle]
    new_filter = hash_table.get_filter()
    
    if new_filter is not None:
      return new_filter
    else:

      # this case shall never happen as it would
      # mean that a server has more than 4095 large/long
      # flows
      if not self.alloc_tables:
        raise ValueError("_FilterHandle::get_filter_handle: requires a new table allocation, but it is not allowed by default (Too many large/long flows).")

      # first, need to check if there are any pre-allocated
      # hash tables
      if self.empty_tables:
        self.cur_handle = self.empty_tables[-1][0]
        self.tables[self.empty_tables[-1][0]] = self.empty_tables[-1][1]
        self.empty_tables.pop() 
   
      else: 
        # need to create a new table to the htb qdisc 
        # and htb class

        # for htb qdisc (40:0)
        sys_tc.new_filter_table(if_name=self.link_label, 
                                handle=hex(self.next_handle),
                                prio="10",
                                parent="40:0",
                                ip_field="protocol",
                                match_val="{0}".format(hex(socket.IPPROTO_TCP)),
                                match_mask="0xff")

        # for htb class (40:1)
        sys_tc.new_filter_table(if_name=self.link_label, 
                                handle=hex(self.next_handle),
                                prio="10",
                                parent="40:1",
                                ip_field="dsfield",
                                match_val="0x00",
                                match_mask="0x00")

        # no exception has occurred, create a new
        # state for the newly created hash tables
        self.tables[self.next_handle] = _FilterHandle.TableState(self.next_handle, 1)
        self.cur_handle = self.next_handle
        self._increment_next()

      # must contain some ids
      return self.tables[self.cur_handle].get_filter()
  

  def set_link_label(self, if_label):
    self.link_label = if_label

  



# Internally maintained structure for recording all 
# scheduled flows
_sch_lock   = threading.Lock() # for locking global structures
_PyFlowMap  = {}
_PyClassid  = collections.deque(xrange(100, 65536, 1))
_PyFilterid = None
_PyRoute    = None
_IfcIndex   =  -1
_IfcLabel   = ""
_LinkSpeed  = -1 # Mbps

# Global dict for mapping `dscp` values to 
# `dsmark` classes
_DSMARK_MAP = { 0x80 : "0x0001",
                0x40 : "0x0002",
                0x20 : "0x0003",
                0x00 : "0x0004" }


# Utility function for creating a string used to refer
# to active flows
def create_py_flow_key(py_flow):
  return "{0}:{1}:{2}:{3}:{4}".format(
                                 hex(py_flow.src_ip).rstrip("L"), 
                                 hex(py_flow.src_port).rstrip("L"),
                                 hex(py_flow.dst_ip).rstrip("L"), 
                                 hex(py_flow.dst_port).rstrip("L"),
                                 hex(py_flow.prot).rstrip("L"))


# Utility function for creating new filters for
# scheduling flows
def _add_filter(ifc_idx,   htb_id, 
                filter_id, flow_info):


  global _PyRoute, _DSMARK_MAP, _LinkSpeed
  
  # compute hexadecimal strings
  hex_classid = hex(htb_id).rstrip("L")  
  
  # make sure rate is not too large
  rate_val = int(min(flow_info.rate, _LinkSpeed))

 
  # try to add  new classes and new filters
  _PyRoute.tc("add-class", "htb", ifc_idx,
              "40:{0}".format(hex_classid),
               parent="40:1", rate="{0}mbit".format(rate_val),
               ceil="{0}mbit".format(rate_val),
               prio=2)
 

  # add the dsmark queueing discipline for
  # marking packets
  _PyRoute.tc("replace", "dsmark", ifc_idx,
              "{0}:0".format(hex_classid),
               parent="40:{0}".format(hex_classid),
               indices=0x08, default_index=0x04)
  

  # set all marks for classes (dsmark classes)
  # `dsmark` changes Ipv4 dsfields according to the
  # `dscp` value 
  _PyRoute.tc("change-class", "dsmark", ifc_idx,
              "{0}:0x0001".format(hex_classid), 
               parent="{0}:0".format(hex_classid),
               mask=0x03, value=0x80)

  _PyRoute.tc("change-class", "dsmark", ifc_idx,
              "{0}:0x0002".format(hex_classid), 
               parent="{0}:0".format(hex_classid),
               mask=0x03, value=0x40)
  
  _PyRoute.tc("change-class", "dsmark", ifc_idx,
              "{0}:0x0003".format(hex_classid), 
               parent="{0}:0".format(hex_classid),
               mask=0x03, value=0x20)

  _PyRoute.tc("change-class", "dsmark", ifc_idx,
              "{0}:0x0004".format(hex_classid), 
               parent="{0}:0".format(hex_classid),
               mask=0x03, value=0x00)


  # eventually, add the filters 
  # `htb` queueing discipline filter
  _PyRoute.tc("add-filter", "u32",
               ifc_idx,     filter_id,
               parent="40:0",
               protocol=pyroute2.protocols.ETH_P_IP,
               prio=10,
               ht=filter_id & 0xfff00000,
               target="40:1",
       keys=["{0}/0xffffffff+12".format(hex(flow_info.src_ip).rstrip("L")),
             "{0}/0xffffffff+16".format(hex(flow_info.dst_ip).rstrip("L")),
             "{0}/0xffff+20".format(hex(flow_info.src_port).rstrip("L")),
             "{0}/0xffff+22".format(hex(flow_info.dst_port).rstrip("L")),
             "{0}/0x00ff+8".format(hex(flow_info.prot).rstrip("L"))])


  # root `htb` class for sharing  rate
  _PyRoute.tc("add-filter", "u32",
               ifc_idx,     filter_id,
               parent="40:1",
               protocol=pyroute2.protocols.ETH_P_IP,
               prio=10,
               ht=filter_id & 0xfff00000,
               target="40:{0}".format(hex_classid),
       keys=["{0}/0xffffffff+12".format(hex(flow_info.src_ip).rstrip("L")),
             "{0}/0xffffffff+16".format(hex(flow_info.dst_ip).rstrip("L")),
             "{0}/0xffff+20".format(hex(flow_info.src_port).rstrip("L")),
             "{0}/0xffff+22".format(hex(flow_info.dst_port).rstrip("L")),
             "{0}/0x00ff+8".format(hex(flow_info.prot).rstrip("L"))])


  # filter for setting the appropriate 
  # network priority
  dsclass = _DSMARK_MAP.get(flow_info.dscp, "0x0004")


  # this one uses the default system hash table
  # `handle` "800::800"
  _PyRoute.tc("add-filter", "u32",
              ifc_idx,      
              parent="{0}:0".format(hex_classid),
              protocol=pyroute2.protocols.ETH_P_IP,
              prio=10,
              target="{0}:{1}".format(hex_classid, dsclass),
              keys=["0x0/0x0+0"])



  # return a filter handle
  return _PyFilter(classid       = "40:{0}".format(hex_classid),
                   dsmarkid      = "{0}".format(hex_classid),
                   qdisc_filter  = filter_id,
                   class_filter  = filter_id,
                   dsmark_filter = 0x80000800, # `800::800`
                   rate          = flow_info.rate,
                   ceil          = flow_info.rate,
                   dscp          = flow_info.dscp)


# For modifying rate and ceil of 
# the class for an ongoing flow
def _modify_class(ipr,       ifc_idx, 
                  classid,   new_rate,
                  new_ceil):

  # the class must already exist
  ipr.tc("change-class", "htb", ifc_idx,
          classid, 
          parent="40:1", rate="{0}mbit".format(new_rate),
          ceil="{0}mbit".format(new_ceil),
          prio=2)

  

# For modifying priority of an ongoing
# large/long flow
def _modify_dsmark(ipr,           ifc_idx, 
                   dsmark_filter, dsmarkid,
                   new_dscp):


  global  _DSMARK_MAP
  new_class = _DSMARK_MAP.get(new_dscp, None)
  assert new_class != None, "_modify_dsmark: cannot find `{0}` mapping.".format(new_dscp)



  # the filter must already exist
  ipr.tc("change-filter", "u32", 
          ifc_idx,         dsmark_filter,
          parent="{0}:{1}".format(dsmarkid, "0"),
          protocol=pyroute2.protocols.ETH_P_IP,
          prio=10,
          target="{0}:{1}".format(dsmarkid, new_class),
          keys=["0x0/0x0+0"])  


# Utility function for modifying an ongoing flow
def _modify_filter(ifc_idx, ac_filter, flow_info):
  
  # check if don't need to change anything
  if ac_filter.rate == flow_info.rate and ac_filter.dscp == flow_info.dscp:
    return # nothing to be changed
  else:

    global _PyRoute, _LinkSpeed 

    # make sure the rates are in the correct ranges
    rate_val      = int(min(_LinkSpeed,   flow_info.rate))
    max_ceil      = int(min(_LinkSpeed, 2*flow_info.rate))
    ceil_new_rate = int(min(ac_filter.ceil, max_ceil))
    
    # make sure that the computed ceiling value is at least
    # as large as the computed rate
    if rate_val > ceil_new_rate:
      ceil_new_rate = rate_val

    # need to modify at least one of the 
    # scheduling strucutres (either class, priority, or both)
    #if ac_filter.rate != flow_info.rate:
    _modify_class(_PyRoute, ifc_idx, ac_filter.classid, 
                  rate_val, ceil_new_rate)

   

    #if ac_filter.dscp != flow_info.dscp:
    _modify_dsmark(_PyRoute, ifc_idx, 
                     ac_filter.dsmark_filter, ac_filter.dsmarkid,
                     flow_info.dscp)

    # update the filter information
    ac_filter.rate = flow_info.rate # use the received 'rate'
    ac_filter.ceil = ceil_new_rate
    ac_filter.dscp = flow_info.dscp





# Function for removing an active filter from
# the kernel
def _delete_filter(ipr,       ifc_idx,   parent_id, 
                   target_id, filter_id, matches):


  ipr.tc("del-filter", "u32",
          ifc_idx,     filter_id,
          parent=parent_id,
          protocol=pyroute2.protocols.ETH_P_IP,
          prio=10,
          ht=filter_id & 0xfff00000,
          target=target_id,
          keys=matches)
          

# Function for removing the allocated 
# `htb` class for a large/long flow
def _delete_class(ipr,       ifc_idx, 
                  parent_id, class_id,
                  htb_rate,  htb_ceil):

  ipr.tc("del-class", "htb", ifc_idx,
         class_id,
         parent=parent_id, 
         prio=2,
         rate=htb_rate,
         ceil=htb_ceil)




# A flow has completed and the allocated `htb`
# class with `dsmark` class have to be removed.
# In addition, the filters must be removed.
# (filters removed first, then `dsmark` and eventually 
#  `htb` class).
def _release_resources(filter_context, flow_info):
  global _PyRoute, _IfcIndex
  
  # delete the top filters (qdisc and root class)
  flow_5_tuple = flow_info.split(":")  

  # matching keys
  match_fields = ["{0}/0xffffffff+12".format(flow_5_tuple[0]),
                  "{0}/0xffffffff+16".format(flow_5_tuple[2]),
                  "{0}/0xffff+20".format(flow_5_tuple[1]),
                  "{0}/0xffff+22".format(flow_5_tuple[3]),
                  "{0}/0x00ff+8".format(flow_5_tuple[4])]



  _delete_filter(_PyRoute,  _IfcIndex, "40:0",
                 "40:0x0001", 
                 filter_context.qdisc_filter,
                 matches=match_fields)


  _delete_filter(_PyRoute,  _IfcIndex, "40:1",
                 filter_context.classid,
                 filter_context.class_filter,
                 matches=match_fields)
  

  # now it is safe to delete
  # the `htb` class allocated for the flow
  _delete_class(_PyRoute,  _IfcIndex, 
               "40:1",     filter_context.classid,
                htb_rate=filter_context.rate,
                htb_ceil=filter_context.ceil)



# Function for scheduling
# a long/large flow 
#
# params:
#   flow_info : PyrouteTuple info for scheduling a large
#               flow
#
#   return : on success, a handle needed to unregister 
#            scheduling, a ValueError on failure
def schedule_flow(py_handle):


   # may have been already registered for
   # scheduling
   flow_key   = create_py_flow_key(py_handle)
   
   # lock global structures
   global _PyFlowMap, _sch_lock, _PyFilerid, _PyClassid, _IfcIndex


   try:
     _sch_lock.acquire()

     cur_filter = _PyFlowMap.get(flow_key, None) 
   
     if cur_filter: # found an active filter
       _modify_filter(_IfcIndex, cur_filter, py_handle)
     else: # need to create a new filter
       _PyFlowMap[flow_key] = _add_filter(_IfcIndex,
                                          _PyClassid.popleft(),
                                          _PyFilterid.get_filter_handle(),
                                          py_handle)
   except Exception as exp:
     raise exp
   finally:
     # make sure to release the lock
     _sch_lock.release()

   # return the string that represents the flow
   return flow_key
       


# Function for removing scheduling information
# for a particular flow
#
# params:
#   py_handle : handle returned by the 'schedule_flow'
#               
def remove_flow(py_handle):


  # check if the flow has been registered for 
  # being scheduled
  
  global _PyFlowMap, _sch_lock, _PyFilterid, _PyClassid 
  
  
  # lock the global structures
  _sch_lock.acquire()
  # try to retrieve an existing handle
  flow_filter = _PyFlowMap.get(py_handle, None)

  try:
    if flow_filter is not None:
      
      # keep the lock and release the resources
      del _PyFlowMap[py_handle]
      _PyFilterid.reclaim_filter_handle(flow_filter.class_filter)
      
      tmp_id = int(flow_filter.classid.split(":")[1], 16)
      _PyClassid.append(tmp_id)

      # release the reources
      _release_resources(flow_filter, py_handle)


  except:
    raise
  finally:
    _sch_lock.release() # release the lock

 

# Same as schedule_flow, but instead 
# takes an iterable object and schedules
# multiple flows under the one lock acquisition
#
# return : iterable (if an exception occurred, it is returned)
def schedule_flows(py_handles):

  if not py_handles:
    return ()

  results = [create_py_flow_key(one_handle)\
               for one_handle in py_handles]


  # access the global structures
  global _PyFlowMap, _sch_lock, _IfcIndex, _PyClassid, _PyFilterid

  # lock the map
  _sch_lock.acquire()

  for fd_idx in xrange(0, len(py_handles), 1):
    cur_filter = _PyFlowMap.get(results[fd_idx], None)
      
    # handle exceptions and return them
    try:
      if cur_filter: # found an active filter
        _modify_filter(_IfcIndex, cur_filter, py_handles[fd_idx])
      else: # need to create a new filter
        _PyFlowMap[results[fd_idx]] = _add_filter(_IfcIndex,
                                          _PyClassid.popleft(),
                                          _PyFilterid.get_filter_handle(),
                                          py_handles[fd_idx])

    except Exception as exp:
      # replace the py_handle with an exception
      results[fd_idx] = exp

  # unlock
  _sch_lock.release()

  # return the scheduling results
  return results

 

# Same as `remove_flow` except that
# this function operates on an iterable of
# objects
def remove_flows(py_handles):
 
  if py_handles:
  
    # result for removing structures
    results = list(py_handles)

    # access the global structures
    global _PyFlowMap, _sch_lock, _IfcIndex, _PyClassid, _PyFilterid

    # lock the global structures
    _sch_lock.acquire()
    
    for map_idx in xrange(0, len(py_handles), 1):
      cur_handle = _PyFlowMap.get(py_handles[map_idx], None)
      if cur_handle:
        # exists
        del _PyFlowMap[py_handles[map_idx]]
        # keep the lock and release the resources
        _PyFilterid.reclaim_filter_handle(cur_handle.class_filter)
        tmp_id = int(cur_handle.classid.split(":")[1], 16)
        _PyClassid.append(tmp_id)

        try:
          # release the reources
          _release_resources(cur_handle, py_handles[map_idx])
        except Exception as exp:
          results[map_idx] = exp # record the error

    _sch_lock.release() # unlock the global structures

    return results # results of operations

  else:
    # empty list (nothing to release)
    return ()




# Initialize the global states without holding the global 
# lock. This must be called before starting to scheduling
# the flows.
def initialize(if_index, if_label, dscp_vals):
  global _PyFilterid, _PyRoute, _IfcIndex, _IfcLabel, _DSMARK_MAP, _LinkSpeed
  
  if len(dscp_vals) != len(_DSMARK_MAP):
    raise IOError("`pyroute` moduel expects a different number of IPv4 DSCP values.")
  
  # check if values match the order in the map
  tmp_sorted = list(_DSMARK_MAP)
  tmp_sorted.sort()

  for recv_dscp, map_dscp in zip(dscp_vals, tmp_sorted):
    if recv_dscp != map_dscp:
      raise IOError("`pyroute` _DSMARK_MAP has to be modified to match your  IPv4 DSCP values.")  

  _LinkSpeed  = resources.link_speed(if_label)
  if _LinkSpeed <= 0:
    raise IOError("Cannot determine link speed of `{0}`".format(if_label)) 
  _PyFilterid = _FilterHandle(if_label)
  _PyRoute    = pyroute2.IPRoute()
  _IfcIndex   = int(if_index)
  _IfcLabel   = if_label



# Release global resources without holding the global
# lock. This must be called at the very end of program
def close():
  global  _PyRoute, _IfcIndex, _IfcLabel
  if _PyRoute:
    try:
      _PyRoute.close()
    except:
      pass
    finally:
      _PyRoute  = None # closed
      _IfcIndex = -1
      _IfcLabel = ""
