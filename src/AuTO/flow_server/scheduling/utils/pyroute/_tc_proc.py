# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import


# compatible int
from builtins import int

import sys

# import the appropriate version of the
# `subprocess` module
if sys.version_info[0] == 2 and sys.version_info[1] == 7:
  try:
    # suprocess32 is a dependency of the project
    import subprocess32 as subprocess
  except ImportError:
    import subprocess
elif sys.version_info[0] == 3:
  import subprocess
else:
  raise RuntimeError("Python {0}.{1} is not supported.".format(sys.version_info[0], sys.version_info[1]))
 

# Utility function for adding new hash tables
# for `tc` filters. This function is implemented as 
# calls to separate processes since it is assumed that
# it is either never called or called on very rare
# occassions. 
def new_filter_table(if_name, handle,  prio, 
                     parent,  ip_field,
                     match_val="0x00",
                     match_mask="0x00"):


  subprocess.check_call(["/sbin/tc", "filter", "add", "dev", 
                          if_name,   "parent",  parent, 
                          "handle",   handle,  "protocol", "ip",
                          "prio",     prio,    "u32",      "divisor", "1"],
                        stdin=None,  stdout=None, stderr=None,
                        shell=False, timeout=None)


  subprocess.check_call(["/sbin/tc", "filter",  "add",  "dev", 
                          if_name,   "parent",   parent, 
                         "protocol", "ip",      "prio",  prio,
                         "u32",      "link",     handle, "match",
                         "ip",       ip_field, match_val, match_mask],
                        stdin=None,  stdout=None, stderr=None,
                        shell=False, timeout=None)
