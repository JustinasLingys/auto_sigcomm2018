# -*- coding: utf-8 -*-

from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import



# Compatible integers
from builtins import int

# Compatible xrange
try:
  xrange
except NameError:
  xrange = range


import os
import os.path
import pwd
import grp
import sys


from scheduling.utils import resources

__DIR_MODE__   = 508

# port number on which the AuTO scheduler listens
# for incoming connection requests from the
# CS server.
from scheduling.core.auto     import AuTO_PORT
from scheduling.utils.pyroute import ipv4_to_if_index
from scheduling.utils.pyroute import ipv4_to_if_label

# AuTO configuration is similar to the QLAS and QSJF 
# configuration files, only that AuTO also takes some
# extra parameters.



# Convert a string into an integer value
def _to_int(str_val):
  conv_value = None

  try:
    conv_value = int(str_val, 10)
  except:
    
    # try different base
    try:
      conv_value = int(str_val, 16)
    except:
      raise IOError("'{0}' cannot be converted into an integer value.".format(str_val))
    else:
      return conv_value
  else:
    return conv_value



# Convert a string to a floating point value
def _to_float(str_val):

  float_val = None

  try:
    float_val = float(str_val)
  except:
    raise IOError("'{0}' cannot be converted into a floating point value.".format(str_val))
  else:
    return float_val


# Check if the given value is either strictly positive
# or non-negative

# Input:     numeric value
# Return:    same value
# Excpetion: is not non-negative/positive
def _check_positive(num_val, zero_allowed=False):
  if zero_allowed:
    # check if non-negative
    if float(num_val) < 0.0:
      raise IOError("'{0}' given where only a non-negative value is valid.".format(num_val))
    else:
      return num_val

  else: # must be a positive value
    if float(num_val) <= 0.0:
      raise IOError("'{0}' given where only a positive value is valid.".format(num_val))


    else:
      return num_val


# Check if the given value is within the range.
# Input: [lower_limit, upper_limit], numeric value
# Return: same value 
# Excpetion: the value is not within the range
def _check_range(lower_limit, upper_limit, value):

  if value >= lower_limit and value <= upper_limit:
    return value
  else:
    raise IOError("'{0}' value is not in the range [{1}, {2}]".format(value, lower_limit, upper_limit))


# Retrieve infomration about the server interface
def _proc_ifc_info(server_addrs):
  return (ipv4_to_if_index(server_addrs[0][0]), ipv4_to_if_label(server_addrs[0][0]))

# Check if the address string follow
# the IP pattern
def _proc_addr(addr_str, remote=False):


  # split the line into a list of addresses

  got_addrs = addr_str.split(",")

  if not got_addrs:
    raise IOError("No IP address has been found.")


  # list of server addresses
  server_addrs = []


  for one_addr in got_addrs:

    address = one_addr.split(":")

    if len(address) != 2:
      raise IOError("Server address '{0}' is an invalid address.".format(addr_str))

    # check if the port is within [0, 2^16-1]
    port = -1

    try:
      port = int(address[1], 10)
    except:
      raise IOError("Invalid port number: {0}.".format(address[1]))
    else:
      if port < 0 or port > (2**16-1):
        raise IOError("Invalid port number: {0}.".format(address[1]))

      if not remote and port == AuTO_PORT:
        raise IOError("Port number '{0}' is allocated for internal use.".format(port))


    # port number has been checked

    # check if the IP address
    ip_octets = address[0].split(".")

    if len(ip_octets) != 4:
      raise IOError("Invalid IPv4 address: {0}.".format(address[0]))

    # need to check if each of the octets is within the range [0, 255]
    for octet_val in ip_octets:
    
      num_octet = -1
      try:
        num_octet = int(octet_val, 10)

      except:
        raise IOError("Invalid IPv4 address: {0}.".format(address[0]))

      else:
        if num_octet < 0 or num_octet > 255:
          raise IOError("Invalid Ipv4 address: {0}.".format(address[0])) 



    # correct IPv4:port
    server_addrs.append((str(address[0]), int(port)))


  # reuturn a list of addresses
  return server_addrs
      


# Process measurement directory
# Check if files and directories can be created 
# within the directory
def _proc_log_dir(dir_path):

  # is a directory?
  if os.path.exists(dir_path) and os.path.isdir(dir_path):
    # check if this process can read/write the directory
    if os.access(dir_path, os.R_OK) and os.access(dir_path, os.W_OK):
      return str(dir_path) # can access the directory

    else:
      raise IOError("Cannot READ and/or WRITE directory at '{0}'.".format(dir_path))


  elif not os.path.exists(dir_path):
   # try to create a directory at the given path
   try:
     os.mkdir(dir_path, __DIR_MODE__)
   except:
     raise IOError("Cannot create a directory at '{0}'.".format(dir_path))

   else:
     return str(dir_path)
  
  else:
    raise IOError("Logging directory '{0}' is not a valid directory.".format(dir_path))




# Process the thresholds to determine how
# flows are subdivided into priorities
def _proc_threshs(thresholds):
  # must be a line of integers separated by commas

  int_values = thresholds.split(",")

  if int_values == 0: # not thresholds
    raise IOError("No priority thresholds are passed.")

  # need to make sure these are all valid threholds (positive and increasing order)
  raw_values = []

 

  for one_thrsh in int_values:
 
    tmp_val = -1

    try:
      tmp_val = int(one_thrsh, 10)

    except:
      # try different base
      try:
        tmp_val = int(one_thrsh, 16)

      except:
        raise IOError("Invalid threshold value: '{0}'.".format(one_thrsh))

      else:
        raw_values.append(int(tmp_val))

    else:
      raw_values.append(int(tmp_val))

   
  # need to check thresholds  are in increasing order and are positive
  for idx in xrange(0, len(raw_values)-1, 1):
    if raw_values[idx] <= 0:
      raise IOError("Thresholds must be positive integers.")

    if raw_values[idx] >= raw_values[idx+1]:
      raise IOError("Thresholds must be written in strictly ascending order.")


  # done processing the thresholds
  return tuple(raw_values)
    



# Dscp values for enforcing priorities
def _proc_ip_tos(ip_tos_string):
  # must be a line of integers separated by commas

  ip_tos_values = ip_tos_string.split(",")

  if len(ip_tos_values) == 0: # not thresholds
    raise IOError("No IPv4 ToS is given.")

  # need to make sure these are all valid DSCP values (positive and increasing order)
  raw_values = []

  for one_ip_tos in ip_tos_values:

    tmp_val = -1

    try:
      tmp_val = int(one_ip_tos, 10)
    except:
     # try different base
     try:
       tmp_val = int(one_ip_tos, 16)
     except:
       raise IOError("Invalid IPv4 ToS value: '{0}'.".format(one_ip_tos))
     else:
       raw_values.append(int(tmp_val))

    else:
      raw_values.append(int(tmp_val))

 
  # need to check if IPv4 ToS values are non-negative and within [0, 255]
  for idx in xrange(0, len(raw_values), 1):
    if raw_values[idx] < 0 or raw_values[idx] > 255:
      raise IOError("IPv4 ToS values must be in the range [0, 255].")


  # done processing the DSCP values
  return tuple(raw_values)
    

# A utility function for checking the the given user
# exists in the system.
def _check_user(username):
  try:
    pwd.getpwnam(username)
  except:
    raise IOError("User '{0}' does not exist in the system.".format(username))
  else:
    # make sure that the user has a group whose 
    # name is the same as the username
    try:
      grp.getgrnam(username)
    except:
      raise IOError("Group '{0}' does not exist in the system. Make sure that the user '{1}' has its own group.".format(username, username))
    else:
      # user and its group exist
      return username


# Compute the default CPU number
def _compute_cpus():

  avail_cpus = resources.cpu_count()
  avail_cpus = avail_cpus if (avail_cpus > 0) else 1

  if avail_cpus > 1:
    # allocate one core for the RL agent
    return (avail_cpus - 1)

  else:
    return 1
 


def _different_nics(server_addrs, cs_addr=None):

  if cs_addr:
    # check if the server addrs and cs_addr reside
    # one separate nics
    server_ip = server_addrs[0][0]
    cs_ip     = cs_addr[0][0]
    
    tmp_if_idx_server = ipv4_to_if_index(server_ip)
    tmp_if_idx_cs     = ipv4_to_if_index(cs_ip)

    return tmp_if_idx_server != tmp_if_idx_cs

  else:
    # check if all the server processes
    # have to be bound to the same interface
    if len(server_addrs) == 1:
      return True
    else:
      # just check the first one
      # with the rest
      first_ip = server_addrs[0][0]

      for src_idx in xrange(1, len(server_addrs), 1):
        if first_ip != server_addrs[src_idx][0]:
          return True

      return False

    
    



# Ensure that relationships among the configurations are
# respected
def _check_relations(configs_dict):

  # first, need to make sure that server processes are 
  # listening on the same ip address (different POrt numbers)
  # and that the CS interface resides on a separate
  # device (there must be two physical NICs)
  if _different_nics(configs_dict["addresses"]):
    raise IOError("Server processes must use the same phyisical interface.")

  # just need to make sure that the number of thresholds 
  # is one smaller than the number of DSCP values
  if len(configs_dict["demotion_thresholds"]) + 1 != len(configs_dict["ipv4_tos"]):
    raise IOError("The number of IPv4 ToS values must be greater than the number of demotion thresholds by exactly 1.")

  # "n_long" must be <= "available_long_slots"
  if int(configs_dict["long_active_flows"]) > int(configs_dict["available_long_slots"]):
    raise IOError("'long_active_flows' cannot be freater than 'available_long_slots'")

  # add interface index and interface label for scheduling purposes
  tmp_ipv4_addr           =  configs_dict["addresses"][0][0]
  configs_dict["if_info"] = (ipv4_to_if_index(tmp_ipv4_addr), 
                             ipv4_to_if_label(tmp_ipv4_addr)) 




__CONFIGS__ = {
  "addresses"            : _proc_addr, # addresses of this server 
                           # (e.g, "10.0.0.10:8800,10.0.10:8801")

  "cs_server"            : lambda str_val: _proc_addr(str_val, True), 
                                       # address of the Central
                                       # System server
                                       # (e.g, "10.0.0.254:8000")

                                       # AuTo communication interface
                                       # for sending/receiving
                                       # management messages 

  "log_dir"              : _proc_log_dir, # log directory for logging
                                          # errors

  "eval_dir"             : _proc_log_dir, # for storing evaluation data 

  "demotion_thresholds"  : _proc_threshs, # demotion thresholds 
                                          # (e.g, 1600,4800,12000)

  "ipv4_tos"             : _proc_ip_tos,  # Ipv4 ToS values used to enforce priorities
                                          # 0x08,0x04,0x02,0x00 
                                          # (len(DSCP) - 1 == len(demotion_thresholds)) 

  "short_completed_flows" : lambda str_val: _check_positive(_to_int(str_val), False),                                # state size for m_s

  "long_completed_flows"  : lambda str_val: _check_positive(_to_int(str_val), False),                                # state size for m_l

  "long_active_flows"     : lambda str_val: _check_positive(_to_int(str_val), False),                                # state size for n_l

  "reward_ratio"          : lambda str_val: _check_range(0.1, 1.0, _to_float(str_val)),                              # this ratio determines
                                         # when an action is considered
                                         # to be a success
                                         # (non-negative 
                                         # reward is generated)

  "sample_size"           : lambda str_val: _check_positive(_to_int(str_val), False),                                # how many flows are used
                                         # to compute the reward r_t.
                                         # (this can be set to a large
                                         # integer to guarantee that
                                         # the RL agent does not
                                         # gets stuck retrieving info
                                         # about flows). If flows
                                         # keep completing during
                                         # reward computation, the agent
                                         # may never compute a reward r_t.

  "rl_update_period"      : lambda str_val: _check_positive(_to_float(str_val), True),                               # how often send the state
                                         # to the RL server
  "system_user"           : _check_user,

  "available_long_slots"  : lambda str_val: _check_positive(_to_int(str_val))
                            # how many long flows can there be 
                            # (for how many long flows to allocate
                            # array memory). Usually, data center
                            # don't have many large flows. 

                                          
              }




# If these parameters are not explicitly set in the
# configuration file, default values are used.
__OPTIONALS__ = {
  "reward_ratio"  : lambda:  1.0,
                }



# Function fill the default options for 
# optional parameters.
def _complete_dict(configs_dict):

  for opt_conf in __OPTIONALS__:
    if not opt_conf in configs_dict:
      handler = __OPTIONALS__[opt_conf]

      # use a default value
      configs_dict[str(opt_conf)] = handler()


  # add interface information to
  # the dictionary
  configs_dict["ifc_info"] = _proc_ifc_info(configs_dict["addresses"])



# Input:   configuration file
# return:  configuration dictionary.

# Excpetion is raised on an error
def read_configs(filename):
  print("AuTO has been selected for flow scheduling.")

  # read the configuration file and
  # create a dictionary.

  configs_dict = {"type" : "AuTO"}
  config_fd    = None 


  try:
    config_fd = open(filename, "r")

    for conf_line in config_fd:
  	  # remove all white spaces
      proc_line = "".join(conf_line.split())
    
      # split the line into a key and a value
      values = proc_line.split("=")

      # ignore all empty lines
      empty_line = True

      for one_str in values:
        for each_char in one_str:
          if not each_char.isspace():
            empty_line = False
            break

        if not empty_line:
          break # ignore all empty lines

      if empty_line:
        continue # ignore the empty line

      # need to check if the file follows the pattern
      if len(values) != 2:
        raise IOError("Your configuration file '{0}' contains a line '{1}' that does not follow the required key=value pattern.".format(filename, conf_line))

      else: # decode the line and process it
        
        # make sure the keys are in lower case
        key_val = values[0].lower()

        if key_val in __CONFIGS__:
          # need to make sure that the configuration 
          # is not repeated multiple times
          if key_val in configs_dict:
            raise IOError("Configuration '{0}' repeated multiple times.".format(values[0]))
          else:
            handler = __CONFIGS__[key_val]

            # the handler returns a value understood 
            # by the scheduling process (may raise an exception)
            param_val = handler(str(values[1]))

            # successfully processed the value
            configs_dict[key_val] = param_val

        else: # there is no valid configuration
          raise IOError("Configuration '{0}' is not a valid one.".format(values[0]))
          
         

  finally:
    # close the file
    if config_fd:
      config_fd.close()



  # fill the missing fields with default 
  # options for oprtional parameters
  _complete_dict(configs_dict)


  # need to check if all required configurations
  # have been found in the configuration file
  missing_configs = [str(tmp_conf) for tmp_conf in __CONFIGS__ if  not tmp_conf in configs_dict]


  # there are some missing configurations
  if missing_configs:
    raise IOError("Missing required configurations: {0}".format(missing_configs))

  else:

    # check if all relationships are respected
    _check_relations(dict(configs_dict))

    # successfully read the configuration file
    return configs_dict
