# -*- coding: utf-8 -*-

from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import



# Compatible integers
from builtins import int

# Compatible xrange
try:
  xrange
except NameError:
  xrange = range

import os
import os.path
import sys


from scheduling.utils import resources

__DIR_MODE__ = 508



# QLAS and QSJF configuration files are the same, only their
# types are different.


# Check if the address string follow
# the IP pattern
def _proc_addr(addr_str):


  # first split all ip addresses
  # into a list of addresses
  got_addrs = addr_str.split(",")

  if not got_addrs:
    raise IOError("No ip address has been found.")


  # list of IP addreses returned on a successful
  # check
  server_addrs = []

  # loop over all addresses and check each of
  # them
  for one_addr in got_addrs:

    address = one_addr.split(":")

    if len(address) != 2:
      raise IOError("Server address '{0}' is an invalid address.".format(addr_str))

    # check if the port is within [0, 2^16-1]
    port = -1

    try:
      port = int(address[1], 10)
    except:
      raise IOError("Invalid port number: {0}.".format(address[1]))
    else:
      if port < 0 or port > (2**16-1):
        raise IOError("Invalid port number: {0}.".format(address[1]))


    # port number has been checked

    # check if the IP address
    ip_octets = address[0].split(".")

    if len(ip_octets) != 4:
      raise IOError("Invalid IPv4 address: {0}.".format(address[0]))

    # need to check if each of the octets is within the range [0, 255]
    for octet_val in ip_octets:
    
      num_octet = -1
      try:
        num_octet = int(octet_val, 10)

      except:
        raise IOError("Invalid IPv4 address: {0}.".format(address[0]))

      else:
        if num_octet < 0 or num_octet > 255:
          raise IOError("Invalid Ipv4 address: {0}.".format(address[0])) 



    # correct IPv4:port
    server_addrs.append((str(address[0]), int(port)))
      

  # return a list of server addresses
  return server_addrs


# Process measurement directory
# Check if files and directories can be created 
# within the directory
def _proc_log_dir(dir_path):

  # is a directory?
  if os.path.exists(dir_path) and os.path.isdir(dir_path):
    # check if this process can read/write the directory
    if os.access(dir_path, os.R_OK) and os.access(dir_path, os.W_OK):
      return str(dir_path) # can access the directory

    else:
      raise IOError("Cannot READ and/or WRITE directory at '{0}'.".format(dir_path))


  elif not os.path.exists(dir_path):
   # try to create a directory at the given path
   try:
     os.mkdir(dir_path, __DIR_MODE__)
   except:
     raise IOError("Cannot create a directory at '{0}'.".format(dir_path))

   else:
     return str(dir_path)
  
  else:
    raise IOError("Logging directory '{0}' is not a valid directory.".format(dir_path))




# Process the thresholds to determine how
# flows are subdivided into priorities
def _proc_threshs(thresholds):
  # must be a line of integers separated by commas

  int_values = thresholds.split(",")

  if int_values == 0: # not thresholds
    raise IOError("No priority thresholds are passed.")

  # need to make sure these are all valid threholds (positive and increasing order)
  raw_values = []

 

  for one_thrsh in int_values:
 
    tmp_val = -1

    try:
      tmp_val = int(one_thrsh, 10)

    except:
      # try different base
      try:
        tmp_val = int(one_thrsh, 16)

      except:
        raise IOError("Invalid threshold value: '{0}'.".format(one_thrsh))

      else:
        raw_values.append(int(tmp_val))

    else:
      raw_values.append(int(tmp_val))

   
  # need to check thresholds  are in increasing order and are positive
  for idx in xrange(0, len(raw_values)-1, 1):
    if raw_values[idx] <= 0:
      raise IOError("Thresholds must be positive integers.")

    if raw_values[idx] >= raw_values[idx+1]:
      raise IOError("Thresholds must be written in strictly ascending order.")


  # done processing the thresholds
  return tuple(raw_values)
    



# Dscp values for enforcing priorities
def _proc_ip_tos(ip_tos_string):
  # must be a line of integers separated by commas

  ip_tos_values = ip_tos_string.split(",")

  if len(ip_tos_values) == 0: # no thresholds
    raise IOError("No IPv4 ToS is given.")

  # need to make sure these are all valid DSCP values (positive and increasing order)
  raw_values = []

  for one_tos in ip_tos_values:

    tmp_val = -1

    try:
      tmp_val = int(one_tos, 10)
    except:
     # try different base
     try:
       tmp_val = int(one_tos, 16)
     except:
       raise IOError("Invalid Ipv4 ToS value: '{0}'.".format(one_tos))
     else:
       raw_values.append(int(tmp_val))

    else:
      raw_values.append(int(tmp_val))

 
  # need to check if IPv4 ToS values are non-negative and within [0, 255]
  for idx in xrange(0, len(raw_values), 1):
    if raw_values[idx] < 0 or raw_values[idx] > 255:
      raise IOError("IPv4 ToS values must be in the range [0, 255].")


  # done processing the Ipv4 ToS values
  return tuple(raw_values)
    


# Compute the default CPU number
def _compute_cpus():
  cpus = resouces.cpu_count()

  if cpus > 0:
    return cpus
  else:
    return 1
  

# Ensure that relationships among the configurations are
# respected
def _check_relations(configs_dict):

  # just need to make sure that the number of thresholds 
  # is one smaller than the number of DSCP values
  if len(configs_dict["demotion_thresholds"]) + 1 != len(configs_dict["ipv4_tos"]):
    raise IOError("The number of IPv4 ToS values must be greater than the number of demotion thresholds by exactly 1.")




__CONFIGS__ = {
  "addresses"            : _proc_addr, # addresses of this server 
                           # (e.g, 10.0.0.10:8800,10.0.0.10:8801)

  "log_dir"              : _proc_log_dir, # log direcotry for logging
                                          # errors

  "eval_dir"             : _proc_log_dir, # for storing evaluation data

  "demotion_thresholds"  : _proc_threshs, # demotion thresholds 
                                          # (e.g, 1600,4800,12000)

  "ipv4_tos"            : _proc_ip_tos,  # IPv4 TOS values used to enforce priorities
                                          # 0x08,0x04,0x02,0x00 
                                          # (len(DSCP) - 1 == len(demotion_thresholds)) 
  
              }




# Optional paramters
# These parameter may not be provided. If they are not explicitly 
# given, default values are used.
__OPTIONAL__ = {}



# Fill default values for non-set parameters
def _complete_dict(configs_dict):

  for opt_conf in __OPTIONAL__:
    if not opt_conf in configs_dict:
      handler = __OPTIONAL__[opt_conf]      

      configs_dict[str(opt_conf)] = handler()



# Input:   configuration file
# return:  configuration dictionary.

# Excpetion is raised on an error
def read_configs(filename):
  print("QSJF has been selected for flow scheduling.")

  # read the configuration file and
  # create a dictionary.

  configs_dict = {"type" : "QSJF"}
  config_fd    = None 


  try:
    config_fd = open(filename, "r")

    for conf_line in config_fd:
  	  # remove all white spaces
      proc_line = "".join(conf_line.split())
    
      # split the line into a key and a value
      values = proc_line.split("=")

      # ignore all empty lines
      empty_line = True

      for one_str in values:
        for each_char in one_str:
          if not each_char.isspace():
            empty_line = False
            break

        if not empty_line:
          break

      if empty_line:
        continue # don't check this line

      # need to check if the file follows the pattern
      if len(values) != 2:
        raise IOError("Your configuration file '{0}' contains a line '{1}' that does not follow the required key=value pattern.".format(filename, conf_line))

      else: # decode the line and process it
        
        # make sure the keys are in lower case
        key_val = values[0].lower()

        if key_val in __CONFIGS__:
          # need to make sure that the configuration 
          # is not repeated multiple times
          if key_val in configs_dict:
            raise IOError("Configuration '{0}' repeated multiple times.".format(values[0]))
          else:
            handler = __CONFIGS__[key_val]

            # the handler returns a value understood 
            # by the scheduling process (may raise an exception)
            param_val = handler(str(values[1]))

            # successfully processed the value
            configs_dict[key_val] = param_val

        else: # there is no valid configuration
          raise IOError("Configuration '{0}' is not a valid one.".format(values[0]))
          
         

  finally:
    # close the file
    if config_fd:
      config_fd.close()


  # fill the defaults for non-provided 
  # parameters
  _complete_dict(configs_dict)


  # need to check if all required configurations
  # have been found in the configuration file
  missing_configs = [str(tmp_conf) for tmp_conf in __CONFIGS__ if  not tmp_conf in configs_dict]


  # there are some missing configurations
  if missing_configs:
    raise IOError("Missing required configurations: {0}".format(missing_configs))

  else:

    # check if all relationships are respected
    _check_relations(dict(configs_dict))

    # successfully read the configuration file
    return configs_dict
