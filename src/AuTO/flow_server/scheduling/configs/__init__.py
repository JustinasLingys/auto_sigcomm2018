# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import


# Compatible integers
from builtins import int


from scheduling.configs import qlas_config
from scheduling.configs import qsjf_config
from scheduling.configs import auto_config




__SCHEDULERS__ = ("QLAS", "QSJF", "AuTO")
__CONFIG_MAP__ = {
                  "QLAS" : qlas_config,
                  "QSJF" : qsjf_config,
                  "AuTO" : auto_config
                 }


# return all available scheduling mechanisms
def get_sch_types():
  return tuple(__SCHEDULERS__)


# utility function for calling the correct function
# for reading an initialization file.
# The initialization file has to follow the 'key = value'
# pattern. The configuration name is on the left side of
# an assignment and the value is on the right side of the 
# assignment sign. All Spaces are ignored.
# A configuration has to reside on its own line
# Number of configurations is equalto the number
# of lines.
def read_configs(sch_type, config_file):
  read_module = __CONFIG_MAP__.get(sch_type, None)

  if read_module is None:
    raise IOError("'{0}' scheduling does not exist.".format(sch_type)) 
  else:
    return read_module.read_configs(config_file) 
