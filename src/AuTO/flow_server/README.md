# Flow Scheduling Implementations


This firectory contains Python implemntations
of the scheduling schemes used in our evaluation.
To run a particular scheduling command, type `python run_sch --type {QLAS, QSJF, AuTO} --config [config_file]  --log_file [log_file]`


Examples of possible configuration files can be found in the `configs`
directory. To learn more about the fields, please read the `scheduling/configs`
directory. The files in the `scheduling/configs` directory contains 
`Python` dictionaries with  explanations for the fields. 


For selecting the initial demotion thresholdsin the configuration files,
please follow instructions at [Threshold Calculator](https://github.com/li-ch/pias-thres). 


# WARNING (Host (Flow Server) Configuration)
Before running any programs, please run the `tcp_setting.h` script from 
the `scripts` directory on every server (flow server and client) 
to enable DCTCP.

Furthermore, on every flow server,  you must run the either `auto_mlfq.sh` or `mlfq_tc.sh` script from the `scripts` directory to enable scheduling on
of the IPv4 network links.  `cleanup_mlfq.sh` may be run to clean up the 
allocated scheduling resources after testing.


# WARNING (Switch Configuration)
Your network switches must be configured with strict priority scheduling
based on the IPv4 DSCP packet field. Fields must match the ones in the 
configuration files.
