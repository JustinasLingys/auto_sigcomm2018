# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import


# Compatible integers
from builtins import int


import argparse
import os
import sys


# Custom Python modules for running 
# a test.
from scheduling import read_configs
from scheduling import get_sch_types
from scheduling import run_scheduling



# a utility function for checking if the 
# two required files can be read/written.
def _check_file_caps(filepath, capability):
  
  # check if the given file can be read/written
  file_desc = None

  try:
    file_desc = open(filepath, capability)
  except IOError:
    return False

  else:
    return True

  finally:
    if file_desc:
      file_desc.close()



def main(sch_type, config_file, log_file):
  
  # make sure that the configuration file can be opened for reading
  # and the lg file can be opened for writing.
  if not _check_file_caps(config_file, "r") or\
     not _check_file_caps(log_file, "w"):
    print("Your passed configuration file '{0}'".format(config_file), sep=" ")
    print("or log file '{0}' cannot be read/written.".format(log_file))


  else:
    # read the configuration file and make sure 
    # that it contains all required values for initializing
    # the scheduling

    sch_configs = None

    try:
      sch_configs = read_configs(sch_type, config_file)
    except IOError as exp:
      # configuration does not match the requirements
      print("Error:", exp)

    else:
      # start a daemon process for running the scheduling 
      # test

      try: 
        run_scheduling(log_file, sch_configs)
      except (IOError, OSError) as err_exp:
        print("Error:", err_exp)

      else:
        print("'{0}' scheduling has been successfully started.".format(sch_type))

    

 

if __name__ == "__main__":

  # need to pass two files:
  # one is a configuration file which used by
  # the scheduling mechanism to initialize itself.


  parser = argparse.ArgumentParser(description="Parsing input arguments.")
  
  # scheduling scheme
  parser.add_argument("--type", type=str,
                       help="Flow Scheduling Mechanism.",
                       choices=get_sch_types(),
                       required=True)

  # file for configuring the selected scheduling 
  # mechanism
  parser.add_argument("--config", type=str,
                       help="Flow Scheduling Mechanism configuration file.",
                       required=True)


  # log file is used for logging errors and other information 
  # the file is passed to a child process as a replacement for
  # STDOUT/STDERR
  parser.add_argument("--log_file", type=str,
                       help="Log file for printing all messages.",
                       required=True)


  args = parser.parse_args()

 
  main(sch_type=str(args.type), config_file=str(args.config),
       log_file=str(args.log_file))
