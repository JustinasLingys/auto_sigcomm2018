# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import


# Compatible integers
from builtins import int

# Path for nodifying not a physical file
from  central_system.configs.default_configs import EMPTY_PATH
from  central_system.configs                 import default_configs


  


# Function for reading a file and
# initialiazing the central system according to
# the parameters in the file.
def read_configs(config_file):
  return default_configs.read_configs(config_file)
