# -*- coding: utf-8 -*-

from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import



# Compatible integers
from builtins import int

# Compatible xrange
from six.moves import xrange

import os
import os.path
import sys


from central_system.utils import resources


# A sign that no file has been passed to 
# a particular option.
EMPTY_PATH = None


# Convert a string into an integer value
def _to_int(str_val):
  conv_value = None

  try:
    conv_value = int(str_val, 10)
  except:
    
    # try different base
    try:
      conv_value = int(str_val, 16)
    except:
      raise IOError("'{0}' cannot be converted into an integer value.".format(str_val))
    else:
      return conv_value
  else:
    return conv_value



# Convert a string to a floating point value
def _to_float(str_val):

  float_val = None

  try:
    float_val = float(str_val)
  except:
    raise IOError("'{0}' cannot be converted into a floating point value.".format(str_val))
  else:
    return float_val

# Convert a string to a boolean flag
def _to_bool(str_val):
  flag_val = str_val.lower()

  if flag_val == "true":
    return True
  elif flag_val == "false":
    return False
  else:
    raise IOError("'{0}' cannot be converted into a boolean value.".format(str_val))


# Check if the given value is either strictly positive
# or non-negative

# Input:     numeric value
# Return:    same value
# Excpetion: is not non-negative/positive
def _check_positive(num_val, zero_allowed=False):
  if zero_allowed:
    # check if non-negative
    if float(num_val) < 0.0:
      raise IOError("'{0}' given where only a non-negative value is valid.".format(num_val))
    else:
      return num_val

  else: # must be a positive value
    if float(num_val) <= 0.0:
      raise IOError("'{0}' given where only a positive value is valid.".format(num_val))


    else:
      return num_val


# Check if the given value is within the range.
# Input: [lower_limit, upper_limit], numeric value
# Return: same value 
# Excpetion: the value is not within the range
def _check_range(lower_limit, upper_limit, value):

  if value >= lower_limit and value <= upper_limit:
    return value
  else:
    raise IOError("'{0}' value is not in the range [{1}, {2}]".format(value, lower_limit, upper_limit))




# Check if the address string follow
# the IP pattern
def _proc_addr(addr_str):


  # split the line into a list of addresses
  got_addrs = addr_str.split(",")

  if not got_addrs:
    raise IOError("No IP address has been found.")


  # list of server addresses
  server_addrs = []


  for one_addr in got_addrs:

    address = one_addr.split(":")

    if len(address) != 2:
      raise IOError("Server address '{0}' is an invalid address.".format(addr_str))

    # check if the port is within [0, 2^16-1]
    port = -1

    try:
      port = int(address[1], 10)
    except:
      raise IOError("Invalid port number: {0}.".format(address[1]))
    else:
      if port < 0 or port > (2**16-1):
        raise IOError("Invalid port number: {0}.".format(address[1]))


    # port number has been checked

    # check if the IP address
    ip_octets = address[0].split(".")

    if len(ip_octets) != 4:
      raise IOError("Invalid IPv4 address: {0}.".format(address[0]))

    # need to check if each of the octets is within the range [0, 255]
    for octet_val in ip_octets:
    
      num_octet = -1
      try:
        num_octet = int(octet_val, 10)

      except:
        raise IOError("Invalid IPv4 address: {0}.".format(address[0]))

      else:
        if num_octet < 0 or num_octet > 255:
          raise IOError("Invalid Ipv4 address: {0}.".format(address[0])) 



    # correct IPv4:port
    server_addrs.append((str(address[0]), int(port)))


  # reuturn a list of addresses
  return server_addrs
      


# Check if the current process has capabilities required
# on the given file.
def _proc_file(filename, capability):


 # ignore file with no valid path (paths which are not passed)
  if filename is EMPTY_PATH:
    return EMPTY_PATH


  file_desc = None

  try:
    file_desc = open(filename, capability)
  except IOError as exp:
    raise exp

  else:
    return filename

  finally:
    if file_desc:
      file_desc.close()



# Check if the given path is a directory and
# that the user is eligible for creating 
# files in the directory.
def _proc_dir(dirpath):

  if os.path.isdir(dirpath) and os.access(dirpath, 
                                  os.R_OK | os.W_OK | os.X_OK):
    return dirpath
  else:
    raise IOError("'{0}' directory cannot be written, read and executed by the current process.".format(dirpath))



# Compute the default CPU number
def _compute_cpus():

  avail_cpus = resouces.cpu_count()
  avail_cpus = avail_cpus if (avail_cpus > 0) else 1

  if avail_cpus > 1:
    # allocate one core for the RL agent
    return (avail_cpus - 1)

  else:
    return 1
 

# Ensure that relationships among the configurations are
# respected
def _check_relations(configs_dict):
  # It is impossible to have a file of weights without 
  # a file of model since the weights must be applied to 
  # the given model.

  if configs_dict["long_rla_model"] is EMPTY_PATH and\
     configs_dict["long_rla_weights"] is not EMPTY_PATH:
    raise IOError("lRLA cannot be initialized with only weights file. A model file is required.")

  


# Need to split the string into two files separated by
# a comma
def _proc_short_rla_load(weights_files):

  input_files = weights_files.split(",")

  if len(input_files) != 2:
    raise IOError("sRLA requires exactly two files separated by ',' for weight initialization.")

  # make sure both files can be read successfully
  for single_file in input_files:
    _proc_file(single_file, "r")

  # now return a dictionary
  # first file must be the actor's, second the critic's 
  return {"actor_weights_filename"  : str(input_files[0]),
          "critic_weights_filename" : str(input_files[1])}


# Need to split a string into two files for storing
# the weights (actor and critic).
def _proc_short_rla_store(weights_files):

  input_files = weights_files.split(",")
  
  if len(input_files) != 2:
    print(input_files)
    raise IOError("sRLA requires exactly two files separated by ',' for storing model weights.")

  # make sure both files can be written
  for single_file in input_files:
    _proc_file(single_file, "a")

  # now return a dictionary for using in next steps
  return {"actor_save_weights_filename"  : str(input_files[0]),
          "critic_save_weights_filename" : str(input_files[1])}
  

  

__CONFIGS__ = {
  "address"              : _proc_addr, # addresses of this RL server 
                             # (e.g, "10.0.0.10:8800")


  "long_rla_model"         : lambda str_val: _proc_file(str_val, "r"), 

  "long_rla_weights"       : lambda str_val: _proc_file(str_val, "r"),
 
  "save_long_rla_model"    : lambda str_val: _proc_file(str_val, "a"),
  
  "save_long_rla_weights"  : lambda str_val: _proc_file(str_val, "a"),

  "short_rla_model"        : lambda str_val: True,

  "short_rla_weights"      : _proc_short_rla_load,


  "save_short_rla_model"   : lambda str_val: True,

  "save_short_rla_weights" : _proc_short_rla_store,

  "save_weights_period"    : lambda str_val: _check_positive(_to_float(str_val), False),

  "log_directory"          : lambda str_val: _proc_dir(str_val),

  "training_phase"         : _to_bool                                          
              }




# If these parameters are not explicitly set in the
# configuration file, default values are used.
__OPTIONALS__ = {
  "save_weights_period"  : lambda: 3600.0,    # save weights every hour
  "long_rla_model"       : lambda: EMPTY_PATH,
  "long_rla_weights"     : lambda: EMPTY_PATH,
  "short_rla_model"      : lambda: EMPTY_PATH,
  "short_rla_weights"    : lambda: EMPTY_PATH,
  "training_phase"       : lambda: True
                }




# Function fill the default options for 
# optional parameters.
def _complete_dict(confs_dict):

  for opt_conf in __OPTIONALS__:
    if not opt_conf in confs_dict:
      handler = __OPTIONALS__[opt_conf]

      # use a default value
      confs_dict[opt_conf] = handler()




# Input:   configuration file
# return:  configuration dictionary.

# Excpetion is raised on an error
def read_configs(filename):
  print("Cental System of AuTO is being initialized.")

  # read the configuration file and
  # create a dictionary.

  configs_dict = {}
  config_fd    = None 


  try:
    config_fd = open(filename, "r")

    for conf_line in config_fd:
  	  # remove all white spaces
      proc_line = "".join(conf_line.split())
    
      # split the line into a key and a value
      values = proc_line.split("=")

      # ignore all empty lines
      empty_line = True

      for one_str in values:
        for each_char in one_str:
          if not each_char.isspace():
            empty_line = False
            break

        if not empty_line:
          break

      if empty_line:
        continue   # ignore lines of white spaces
      


      # need to check if the file follows the pattern
      if len(values) != 2:
        raise IOError("Your configuration file '{0}' contains a line '{1}' that does not follow the required key=value pattern.".format(filename, conf_line))

      else: # decode the line and process it
        
        # make sure the keys are in lower case
        key_val = values[0].lower()

        if key_val in __CONFIGS__:
          # need to make sure that the configuration 
          # is not repeated multiple times
          if key_val in configs_dict:
            raise IOError("Configuration '{0}' repeated multiple times.".format(values[0]))
          else:
            handler = __CONFIGS__[key_val]

            # the handler returns a value understood 
            # by the scheduling process (may raise an exception)
            param_val = handler(str(values[1]))

            # successfully processed the value
            configs_dict[key_val] = param_val

        else: # there is no valid configuration
          raise IOError("Configuration '{0}' is not a valid one.".format(values[0]))
          
         

  finally:
    # close the file
    if config_fd:
      config_fd.close()



  # fill the missing fields with default 
  # options for oprtional parameters
  _complete_dict(configs_dict)


  # need to check if all required configurations
  # have been found in the configuration file
  missing_configs = [str(tmp_conf) for tmp_conf in __CONFIGS__ if  not tmp_conf in configs_dict]


  # there are some missing configurations
  if missing_configs:
    raise IOError("Missing required configurations: {0}".format(missing_configs))

  else:

    # check if all relationships are respected
    _check_relations(dict(configs_dict))

    # successfully read the configuration file
    return configs_dict
