# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import


# Compatible integers
from builtins import int


# Python std lib
import os
import os.path
import sys

# Use factory functions for creating 
# abstractions of RSAs

from central_system.core.processor.long_rla.encoder import encode_action_to_binary
from central_system.core.processor.long_rla.agents  import DefaultLongFlowAgent



# Function for creating a long-flow RSA.
def create_lrla(comms, configs):

  # retrieve all the required values 
  # from the configuration dictionary
  log_dir = configs["log_directory"]
  lrla_log_file = os.path.join(log_dir, "long-rla.log")  


  lrla = DefaultLongFlowAgent()
  lrla.initialize(comms, 
                  configs["save_weights_period"],
                  configs["save_long_rla_model"], 
                  configs["save_long_rla_weights"],
                  lrla_log_file,
                  configs["long_rla_model"], 
                  configs["long_rla_weights"],
                  configs["training_phase"])
 
  return lrla
