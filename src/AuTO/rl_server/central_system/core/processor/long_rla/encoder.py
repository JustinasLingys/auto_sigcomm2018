# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import


# Compatible integers
from builtins import int


# Python std lib
import struct
import collections

_ACTION_SIZE = 8


# custom modules
from central_system.algorithms import vreinforce

def _encode_one_long_action(one_action):
  assert len(one_action) == _ACTION_SIZE, "_encode_one_long_action: len(one_action) = {0}".format(len(one_action))
  return struct.pack("<IHIHBBHH", *one_action)

# utility function for encoding long actions of a sample.
# The function takes an action produced by a deep reinforcement learning
# algorith and produces the binary representation of the action.  
def encode_action_to_binary(actions):
  # Tuple: (long_flows, action_indices)

  if not actions or len(actions) == 0:
    return b"" # nothing to send 
 
  data_list = [] # a list of binary actions
  for long_flow, long_action, long_idx in zip(actions[0], actions[1], actions[2]):
    tmp_flow     = list(long_flow)
    
    # set actions (priority is last attribute)
    tmp_flow[-1] = vreinforce.ACTION_SPACE[long_action]["priority"]
    tmp_flow.append(vreinforce.ACTION_SPACE[long_action]["rate"])
    tmp_flow.append(long_idx)
    data_list.append(_encode_one_long_action(tmp_flow))
      
  # return a binary string of actions
  return b"".join(data_list)
