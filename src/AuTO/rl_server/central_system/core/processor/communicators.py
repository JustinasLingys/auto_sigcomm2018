# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import


# Compatible integers
from builtins import int
# for iterating dictionaries
import six


# Python std lib
import os 
import os.path
import multiprocessing
import collections
import socket
import struct
import signal
import errno
import hashlib


try:
  import Queue
  import selectors2 as selectors

except ImportError:
  import queue as Queue
  import selectors
  xrange = range


from central_system.core.processor.long_rla  import encode_action_to_binary as encode_lrla_action
from central_system.core.processor.short_rla import encode_action_to_binary as encode_srla_action 

from central_system.utils import logging as utl_log
from central_system.utils import resources

# A Port number used by the clients to listen on incoming requests
# from the Central System server for sending actions back.
_ACTION_PORT = 60001



# Utility function
#
# One core - http server
def _compute_cpus_for_comms():
  avail_cpus = resources.cpu_count()
  avail_cpus = avail_cpus if avail_cpus > 0 else 1

  # considering that this is running on a server,
  # the server is epexted to have more than 10 cores
  
  # one core for the http server and one half of the cores
  # for the training/inference
  return int(max(((avail_cpus // 2) - 1), 1))    


# utility function for computing a consistent hash
# for the given string. The hash returned as an integer.
def _compute_hash(str_val):
  return int(hashlib.md5(str_val.encode("utf-8")).hexdigest(), 16)

# Fot creating non-blocking sockets
def _create_nonblocking_socket():
  
  # try to create a non-blocking socket
  sockfd = None
  try:
    sockfd = socket.socket(socket.AF_INET, socket.SOCK_STREAM,
                           socket.IPPROTO_TCP)

    # initialize the sokcet with  some socket options
    sockfd.setsockopt(socket.SOL_SOCKET,  socket.SO_REUSEADDR, 1)
    sockfd.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY,  1)
    sockfd.setblocking(False) # non-blocking socket

  except:
    if sockfd is not None:
      # close the socket
      sockfd.close()
      sockfd = None

    raise
  else:
    return sockfd # return a new TCP socket



# Utility function for sending data
# to a remote server
def _send_data_func(sockfd, bin_data):

  # try to write as much data as possible
  try:
    sent_bytes = sockfd.send(bin_data, 0)
  except Exception as op_exp:
    if op_exp.errno == errno.EAGAIN or op_exp.errno == errno.EWOULDBLOCK:
      return 0
    else:
      raise op_exp
  else:
    return sent_bytes # some bytes has been written



# length of message integer
_MSG_SIZE = len(struct.pack("<I", 0))

# number of retries to connect before giving up
_NUM_RETRIES = 3 

class CommProcess(multiprocessing.Process):

  class ConnContext(object):
    """
      A Context structure used by the communication
      processes to send actions back to the clients.
      The context helps to ensure that an action is only 
      sent when responses from both RSAs arrive.
    """
    def __init__(self):
      self._sock = None # data socket
     
      # binary data sent to a client (long-flow RLA action)
      self._lrla = collections.deque(maxlen=None)     
      
      # binary data sent to a client (short-flow RLA action)
      self._srla = collections.deque(maxlen=None)    

      # request id for sending data to a remote server 
      self._req_id = -1
      self._offset = 0
      self._raw_data = b""
      self._retries  = _NUM_RETRIES # when to stop connecting


    """
      Prepare for sending data
    """
    def prepare_send(self):
      assert self._raw_data == b"", "ConnContex::_prepare_send: `raw_data` is non-empty"
      assert self._req_id  >= 0, "ConnContext::prepare_send: `req_id` is negative"
  
      # prepare the data to be sent
      bin_id = struct.pack("<I", self._req_id)
        

      # message structure:
      # msg_size req_id short_action long_action
      tmp_val        = b"".join([bin_id, 
                                 self._srla.popleft(), 
                                 self._lrla.popleft()])
      self._raw_data = b"".join([struct.pack("<I", _MSG_SIZE+len(tmp_val)), tmp_val])
      self._offset   = 0
      


    def enqueue_short_action(self, binary_action):
    
      self._srla.append(binary_action)

      # return if the actions can be sent to 
      # the client
      return self._can_send()
      


    def enqueue_long_action(self, binary_action):

      self._lrla.append(binary_action)

      # return True if both actions have 
      # been received
      return self._can_send()
         


    """
      Method for checking if actions are ready to be sent.
    """
    def _can_send(self):
      return (len(self._srla) > 0 and len(self._lrla) > 0)

    """
      To determine if there is anything to send
    """
    def empty(self):
      return (self._raw_data == b"")

    @property
    def binary_data(self):
      if self.empty():
        return b""
      else:
        return self._raw_data[self._offset::1]

    @property
    def req_id(self):
      return self._req_id

    @req_id.setter
    def req_id(self, new_req):
      self._req_id = new_req

    @property
    def sock(self):
      return self._sock


    @property  
    def rem_bytes(self):
      return (len(self._raw_data) - self._offset)


    @sock.setter
    def sock(self, new_sock):
      # set a new socket
      self._sock = new_sock
    
    """
      Method is called every time the communicator fails
      to connect to a remote server. It returns if a fixed
      number of attempts has been tried.
    """
    def failed_connect(self):
      if self._retries > 0:
        self._retries -= 1

      return (self._retries == 0)

    def success_connect(self):
      self._retries = _NUM_RETRIES

    """
      For updating sent bytes
    """
    def sent_bytes(self, value):
      self._offset += value

      assert self._offset <= len(self._raw_data), "ConnContext::sent_bytes:  offset > len(raw_data)"
       
      # signal a success
      self.success_connect()

      # check if completed
      if self._offset == len(self._raw_data):
        # reset data
        self._offset   = 0
        self._raw_data = b""
        self._req_id   = -1
        
        return True # done sending
      else:
        return False # need to send more

    def avail_short_acts(self):
      return len(self._srla)

    def avail_long_acts(self):
      return len(self._lrla)

    def dequeue_long_act(self):
      return self._lrla.popleft()

    def dequeue_short_act(self):
      return self._srla.popleft()

    """
      Clear context.
    """
    def clear_context(self):
      self._sock     = None
      self._srla.clear()
      self._lrla.clear()
      self._raw_data = b""
      self._offset   = 0
      self._req_id   = -1
      self._retries  = _NUM_RETRIES



  """
    A Simple Process Class for dealing with persistent TCP Connections.
    Objects of this class send back actions to the clients.
  """
  def __init__(self, srla_tasks, lrla_tasks, log_file):
    super(CommProcess, self).__init__()
    
    self.daemon = False # not a daemon    

    # queues for the RLAs to pass their
    # actions as a tuple: (address, actions)
    self.short_tasks = srla_tasks
    self.long_tasks  = lrla_tasks

    self.sys_logger  = log_file # for logging errors    
    
    # for starting and terminating a process
    self.loop_flag   = multiprocessing.Event()


  """
    Method for starting a communicator process.
  """
  def start_comm(self):
    if not self.loop_flag.is_set():
      self.loop_flag.set()
      self.start()

  """ 
    Method for stopping a communicator process.
  """
  def stop_comm(self):
    self.loop_flag.clear()

    # try to send a signal to the child process
    
    try:
      self.long_tasks.put(None, block=False)
    except Queue.Full:
      pass


    try:
      self.short_tasks.put(None, block=False)
    except Queue.Full:
      pass



  def run(self):
    
    # an IO selector is created for handling IO in 
    # a non-blocking way
    self._selector = selectors.DefaultSelector()
    self._states   = {} # mapping from a server to its 
                        # current state

    self._pending  = {} # ongoing IO operations 
                        # (data has to be sent)

    self._failures = {} # failed attempts to connect to a 
                        # remote server (few times are tried)
 

    # create a logger for logging errors
    logger  = utl_log.create_logger(self.sys_logger)
    self.sys_logger = logger

    need_to_terminate = False

    # logger is also a context manager
    with logger:
      logger("Starting running a communicator process!")
      # run the communicator
      while self.loop_flag.is_set():

        # give preference to ongoing IO operations
        if self._pending: 
          # means there are some ongoing IO operations
          reqs = self._selector.select(timeout=0.02)

          for ctx, mask in reqs:
            # ctx.fileobj : socket
            # ctx.data[0] : callback
            # ctx.data[1] : data to be passed to `callback`
            callback = ctx.data[0]
            data     = ctx.data[1]
            callback(ctx.fileobj, mask, data)

        # some tries to connect failed
        # retry a few more times
        if self._failures:
          self._retry_connect()


        # now check for new incoming requests
        self._check_for_reqs(not self._pending and not self._failures)

  


    # clean up this communicator
    self._clean_up()
    # append a log that the communicator is done
    with logger:
      logger("Communicator process has terminated!")



  """
    Read from the two queues the newly computed actions
    and send them to remote servers.
  """
  def _check_for_reqs(self, can_loop):
    
    short_actions = []
    long_actions  = []

    # keep looping and waiting for incoming actions
    # (if no IO ongoing, keep looping)
    # Since it is apected to receive many incoming requests
    # from many servers and a communicaotr runs on a 
    # dedicated CPU core, looping is a viable approach

    while can_loop:
     
      try:
        while True:
          req = self.long_tasks.get(block=False)
          if req is None:
            # no more processing
            return
          else:
            long_actions.append(req)
      except Queue.Empty:
        pass

       
      try:
        while True:
          req = self.short_tasks.get(block=False)
          if req is None:
            # done
            return # no more prcoessing
          else:
            short_actions.append(req)
      except Queue.Empty:
        pass

      # action is a tuple:
      # (IPv4, req_id, action)      

      # need to loop over the received requests
      # and check if need to process furhter
      for one_act in long_actions:
        self._process_one_action(one_act, True)

      for one_act in short_actions:
        self._process_one_action(one_act, False)

      if long_actions or short_actions:
        can_loop = False # need to handle IO
        
        # reset the lists to be empty
        long_actions  = []
        short_actions = []  
                       
     


  """
    Method for handling an action.  
  """
  def _process_one_action(self, act_state, long_act):  

    # action is a tuple:
    # (Ipv4, req_id, action)
    
    conn_ctx      = self._states.get(act_state[0], None)
    call_ref      = self._start_sending # call for sending data 

    # if the action is already being processed as 
    # a failure is pening, stop processing furhter and 
    # try from the scratch
    # (we have received a new action, means the remote server
    # has terminated the previous one)
    if act_state[0] in self._failures:
      if conn_ctx is not None:
        conn_ctx.clear_context() # clear the old state
      del self._failures[act_state[0]]
          

    if conn_ctx is None:
      # create a new context
      conn_ctx                   = CommProcess.ConnContext()
      self._states[act_state[0]] = conn_ctx
      call_ref                   = self._connect # need to connect first
    else:
      # a new requsr may arrive before 
      # an old one has been completely sent     

      if not conn_ctx.empty():
        
        # this context is already being processed
        assert act_state[0] in self._pending

        # cancel the previous request and reuse the connection
        # for the new action
        tmp_var       = conn_ctx.sock
        conn_ctx.clear_context() # prepare for a new request
        conn_ctx.sock = tmp_var
        
        # the operation is in progress, don't need
        # to start a new one
        call_ref = lambda op_ctx, ip_addr: None
      

      else:
        # need to check the socket has been set
        if conn_ctx.sock is None:
          call_ref = self._connect

   
    # get number of available long/short actions
    pend_long  = conn_ctx.avail_long_acts()
    pend_short = conn_ctx.avail_short_acts()
 
    if long_act: # long action 

      # if there are more than one short actions,
      # and no long actions, only dequeue front and 
      # do nothing
      if pend_long == 0 and pend_short > 1:
        conn_ctx.dequeue_short_act()
        return        

  
      # encode action
      bin_action = encode_lrla_action(act_state[-1])

      if conn_ctx.enqueue_long_action(bin_action):
        # need to send this action
        conn_ctx.req_id = act_state[1] # request id
        conn_ctx.prepare_send()

        # call the callback
        call_ref(conn_ctx, act_state[0])

    else: # short action 
       
      # this short action is an old one.
      # dequeue an old long action
      if pend_long > 1 and pend_short == 0:
        conn_ctx.dequeue_long_act()
        return   
      
      # encode action
      bin_action = encode_srla_action(act_state[-1])
   
 
      if conn_ctx.enqueue_short_action(bin_action):
        # need to send this action
        conn_ctx.req_id = act_state[1] # request id
        conn_ctx.prepare_send()

        # call the callback
        call_ref(conn_ctx, act_state[0])
  
  """
    Retry to connect to some remote servers
  """
  def _retry_connect(self):
    # get a list of IP addresses
    itr_list = list(self._failures)

    for ip_addr in itr_list:
      # connection context
      conn_ctx = self._states[ip_addr]
      sockfd   = self._do_connect(ip_addr)

      if sockfd is not None:
        # success
        conn_ctx.sock = sockfd
        del self._failures[ip_addr]
        # a new IO operation 
        self._pending[ip_addr] = True
        
      else:
        # failed to init connect again
        if conn_ctx.failed_connect():
          # done with this server
          conn_ctx.clear_context()
          del self._states[ip_addr]
          del self._failures[ip_addr]
      
   
  """
    Creates a socket and tries to connect
  """
  def _do_connect(self, ip_addr):
    try:
      conn = _create_nonblocking_socket()
    except Exception as sock_exp:
      # if this call fails, terminate the process
      self.sys_logger("CommProc::_connect: `{0}`".format(sock_exp))
      self.stop_comm()
      return None # failure (need to terminate as soon as possible)
    else:
      # try to connect asynchronously
      if self._async_connect(conn, ip_addr):
        return conn # success
      else:
        conn.close()
        return None # failure
    

  
  """
    Try to connect to a remote server asynchronously
  """
  def _async_connect(self, sockfd, ip_addr):

    try:
      sockfd.connect((ip_addr, _ACTION_PORT))
    except Exception as conn_exp:
      if conn_exp.errno != errno.EAGAIN and conn_exp.errno != errno.EWOULDBLOCK and conn_exp.errno != errno.EINPROGRESS:
        # failed to initiate a `connect` request
        self.sys_logger("_async_connect: faild to initiatie a `connect` request: `{0}`".format(conn_exp))
        return False
      else:
        # register the socket with the selector
        self._selector.register(sockfd, selectors.EVENT_WRITE,
                                data=(self._send_data, (ip_addr, False)))
        return True
    else:
      # register the socket with the selector
      self._selector.register(sockfd, selectors.EVENT_WRITE,
                             data=(self._send_data, (ip_addr, False)))
      return True
   

  """
    Close an active socket (registered with the selector)
  """
  def _close_act_conn(self, sockfd):
    if sockfd is not None:
      self._selector.unregister(sockfd)
      self._close_conn(sockfd)

  
  """
    Close non-active sockets (not registered with the selector)
  """
  def _close_conn(self, sockfd):
    if sockfd is not None:
     
      # try shutting down the socket
      try:
        sockfd.shutdown(socket.SHUT_RDWR)
      except:
        pass
      finally:
        # close the socket
        sockfd.close()

  """
    A new connection has to be set up.
  """
  def _connect(self, op_ctx, ip_addr):
    sockfd = self._do_connect(ip_addr)
   
    if sockfd is not None:
      # successfully initiatied a `connect` process
      op_ctx.sock            = sockfd
      self._pending[ip_addr] = True
    else:
     
      # failed to connect
      if op_ctx.failed_connect():
        # done with this connection
        op_ctx.clear()
        del self._states[ip_addr]
      else:
        # retry a bit later
        self._failures[ip_addr] = True
          

  """
    Method to initiate a new send operation. The connection
    has already been established.
  """
  def _start_sending(self, op_ctx, ip_addr):
    assert op_ctx.sock is not None, "CommProc::_start_sending: op_ctx.sock is `None`"

    if ip_addr in self._pending:
      return # wait for the IO to complete
    else:
      # try immediately as much data as possible
      try:
        written_data = _send_data_func(op_ctx.sock, op_ctx.binary_data)
      except: # connection failed
        # close the socket
        self._close_conn(op_ctx.sock)
        op_ctx.clear_context()
        del self._states[ip_addr]
      else:
        # sent some data
        if not op_ctx.sent_bytes(written_data):
          # need to register for an IO and write later
          self._pending[ip_addr] = True
          self._selector.register(op_ctx.sock, selectors.EVENT_WRITE,
                                  data=(self._send_data, (ip_addr, True)))        

  """
    Method for handlng send operations
  """
  def _send_data(self, sockfd, mask, data):
    ip_addr = data[0]
    op_ctx  = self._states[ip_addr] # operation context
    
  
    if not op_ctx.empty(): # there is something to send
      try:
        written_bytes = _send_data_func(sockfd, op_ctx.binary_data)
      except Exception as op_exp:
        self.sys_logger("CommProc::_send_data: `{0}`".format(op_exp))
        self._close_act_conn(sockfd)
        
        if data[1] is True:
          op_ctx.clear_context()
          del self._states[ip_addr]
        else:# failed to connect 
          if op_ctx.failed_connect():
            op_ctx.clear_context()
            del self._states[ip_addr]
          else:
            self._failures[ip_addr] = True

        del self._pending[ip_addr] # this IP address is
                                   # done

      else:
        if op_ctx.sent_bytes(written_bytes):
          # done sending data
          self._selector.unregister(sockfd)
          del self._pending[ip_addr]
      
         


  """
    Method for cleaning up the process and releasing all
    the allocated resources.
  """
  def _clean_up(self):

    # clear the contexts
    for map_key, map_val in six.iteritems(self._states):
      if map_key in self._pending: # active connection
        self._close_act_conn(map_val.sock)
      else:
        self._close_conn(map_val.sock)
      map_val.clear_context()    

        
    # clear the dictionaries
    self._states   = {}
    self._failures = {}
    self._pending  = {}
    
    # make sure that the task queues are empty
    try:
      while True:
        self.long_tasks.get(block=False)

    except Queue.Empty:
      pass

    try:
      while True:
        self.short_tasks.get(block=False)

    except Queue.Empty:
      pass

        
    # close the selector
    self._selector.close()


class RLAMessenger(object):
  """
    Class for interacting with communication processes.
    Instances of this class are used by reinforcement learning
    agents to send actions back to clients.
  """
  def __init__(self, msg_queues):
    self.comm_queues = msg_queues

  """
    Method used by RLAs to pass actions to the communication
    processes.
  
    Params:
      index   : a handle to determine which process
                to use for sending the action back
   
      ip_addr : IPv4 address of the client server
 
      req_id  : request id received from a remote server
                (this value is forwarded by the http server)     

      action  : RLA generated action
                (communicator will encode the action into binary)

  """
  def send_action(self, index, ip_addr, req_id,  action):
    assert index >= 0 and index < len(self.comm_queues)
    self.comm_queues[index].put((ip_addr, req_id, action), block=True)



# Utility function for creating and initialing
# the communicators used for sending back 
# actions to clients
def create_comm_pool(configs):

  # retrieve log directory (must exist)
  log_dir = configs["log_directory"]
 
  comm_pool_path = os.path.join(log_dir, "communicators")
  
  if not os.path.isdir(comm_pool_path):
    # first try to create a separate directory for
    # communicator log files.
    os.mkdir(comm_pool_path, 0o775) # drwxrwxr-x
    


  # cores are allocated for other reasons:
  # one  - for the http server, 
  # some - for short RSA,
  # some - for long RSA.
  cpu_cores = _compute_cpus_for_comms()
  

  # now create that many communicator processes
  comms = []

  for core_idx in xrange(0, cpu_cores, 1):
    tmp_srl_queue = multiprocessing.Queue(maxsize=0)
    tmp_lrl_queue = multiprocessing.Queue(maxsize=0)

    comms.append((CommProcess(tmp_srl_queue, tmp_lrl_queue,
        os.path.join(comm_pool_path, "comm_{0}.log".format(core_idx))),
        tmp_srl_queue, tmp_lrl_queue))

  # create a temporary class for single object creation
  class CommPool(object):
    """
      This class encapsulates a pool of processes for sending
      respones back to the clients.
    """
    def __init__(self, comm_list):
      self.comms   = comm_list


    """
      Method for creating an interface used by an 
      sRLA to pass actions to the communication processes.
    """
    def get_srla_messenger(self):
      return RLAMessenger(tuple([one_queue[1] for one_queue in self.comms]))

    
    """
      Method for creating an interface used by an
      lRLA to pass actions to the communication processes.
    """
    def get_lrla_messenger(self):
      return RLAMessenger(tuple([one_queue[2] for one_queue in self.comms]))


    """
      A method for creating a load balancer for balancing load
      among the communication processes. This method returns a 
      simple load balancer as only one instance is created
      in the entire program.
    """
    def get_load_balancer(self):
    
      class SimpleHashBalancer(object):
        """
          A straight-forward hash load balancer which just
          assigns an index according to the given string. 
          Since data center topologies are rather symmetric and
          our testbed follows symmetry, such a simple
          balancer is enough for our evaluation.
        """
        def __init__(self, upper_limit):
          self.max_limit = upper_limit

        """
          Class implementes the functor pattern.
        """
        def __call__(self, ip_addr):
          return (_compute_hash(ip_addr) % self.max_limit)


      return SimpleHashBalancer(len(self.comms))
      

    """
      Method for stopping the pool and releasing all the
      allocated resources.
    """
    def stop_pool(self):
      for one_comm in self.comms:
        #os.kill(one_comm[0].pid, signal.SIGTERM)
        one_comm[0].stop_comm()

      for one_comm in self.comms:
        one_comm[0].join(5.0) # wait to terminate the
                              # processes

      # iterate one more time and kill the remaining
      # processes if they are still active
      for one_comm in self.comms:
        if one_comm[0].is_alive():
          os.kill(one_comm[0].pid, signal.SIGKILL)
          one_comm[0].join()


      # clear the resources
      self.comms    = []
      self.clients  = {} 
      self.curr_idx = 0


    """
      Start a pool of processes.
    """
    def start_pool(self):
      for one_comm in self.comms:
        one_comm[0].start_comm()


  # create and return a pool of communication
  # processes
  return CommPool(comms)  
