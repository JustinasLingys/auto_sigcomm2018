# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import


# Compatible integers
from builtins import int


# Python std lib

import multiprocessing
import collections

# for Python2/3

try:
  import Queue
except ImportError:
  import queue as Queue


# Python custom modules
from central_system            import algorithms
from central_system.algorithms import ddpg


# Utility function for creating the deep reinforcement 
# learning model for handling small flows
def _create_rl_model(load_model,  load_weights,
                     save_model,  save_weights,
                     save_period, training):

  # create an instance of the DDPG algorihtm
  model = ddpg.Model()

  try:
    model.initialize(save_model, save_weights,
                     save_period,
                     load_model, load_weights,
                     training)

  except algorithms.AlgorithmError:
    return None
  else:
    return model # successfully built a DDPG


# Function for handling incoming requests
# from remote servers
def _process_func(sample_queue, comms,
                  save_period,  save_model,
                  save_weights, log_file,
                  load_model,   load_weights,
                  training):

  rl_model = _create_rl_model(load_model,  load_weights,
                              save_model,  save_weights,
                              save_period, training)

  if rl_model is None: # something went wrong
    with open(log_file, "a") as model_log:
      model_log.write("sRSA: '_create_rl_model' has failed.\n")

  else: # can start processing the requests
    batch = [] # a list of requests
    need_to_terminate = False # flag for exiting 
                              # the loop

    while not need_to_terminate:
      req = sample_queue.get(block=True) # wait for an 
                                         # incoming request

      if req is None:
        break # done processing 

      batch.append(req) # batch requests
      
      try:
        while True:
          req = sample_queue.get(block=False)
          
          if req is None:
            need_to_terminate = True
            break
          else:
            batch.append(req)

      except Queue.Empty:
        pass # done retrieving from the 
             # queue

      if need_to_terminate:
        break # done processing

      # process the batch and receive actions
      res, actions = rl_model.predict_actions(batch)
      assert actions, "rl_model.predict_actions is Empty/None"
      
      # send the actions back to the clients
      for one_act in actions:
        comms.send_action(one_act[0], one_act[1], 
                          one_act[2], one_act[3])


      # after dequeueing all the actions, need to
      # check if it's time to train the model
      if res and training:
        rl_model.train_model()

      
      # done sending the actions
      # reset the batch and wait for a new batch
      batch = []


  # need to clean up the resources
  rl_model.close_model()



class _SRLAImpl(object):

  """
    Short flow agent implementation used by the paper 'AuTO'.
  """
  def __init__(self):
    self.proc_handle = None
    self.proc_queue  = None
   


  """
    Method for initializing the agent. 
    

    @param communicators: reference to processes for sending 
                          actions back to the clients 

    @param save_period  : how often to save the model (seconds)
   
    @param save_model   : file the model will be saved to
    @param save_weights : file for saving the weights

    @param log_file     : file for logging erros end exceptions

    @param load_model   : file for model loading

    @param load_weights : file for loading weights

    @param train        : the training/inference step

  """

  def initialize(self,            communicators, 
                 save_period,     save_model, 
                 save_weights,    log_file,   
                 load_model=None, load_weights = None,
                 train=True):

    # check if it is the training step
    if train is False and load_weights is None:
      raise IOError("Inference requires weights files.")

    self.proc_queue  = multiprocessing.Queue(maxsize=0)
    self.proc_handle = multiprocessing.Process(
                         target=_process_func,
                         args=(self.proc_queue, communicators,
                               save_period,     save_model,
                               save_weights,    log_file,
                               load_model,      load_weights,
                               train))

  

    
  """
    Method called by a user of the agent to pass a new
    training sample.
  """
  def process_sample(self, one_sample):
    # no exception shall occur
    self.proc_queue.put(one_sample, block=False)
    
      
  """
    Start the training process.
  """
  def start_rla(self):
    # check if the process has not been started
    # before
    if self.proc_handle is not None and not self.proc_handle.is_alive():
      self.proc_handle.start()     
    

  """
    Stop the training process.
  """
  def stop_rla(self):
    # check if the process has been started before trying to 
    # stop it
    if self.proc_handle is not None and self.proc_handle.is_alive():
      self._join_proc()


  """
    Stop the training process from running and 
    wait for it to terminate.
  """
  def _join_proc(self):
    self.proc_queue.put(None, block=True)
    # wait for the process to terminate
    self.proc_handle.join()


# Default implementation of the short rl agent
DefaultShortFlowAgent = _SRLAImpl
