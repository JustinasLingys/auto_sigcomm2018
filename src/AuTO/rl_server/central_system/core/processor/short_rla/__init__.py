# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import


# Compatible integers
from builtins import int


# Python std lib
import os
import os.path

# Use factory functions for creating 
# abstractions of RSAs

from central_system.core.processor.short_rla.encoder import encode_action_to_binary

from central_system.core.processor.short_rla.agents  import DefaultShortFlowAgent



#import central_system.core.processor.short_rla.agents as agents

def create_srla(comms, configs):

  # retrieve all the required values
  # from the configuration dictionary
  log_dir = configs["log_directory"]
  srla_log_file = os.path.join(log_dir, "short-rla.log")

  srla = DefaultShortFlowAgent()


  srla.initialize(comms,
                  configs["save_weights_period"],
                  configs["save_short_rla_model"],
                  configs["save_short_rla_weights"],
                  srla_log_file,
                  configs["short_rla_model"],
                  configs["short_rla_weights"],
                  configs["training_phase"])
   
  return srla
