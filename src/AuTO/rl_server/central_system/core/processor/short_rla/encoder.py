# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import


# Compatible integers
from builtins import int


# Python std lib
import struct

# Custom Python modules
from central_system.algorithms import SHORT_RLA_ACTION_SPACE


# Maximum threshold value
_MAX_LIMIT = (2**32 - 1) 
_MIN_VAL   = 16 # minimum value for a gamma value
                # 16 bytes since 8-byte key and 8-byte
                # value is a common pattern in data centers
                # (this is a parameter which can be adjusted later) 

def _proc_value(value):
  
  # actions are real values, so need
  # to convert to integers
  check_val = int(value)

  if check_val > _MAX_LIMIT:
    return _MAX_LIMIT
  else:
    return check_val if (check_val > 0) else _MIN_VAL

# utility function for encoding short actions of a sample.
# The function takes an action produced by a deep reinforcement learning
# algorith and produces the binary representation of the action.  
def encode_action_to_binary(action):
  values = [ _proc_value(thrsh) for thrsh in action ]

  # encode the values into a binary string
  return struct.pack("".join(["<", "I"*len(values)]), *values)
  
  
