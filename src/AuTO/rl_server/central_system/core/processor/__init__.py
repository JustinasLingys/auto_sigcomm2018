# -*-coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import


# Compatible integers
from builtins import int


# Define an exception for throwing when 
# something wrng goes with the RSAs

try:
  import Queue
except ImportError:
  import queue as Queue

RLAError = Queue.Full # excpetion for notifying some 
                      # inter-process communication error.


# Use factory functions for creating 
# abstractions of RSAs

from central_system.core.processor.long_rla      import create_lrla
from central_system.core.processor.short_rla     import create_srla
from central_system.core.processor.communicators import create_comm_pool
