# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import


# Compatible integers
from builtins import int


# Python std lib
import signal
import threading
import json
import sys
import socket


# Python2/3 compatibility
try:
  import BaseHTTPServer
  import httplib
except ImportError:
  import http.server as BaseHttpServer
  import http.client as httplib




# For terminating the program
_GLOBAL_SIGNAL = threading.Event()
_GLOBAL_SIGNAL.set()

# Function for handling asynchronous signals
def _signal_handler(signum, frame):
  if signum == signal.SIGTERM:
    global _GLOBAL_SIGNAL
    _GLOBAL_SIGNAL.clear() # clear the flag



# Function for running the http server 
def thread_func(httpd, log_file):
  try:
    httpd.serve_forever(poll_interval=0.1)
  except Exception as exp:
    # log the exception to the given log file
    with open(log_file, "a") as http_err:
      http_err.write("http_server::serve_forever: {0}\n".format(exp))
    
  
# Utility function to create an htpp handler
# for handling http request coming from flow servers.
def _make_request_handler(balancer,
                          short_rsa, long_rsa):

  class CustomHTTPHandler(BaseHTTPServer.BaseHTTPRequestHandler):
    """
      Class for creating HTTP request handlers. The handler 
      encapsulates the HTTP request processing and inter-process
      comminication.
    """

    # Class-level shared objects for
    # passing requests to other processes
    sh_balancer  = balancer
    sh_short_rsa = short_rsa
    sh_long_rsa  = long_rsa

    def __init__(self,  *args, **kwargs):
      self.timeout = 60*20 # 20 min timeout
   
      BaseHTTPServer.BaseHTTPRequestHandler.__init__(self, *args, **kwargs)

      # keep connection open after successfull 
      # HTTP operation
      self.close_connection = False
      self.protocol_version = "HTTP/1.1"



    """
      The Central System communicates with flow servers 
      by encapsulating infomration into an HTTP POST message.
    """
    def do_POST(self):
    
      # read bytes and convert bytes into strings
      try:
 
        content_length = int(self.headers["Content-Length"])
        raw_bytes      = self.rfile.read(content_length)

        # the original version of AuTO used Python RPCs, but
        # later it was found that using http produces a bit
        # better performance.
        post_data      = json.loads(raw_bytes.decode("utf-8"))
    
        # check if the fields exist
        req_id         = int(post_data.get("req_id", -1))
        reward         = float(post_data.get("reward", 0.0))

        # state representation of long/short flows
        short_done     = post_data.get("short_completed", [])
        long_done      = post_data.get("long_completed",  [])
        long_active    = post_data.get("long_active",  [])
        long_idx       = post_data.get("long_idx",     [])   
 

        # received the states
        if (short_done or long_done or long_active) and req_id >= 0:
    
          # pass the state/reward info to the corrent 
          # learners and assign the communication process
          # for further processing
          comm_proc  = CustomHTTPHandler.sh_balancer(self.client_address[0])
          data_short = (comm_proc,   self.client_address[0],
                        req_id,      reward, 
                        short_done)

          data_long  = (comm_proc, self.client_address[0], 
                        req_id,    reward, 
                        (long_done, long_active, long_idx))

          # enqueue the samples to the rsas
          CustomHTTPHandler.sh_short_rsa.process_sample(data_short)
          CustomHTTPHandler.sh_long_rsa.process_sample(data_long)

          # send a signal to the client that the message
          # has successfully been processed
          self.send_response(httplib.OK)
          self.send_header("Content-Type", "text/plain")
          self.send_header("Content-Length", len(b"OK"))
          self.end_headers()
          self.wfile.write(b"OK")

        else: # some error occurred. ignore the message and
              # wait for a new one from the client
          self.send_response(httplib.OK)
          self.send_header("Content-Type", "text/plain")
          self.send_header("Content-Length", len(b"Continue"))
          self.end_headers()
          self.wfile.write(b"Continue")


      except Exception:
        # close the connection on any error
        self.finish()
        self.connection.close()
     
      

    """
      This method is not used.
    """
    def do_GET(self):
      pass # 


    """
      This method is not used
    """
    def log_messages(self, format, *args):
      pass


  # HTTP request handler for the REST interface
  return CustomHTTPHandler






# Function to start a server process.
# 
# params :
#     server_ip_addr : ipv4 address
#     load_balancer  : functor for determining communication process
#     short_rsa      : reference to theshort RSA agent 
#     long_rsa       : reference to the long RSA agent
#     log_file       : file for logging http errors
def run_server(server_ip_addr, load_balancer, 
               short_rsa, long_rsa, log_file):

  # create an http server object
  httpd = BaseHTTPServer.HTTPServer(server_ip_addr,
                                    _make_request_handler(load_balancer,
                                                          short_rsa,
                                                          long_rsa))
 
  # run the instance of the server on a separate
  # thread
  http_thread = threading.Thread(target=thread_func, args=(httpd, log_file))
  http_thread.daemon = False


  # register for signals
  signal.signal(signal.SIGTERM, _signal_handler)

  http_thread.start()

  global _GLOBAL_SIGNAL

  while _GLOBAL_SIGNAL.is_set():
    signal.pause() # suspend the current thread and wait for SIGTERM

  
  # terminate the server thread
  try:
    httpd.socket.shutdown(socket.SHUT_RDWR)
  except:
    pass # ignore
  finally:
    # close the socket and stop the server
    httpd.socket.close()
    httpd.shutdown()
  

  http_thread.join()
  

  # signal that the server has successfully completed
  with open(log_file, "a") as final_fd:
    final_fd.write("run_server: has successfully completed.\n")

  # done processing 
