# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import


# Compatible integers
from builtins import int


# Python std lib
import os
import os.path


from central_system.core.server import simple_http_server


def start_server(config_dict, load_balancer, 
                 short_rsa,   long_rsa):

  
  log_dir = config_dict["log_directory"]
  # create a log file for the http server
  http_log_file = os.path.join(log_dir, "http_server.log")
  
  # run the http server
  serv_ip_addr = tuple(config_dict["address"][0])
  simple_http_server.run_server(serv_ip_addr,
                                load_balancer,
                                short_rsa, long_rsa,
                                http_log_file)
