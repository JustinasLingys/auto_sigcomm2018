# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import


# Compatible integers
from builtins import int

import sys
import os
import os.path


# Exception for notifying that need to redo data enqueueing
from central_system.core.processor import RLAError
from central_system.core           import processor
from central_system.core           import server


# Function for running the RL server.
# This function picks the type of the server
# and uses the type to start it as a separate process.
def _run_central_system(config_dict):

  # first of all, try to create a pool of 
  # processes for sending respones back to 
  # clients
  comm_pool = processor.create_comm_pool(config_dict)
 
  # now create the reinforcement learning agents 
  long_agent  = processor.create_lrla(comm_pool.get_lrla_messenger(), 
                                      config_dict)
  short_agent = processor.create_srla(comm_pool.get_srla_messenger(), 
                                      config_dict)
 

  # start the created objects before 
  # starting the process of an HTTP server
  comm_pool.start_pool()
  long_agent.start_rla()
  short_agent.start_rla()


  # start running the server
  try:
    server.start_server(config_dict,
                        comm_pool.get_load_balancer(), 
                        short_agent, 
                        long_agent)
  except Exception as exp:
    raise exp
  finally:
    # need to clean the reousurces up
    short_agent.stop_rla()
    long_agent.stop_rla()
    comm_pool.stop_pool()

    short_agent = None
    long_agent  = None
    comm_pool   = None
