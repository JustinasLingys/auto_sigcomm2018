# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import


import platform
import multiprocessing


# Read the number of cpus from
# a system file
def _read_cpu_count():

  sys_conf = None

  try:
    sys_conf = open("/proc/cpuinfo", "r")
    cpu_num  = sys_conf.read().count("processor\t:")
  except:
    if sys_conf is not None:
      sys_conf.close()
    cpu_num = -1
  finally:
    return cpu_num


# Utility function for returning the number of 
# logical cores on the platform
#
#
# Return a positive integer if it posible to determine
# number of cores. A negative integer, if it is not possible.
# 
def cpu_count():

  sys_name = platform.system().lower()
  if sys_name is "" or not "linux" in sys_name:
    raise IOError("System is not Linux!")
  else:
    try:
      cpu_count = multiprocessing.cpu_count()
    except NotImplementedError:
      # try to retrive the number from the system files
      cpu_count = _read_cpu_count()
    finally:
      return cpu_count # may be negative 
