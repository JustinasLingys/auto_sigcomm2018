# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import




import sys
import os
import os.path
import ast


# include the very top of the central system directory
sys.path.insert(1, os.path.abspath(os.path.join(__file__, os.pardir,
                                               os.pardir)))

# Import the core of the process
from central_system.core import _run_central_system

def main(params):
  # convert a string into a Python dictionary
  param_dict = ast.literal_eval(params)
  print("A child process has been successfully started")
  _run_central_system(param_dict)


if __name__ == "__main__":
  # make sure that the process receives a dictionary
  # of parameters
  assert len(sys.argv) == 2, "Child process has not received a dictionary of parameters."

  main(sys.argv[1])

