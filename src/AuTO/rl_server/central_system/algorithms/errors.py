# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import


# Compatible integers
from builtins import int


# Define a custom exception thrown by
# the algorithm on errors

class AlgorithmError(Exception):
  """
    Base class for defining the algorithm exceptions.
  """
  def __init__(self, message):
    super(AlgorithmError, self).__init__(message)
