# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import

import numpy as np

def orstein_uhlenbeck_process(point, mu, theta, sigma):
  return (theta*(mu - point) + sigma * np.random.randn(1))
