# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import


# Compatible integers
from builtins import int

class IncrementalReward(object):
  """
    Class for computing moving average reward.
    One object is maintained per server.
  """
  def __init__(self, init_reward=0.0):
    self._avg_val  = 0.0
    self._steps    = int(0) # number of updates


  """
    Method for appending a new reward value to 
    the ongoing average.
  """
  def reward(self, reward_val):

    # a new request has arrived
    # (update the incremental average of rewards)
    self._steps   += 1 # one extra reward
    self._avg_val += ((reward_val - self._avg_val) / self._steps)
   

  """
    Return the current average reward value
  """
  def average(self):
    return self._avg_val
