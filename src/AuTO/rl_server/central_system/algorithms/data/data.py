# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import

# Dependencies
from builtins import int
import numpy as np
import six


import collections
import random


# Custom Python modules
from central_system.algorithms.data.ids     import ReqID
from central_system.algorithms.data.rewards import IncrementalReward


class ReplayBuffer(object):

  def __init__(self, buffer_size):
    self.buffer      = collections.deque(maxlen=buffer_size)
    self.buffer_size = int(buffer_size)
    self.next_sample = None
    self.num_samples = 0


  def get_batch(self, batch_size):
    # Randomly sample from the buffer and return 
    # a batch of experiences
    if self.num_samples < batch_size:
      return random.sample(self.buffer, self.num_samples)
    else:
      return random.sample(self.buffer, batch_size)


  def get_size(self):
    return self.num_samples

  
  def get_next_sample(self):
    return self.next_sample

  def set_next_sample(self, state, action, reward):
    self.next_sample = [state, action, reward, np.zeros_like(state)]

    
  def add_sample(self, state, action, reward):
  
    # adding a new sample only happens once the update on 
    # the previous one happens
    if self.next_sample is None:
      self.next_sample = [state, action, 0.0, np.zeros_like(state)]
      return

    # need to update the previous state and add it to the
    # sample population. The new sample becomes the next_sample
    
    self.next_sample[-1][:] = state  # copy the next_state
    self.next_sample[2]     = reward # reward comes now

    if self.num_samples < self.buffer_size:
      self.buffer.append(self.next_sample)
      self.num_samples += 1

    else:
      self.buffer.popleft() # oldest data item
      self.buffer.append(self.next_sample)


    # update the next sample
    self.set_next_sample(state, action, 0.0)
   


  def clear(self):
    self.buffer.clear()
    self.num_samples = 0
    self.next_sample = None


# A class which encapsules appending items to a particular array
# and samples from a shared buffer.
class SRLABuffer(object):
  def __init__(self, agent_buffer_size):
    self.agent_buffer = int(agent_buffer_size)
    self.agents       = {}  # dictionary which stores 
                            # agent's ip and its buffer

    self.total_samples = 0 # total number of samples
    self.num_agents    = 0 # number of agents

  # new states are only observed after a server sends 
  # a new update
  def add_sample(self, ip_addr, req_id, state, action, reward):

    serv_state = self.agents.get(ip_addr, None) 

    # check if the server has been added before
    if serv_state is None: 
      # keep track of the request id and new buffer
      self.agents[ip_addr] = [ReqID(req_id), ReplayBuffer(self.agent_buffer)]
      self.agents[ip_addr][1].add_sample(state, action, reward)
      self.num_agents    += 1  # increment the number of agents
    else:
      # now update new_state to state
      # append a new sample 
      # (make sure the latest sample is an old one)
      if serv_state[0] < req_id: # new sample
        serv_state[1].add_sample(state, action, reward)

        # buffer has a limited size
        if serv_state[1].get_size() < self.agent_buffer:
          self.total_samples += 1

      else:
        # set the previous sample to this value
        serv_state[1].set_next_sample(state, action, reward)
  
      # update request id
      serv_state[0].set(req_id) # update request id


  # get total number of samples
  def get_num_samples(self):
    return self.total_samples


  # clear structure
  def clear(self):
    for (ip_addr, sam_str) in six.iteritems(self.agents):
      sam_str[1].clear()
      sam_str[0].set(0)
     

    self.agents = {}
    self.num_agents = 0
    self.total_samples = 0

  # return 
  def get_samples(self, sample_size):

    if self.total_samples <= sample_size:
      # return a list of all valid samples
      samples = []
      for (ip_addr, sam_str) in six.iteritems(self.agents):
        samples.extend(sam_str[1].get_batch(sample_size))

      return samples

    else:
      # return random samples from each of the agents
      samples = [] # list of samples

      per_node_samples = sample_size // self.num_agents
      need_read = per_node_samples    


      for (ip_addr, sam_str) in six.iteritems(self.agents):
        rec_samples = sam_str[1].get_batch(need_read)

        if len(rec_samples) < need_read:
          need_read = (need_read << 1) - len(rec_samples)
        else:
          need_read = per_node_samples

        samples.extend(rec_samples)
   

      # handle the remainder part
      if sample_size % self.num_agents: # need to read extra
        # each client should have at least one sample
        need_extra_read = sample_size % self.num_agents
  

        for (ip_addr, sam_str) in six.iteritems(self.agents):
          extra_samples = sam_str[1].get_batch(need_extra_read)
        
          need_extra_read -= len(extra_samples)
          samples.extend(extra_samples)

          if need_extra_read <= 0:
            break         
  
      return samples # return read samples




# Version a ReplayBuffer with advanced 
# features (average maintenance)
class ReplayBuffer2(object):

  def __init__(self, buffer_size):
    self.buffer      = collections.deque(maxlen=buffer_size)
    self.buffer_size = int(buffer_size)
    self.next_sample = None
    self.num_samples = 0
    self.req_id      = None # request id


  def get_batch(self, batch_size):
    # Randomly sample from the buffer and return 
    # a batch of experiences
    if self.num_samples < batch_size:
      return random.sample(self.buffer, self.num_samples)
    else:
      return random.sample(self.buffer, batch_size)


  def get_size(self):
    return self.num_samples

  def set_next_sample(self, state, action, reward):
    self.next_sample = [state, action, reward]  

  def get_next_sample(self):
    return self.next_sample

    
  def add_sample(self, req_id, state, action, reward):
  
    
    # adding a new sample only happens once the update on 
    # the previous one happens
    if self.next_sample is None:
      self.next_sample = [state, action, 0.0] # ignore reward 
      self.req_id      = ReqID(req_id)
      return (False, 0.0) # don't update reward

    if self.req_id == req_id:
      # update next sample
      self.set_next_sample(state, action, 0.0)
      return (False, 0.0)

    if self.req_id < req_id:
      # need to update the previous state and add it to the
      # sample population. The new sample becomes the next_sample
    
      self.next_sample[2] = reward # now we got a reward

      if self.num_samples < self.buffer_size:
        self.buffer.append(self.next_sample)
        self.num_samples += 1

      else:
        self.buffer.popleft() # oldest data item
        self.buffer.append(self.next_sample)


      # update the next sample
      self.set_next_sample(state, action, 0.0)
      # update the request id
      self.req_id.set(req_id)

      return (True, reward) # reward value

    else: # means self.req_id > req_id  
      # update the next sample
      self.set_next_sample(state, action, 0.0)
      # update the request id
      self.req_id.set(req_id)

      return (False, 0.0)


  def clear(self):
    self.buffer.clear()
    self.num_samples = 0
    self.next_sample = None
    self.req_id      = None



# A class which encapsules appending items to a particular array
# and samples from a shared buffer. Used by long/large
# learning agent
class LRLABuffer(object):
  def __init__(self, agent_buffer_size):
    self.agent_buffer = int(agent_buffer_size)
    self.agents       = {}   # dictionary which stores 
                             # agent's ip and its buffer

    self.total_samples = 0   # total number of samples
    self.num_agents    = 0   # number of agents

    # average of rewards
    self.avg_reward    = IncrementalReward(0.0)


  # new states are only observed after a server sends 
  # a new update
  def add_sample(self, ip_addr, req_id, state, action, reward):

    serv_state = self.agents.get(ip_addr, None) 

    # check if the server has been added before
    if serv_state is None: 
      # keep track of the request id and new buffer
      serv_state           = ReplayBuffer2(self.agent_buffer)
      self.agents[ip_addr] = serv_state
      self.num_agents     += 1  # increment the number of agents
    
    # now try to add a sample and record its reward if
    # a success
    success, add_val = serv_state.add_sample(req_id, state, action, reward)

    if success:
      # update the average reward
      self.avg_reward.reward(add_val)
      if serv_state.get_size() < self.agent_buffer:
        self.total_samples += 1 # new one sample


  # get total number of samples
  def get_num_samples(self):
    return self.total_samples


  # clear structure
  def clear(self):
    for (ip_addr, sam_str) in six.iteritems(self.agents):
      sam_str.clear()
      

    self.agents = {}
    self.num_agents = 0
    self.total_samples = 0
    self.avg_reward    = IncrementalReward(0.0)


  def get_average_reward(self):
    return self.avg_reward.average()


  # return 
  def get_samples(self, sample_size):

    if self.total_samples <= sample_size:
      # return a list of all valid samples
      samples = []
      for (ip_addr, sam_str) in six.iteritems(self.agents):
        samples.extend(sam_str.get_batch(sample_size))

      return samples

    else:
      # return random samples from each of the agents
      samples = [] # list of samples

      per_node_samples = sample_size // self.num_agents
      need_read = per_node_samples    


      for (ip_addr, sam_str) in six.iteritems(self.agents):
        rec_samples = sam_str.get_batch(need_read)

        if len(rec_samples) < need_read:
          need_read = (need_read << 1) - len(rec_samples)
        else:
          need_read = per_node_samples

        samples.extend(rec_samples)
   

      # handle the remainder part
      if sample_size % self.num_agents: # need to read extra
        # each client should have at least one sample
        need_extra_read = sample_size % self.num_agents
  

        for (ip_addr, sam_str) in six.iteritems(self.agents):
          extra_samples = sam_str.get_batch(need_extra_read)
        
          need_extra_read -= len(extra_samples)
          samples.extend(extra_samples)

          if need_extra_read <= 0:
            break         
  
      return samples # return read samples
