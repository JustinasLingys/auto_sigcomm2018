# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import


# Compatible integers
from builtins import int


_MAX_REQ_ID_MOD = 2**32
_MAX_REQ_ID     = 2**32-1

class ReqID(object):
  """
    Object represents a request ID received
    from a remote server. The object maintains 
    internal state and logic how to compare req ids.
  """
  def __init__(self, init_id):
    self._req_id = init_id


  def increment(self):
    self.add(1)


  def set(self, value):
    self._req_id = value


  def __eq__(self, value):
    return (self._req_id == value)


  def __ne__(self, value):
    return (self._req_id != value)


  def __gt__(self, value):
    if value == _MAX_REQ_ID and self._req_id == 0:
      return True
    elif value == 0 and self._req_id == _MAX_REQ_ID:
      return False
    else:
      return self._req_id > value


  def __lt__(self, value):
    if value == _MAX_REQ_ID and self._req_id == 0:
      return False
    elif value == 0 and self._req_id == _MAX_REQ_ID:
      return True
    else:
      return self._req_id < value


  def __ge__(self, value):
    if self._req_id == value:
      return True

    else:
      return self._gt__(value)


  def __le__(self, value):
    if self._req_id == value:
      return True
    else:
      return self.__lt__(value)


  def add(self, diff):
    self._req_id = (self._req_id + diff) % _MAX_REQ_ID_MOD


  def sub(self, diff):
    if diff > self._req_id:
      self._req_id = (_MAX_REQ_ID_MOD - (diff - self._req_id))
    else:
      self._req_id = (self._req_id - diff)
