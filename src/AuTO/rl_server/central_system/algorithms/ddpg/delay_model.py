# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import


# File contains the logic of running an RL model and handling learning
# predictions.


# Python 2/3 std
import time
import os
import json


# other modules
import tensorflow as tf
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

from keras         import backend         as kBackend 
import                    numpy           as np

# Custom modules
from central_system.algorithms.ddpg   import networks
from central_system.algorithms.data   import noise  
from central_system.algorithms.data   import data as ddpgdata
from central_system.algorithms.errors import AlgorithmError


try:
  xrange
except NameError:
  xrange = range


# Hyper parameters
ACTIVE_SIZE        = 11*6
DONE_SIZE          = 10*7
STATE_VECTOR_SIZE  = DONE_SIZE + ACTIVE_SIZE
ACTION_VECTOR_SIZE = 3   # thresholds = (prio queues - 1)
BATCH_SIZE         = 128 # some multiple of # of servers 
BUFFER_SIZE        = BATCH_SIZE*100 # per agent buffer size 
TNH                = 0.001 # Target Network Hyper parameter
                           # a small value ensure stable learning
                           # (see the DDPG paper)


GAMMA              = 0.99  # disccout factor (no discount since
                           # one-action state)

                           # foe epsilon greedy 
EXPLORE            = 40000

INIT_EPSILON       = 1     # initial epsilon value

SLRA               = 0.1   # starting learning rate - actor
SLRC               = 0.1   # starinng learning rate - critic



class DDPG(object):
  '''
  This model encapsulates the required networks from the server
  and all the learning logic. The server communicates with the model
  through simple interfaces.
  '''
  def __init__(self):
    
    self.sess = tf.Session()
    kBackend.set_session(self.sess)

    self.actor  = networks.ActorNetwork(self.sess, STATE_VECTOR_SIZE,
                                        ACTION_VECTOR_SIZE,
                                        TNH, SLRA)

    self.critic = networks.CriticNetwork(self.sess, STATE_VECTOR_SIZE,
                                         ACTION_VECTOR_SIZE,
                                         TNH, SLRC)

    self.agents = ddpgdata.SRLABuffer(BUFFER_SIZE) # replay memory 
                                                   # buffer
                                                   # for a few agents

    self.accumulated = 0 # number of updates (states observed)
    self.epsilon     = INIT_EPSILON 

    self.train_flag  = True # training phase
   

  # Method for initialing the model
  
  # return: true on success; false on failure

  def initialize(self, save_models, save_weights,
                 period=3600.0,     load_models=None,
                 load_weights=None, training=True): 

    if not self.sess:
      raise AlgorithmError("Cannot initialize the session of DDPG.")

    if load_weights is None and training is False:
      raise AlgorithmError("Must provide load weights files for inference.")

    # create the models first and then initialize their weights
    if load_weights:
      try:
        self._load_weights(load_weights["actor_weights_filename"],
                           load_weights["critic_weights_filename"])
      except Exception as exp:
        self.sess.close()
        raise AlgorithmError(exp)

    # intialize all variables
    self.sess.run(tf.global_variables_initializer())

    # before starting to train, save the weights only
    try:
      self._save_weights(save_weights["actor_save_weights_filename"],
                         save_weights["critic_save_weights_filename"])    
    except Exception as exp:
      self.sess.close()
      raise exp

    # save save values and save weights

    self.model_files    = None
    self.weights_files  = dict(save_weights)
    
    if period > 0:
      self.train_time = time.time()
    
    self.period = float(period) # in seconds

    if not training:
      self.train_flag = False # not the trainin phase

  """
    Method for loading the models from files.
    Two files must be provided: one for the actor and
    one for the critic.
  """
  def _load_models(self, actor_file, critic_file):
    pass


  """
    Method for loading the weights of the models from
    the given files. 
  """ 
  def _load_weights(self, actor_file, critic_file):

    self.actor.model.load_weights(actor_file)   # actor model
    self.critic.model.load_weights(critic_file) # critic model
      
    # targets
    self.actor.target_model.load_weights(actor_file) 
    self.critic.target_model.load_weights(critic_file)
    

  """
    Make sure the state is of the appropriate size.
  """
  def _preproc_state(self, recv_state):
    
    # state conatins (short completed flows)
    short_state = recv_state[:]

    if len(short_state) < STATE_VECTOR_SIZE:
      short_state.extend([0]*(STATE_VECTOR_SIZE - len(short_state)))
    else:
      short_state = short_state[0:STATE_VECTOR_SIZE:1]

    # state representation 
    return np.array(short_state, dtype=np.float32).reshape((1, STATE_VECTOR_SIZE))
   
  """
    Method for inference (no training)
  """
  def _produce_actions(self, state_batch):
  
    actions = [] # produced actions  

    for update in state_batch: # for each update do updates
        
        features = self._preproc_state(update[4])
        action_p = self.actor.model.predict(features)

        # copy the actions into a list
        vals = [0]*ACTION_VECTOR_SIZE
        idx  = 0 
        
        for act_th in action_p[0]:
          vals[idx] = act_th
          idx += 1

   
        # append vals to actions
        actions.append((update[0], update[1], update[2], vals))


    return (False, actions) # no training

  # Receive a list of requests and predict their values.
  # return a tuple: (Bool, list_of_actions)
  # Bool is True if need to train; otherwise False
  #
  # one_state: comm_idx, ip_addr, req_id, reward, features
  def predict_actions(self, states):  
    if not states:
      return (False, None)

    if not self.train_flag: # no training
      return self._produce_actions(states)

    # item: index, ip_addr, req_id, reward, features

    self.accumulated += len(states) # when to train on a batch
   
    # make quick predictions and return if need to do model updates
   
    actions = [] # list of actions

    # generate a random probability for epsilon-greedy policy
    eps_rand = np.random.uniform(low=0.0, high=1.0, size=None)
   
    if self.epsilon <= 0.0 or self.epsilon < eps_rand: # no more noise
     
      for update in states: # for each update do updates
        
        features = self._preproc_state(update[4])
        action_p = self.actor.model.predict(features)

        # copy the actions into a list
        vals = [0]*ACTION_VECTOR_SIZE
        idx  = 0 
        for act_th in action_p[0]:
          vals[idx] = act_th
          idx += 1

        # batch the update
        self.agents.add_sample(update[1],   update[2], 
                               features[0], action_p[0], 
                               update[3])
    
        # append vals to actions
        actions.append((update[0], update[1], update[2], vals))

    else: # means epsilon has not converged yet
      self.epsilon -= (1.0 / EXPLORE)

      for update in states:
        
        # action and feature vectors
        action_t = np.zeros([1, ACTION_VECTOR_SIZE])
        features = self._preproc_state(update[4])   

        action_pred = self.actor.model.predict(features)
        vals = [0]*ACTION_VECTOR_SIZE # value of actions

        # perturb the actions by introducing some noise to the values
        for act_idx in xrange(0, ACTION_VECTOR_SIZE, 1):
          noise_t = self.epsilon*noise.orstein_uhlenbeck_process(action_pred[0][act_idx], 0.0, 0.6,0.3)
          action_t[0][act_idx] = 0 if (action_pred[0][act_idx] + noise_t < 0) else (action_pred[0][act_idx] + noise_t)
          vals[act_idx] = action_t[0][act_idx]

        actions.append((update[0], update[1],   # need to 
                        update[2], vals))       # return info    
                                                   
        # enqueue the update
        self.agents.add_sample(update[1],   update[2],
                               features[0], action_t[0], 
                               update[3])
  
  
    if self.accumulated >= BATCH_SIZE: # need to train
      self.accumulated = 0 # reset counter
      return (True, actions)


    return (False, actions) # no need to train yet


  # This method is only called if predict_actions 
  # have told to call it
  def train_model(self):
   
   
    # DO batch update
    batch       =   self.agents.get_samples(int(BATCH_SIZE))
    if len(batch) == 0:
      return # skip this step


    states      =   np.asarray([sam[0] for sam in batch])
    actions     =   np.asarray([sam[1] for sam in batch])
    rewards     =   np.asarray([sam[2] for sam in batch])
    new_states  =   np.asarray([sam[3] for sam in batch])

    y_labels    =   np.zeros_like(rewards, dtype=np.float32)
     

    target_q_values = self.critic.target_model.predict(
                           [new_states, 
                           self.actor.target_model.predict(new_states)])



    for idx_ in xrange(0, len(batch), 1): # always use predictions for
                                          # updates
                                          # non-episodic training
      y_labels[idx_] = rewards[idx_] + GAMMA*target_q_values[idx_]

 
    # retrain the models in order to improve their performance
    self.critic.model.train_on_batch([states, actions], y_labels)
    acts_for_upd = self.actor.model.predict(states)
    grads        = self.critic.gradients_critic(states, acts_for_upd)
    self.actor.train_actor(states, grads) # train the actor
   
      
    # update the target networks
    self.actor.train_actor_target()
    self.critic.train_critic_target()



    # if the time of saving the weights has passed
    # save critic_file    
    if self.period > 0.0:
      cur_time = time.time()
      if cur_time >= self.train_time + self.period:
        # save the weights
        self._save_weights(
          self.weights_files["actor_save_weights_filename"],
          self.weights_files["critic_save_weights_filename"])    
        
        # reset counting
        self.train_time = cur_time

   

  # stop model and save the weights
  def close_model(self):
    # need to release all the resources
   

    # release all samples
    self.agents.clear() 

    
    # save all the data
    if self.model_files:
      # save model
     
      with open(self.model_files[0], 'w') as fd:
        json.dump(self.actor.model.to_json(), fd)

      with open(self.model_files[1], 'w') as fd:
        json.dump(self.critic.model.to_json(), fd)

    # save the weights
   
    try:
      self._save_weights(
        self.weights_files["actor_save_weights_filename"],
        self.weights_files["critic_save_weights_filename"])    
    except Exception as exp:
      raise exp
    finally:
      # now it's safe to close the tensorflow session
      self.sess.close()
    

  # try saving the weights
  def _save_weights(self, actor_file, critic_file):
    self.actor.model.save_weights(actor_file, overwrite=True) 
    self.critic.model.save_weights(critic_file, overwrite=True)
