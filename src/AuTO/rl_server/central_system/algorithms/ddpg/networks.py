# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import


import numpy as np
import os
import tensorflow as tf
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

# Keras stuff
import keras.models
import keras.layers
import keras.optimizers
import keras.backend as KB


try:
  xrange
except NameError:
  xrange = range


# actor hyperparameters
ACTOR_DECAY_RATE = 0.96
ACTOR_DECAY_STEPS = 1000000

ACTOR_HIDDEN_ONE = 600
ACTOR_HIDDEN_TWO = 600


# critic hyperparameters
CRITIC_DECAY_RATE = 0.96
CRITIC_DECAY_STEPS = 1000000

CRITIC_HIDDEN_ONE = 600
CRITIC_HIDDEN_TWO = 600



class ActorNetwork(object):
  """
  Observes active/completed flows and outputs continuous values.
  Based on the DDPG algorithm ('Continuous cotrol with deep reinforcement learning', Lillicrap)
  """

  def __init__(self, sess, state_dim, action_dim, tnh, learning_rate):
    self.sess = sess
    self.target_net_hyper = tnh
    self.global_step = tf.Variable(1, trainable=False, name='actor_step',
                                   dtype=tf.int32)

    # set backed session
    KB.set_session(sess)

    # Create the actor's model
    self.model, self.weights, self.state = self._create_actor_network(state_dim, action_dim)
    
    # as the DDPG paper suggests, having a copy of the network and bootstraping on
    # the copy helps stabilize training
    self.target_model, self.target_weights, self.target_state = self._create_actor_network(state_dim, action_dim)

    # gradients are fed from the DDPG critic
    self.action_gradient = tf.placeholder(tf.float32, shape=[None, action_dim])
    self.params_grad = tf.gradients(self.model.output, self.weights, self.action_gradient)
    grads = zip(self.params_grad, self.weights)
    
    self.optimize = tf.train.AdamOptimizer(tf.train.exponential_decay(learning_rate, self.global_step,
                                           ACTOR_DECAY_STEPS, ACTOR_DECAY_RATE, staircase=True)).apply_gradients(grads)

   

  def train_actor(self, states, action_grads):
    self.sess.run(self.optimize, feed_dict={self.state: states,
                                            self.action_gradient: action_grads}) 
    # update training decay variable
    self.sess.run(tf.assign(self.global_step, self.global_step+1))

  def train_actor_target(self):
    actor_weights = self.model.get_weights()
    actor_target_weights = self.target_model.get_weights()
    
    for idx in xrange(0, len(actor_weights), 1):
      actor_target_weights[idx] = self.target_net_hyper * actor_weights[idx] + (1 - self.target_net_hyper)*actor_target_weights[idx]

    self.target_model.set_weights(actor_target_weights)

    

  def _create_actor_network(self, state_dim, action_dim):
  
    # since tensorflow used as the  backend, use scope

    in_layer   = keras.layers.Input(shape=[state_dim])
    hidden_one = keras.layers.Dense(ACTOR_HIDDEN_ONE, activation='relu', use_bias=True,
                                    kernel_initializer='glorot_normal',
                                    bias_initializer='glorot_normal')(in_layer)

    hidden_two = keras.layers.Dense(ACTOR_HIDDEN_TWO, activation='relu', use_bias=True,
                                    kernel_initializer='glorot_normal',
                                    bias_initializer='glorot_normal')(hidden_one)

    # Layers of thresholds (vector: delta theta)
    theta_1 = keras.layers.Dense(1, activation='relu', use_bias=True, 
                                 kernel_initializer='glorot_normal',
                                 bias_initializer='glorot_normal')(hidden_two)

    theta_2 = keras.layers.Dense(1, activation='relu', use_bias=True, 
                                 kernel_initializer='glorot_normal',
                                 bias_initializer='glorot_normal')(hidden_two)

    theta_3 = keras.layers.Dense(1, activation='relu', use_bias=True, 
                                 kernel_initializer='glorot_normal',
                                 bias_initializer='glorot_normal')(hidden_two)

    #theta_4 = keras.layers.Dense(1, activation='relu', use_bias=True, 
    #                             kernel_initializer='glorot_normal',
    #                             bias_initializer='glorot_normal')(hidden_two)

    #theta_5 = keras.layers.Dense(1, activation='relu', use_bias=True, 
    #                             kernel_initializer='glorot_normal',
    #                             bias_initializer='glorot_normal')(hidden_two)

    #theta_6 = keras.layers.Dense(1, activation='relu', use_bias=True, 
    #                             kernel_initializer='glorot_normal',
    #                               bias_initializer='glorot_normal')(hidden_two)

    #theta_7 = keras.layers.Dense(1, activation='relu', use_bias=True, 
    #                             kernel_initializer='glorot_normal',
    #                             bias_initializer='glorot_normal')(hidden_two)

    # merge the layers
    out_layer = keras.layers.concatenate([theta_1, theta_2, theta_3])  

    model = keras.models.Model(inputs=in_layer, outputs=out_layer)

    return (model, model.trainable_weights, in_layer)



class CriticNetwork(object):
  '''
  Critic as stated in the DDPG paper
  '''

  def __init__(self, sess, state_dim, action_dim, tnh, learning_rate):
    self.sess = sess
    self.target_net_hyper = tnh
    self.action_dim       = action_dim
    self.global_step      = tf.Variable(1, name='critic_step', 
                                        trainable=False, dtype=tf.int32)


    # set backed 
    KB.set_session(sess)
    
    # create critic
    self.model, self.action, self.state = self._create_critic_network(state_dim, action_dim, learning_rate)
    self.target_model, self.target_action, self.target_state = self._create_critic_network(state_dim, action_dim, learning_rate)

    self.action_grads = tf.gradients(self.model.output, self.action)



  def gradients_critic(self, states, actions):
    # increment decay
    self.sess.run(tf.assign(self.global_step, self.global_step+1))
    return self.sess.run(self.action_grads, feed_dict={self.state: states,
                                                       self.action: actions})[0]

  def train_critic_target(self):
    critic_weights = self.model.get_weights()
    critic_target_weights = self.target_model.get_weights()

    for idx in xrange(0, len(critic_weights), 1):
      critic_target_weights[idx] = self.target_net_hyper*critic_weights[idx] + (1-self.target_net_hyper)*critic_target_weights[idx]

    self.target_model.set_weights(critic_target_weights)



  def _create_critic_network(self, state_dim, action_dim, starting_rate):
  
    in_critic_layer = keras.layers.Input(shape=[state_dim]) 
    in_actor_layer  = keras.layers.Input(shape=[action_dim], name='action2')
    
    hidden_one = keras.layers.Dense(CRITIC_HIDDEN_ONE, activation='relu', use_bias=True,
                                    kernel_initializer='glorot_normal', 
                                    bias_initializer='glorot_normal')(in_critic_layer)

    actor_hidden = keras.layers.Dense(CRITIC_HIDDEN_TWO, activation='linear', use_bias=True,
                                      kernel_initializer='glorot_normal',
                                      bias_initializer='glorot_normal')(in_actor_layer)

    hidden_two = keras.layers.Dense(CRITIC_HIDDEN_TWO, activation='linear', use_bias=True,
                                    kernel_initializer='glorot_normal',
                                    bias_initializer='glorot_normal')(hidden_one)

    merge_layer = keras.layers.add([hidden_two, actor_hidden])

    hidden_three = keras.layers.Dense(CRITIC_HIDDEN_TWO, activation='relu', use_bias=True,
                                      kernel_initializer='glorot_normal',
                                      bias_initializer='glorot_normal')(merge_layer)

    out_layer = keras.layers.Dense(1, activation='linear', use_bias=True,
                                   kernel_initializer='glorot_normal',
                                   bias_initializer='glorot_normal')(hidden_three)

    # create a model
    model = keras.models.Model(inputs=[in_critic_layer, in_actor_layer], outputs=out_layer)
    # use a tensorflow optimizer
    tf_adam = tf.train.AdamOptimizer(tf.train.exponential_decay(
                                     starting_rate, self.global_step,
                                     CRITIC_DECAY_STEPS, CRITIC_DECAY_RATE, 
                                     staircase=True))

    model.compile(loss='mse', optimizer=keras.optimizers.TFOptimizer(tf_adam))

    return (model, in_actor_layer, in_critic_layer)
    

