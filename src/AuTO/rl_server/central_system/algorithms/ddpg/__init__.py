# -*- coding: utf-8 -*- 

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import


# Compatible integers
from builtins import int

from central_system.algorithms.ddpg.delay_model import DDPG as Model
