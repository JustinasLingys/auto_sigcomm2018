# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import


# Compatible integers
from builtins import int


# Import the modules to be accessible globally
from central_system.algorithms.errors import AlgorithmError
from central_system.algorithms        import ddpg
from central_system.algorithms        import vreinforce

# Also import some constants from the algorithms
from central_system.algorithms.ddpg.delay_model import ACTION_VECTOR_SIZE as SHORT_RLA_ACTION_SPACE
