# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import



# File contains a variant of a traditional Policy Gradient 
# algorithm. The core of the algorith is the REINFORCE algorithm
# with a slight modification described in the sigcomm 2018 paper.

# Compatibilty between Python 2 and Python 3
from builtins import int
try:
  xrange
except NameError:
  xrange = range


# ignore warnings
import os
import tensorflow as tf
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'


# Dependencies
from   keras.models import Model           as kModel 
from   keras.models import model_from_json as kmodel_form_json
from   keras.layers import Input           as kInput
from   keras.layers import Dense           as kDense 
from   keras        import backend         as kBackend
from   keras        import optimizers      as koptimizers
import numpy                               as np


# Python std lib
import itertools
import time

# Custom Python modules
from central_system.algorithms.data   import data as dpgdata
from central_system.algorithms.data   import noise
from central_system.algorithms.errors import AlgorithmError


# Constants used to construct the deep neural network

# Action space which is related to the Linux Traffic Control (TC)
# possibilities (for 1G network links)

# 'priority' : DSCP value enforced at end hosts and
#              throughout the network fabric
#
# 'rate'     : Linux TC 'htb' rate enforced at end hosts

ACTION_SPACE = (
  {"priority" : 0x00, "rate" : 100},
  {"priority" : 0x00, "rate" : 200},
  {"priority" : 0x00, "rate" : 300},
  {"priority" : 0x00, "rate" : 400},
  {"priority" : 0x00, "rate" : 500},
  {"priority" : 0x00, "rate" : 600},
  {"priority" : 0x00, "rate" : 700},
  {"priority" : 0x00, "rate" : 800},
  {"priority" : 0x00, "rate" : 900},
  {"priority" : 0x20, "rate" : 100},
  {"priority" : 0x20, "rate" : 200},
  {"priority" : 0x20, "rate" : 300},
  {"priority" : 0x20, "rate" : 400},
  {"priority" : 0x20, "rate" : 500},
  {"priority" : 0x20, "rate" : 600},
  {"priority" : 0x20, "rate" : 700},
  {"priority" : 0x20, "rate" : 800},
  {"priority" : 0x20, "rate" : 900},
  {"priority" : 0x40, "rate" : 100},
  {"priority" : 0x40, "rate" : 200},
  {"priority" : 0x40, "rate" : 300},
  {"priority" : 0x40, "rate" : 400},
  {"priority" : 0x40, "rate" : 500},
  {"priority" : 0x40, "rate" : 600},
  {"priority" : 0x40, "rate" : 700},
  {"priority" : 0x40, "rate" : 800},
  {"priority" : 0x40, "rate" : 900},
  {"priority" : 0x80, "rate" : 100},
  {"priority" : 0x80, "rate" : 200},
  {"priority" : 0x80, "rate" : 300},
  {"priority" : 0x80, "rate" : 400},
  {"priority" : 0x80, "rate" : 500},
  {"priority" : 0x80, "rate" : 600},
  {"priority" : 0x80, "rate" : 700},
  {"priority" : 0x80, "rate" : 800},
  {"priority" : 0x80, "rate" : 900} 
              )


# Action space size
ACTION_VECTOR_SIZE = len(ACTION_SPACE)
# A single hidden layer with that many hidden units
HIDDEN_UNITS       = 300
HIDDEN_LAYERS      = 9

# Input layer
ACTIVE_VECTOR      = 10*6 # num of flows * feature size
DONE_VECTOR        = 10*7 # num of flows * feature size

# 10 active large flows + 10 completed large flows +
# 1 active large flow for which action is computed (1 out of 10)
FEATURE_VECTOR = ACTIVE_VECTOR + DONE_VECTOR + 6 # active/completed flows 

BATCH_SIZE   = 32 # this match size was used 
                  # (was observed that has to be much smaller than
                  #  'BATCH_SIZE' of SRLA since there are much more
                  #  small flows than large ones in data centers)
BUFFER_SIZE  = BATCH_SIZE*1000
EXPLORE      = 40000 
INIT_EPSILON = 1 # initial value for epsilon 


# Utility function for flattening a list of lists out
def _flatten_list_out(list_of_lists):
  return list(itertools.chain.from_iterable(list_of_lists))


class vReinforce(object):
  """
    A Polici Gradient algorithm used to schedulre large data
    flows at servers.
  """
  def __init__(self):

    self.clients     = {}   # map between IP address and a reward structure
    self.model       = None # Deep neural network model
    self.epsilon     = INIT_EPSILON  # for epsilon-greedy approach
    

    self.accumulated = 0              # for training on batches



    self.period       = 0.0 # how often to save the weights 
    self.train_time   = 0.0 # when was the last time for saving weights
    self.model_file   = ""  # for saving the model structure
    self.weights_file = ""  # for saving the model weights

    self.sample_mem    = dpgdata.LRLABuffer(BUFFER_SIZE) # replay memory
   
    self.train_flag    = True # if training phase

  
  """
    Method for initializing the PG model. It takes files for 
    saving the model and weights. Optionally, period of saving
    and initial model structure and weights may be passed.
  """
  def initialize(self, save_model, save_weights,
                 save_period=3600.0, 
                 load_model=None, load_weights=None,
                 training=True):

    # inference phase requires weights
    if training is False and load_weights is None:
      raise AlgorithmError("Inference requires weights files!")

    
    if load_model and load_weights: # both files must exist
      self.model = self._load_model(load_model, load_weights)
      # make sure the model input/output vector sizes are correct
      if self.model.input.shape[1] != FEATURE_VECTOR or self.model.output.shape[1] != ACTION_VECTOR_SIZE:
        raise AlgorithmError("Loaded deep learning model does not have required input/output shapes. Required input shape = (None, {0}) and Required ouput shape = (None, {1}). Received input = {2} and ouput = {3} !".format(FEATURE_VECTOR, ACTION_VECTOR_SIZE, self.model.input.shape, self.model.ouput.shape)) 

    else:
      self.model = self._create_model() # create a new model from scratch 

    # build a customized training function
    self.train_fn = self._create_train_func()


    # save the model and weights at the very beginning
    self.period     = float(save_period)
    self.train_time = time.time() 
    self._save_model(save_model, save_weights)
    self.model_file   = save_model
    self.weights_file = save_weights

    if not training: # no epsilon-greedy policy
      self.train_flag = False



  """
    Method for bulding a deep reinforcement learning model 
    from the given files.
  """
  def _load_model(self, model_file, weights_file):

    # load the model structure/architecture from a JSON file
    json_string = ""
    json_fd     = None
    
    try:
      json_fd = open(model_file, "r")
      json_string = json.load(json_fd, encoding="utf-8")
    except Exception as exp:
      raise AlgorithmError(exp)
 
    else: # initialize the model
      tmp_model = kmodel_from_json(json_string)

      # initialize the weights
      tmp_model.load_weights(weights_file)

      return tmp_model

    finally:
      if json_fd:
        json_fd.close()


  """
    Method for creating a new model.
    The model uses the predefined constants to 
    create and initialize its weights
  """
  def _create_model(self):
    
    # input layer
    in_layer      = kInput(shape=(FEATURE_VECTOR,))
    curr_layer    = in_layer

    # build hidden layers
    for _ in xrange(0, HIDDEN_LAYERS, 1):
      curr_layer = kDense(HIDDEN_UNITS, 
                          kernel_initializer="glorot_uniform",
                          activation="sigmoid")(curr_layer)
  


    # output layer
    curr_layer = kDense(ACTION_VECTOR_SIZE,
                        kernel_initializer="glorot_uniform",
                        activation="softmax")(curr_layer) 
  

    # return an instance of the Model class
    return kModel(inputs=in_layer, outputs=curr_layer)

  """
    Method for creating a custom training function. This 
    function is required since Keras provided once restriscts
    some updates (need to have some access to the internal values)

    The loss function used by the deep learning framework
    for updating the weights.
    The loss function follows the modified version of the 
    REINFORCE weight update.

  """
  def _create_train_func(self):
    # action probabilities
    action_prob_placeholder = self.model.output

    # actions (outputs of the neural network)
    action_placeholder      = kBackend.placeholder(shape=(None, ACTION_VECTOR_SIZE), name="action_vectors") 

    # a scalar reward value
    reward_placeholder      = kBackend.placeholder(shape=(None, 1), name="action_rewards")
    
    # actions (probability values)
    action_prob = kBackend.sum(action_prob_placeholder*action_placeholder,
                              axis=1)

    # a log of actions in the formula log(pi(s,a))
    log_action_prob = kBackend.log(action_prob)

    # (reward - average_reward) -- for reducing variance
    loss_val  = -log_action_prob*reward_placeholder
    loss_val = kBackend.mean(loss_val)

    # SGD optimizer
    # (learning rate was tested and eventaully selected after
    # testing. Also, the selected values are common for 
    # deep reinforcement learning works (e.g, Alpha Go))
    sgd_op    = koptimizers.SGD(lr=0.01,      momentum=0.9, 
                                decay=1e-6,   nesterov=False,
                                clipnorm=1.0, clipvalue=0.5)

    gradients = sgd_op.get_updates(params=self.model.trainable_weights,
                                   loss=loss_val)

    return kBackend.function(inputs=[self.model.input,
                                     action_placeholder,
                                     reward_placeholder],
                             outputs=[],
                             updates=gradients) 

  """
    Method for saving the model and the weights of the 
    active deep reinforcement model.
  """
  def _save_model(self, model_file, weights_file):
    
    self._save_weights(weights_file)
    #self._save_model_structure(model_file)


  """
    A separate method for saving the model structure into a json
    file.
  """
  def _save_model_structure(self, filename):
    json_model = self.model.to_json() # save as a json string

    with open(filename, "w") as json_file:
      json.dump(json_model, json_file, encoding="utf-8")

  """
    A separate method for just saving the weights of the model
    to a h5 file.
  """
  def _save_weights(self, filename):
    self.model.save_weights(filename, overwrite=True)


  """
    The loss function used by the deep learning framework
    for updating the weights.
    The loss function follows the modified version of the 
    REINFORCE weight update.

    Sonce the loss function has to be minimzed, we return
    the negative value so that a postive reward would produce
    a more negative value.
  """
  def loss_function(self, y_true, y_pred):
    
    if self.rinfo:
      # use the modified version
      return -Backend.log(y_true)*(self.reward -\
                                   self.rinfo.get_baseline())
    else:
      # use the default REINFORCE update
      return -Backend.log(y_pred)*self.reward

  """
    Return a combination of two types of flows
  """
  def _preproc_state(self, compl_flows, ongoing_flows):

    # if there are no ongoing flows,
    # nothing to train on what (no decisions)
    if not ongoing_flows:
      return (False, None)

    if len(compl_flows) < DONE_VECTOR:
      done_flows = compl_flows + [0]*(DONE_VECTOR - len(compl_flows))
    else:
      done_flows = compl_flows[0:DONE_VECTOR:1] 

    # active flows is a list of lists
    active_flows = _flatten_list_out(ongoing_flows)[0:ACTIVE_VECTOR:1]
    
    if len(active_flows) < ACTIVE_VECTOR:
      active_flows.extend([0]*(ACTIVE_VECTOR - len(active_flows)))
    
    # an numpy array that merges all lists into one array
    # 1. zero vector for replacement of one active for computation;
    # 2. completed flows; 3. active flows; 
    state_rep = np.concatenate(([0]*6, done_flows, active_flows), axis=0)
    state_rep = np.reshape(state_rep, (1, FEATURE_VECTOR))
    state_rep = np.tile(state_rep, (len(ongoing_flows), 1))

    # fill in hte information of the ongoin flows
    for flow_idx in xrange(0, len(ongoing_flows), 1):
      state_rep[flow_idx][0:6:1] = ongoing_flows[flow_idx]

    return (True, state_rep) # representation of the state
  

  """
    Method for inference (no training)
  """
  def _produce_actions(self, state_batch):

    actions = [] # a list of actions

    for update in state_batch:
 
      avail, features = self._preproc_state(update[4][0], update[4][1])
      if not avail: # nothing to send
        actions.append((update[0], update[1], update[2], ()))
      else:
        action_v = self.model.predict(features,  features.shape[0], 
                                      verbose=0, steps=None)

        # maximum of each row
        argmaxes = action_v.argmax(axis=1)

        # append actions
        actions.append((update[0],    update[1], update[2], 
                       (update[4][1], argmaxes,  update[4][-1])))   


    # never train on this data
    return (False, actions)

  """
    The method is an interface for an agent to process
    a batch of samples. A sample contains an IP address,
    a reward scalar and a feature vector.

    batch contains an iterable of tuples:
    Tuple: (comm_idx, IPv4 address, req_id, reward, 
            (completed flows, active_flows))

    return: tuple: (Bool, Actions)
  """
  def predict_actions(self, batch):
  
    if not batch:
      return (False, None)

    if not self.train_flag: # no training
      return self._produce_actions(batch)

    # when to train on a batch
    self.accumulated += len(batch)

    # a list of actions for the list of samples
    actions = []
   

    if self.epsilon <= 0.0: # no more greedy policy (converged)

      for update in batch:

        avail, features = self._preproc_state(update[4][0], update[4][1])
        if not avail: # no long flows
          actions.append((update[0], update[1], update[2], ()))
          self.accumulated -= 1 # invalid sample
          continue

        action_v = self.model.predict(features,  features.shape[0], 
                                      verbose=0, steps=None)


        # 'action_vectors' are one-hot vectors, so transform
        # the predictions into a one-hot vector
        one_hot_vect = np.zeros((action_v.shape[0], action_v.shape[1]),
                                 dtype=np.int)

        action_data = [0]*action_v.shape[0] # the actions to be sent

        # maximum of each row
        argmaxes = action_v.argmax(axis=1)

        for act_idx in xrange(0, action_v.shape[0], 1):
          # be greedy and pick an action with the highest
          # probability
          one_hot_vect[act_idx][argmaxes[act_idx]] = 1
          action_data[act_idx] = argmaxes[act_idx]

        

        # add the sample to the replay memory/buffer
        self.sample_mem.add_sample(update[1], update[2],
                                   features,  one_hot_vect,
                                   update[3])
        
        
        # append actions
        actions.append((update[0],    update[1],   update[2], 
                       (update[4][1], action_data, update[4][-1])))

 
    else: # epsilon-greedy

      valid_sample = False # if has any valid large flows    

      for update in batch:
        avail, features = self._preproc_state(update[4][0], update[4][1])
        
        if not avail: # no long flows
          actions.append((update[0], update[1], update[2], ()))
          self.accumulated -= 1 # invalid sample
          continue
      
        valid_sample = True # found a valid flow
        action_v = self.model.predict(features, features.shape[0], 
                                      verbose=0,  steps=None)

        # 'action_vectors' are one-hot vectors, so transform
        # the predictions into a one-hot vector
        one_hot_vect = np.zeros((action_v.shape[0], action_v.shape[1]),
                                 dtype=np.int)

        action_data = [0]*action_v.shape[0] # the actions to be sent

        # maximum of each row
        argmaxes = action_v.argmax(axis=1)

 
        # generate some random probabilities
        greedy_probs = np.random.uniform(low=0.0, high=1.0, 
                                         size=(action_v.shape[0], 1))

        # random integers (per row index)
        random_actions = np.random.randint(low=0, 
                                           high=action_v.shape[1],
                                           size=(action_v.shape[0],1))
   

        for act_idx in xrange(0, action_v.shape[0], 1):
          if self.epsilon >= greedy_probs[act_idx]:
            # pick a random action
            one_hot_vect[act_idx][random_actions[act_idx]] = 1
            action_data[act_idx] = random_actions[act_idx][0]
          else:
            # be greedy and pick an action with the highest
            # probability
            one_hot_vect[act_idx][argmaxes[act_idx]] = 1
            action_data[act_idx] = argmaxes[act_idx]

        

        # add the sample to the replay memory/buffer
        self.sample_mem.add_sample(update[1], update[2],
                                   features,  one_hot_vect,
                                   update[3])
        
        
        # append actions
        actions.append((update[0],    update[1],   update[2], 
                       (update[4][1], action_data, update[4][-1])))

            
      # update epsilon to make the policy more greedy
      if valid_sample:
        self.epsilon -= (1.0 / EXPLORE)
        self.epislon  = max(self.epsilon, 0.0)
 
    if self.accumulated >= BATCH_SIZE: # need to train 
                                       # the model
      self.accumulated = 0 # reset counter
      return (True, actions)

 
    return (False, actions)   


  """
    Method is called whenever the model has to be
    trained.
  """
  def train_model(self):
    batch_samples = self.sample_mem.get_samples(BATCH_SIZE)
    if len(batch_samples) == 0:
      return # skip this step
  
    # get the current average reward value so that the value
    # could be used in training
    avg_reward = self.sample_mem.get_average_reward()

    # expand the batch samples into states, actions, and rewards
    states         = np.vstack([one_sam[0] for one_sam in batch_samples])  
    action_vectors = np.vstack([one_sam[1] for one_sam in batch_samples])
    action_rewards = np.vstack([(one_sam[2] - avg_reward) for one_sam in batch_samples])


    
    # train on the batch
    self.train_fn([states, action_vectors, action_rewards])

    # check if need to save the weights
    if self.period > 0 and self.train_time + self.period >= time.time():
      # save weights
      self._save_model(self.model_file, self.weights_file)


  """
    Done with this model
  """
  def close_model(self):
    self._save_model(self.model_file, self.weights_file)
    self.sample_mem.clear() 
    self.model = None
