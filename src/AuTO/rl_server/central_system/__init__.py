# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import


# Compatible integers
from builtins import int


from central_system.configs import read_configs


import sys
import os
import os.path


if sys.version_info[0] == 3 and sys.version_info[1] >= 3:
  import subprocess as py_process
else:
  try:
    # subprocess32 is a dependency of the project
    import subprocess32 as py_process
  except ImportError:
    import subprocess   as py_process


PY_COMMAND = "python3" if sys.version_info[0] == 3 else "python2"
_CHILD_PROCESS_PATH = os.path.abspath(os.path.join(__file__,
                                      os.pardir, os.pardir,
                                      "_cs_process.txt"))
COMMAND_PATH = os.path.abspath(os.path.join(__file__, 
                               os.pardir, "cs_process.py"))

# Function for running the RL server.
# This function picks the type of the server
# and uses the type to start it as a separate process.
def run_central_system(log_file, config_dict):

  # logging file
  log_handle = open(log_file, "w")

  # start a new daemon process and pass the paramters
  daemon_proc = py_process.Popen(
                  args=[PY_COMMAND, COMMAND_PATH, str(config_dict)],
                  bufsize=-1, stdin=None, stdout=log_handle,
                  stderr=log_handle, shell=False,
                  close_fds=True, universal_newlines=True
                )

  # a child daemon process has been started
  # save the id for future
  with open(_CHILD_PROCESS_PATH, "w") as child_fd:
    child_fd.write("Central System process pid: {0}\n".format(daemon_proc.pid))


