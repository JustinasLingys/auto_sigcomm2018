from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import


# Compatible integers
from builtins import int


# Python std lib
import argparse
import os
import os.path
import sys


# Custom Python modules for running
# the central system
from central_system import read_configs
from central_system import run_central_system



# Function for checking the capabilities 
# on the given file.
def _check_file_caps(filepath, capability):

  # check if the file can be opened with
  # the required capabilities
  
  file_desc = None

  try:
    file_desc = open(filepath, capability)
  except IOError:
    return False

  else:
    return True

  finally:
    if file_desc:
      file_desc.close()



def main(config_file, log_file):

  # need to make sure that the configuration file can
  # be read and the log file can be written
  if not _check_file_caps(log_file, "w"):
    print("Your passed log file '{0}' cannot be written.".format(log_file))
    return

  if not _check_file_caps(config_file, "r"):
    print("Your passed configuration file '{0}' cannot be read.".format(config_file))
 
  else: 
    # the configuration file can be read
    
    cs_configs = None # configurations

    try:
      cs_configs = read_configs(config_file)

    except IOError as exp:
      print("Error:", exp)

    else:
      # start a daemon process for running the central system
      
      try:
        run_central_system(log_file, cs_configs)
      except (IOError, OSError) as err_exp:
        print("Error:", err_exp)

      else:
        # notify the user of a successful start
        print("Central System of AuTO has been successfully started.")



if __name__ == "__main__":

  # parser for processing program parameters
  parser = argparse.ArgumentParser(description="Processing of settings of the Central System of AuTO.")

  parser.add_argument("--config", type=str,
                      help="Central System configuration file.",
                      required=True)

  parser.add_argument("--log_file", type=str,
                      help="Log file for the CS process",
                      required=True) 

  
  # parse arguments and start initializing the system
  args = parser.parse_args()

  main(config_file = str(args.config),
       log_file    = str(args.log_file)) 
