# Central System Server


This directory contains the implementation of the `Central System`
of AuTO. To run the central system, you need to enter `python run_rl_server  --config [cong_file], --log_dir [log_file]`. The `Central System` server
must be started before any flows start flowing since AuTO schedulers
on flow servers initially try to connect to the `CS` server before entering
 the flow scheduling phase.

To learn about the configuration, please refer to the `central_system/configs` direcotry.
