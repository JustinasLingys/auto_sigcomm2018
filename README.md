# AuTO SIGCOMM2018


This repository contains slightly modifed files of the AuTO research 
paper published at ACM SIGCOMM 2018. The original code has been 
refactored a bit to make it easier for other people to read.
In addition, this version has an experimental support for Python3
if other researchers prefer this version of Python 
(original code was purely written in Python2 and only tested with Python2).


The data center workload traces used to evaluate AuTO can be found
in the 'conf' direcotry of the [Traffic Generator](https://github.com/HKUST-SING/TrafficGenerator) repository. 


Copyright "System and Networking Group (SING)", HKUST.


If found any bugs, please send an email to `jlingys@connect.ust.hk`.
