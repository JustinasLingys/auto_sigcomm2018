'''
dsmark 
+++


'''


from pyroute2.netlink import nla
from pyroute2.netlink.rtnl import RTM_NEWQDISC
from pyroute2.netlink.rtnl import RTM_DELQDISC
from pyroute2.netlink.rtnl import TC_H_ROOT

parent = TC_H_ROOT

# This module has been implemented for 
# AuTO paper purpose


# Utility function to check if `num` is
# a power of `2`.
# (`num` must be non-negative)
def _is_power2(num):
    if num == 0:
        return False

    if num == 1:
        return True

    if num % 2 == 1:
        return False

    tmp_val = num

    while True:
        # integer division
        tmp_val = tmp_val // 2

        if tmp_val == 1:
            return True

        if tmp_val % 2 == 1:
            return False


def get_parameters(kwarg):
    # This code is a close copy to 
    # iproute2 implementation

    opt = [['TCA_DSMARK_INDICES', int(kwarg['indices'])]] # required

    if (opt[0][1] <= 0 or not _is_power2(opt[0][1])):
        raise Exception('`indices` must be a value = 2^n, where n >= 0')

    if 'default_index' in kwarg:
        # default index is optional
        tmp_idx = int(kwarg['default_index'])
    
        if tmp_idx >= opt[0][1] or tmp_idx < 0:
          raise Exception('`default_index` < 0 or `default_index` >= `indices`')
        else:
            opt.append(['TCA_DSMARK_DEFAULT_INDEX', tmp_idx])

    if 'set_tc_index' in kwarg:
        opt.append(['TCA_DSMARK_SET_TC_INDEX', True])

    if (not 'set_tc_index' in kwarg and not 'default_index' in kwarg) or\
       (    'set_tc_index' in kwarg and     'default_index' in kwarg):
        raise Exception('Must pass one of two: either `set_tc_index` or `default_index`. Both are not allowed.')


    return {'attrs' :  opt}



def get_class_parameters(kwarg):
    opt = [['TCA_DSMARK_MASK',  int(kwarg['mask'])],   # required
           ['TCA_DSMARK_VALUE', int(kwarg['value'])]]  # required

    # check if the values are in the right range
    if opt[0][1] < 0x00 or opt[0][1] > 0xff:
        raise Exception('dsmark `mask` must be in the range [0, 255]')

    if opt[1][1] < 0x00 or opt[1][1] > 0xff:
        raise Exception('IPv4 DSCP `value` must be in the range [0, 255]')


    return {'attrs' : opt}


def options(msg, *argv, **kwargs):
    if msg['header']['type'] in (RTM_NEWQDISC, RTM_DELQDISC):
        return options_dsmark
    else:
        return options_dsmark_class


class options_dsmark(nla):
    nla_map = (('TCA_DSMARK_UNSPEC',        'none'),
               ('TCA_DSMARK_INDICES',       'uint16'),
               ('TCA_DSMARK_DEFAULT_INDEX', 'uint16'),
               ('TCA_DSMARK_SET_TC_INDEX',  'flag'))
 
    
# `iproute2` uses `__u8` as the type for
# `TCA_DSMARK_MASK` and `TCA_DSMARK_VALUE`. 
#  However, pyroute2's `uint8` does not work and raises
#  the Numerical result out of range exception.
class options_dsmark_class(nla):
  nla_map = (('TCA_DSMARK_UNSPEC',        'none'),
             ('TCA_DSMARK_MASK',          'uint16'), 
             ('TCA_DSMARK_VALUE',         'uint16'))
